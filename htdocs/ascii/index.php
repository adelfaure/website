<!DOCTYPE html>
<!--
<?php include '../html/license.txt'; ?>
-->
<?php
  # FUNCTIONS
  function add($src){
    echo htmlspecialchars(file_get_contents($src,TRUE), ENT_QUOTES);
  }
  
  function link_year($year,$up) {
    echo ($up ? "/\\" : "\\/")." <a id=\"index$year\" href=\"#$year\">$year</a>\n";
  }

  function index_year($year) {
    $path = "./$year/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "\/ <a href=\"#$file\">$file</a>\n";
      }
    }
  }

  function year($year) {
    $path = "./$year/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<span class=\"ascii_art\" id=\"$file\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</span>\n";
        echo "/\ <a href=\"#$year\">$year</a> > $file \n\n";
        echo "\n";
      }
    }
  }
?>
<html>
  <head>
    <title>Adel Faure ‒ ASCII art</title>
    <!-- search engines -->
    <meta charset="utf-8">
    <meta name="description" content="Adel Faure ASCII art collection."/>
    <meta name="keywords" content="Text mode, ASCII art">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- twitter card -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@adelfaure" />
    <meta name="twitter:title" content="Adel Faure ‒ ASCII art" />
    <meta name="twitter:description" content="Adel Faure ASCII art collection" />
    <meta name="twitter:image" content="./card_image_small.png" />
    <link href="../src/style.css" rel="stylesheet">
  </head>
  <body style="display:none" id="top">
<pre>
<?php include '../html/header.html'; ?>
================================================================================
< <a href="../">Back</a>       _____ _____ _____ _____ _____       _____ _____ _____ 
            |  _  |   __|     |     |     |     |  _  |  _  |     |
            |     |__   |   --||   | |   |      |     |    _|_   _|
            |__|__|_____|_____|_____|_____|     |__|__|__\__||___| 
          _____ _____ ___   ___   _____ _____ _____ _____ _____ _____ 
         |     |     |   | |   | |   __|     |     |     |     |     |
         |   --|  |  |   |_|   |_|   __|   --|     ||   ||  |  |  |  |
         |_____|_____|_____|_____|_____|_____||___||_____|_____|__|__|

                       -- 6 years, 157 files, 1.43 Mo --  

================================================================================

<?php
  link_year(2024,false);
  link_year(2023,false);
  link_year(2022,false);
  link_year(2021,false);
  link_year(2020,false);
  link_year(2019,false);
?>

<span id="2024">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |  |  |
                           |   __|  |  |   __|__   |
                           |_____|_____|_____| |___|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2023,false);
  echo "\n";
  index_year(2024);
  echo "\n";
  year(2024); 
?>

<span id="2023">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |__   |
                           |   __|  |  |   __|__   |
                           |_____|_____|_____|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2024,true);
  link_year(2022,false);
  echo "\n";
  index_year(2023);
  echo "\n";
  year(2023); 
?>

<span id="2022">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |__   |
                           |   __|  |  |   __|   __|
                           |_____|_____|_____|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2023,true);
  link_year(2021,false);
  echo "\n";
  index_year(2022);
  echo "\n";
  year(2022); 
?>

<span id="2021">================================================================================
                            _____ _____ _____  ____
                           |__   |     |__   |/    |
                           |   __|  |  |   __| |   |
                           |_____|_____|_____| |___|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2022,true);
  link_year(2020,false);
  echo "\n";
  index_year(2021);
  echo "\n";
  year(2021); 
?>

<span id="2020">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |     |
                           |   __|  |  |   __|  |  |
                           |_____|_____|_____|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2021,true);
  link_year(2019,false);
  echo "\n";
  index_year(2020);
  echo "\n";
  year(2020); 
?>

<span id="2019">================================================================================
                            _____ _____  ____ _____
                           |__   |     |/    |  _  |
                           |   __|  |  | |   |__   |
                           |_____|_____| |___|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2020,true);
  echo "\n";
  index_year(2019);
  echo "\n";
  year(2019); 
?>
/\ <a href="#top">Top</a>
<?php include '../html/footer.html' ?>
</pre>
  </body>
  <script src="../src/script.js"></script>
</html>

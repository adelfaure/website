<!DOCTYPE html>
<!--
<?php include './html/license.txt'; ?>
-->
<html>
  <head>
    <title>Adel Faure ‒ ASCII art</title>
    <!-- search engines -->
    <meta charset="utf-8">
    <meta name="description" content="Adel Faure cool links and friends."/>
    <meta name="keywords" content="Text mode, ASCII art, cool design">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- twitter card -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@adelfaure" />
    <meta name="twitter:title" content="Adel Faure ‒ ASCII art" />
    <meta name="twitter:description" content="Adel Faure cool links and friends." />
    <meta name="twitter:image" content="./card_image_small.png" />
    <link href="src/style.css" rel="stylesheet">
  </head>
  <body>
    <main>
  <?php include './html/header.html'; ?>
<pre id="top">
--------------------------------------------------------------------------------
Figlet: JS Stick Letters                        ./ascii.php | Update: 2021-08-26
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
<a href="./index.php">░░Want░to░go░n░░░   __        __                __         ___ ?░Click░here░n░:)
░░░░______░░/·\░░  |__)  /\  /  ` |__/    |__| /  \  |\/| |__   ░░░______░░/·\░░
░░░/____/ \░/·\░░  |__) /~~\ \__, |  \    |  | \__/  |  | |___  ░░/____/ \░/·\░░
░░░|[][]|n|░░|░░░                                               ░░|[][]|n|░░|░░░</a>
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                   You are on the Cool Links And Friends page
--------------------------------------------------------------------------------

♥ Here I will share and comment links on text mode and computer art ♥

!IMPORTANT! → If you don't want to be on this page or if you want to propose
something to be in it please feel free :-D ! Dm me on twitter or send me an
email.

I really don't know if this page is a good idea or not. I really have the need
to archive the links below (for eg Twitter is great but it make disapear
everything very quick and prevent users from the ability to collection media in
a order they choose). I also really want to support theses artists by sharing
their work BUT I'm am very unseasy with the idea to create by mistake an
enterprise of some kind that utilises the labour of others in a way or another
…

In alphabetical order:

- <a href="#batfeula">Batfeula</a>
- <a href="#dwimmer">Dwimmer</a>
- <a href="#ldb">ldb</a>
- <a href="#polyducks">Polyducks</a>
<!--
Maija Haavisto (still mostly away)
@DiamonDie

james
@uniquename654

Andy Jenkinson
@andyuglifruit

The Manster
@MansterSoft

▙ ▉ ▜▘▉ ▚ ▉
@goto80

Thomas tekst-tv
@thomasboevith

blocktronics
@blocktronics

Horsenburger
@Horsenburger

Sixteen Colors
@sixteencolors

Sibachian
@sibachian

Wim
@wimpie3

Laura Brown Feuille d'érable
@thatgrrl

ailadi
@ailadi
I love anything that makes eyes flirt with the mind

Rick Christy
@grymmjack

Darokin
@darokin

@BucephalusStud1

@Standardcombo                                                              

@nichobi                    

@mistfunk

@whostolehonno

@venividiascii

@Jarfishing_

@pixelblip1              

@UnculturedGames

@JellicaJake

@SimplySarahUK

@sontolhead

@DanWesely

@Zeus_II

@BlackVoidTytan 

@TooMuchTomato                                                       

@LootBndt                     

@thepancakewitch

@microaeris

@buttoncupcake1

-->
</pre>
<pre>
--------------------------------------------------------------------------------
Figlet: JS Stick Letters                                                   
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
<a href="./index.php">░░Want░to░go░n░░░   __        __                __         ___ ?░Click░here░n░:)
░░░░______░░/·\░░  |__)  /\  /  ` |__/    |__| /  \  |\/| |__   ░░░______░░/·\░░
░░░/____/ \░/·\░░  |__) /~~\ \__, |  \    |  | \__/  |  | |___  ░░/____/ \░/·\░░
░░░|[][]|n|░░|░░░                                               ░░|[][]|n|░░|░░░</a>
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
<a href="#top">Top</a>
    
</pre>
<?php include './html/footer.html'; ?>
    </main>
  </body>
  <script src="src/script.js"></script>
</html>

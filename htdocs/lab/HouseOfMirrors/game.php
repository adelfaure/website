<!doctype html>
<html>
<head>
<meta charset="utf-16" />
<style>
  *{
    margin:0;
    padding:0;
    overflow:hidden;
    --webkit-font-smoothing: none;
  }
  @font-face{
    font-family:"jgs";
    src:url("fonts/Jgs-SingleLine.ttf");
  }
  pre {
    font-family:"jgs";
  }
  body{
    width: 100%;
  }
  #assets,#levels{
    display:none;
  }
</style>
<script defer src="src/animation.js"></script>
<script defer src="src/textor.js"></script>
<script defer src="src/ui.js"></script>
<script defer src="src/ground.js"></script>
<script defer src="src/synth.js"></script>
<script defer src="src/entities.js"></script>
<script defer src="src/levels.js"></script>
<script defer src="src/sound.js"></script>
<script defer src="src/music.js"></script>
<script defer src="src/display.js"></script>
<script defer src="src/player.js"></script>
<script defer src="src/events.js"></script>
<script defer src="src/editor.js"></script>
<script defer src="src/colors.js"></script>
<script defer src="src/init.js"></script>
<script defer src="src/script.js"></script>
<script defer src="src/mainGame.js"></script>
</head>
<body>
<?php include("./levels/assets.html") ?>
<div id="levels">
<?php include("./levels/level0.html") ?>
<?php include("./levels/level1.html") ?>
<?php include("./levels/level2.html") ?>
<?php include("./levels/level3.html") ?>
<?php include("./levels/level4.html") ?>
<?php include("./levels/level5.html") ?>
<?php include("./levels/level6.html") ?>
<?php include("./levels/level7.html") ?>
<?php include("./levels/level8.html") ?>
<?php include("./levels/level9.html") ?>
<?php include("./levels/level10.html") ?>
<?php include("./levels/level11.html") ?>
<?php include("./levels/level12.html") ?>
<?php include("./levels/level13.html") ?>
<?php include("./levels/level14.html") ?>
<?php include("./levels/level15.html") ?>
<?php include("./levels/level16.html") ?>
</div>
</body>
</html>

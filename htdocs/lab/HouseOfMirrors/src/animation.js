//  Textor, a javascript text mode engine.
//  Copyright (C) 2024 Adel Faure
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

let animation = false;
let animation_started = false;
let fps = 60;
let animation_fps_count = 0;
let animation_fps = 0;
let time = 0;
let time_between_frames = 0;
let time_between_frames_offset = 0;
let second_duration = 1000;
let time_between_seconds = 0;
let pause_animation = false;
let f = 0;

function start_animation() {
  if (animation_started) return;
  time = Date.now();
  animation_fps = fps;
  run_animation();
  animation_started = true;
}

function run_animation() {
  time_elapsed = Date.now() - time;
  time += time_elapsed;
  time_between_frames += time_elapsed;
  time_between_seconds += time_elapsed;
  let time_interval = second_duration/fps;
  if (!pause_animation && time_between_frames >= time_interval-time_between_frames_offset) {
    animation();
    f++;
    animation_fps_count++;
    time_between_frames_offset += time_between_frames - time_interval;
    time_between_frames = 0;
  }
  if (time_between_seconds >= second_duration) {
    animation_fps = animation_fps_count;
    animation_fps_count = 0;
    time_between_seconds -= second_duration;
  }
  requestAnimationFrame(run_animation);
}

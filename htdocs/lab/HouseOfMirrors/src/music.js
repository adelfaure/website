let levelMusic = [
  // level 0
  () => false,
  // level 1
  tuto,
  // level 2
  tuto,
  // level 3
  tuto,
  // level 4
  tuto,
  // level 5
  tuto,
  // level 6
  tuto,
  // level 7
  genesis,
  // level 8
  genesis,
  // level 9
  genesis,
  // level 10
  genesis,
  // level 11
  genesis,
  // level 12
  genesis_depth,
  // level 13
  genesis_depth,
  // level 14
  depth,
  // level 15 
  calm,
  // level 16
  calm,
];

levelMusic[98] = calm;
levelMusic[99] = false;

function tuto(){
  if (f%(8*32)) return;
  a=[0,3,5,7,10][b%3];
  b++;
  playSynth(
    [
      0.25,   // 0 max gain
      0.2,   // 1 gain
      setNote(a)*Math.pow(2,6),   // 2 freq
      setSmooth(6),    // 3 smooth
      0.1,   // 4 noise
      10,    // 5 res
      0.01, // 6 indur
      3,  // 7 middur
      0.01, // 8 outdur
    ],
    [
      0,    // 9 echo count
      400,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
}

function genesis(){
  if (f%(8*32)) return;
  a=[0,3,5,7,10][b%3];
  b++;
  playSynth(
    [
      0.25,   // 0 max gain
      0.2,   // 1 gain
      setNote(a)*Math.pow(2,6),   // 2 freq
      setSmooth(6),    // 3 smooth
      0.1,   // 4 noise
      10,    // 5 res
      0.01, // 6 indur
      3,  // 7 middur
      0.01, // 8 outdur
    ],
    [
      0,    // 9 echo count
      400,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
  a=[0,3,5,7,10][b%4];
  playSynth(
    [
      0.25,   // 0 max gain
      0.1,   // 1 gain
      setNote(a)*Math.pow(2,8),   // 2 freq
      setSmooth(6),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.02, // 6 indur
      0.5,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      600,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
}
function genesis_depth(){
  if (f%(8*32)) return;
  a=[0,3,5,7,10][b%3];
  b++;
  playSynth(
    [
      0.25,   // 0 max gain
      0.2,   // 1 gain
      setNote(a)*Math.pow(2,6),   // 2 freq
      setSmooth(6),    // 3 smooth
      0.1,   // 4 noise
      10,    // 5 res
      0.01, // 6 indur
      3,  // 7 middur
      0.01, // 8 outdur
    ],
    [
      0,    // 9 echo count
      400,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
  if (f%(8*64)) return;
  playSynth(
    [
      0.25,   // 0 max gain
      0.1,   // 1 gain
      setNote(a)*Math.pow(2,[7,8,9][Math.floor(Math.random()*3)]),   // 2 freq
      setSmooth(5),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.02, // 6 indur
      2,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      900,   // 10 echo latency
      0.95,   // 11 echo modifier
    ]
  );
}

function depth(){
  if (f%(8*64)) return;
  a=[0,3,5,7,10][b%3];
  b++;
  playSynth(
    [
      0.25,   // 0 max gain
      0.2,   // 1 gain
      setNote(a)*Math.pow(2,5),   // 2 freq
      setSmooth(6),    // 3 smooth
      0.5,   // 4 noise
      10,    // 5 res
      1, // 6 indur
      4,  // 7 middur
      3, // 8 outdur
    ],
    [
      0,    // 9 echo count
      400,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
  a=[0,3,5,7,10][b%4];
  playSynth(
    [
      0.25,   // 0 max gain
      0.1,   // 1 gain
      setNote(a)*Math.pow(2,[7,8,9][Math.floor(Math.random()*3)]),   // 2 freq
      setSmooth(5),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.02, // 6 indur
      2,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      900,   // 10 echo latency
      0.95,   // 11 echo modifier
    ]
  );
}

function calm(){
  if (f%(8*24)) return;
  a=[2,5,7,9,12][b%3];
  b++;
  playSynth(
    [
      0.25,   // 0 max gain
      0.3,   // 1 gain
      setNote(a)*Math.pow(2,7),   // 2 freq
      setSmooth(10),    // 3 smooth
      0.1,   // 4 noise
      5,    // 5 res
      0.01, // 6 indur
      2,  // 7 middur
      0.01, // 8 outdur
    ],
    [
      0,    // 9 echo count
      400,   // 10 echo latency
      0.9,   // 11 echo modifier
    ]
  );
  playSynth(
    [
      0.25,   // 0 max gain
      0.3,   // 1 gain
      setNote(a)*Math.pow(2,8),   // 2 freq
      setSmooth(10),    // 3 smooth
      0,   // 4 noise
      4,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.25, // 8 outdur
    ],
    [
      16,    // 9 echo count
      1200,   // 10 echo latency
      0.95,   // 11 echo modifier
    ]
  );
  if (f%(8*48)) return;
  a=[2,5,7,9,12][b%3];
  playSynth(
    [
      0.25,   // 0 max gain
      0.05,   // 1 gain
      setNote(a)*Math.pow(2,8)*(Math.random()>0.5?3:1.5)*(Math.random()>0.5?0.5:1),   // 2 freq
      setSmooth(1),    // 3 smooth
      0,   // 4 noise
      10,    // 5 res
      2, // 6 indur
      1,  // 7 middur
      2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      800,   // 10 echo latency
      0.95,   // 11 echo modifier
    ]
  );
}

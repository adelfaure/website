console.log("textor.js");

// GLOBAL

let w,h,d,c,x,y,di,sprite;
let keys = {};
let key_down = false;
let last_key_down = false;
let input_keys = {};
let input_key_down = false;
let input_last_key_down = false;
let mouse_x = 0;
let mouse_y = 0;

// ENCODING

const encoding = {};
encoding.decoder = new TextDecoder("utf-16");
encoding.encoder = new TextEncoder("utf-16");

function encode(str){
  var code = new Uint16Array(str.length);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    code[i] = str.charCodeAt(i);
  }
  return code.length > 1 ? code : code[0];
}

function decode(code){
  return encoding.decoder.decode(
    isNaN(Number(code)) ? code : new Uint16Array([code])
  );
}

// AREA

Area = function(w,h){
  this.width = w;
  this.height = h;
  this.length = w*h;
  this.data = new Uint16Array(this.length);
  this.pos = [0,0];
  this.origin = new Uint16Array(4);
}

Area.prototype.process = function(shader_chunk){
  w = this.width;
  h = this.height;
  d = this.data;
  for (var i = 0; i < this.data.length; i++) {
    x = i%this.width;
    y = Math.floor(i/this.width);
    di = i;
    c = d[di];
    this.data[i] = shader_chunk();
  }
}

Area.prototype.place = function(pos_x,pos_y,gap_x,gap_y){
  this.pos[0] = pos_x;
  this.pos[1] = pos_y;
  // origin x
  this.origin[0] = (pos_x*-1)%(this.width + gap_x) + this.width + gap_x;
  // origin y
  this.origin[1] = (pos_y*-1)%(this.height + gap_y) + this.height + gap_y;
  this.origin[2] = gap_x;
  this.origin[3] = gap_y;
}

Area.prototype.printOn = function(area) {
  if (
    this.pos[0] + this.width < 0 
    || this.pos[0] >= area.width-1
    || this.pos[1] + this.height < 0 
    || this.pos[1] >= area.height-1
  ) return false;
  let indexList = []
  //let index = Math.floor(this.pos[0]) + Math.floor(this.pos[1]) * area.width;
  for (var y = 0; y < this.height; y++){
    for (var x = 0; x < this.width; x++){
      let index = y*this.width+x;
      let areaIndex = (
        (Math.floor(this.pos[1]) + y)*area.width
        + Math.floor(this.pos[0]) + x
      );
      area.data[areaIndex] = this.data[index];
      indexList.push(areaIndex);
    }
  }
  return indexList;
}

Area.prototype.printAt = function(index,w) {
  ox = this.origin[0];
  oy = this.origin[1];
  gap_x = this.origin[2];
  gap_y = this.origin[3];
  // area x
  let ax = index%w + ox;
  // area y
  let ay = Math.floor(index/w) + oy;
  // area data x
  let adx = ax%(this.width + gap_x);
  if (adx >= this.width) return 0;
  // area data y
  let ady = ay%(this.height + gap_y)*this.width;
  // area data index
  let adi = adx+ady;
  return this.data[adi];
}

Area.prototype.render = function(options){
  if (options.type == "text"){
    return decode(this.data);
  }
}

// ASSET

function strToArea(str) {
  let asset_w = str.indexOf('\n');
  let data = encode(str.replaceAll('\n',''));
  data = data.length ? data : [data];
  let area = new Area(asset_w,Math.floor(data.length/asset_w));
  let w_bak = w
  let h_bak = h
  let d_bak = d
  let x_bak = x
  let y_bak = y
  let di_bak = di;
  area.process(()=>data[di]);
  w = w_bak;
  h = h_bak;
  d = d_bak;
  x = x_bak;
  y = y_bak;
  di = di_bak;
  return area;
}

// SPRITE

Sprite = function(sheet,frames) {
  // sheet must be area
  this.sheet = sheet;
  this.frames = frames;
  this.element = new Area(sheet.width,sheet.height/frames);
  this.width = this.element.width;
  this.height = this.element.height;
  this.shifted = null;
}

Sprite.prototype.animate = function(speed,position){
  sprite = this.element;
  this.sheet.place(
    0,
    this.element.height*speed,
    0,
    0
  );
  let w_bak = w
  let h_bak = h
  let d_bak = d
  let x_bak = x
  let y_bak = y
  let di_bak = di;
  this.element.process(()=>this.sheet.printAt(di,w));
  w = w_bak;
  h = h_bak;
  d = d_bak;
  x = x_bak;
  y = y_bak;
  di = di_bak;
  this.element.place(
    ...position
  );
}

Sprite.prototype.printAt = function(di,w){
  return this.element.printAt(di,w);
}

Sprite.prototype.printOn = function(area){
  return this.element.printOn(area);
}

Sprite.prototype.shift = function(shift,process){
  if (shift == this.shifted) return;
  this.shifted = shift;
  process(shift);
}

// DISPLAY
// display is layers of area ?

Display = function(options){
  this.type = options.type;
  if (this.type == "text"){
    this.channel = [];
    if (options.dom_destination) {
      this.output = [];
      this.dom = document.createElement("div");
      this.dom.className = "display_text";
    }
    let domList = [];
    for (var i = 0; i < options.channels; i++) {
      this.channel.push(new Area(options.width,options.height));
      if (!options.dom_destination) continue;
      let pre = document.createElement("pre");
      pre.style.position = "absolute";
      pre.style.fontSize = options.fontSize + 'px';
      pre.style.lineHeight = options.fontSize + 'px';
      pre.style.top = "0";
      this.output.push(pre);
      domList.push(pre);
    }
    domList.reverse();
    for (var i = 0; i < domList.length; i++){
      this.dom.appendChild(domList[i]);
    }
    if (options.dom_destination) options.dom_destination.appendChild(this.dom);
    this.width = this.channel[0].width;
    this.height = this.channel[0].height;
  }

  // colors
  this.colors = [];
  for (var i = 0; i < options.channels; i++) this.colors = this.colors.concat([0,0,0]);
  
}

Display.prototype.print = function(){
  if (this.type == "text" && this.output) {
    for (var i = 0; i < this.output.length; i++){
      this.output[i].textContent = this.channel[i].render({"type":"text"});
    }
  }
}

Display.prototype.setColor = function(options){
  if (this.type == "text" && this.output) {
    for (var i = 0; i < options.length; i++){
      this.output[i].style.color = options[i];
    }
  }
}

Display.prototype.setBackground = function(option){
  if (this.type == "text" && this.output) {
    this.dom.parentNode.style.backgroundColor = option;
  }
}

Display.prototype.updateColors = function(){
  let hexStr = ''
  for (var i = 0; i < this.colors.length; i++){
    let hex = (this.colors[i]).toString(16);
    hex = hex.length == 1 ? '0'+hex : hex;
    hexStr += hex;
    if (!((i+1)%3)) {
      if (Math.floor(i/3) < this.output.length){
        this.output[Math.floor(i/3)].style.color = '#'+hexStr;
      } else {
        this.dom.parentNode.style.backgroundColor = '#'+hexStr;
      }
      hexStr = '';
    }
  }
}

Display.prototype.printAt = function(index,w) {
  if (this.type == "text") {
    let channels_data = [];
    for (var i = 0; i < this.channel.length; i++){
      let print = this.channel[i].printAt(index,w);
      if (print == undefined || print == 0) return false;
      channels_data.push(print);
    }
    return channels_data;
  }
}

Display.prototype.process = function(shader_chunk){
  if (this.type == "text") {
    d = [];
    w = this.width;
    h = this.height
    for (var i = 0; i < this.channel.length; i++){
      d.push(this.channel[i].data);
    }
    for (var i = 0; i < this.channel[0].data.length; i++) {
      x = i%w;
      y = Math.floor(i/w);
      di = i;
      let data_list = shader_chunk();
      c = [];
      for (var j = 0; j < this.channel.length; j++){
        this.channel[j].data[i] = data_list[j];
        c[j] = data_list[j];
      }
    }
  }
}

Display.prototype.place = function(pos_x,pos_y,gap_x,gap_y){
  if (this.type == "text") {
    for (var i = 0; i < this.channel.length; i++){
      this.channel[i].place(pos_x,pos_y,gap_x,gap_y);
    }
  }
}

Display.prototype.getDomColors = function(dom){
  let hexValues = [
    dom.getAttribute("data-bright").slice(1),
    dom.getAttribute("data-soft").slice(1),
    dom.getAttribute("data-special").slice(1),
    dom.getAttribute("data-bitter").slice(1),
    dom.getAttribute("data-dark").slice(1),
    dom.getAttribute("data-bg").slice(1),
  ];
  this.colors = [];
  for (let i = 0; i < hexValues.length; i++){
    let split = hexValues[i].match(/.{1,2}/g);
    for (let i = 0; i < split.length; i++) {
      this.colors.push(parseInt(split[i],16));
    }
  }
}


// KEYS

let inputMode = false;

document.addEventListener("keydown",function(e){
  if (!audioContext && e.key == "Enter") audioContext = new AudioContext();
  if (inputMode) {
    input_keys[e.key] = input_keys[e.key] ? input_keys[e.key] : f;
    input_key_down = input_key_down ? input_key_down : f;
    input_last_key_down = e.key;
    return false;
  }
  keys[e.key] = keys[e.key] ? keys[e.key] : f;
  key_down = key_down ? key_down : f;
  last_key_down = e.key;
});
document.addEventListener("keyup",function(e){
  keys[e.key] = false;
  input_keys[e.key] = false;
  let all_key_down = true;
  for (var i in keys ) {
    if (keys[i]){
      all_key_down = false; 
      break;
    }
  }
  key_down = all_key_down ? false : input_key_down;
  let input_all_key_down = true;
  for (var i in keys ) {
    if (input_keys[i]){
      input_all_key_down = false; 
      break;
    }
  }
  input_key_down = input_all_key_down ? false : input_key_down;
});

// MOUSE

//document.addEventListener("mousemove",function(e){
//  if (!font) return;
//  mouse_x = e.clientX / (font.width * scale);
//  mouse_y = e.clientY / (font.height * scale);
//});

// DATA
function save(data, filename, type) {
  var file = new Blob(data, {type: type});
  var a = document.createElement("a"), url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);  
  }, 0); 
}


// MAKE THIS AS A PROMISE
function load(contentProcess) {
  var input = document.createElement('input');
  input.type = 'file';
  
  input.onchange = e => { 
  
    // getting a hold of the file reference
    var file = e.target.files[0];
    // setting up the reader
    var reader = new FileReader();
    reader.readAsText(file,'UTF-8');
  
    // here we tell the reader what to do when it's done reading...
    reader.onload = readerEvent => {
      var content = readerEvent.target.result; 
      contentProcess(content);
    }
  
  }
  
  input.click();
}

// ENTITIES

Entity = function(
  type,
  channel,
  spriteFrames,
  positionXFunction,
  positionYFunction,
  animationFunction,
  playerInteractionFunction,
  printFunction,
){
  this.type = type;
  this.channel = channel,
  this.sprite = new Sprite(
    strToArea(document.getElementById(type).textContent),
    spriteFrames
  );
  this.width = this.sprite.width;
  this.height = this.sprite.height;
  this.positionXFunction = positionXFunction;
  this.positionYFunction = positionYFunction;
  this.animationFunction = animationFunction;
  this.playerInteractionFunction = playerInteractionFunction;
  this.printFunction = printFunction;
}

Entity.prototype.animate = function(speed,position) {
  this.sprite.animate(speed,position);
}

Entity.prototype.printAt = function(index,width) {
  return this.sprite.printAt(index,width);
}

Entity.prototype.printOn = function(area) {
  return this.sprite.printOn(area);
}

Entity.prototype.printEvent = function(index) {
  this.printFunction(this,index);
}

// KEY
let playerGotKey = false;
function key(){
  let key = new Entity(
    "key",
    ()=>0,
    2,
    ()=>0,
    ()=>0,
    ()=>(f-x-y)/24,
    (i,x,y)=>{
      playerGotKey = true;
      let grab = new Entity(
        "grab",
    ()=>0,
        4,
        ()=>0,
        ()=>0,
        ()=>Math.floor(f/1),
        ()=>0,
        i=>0
      );
      let entity = grab;
      entities.push([grab,x-camX-1,y-camY-1,sceneWidth*10,sceneHeight*10]);
      setTimeout(function(){
        for (var i = 0; i < entities.length; i++) {
          if (entities[i][0] == grab) {
            let chunkX = Math.floor((x-camX)/sceneWidth);
            let chunkY = Math.floor((y-camY)/sceneHeight);
            entitiesToRemove.push(i);
            entitiesToRemoveChunk.push([chunkX,chunkY,i]);
          }
        }
      },120);
      keySfx();
      toRestore.push(entities[i]);
      return entitiesToRemove.push(i);
    },
    i=>0
  );
  entities.push([key,x,y,entitiesArea.width-key.width,entitiesArea.height-key.height]);
}

// COIN

function coinAmount(){
  let amount = 0;
  for (let i = 0; i < softCoinGrab.length; i++){
    if (!softCoinGrab[i]) continue;
    for (let j = 0; j < softCoinGrab[i].length; j++){
      if (softCoinGrab[i][j]) amount++;
    }
  }
  return amount;
}

let coinIndex = 0;
let hardCoinGrab = [];
let softCoinGrab = [];

function coin(level,xFun,yFun,animFun){
  if (!hardCoinGrab[level]) {
    hardCoinGrab[level] = [];
    softCoinGrab[level] = [];
  }
  if (hardCoinGrab[level][coinIndex] != true) {
    hardCoinGrab[level][coinIndex] = false;
    softCoinGrab[level][coinIndex] = false;
  } else {
    coinIndex++;
    return;
  }
  let localCoinIndex = coinIndex;
  let localCoinLevel = level;
  let coin = new Entity(
    "coin",
    ()=>0,
    8,
    xFun,
    yFun,
    animFun,
    (i,x,y)=>{
      let grab = new Entity(
        "grab",
    ()=>0,
        4,
        ()=>0,
        ()=>0,
        ()=>Math.floor(f/1),
        ()=>0,
        i=>0
      );
      let entity = grab;
      entities.push([grab,x-camX-1,y-camY-1,sceneWidth*10,sceneHeight*10]);
      setTimeout(function(){
        for (var i = 0; i < entities.length; i++) {
          if (entities[i][0] == grab) {
            let chunkX = Math.floor((x-camX)/sceneWidth);
            let chunkY = Math.floor((y-camY)/sceneHeight);
            entitiesToRemove.push(i);
            entitiesToRemoveChunk.push([chunkX,chunkY,i]);
          }
        }
      },120);
      coinSfx();
      softCoinGrab[localCoinLevel][localCoinIndex] = true;
      return entitiesToRemove.push(i);
    },
    i=>0
  );
  addEntityToChunk(coin,x,y);
  entities.push([coin,x-1,y,entitiesArea.width-coin.width,entitiesArea.height-coin.height]);
  coinIndex++;
}

let entityChunk = [];
let entitiesToRemoveChunk = [];

function addEntityToChunk(entity,x,y) {
  let chunkX = Math.floor(x/sceneWidth);
  let chunkY = Math.floor(y/sceneHeight);
  if (!entityChunk[chunkY]) entityChunk[chunkY] = [];
  if (!entityChunk[chunkY][chunkX]) entityChunk[chunkY][chunkX] = [];
  entityChunk[chunkY][chunkX].push([coin,x-1,y,entitiesArea.width-coin.width,entitiesArea.height-coin.height]);
}

// BAT

function bat(xFun,yFun,animFun){
  let bat = new Entity(
    "bat",
    ()=>4,
    4,
    xFun,
    yFun,
    animFun,
    ()=>0,
    i=>0
  );
  entities.push([bat,x,y,entitiesArea.width-bat.width,0]);
}

// ENTRELACES

function hEntrelace(x,y,goX,goY){
  let entity = new Entity(
    "hEntrelace",
    ()=>2,
    1,
    ()=>0,
    ()=>0,
    ()=>0,
    ()=>{
      playerY = goY;
      entrelaceSfx();
      transitionIn = 16;
      console.log(playerVelocityY);
      playerY = (
        playerVelocityY > 0 ? Math.ceil(playerY+1):
        playerVelocityY <= 0 ? Math.floor(playerY-0.1):
        playerY
      );
    },
    i=>0
  );
  entities.push([entity,x,y,entitiesArea.width-entity.width,entitiesArea.height-entity.height]);
}
function vEntrelace(x,y,goX,goY){
  let entity = new Entity(
    "vEntrelace",
    ()=>2,
    1,
    ()=>0,
    ()=>0,
    ()=>0,
    ()=>{
      playerX = goX;
      entrelaceSfx();
      transitionIn = 16;
      playerX = (
        playerVelocityX > 0 ? Math.ceil(playerX+1):
        playerVelocityX <= 0 ? Math.floor(playerX-0.1):
        playerX
      );
    },
    i=>0
  );
  entities.push([entity,x,y,entitiesArea.width-entity.width,entitiesArea.height-entity.height]);
}

// MIRROR

function mirror(x,y,goX,goY,mirrorId){
  let mirror = new Entity(
    "mirror",
    ()=>2,
    1,
    ()=>0,
    ()=>0,
    ()=>0,
    ()=>{
      if (keys[keysBindings["Action"]]) {
        transitionIn = 16;
        if (mirrorGo) {
          playerX = mirrorGo[0]+camX;
          playerY = mirrorGo[1]-1+camY;
        } else {
          playerX = goX;
          playerY = goY;
        }
        console.log(mirrorGo);
        mirrorSfx();
        keys[keysBindings["Action"]] = false;
      }
    },
    i=>0
  );
  mirror.x = x;
  mirror.y = y;
  mirror.goX = goX;
  mirror.goY = goY;
  entities.push([mirror,x,y,entitiesArea.width-mirror.width,entitiesArea.height-mirror.height]);
}

// LEVEL DOORS

function door(levelId,entranceId){
  let lvlId = levelId;
  let door = new Entity(
    "door",
    ()=>2,
    2,
    ()=>0,
    ()=>0,
    ()=>visitedLevels.includes(levelId+':'+entranceId) ? 0 : 1,
    ()=>{
       if (!doorTuto) {
        msg = message(
          "Arrow Down to interact\n"+
          "                      \n"+
          "          ↓           \n"+
          "         ┌--┐         \n"+
          "         |  |         \n"+
          "         |⇕_|         \n"+
          "                      \n"+
          "   Enter to continue  \n"
        );
        messageEvent = true;
        doorTuto = true;
        messageShow = true;
      }
      if (keys[keysBindings["Action"]] && !messageEvent) {
        if (!visitedLevels.includes(currentLevel+':'+entranceId)) visitedLevels.push(currentLevel+':'+entranceId);
        bootLevel("level"+levelId,entranceId);
        doorSfx();
        keys[keysBindings["Action"]] = false;
      }
    },
    i=>0
  );
  entities.push([door,x,y-3,entitiesArea.width-door.width,entitiesArea.height-door.height]);
}
function lockedDoor(levelId,entranceId){
  let lvlId = levelId;
  let door = new Entity(
    "lockedDoor",
    (print,entity)=>(
      print == 9908 ? entity.unlocked ? 0 : 2 : entity.unlocked ? 2 : 4
    ),
    2,
    ()=>0,
    ()=>0,
    ()=>visitedLevels.includes(levelId+':'+entranceId) ? 0 : 1,
    i=>{
      if (playerGotKey) {
        playerGotKey = false;
        entities[i][0].unlocked = true;
        unlockSfx();
      }
      if (keys[keysBindings["Action"]] && !messageEvent) {
        if (!entities[i][0].unlocked) {
          lockedSfx();
          keys[keysBindings["Action"]] = false;
        } else {
          if (!visitedLevels.includes(currentLevel+':'+entranceId)) visitedLevels.push(currentLevel+':'+entranceId);
          bootLevel("level"+levelId,entranceId);
          doorSfx();
          keys[keysBindings["Action"]] = false;
        }
      }
    },
    i=>0
  );
  entities.push([door,x,y-3,entitiesArea.width-door.width,entitiesArea.height-door.height]);
}
function coinDoor(levelId,entranceId,amount){
  let lvlId = levelId;
  let door = new Entity(
    "coinDoor",
    (print,entity)=>{
      if (entity.amount-coinAmount()) {
      let thouAmount = encode(String(Math.floor((entity.amount-coinAmount())/1000)));
      let centAmount = encode(String(Math.floor((entity.amount-coinAmount())/100)));
      let decAmount  = encode(String(Math.floor((entity.amount-coinAmount())/10)));
      let unitAmount = encode(String(Math.floor((entity.amount-coinAmount()))));
      let thouCode = thouAmount.lenght?thouAmount[thouAmount.length-1]:thouAmount;
      let centCode = centAmount.length?centAmount[centAmount.length-1]:centAmount;
      let decCode =  decAmount.length?decAmount[decAmount.length-1]:decAmount;
      let unitCode = unitAmount.length?unitAmount[unitAmount.length-1]:unitAmount;
      entity.sprite.sheet.data[16] = thouCode == 48 ? 79 : thouCode;
      entity.sprite.sheet.data[17] = thouCode == 48 && centCode == 48 ? 79 : centCode;
      entity.sprite.sheet.data[18] = thouCode == 48 && centCode == 48 && decCode == 48 ? 79 : decCode;
      entity.sprite.sheet.data[19] = thouCode == 48 && centCode == 48 && decCode == 48 && unitCode == 48 ? 79 : unitCode;
      } else if (!entity.unlocked){
      let thouAmount = encode(String(Math.floor((entity.amount-coinAmount())/1000)));
      let centAmount = encode(String(Math.floor((entity.amount-coinAmount())/100)));
      let decAmount  = encode(String(Math.floor((entity.amount-coinAmount())/10)));
      let unitAmount = encode(String(Math.floor((entity.amount-coinAmount()))));
      let thouCode = thouAmount.lenght?thouAmount[thouAmount.length-1]:thouAmount;
      let centCode = centAmount.length?centAmount[centAmount.length-1]:centAmount;
      let decCode =  decAmount.length?decAmount[decAmount.length-1]:decAmount;
      let unitCode = unitAmount.length?unitAmount[unitAmount.length-1]:unitAmount;
      entity.sprite.sheet.data[16] = thouCode == 48 ? 79 : thouCode;
      entity.sprite.sheet.data[17] = thouCode == 48 && centCode == 48 ? 79 : centCode;
      entity.sprite.sheet.data[18] = thouCode == 48 && centCode == 48 && decCode == 48 ? 79 : decCode;
      entity.sprite.sheet.data[19] = thouCode == 48 && centCode == 48 && decCode == 48 && unitCode == 48 ? 79 : unitCode;
        unlockSfx();
        entity.unlocked = true
      }
      return(
        print == 79 ? 4 : 
        print >= 48 && print <= 57 ? 2 : 
        print == 9786 ? entity.unlocked ? 0 : 2 : entity.unlocked ? 2 : 
        4
      )
    },
    2,
    ()=>0,
    ()=>0,
    ()=>visitedLevels.includes(levelId+':'+entranceId) ? 0 : 1,
    i=>{
      if (keys[keysBindings["Action"]] && !messageEvent) {
        if (!entities[i][0].unlocked) {
          lockedSfx();
          keys[keysBindings["Action"]] = false;
        } else {
          if (!visitedLevels.includes(currentLevel+':'+entranceId)) visitedLevels.push(currentLevel+':'+entranceId);
          bootLevel("level"+levelId,entranceId);
          doorSfx();
          keys[keysBindings["Action"]] = false;
        }
      }
    },
    i=>0
  );
  door.amount = amount;
  entities.push([door,x,y-3,entitiesArea.width-door.width,entitiesArea.height-door.height]);
}

// CHECKPOINT
function checkPoint(){
  let checkX = x-camX;
  let checkY = y-camY;
  let checkPoint = new Entity(
    "checkpoint",
    ()=>2,
    2,
    ()=>0,
    ()=>0,
    (entity)=>entity.checkPointActive ? 0 : 1,
    i=>{
      if (!entities[i][0].checkPointActive && !playerDead) {
        checkpointSfx();
        entities[i][0].sprite.sheet.data[4] = playerData;
        // Restore checkpoints
        for (var j = 0; j < checkPoints.length; j++) {
          checkPoints[j].checkPointActive = false;
          checkPoints[j].channel = ()=>2;
        }
        entities[i][0].channel = ()=>4;
        entities[i][0].checkPointActive = true;
        playerCheckPointX = playerX;
        playerCheckPointY = playerY-0.5;
      }
    },
    i=>0
  );
  entities.push([checkPoint,x-1,y-2,entitiesArea.width-checkPoint.width,entitiesArea.height-checkPoint.height]);
}

// MOVING PLATFORM

function movingPlatform(xFun,yFun,animFun){
  let movingPlatform = new Entity(
    "platform",
    ()=>2,
    1,
    xFun,
    yFun,
    animFun,
    ()=>0,
    (platform,di)=>{
      movingPlatformGround.area.data[di] = 35;
    }
  );
  entities.push([movingPlatform,x,y,entitiesArea.width-movingPlatform.width,entitiesArea.height-movingPlatform.height]);
}


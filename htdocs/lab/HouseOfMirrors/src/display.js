// TODO SHOULD GO to init game

// DISPLAY

let fontSize,displayWidth,displayHeight,display,sceneWidth,sceneHeight,scene;

function makeDisplay(lineNumber,wantedFontSize){
  let existantDisplay = document.getElementsByClassName("display_text");
  let existedDisplay = existantDisplay.length > 0;
  for (var i = 0; i < existantDisplay.length; i++) {
    existantDisplay[i].parentNode.removeChild(existantDisplay[i]);
  }

  fontSize = Math.floor(Math.floor(window.innerHeight/lineNumber)/wantedFontSize)*wantedFontSize;
  // Display width and height are based on font size and ratio
  displayWidth = Math.floor(window.innerWidth/(fontSize/2));
  displayHeight = Math.floor(window.innerHeight/fontSize);

  // Display 5 channels are for :
  // 0 ground / obstacles (Bright)
  // 1 player / interactive elements (Special)
  // 2 soft ground / soft interactive elements (Soft)
  // 3 dangerous elements (Bitter)
  // 4 decorative elements (Dark)
  display = new Display({
    "type":"text",
    "channels":5,
    "width":displayWidth,
    "height":displayHeight,
    "dom_destination":document.body,
    "fontSize":fontSize
  });

  // SCENE

  sceneWidth = 40;
  sceneHeight = 20;

  scene = new Display({
    "type":"text",
    "channels":5,
    "width":sceneWidth,
    "height":sceneHeight,
  });
  if(existedDisplay)setColors(); 
}

function setColors(){
  display.getDomColors(levelDom);
  display.updateColors();
  //  // Set display background to black
  //  display.setBackground([
  //    levelDom.getAttribute("data-bg"),
  //  ])
  //  // Set channel colors
  //  display.setColor([
  //    levelDom.getAttribute("data-bright"),
  //    levelDom.getAttribute("data-soft"),
  //    levelDom.getAttribute("data-special"),
  //    levelDom.getAttribute("data-bitter"),
  //    levelDom.getAttribute("data-dark"),
  //  ])
} 

function playerDie(x,y){
  if (playerDead) return;
  playerVelocityX = 0;
  playerVelocityY = 0;
  playerY = Math.floor(playerY);
  umbrella = false;
  playerDead = fps/4*2;
  playerDeadTransition = fps/4;
  let startF = f;
  let die = new Entity(
    "grab",
    ()=>3,
    4,
    ()=>0,
    ()=>0,
    ()=>Math.floor((f-startF)),
    ()=>0,
    i=>0
  );
  let entity = die;
  entities.push([die,x-camX-1,y-camY-1,sceneWidth*10,sceneHeight*10]);
  setTimeout(function(){
    for (var i = 0; i < entities.length; i++) {
      if (entities[i][0] == die) entitiesToRemove.push(i);
    }
  },120);
  deathSfx();

}
function message(str){
  let strArea = strToArea(str);
  let area = new Area(strArea.width+4,strArea.height+2);
  strArea.place(2,1,area.width,area.height);
  area.process(()=>{
    if (x == 0 && y == 0) return 9484;
    if (x == w-1 && y == 0) return 9488;
    if (x == 0 && y == h-1) return 9492;
    if (x == w-1 && y == h-1) return 9496;
    if (y == 0 || y == h-1) return 45;
    if (x == 0 || x == w-1) return 124;
    let strAreaPlace = strArea.printAt(di,w);
    if (strAreaPlace) return strAreaPlace;
    return 32;
  });
  messageEvent = true;
  messageShow = true;
  messageEventOut = false;
  messageTransition = 100;
  messageSfx();
  return area;
}

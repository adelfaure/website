// COINSi

function initCoins(level){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let coinX = x;
  let coinY = y;
  if (c == 67 && d[di+1] == 48) {
    coin(
      level,
      ()=>false,
      ()=>false,
      ()=>(f-coinX-coinY)/12,
    );
  } else 
  if (c == 67 && d[di+1] == 49) {
    coin(
      level,
      ()=>Math.sin((f+coinY*10)/40)*6,
      ()=>0,//Math.cos(f/40)*-4,
      ()=>(f-coinX-coinY)/12,
    );
  }
  if (c == 67 && d[di+1] == 50) {
    coin(
      level,
      ()=>0,
      ()=>Math.sin((f+coinX*5)/40)*3,
      ()=>(f-coinX-coinY)/12,
    );
  }
  if (c == 67 && d[di+1] == 51) {
    coin(
      level,
      ()=>Math.sin(f/40)*6,
      ()=>Math.cos(f/40)*-3,
      ()=>(f-coinX-coinY)/12,
    );
  }
  if (c == 67 && d[di+1] == 52) {
    for (var j = 0; j < 4; j++) {
      let offset = j;
      coin(
        level,
        ()=>Math.sin((f+offset*64)/40)*6,
        ()=>Math.cos((f+offset*64)/40)*-3,
        ()=>(f-coinX-coinY)/12,
      );
    }
  }
  if (c == 67 && d[di+1] == 53) {
    for (var j = 0; j < 8; j++) {
      let offset = j;
      coin(
        level,
        ()=>Math.sin((f+offset*48)/60)*8,
        ()=>Math.cos((f+offset*48)/60)*-4,
        ()=>(f-coinX-coinY)/12,
      );
    }
  }
  if (c == 67 && d[di+1] == 54) {
    coin(
      level,
      ()=>Math.sin((f+coinY*10)/40)*-6,
      ()=>0,//Math.cos(f/40)*-4,
      ()=>(f-coinX-coinY)/12,
    );
  }
  if (c == 67 && d[di+1] == 55) {
    coin(
      level,
      ()=>0,
      ()=>Math.sin((f+coinX*5)/40)*-3,
      ()=>(f-coinX-coinY)/12,
    );
  }
}

// BATS

function initBats(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let batX = x;
  let batY = y;
  if (c == 66 && d[di+1] == 48) {
    bat(
      ()=>false,
      ()=>false,
      ()=>(f-batX-batY)/12,
    );
  } else 
  if (c == 66 && d[di+1] == 49) {
    bat(
      ()=>Math.sin((f+batY*10)/40)*6,
      ()=>0,//Math.cos(f/40)*-4,
      ()=>(f-batX-batY)/12,
    );
  }
  if (c == 66 && d[di+1] == 50) {
    bat(
      ()=>0,
      ()=>Math.sin((f+batX*5)/40)*3,
      ()=>(f-batX-batY)/12,
    );
  }
  if (c == 66 && d[di+1] == 51) {
    bat(
      ()=>Math.sin(f/40)*6,
      ()=>Math.cos(f/40)*-3,
      ()=>(f-batX-batY)/12,
    );
  }
  if (c == 66 && d[di+1] == 52) {
    for (var j = 0; j < 1; j++) {
      let offset = j;
      bat(
        ()=>Math.sin((f+offset*64)/40)*6,
        ()=>Math.abs(Math.cos((f+offset*64)/40)*-3)*-1,
        ()=>(f-batX-batY)/12,
      );
    }
  }
  if (c == 66 && d[di+1] == 53) {
    for (var j = 0; j < 8; j++) {
      let offset = j;
      bat(
        ()=>Math.sin((f+offset*48)/60)*12,
        ()=>Math.cos((f+offset*48)/60)*-6,
        ()=>(f-batX-batY)/12,
      );
    }
  }
  if (c == 66 && d[di+1] == 54) {
    bat(
      ()=>Math.sin((f+batY*10)/40)*-6,
      ()=>0,//Math.cos(f/40)*-4,
      ()=>(f-batX-batY)/12,
    );
  }
  if (c == 66 && d[di+1] == 55) {
    bat(
      ()=>0,
      ()=>Math.sin((f+batX*5)/40)*-3,
      ()=>(f-batX-batY)/12,
    );
  }
}

// DOORS

function initMirrors(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let mirrorX = x;
  let mirrorY = y;
  if (c == 68) {
    let unit = Number(decode(d[di+3]));
    let dec = Number(decode(d[di+2]));
    let cent = Number(decode(d[di+1]));
    let mirrorId = cent*100+dec*10+unit;
    if (mirrors[mirrorId]) {
      let goX = mirrors[mirrorId][0];
      let goY = mirrors[mirrorId][1];
      mirror(x,y,goX,goY);
      mirror(goX,goY,x,y);
    } else {
      mirrors[mirrorId] = [mirrorX,mirrorY];
    }
  }
}

// LEVEL DOORS

let currentLevel = -1, fromLevel = -1;

function initDoors(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  if (c == 76) {
    let levelIdDec = Number(decode(d[di+1]));
    let levelIdUnit = Number(decode(d[di+2]));
    let levelId = levelIdDec*10+levelIdUnit;
    let entranceDec = Number(decode(d[di+3]));
    let entranceUnit = Number(decode(d[di+4]));
    let entranceId = entranceDec*10+entranceUnit;
    if (
      playerEntrance == entranceId && levelId == currentLevel && (!playerPlaced || playerWasPlaced)
    ) {
      console.log(playerEntrance,entranceId);
      console.log(levelId,currentLevel);
      playerCheckPointX = x;
      playerCheckPointY = y-0.5;
      playerX = x;
      playerY = y-0.5;
    }
    door(levelId,entranceId);
  }
}
function initLockedDoors(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  if (c == 75) {
    let levelIdDec = Number(decode(d[di+1]));
    let levelIdUnit = Number(decode(d[di+2]));
    let levelId = levelIdDec*10+levelIdUnit;
    let entranceDec = Number(decode(d[di+3]));
    let entranceUnit = Number(decode(d[di+4]));
    let entranceId = entranceDec*10+entranceUnit;
    if (
      playerEntrance == entranceId && levelId == currentLevel && (!playerPlaced || playerWasPlaced)
    ) {
      console.log(playerEntrance,entranceId);
      console.log(levelId,currentLevel);
      playerCheckPointX = x;
      playerCheckPointY = y-0.5;
      playerX = x;
      playerY = y-0.5;
    }
    lockedDoor(levelId,entranceId);
  }
}
function initCoinDoors(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  if (c == 36) {
    let coinThou = Number(decode(d[di-w]));
    let coinCent = Number(decode(d[di-w+1]));
    let coinDec = Number(decode(d[di-w+2]));
    let coinUnit = Number(decode(d[di-w+3]));
    let coinNeed = coinThou*1000+coinCent*100+coinDec*10+coinUnit;
    console.log(coinNeed);
    

    let levelIdDec = Number(decode(d[di+1]));
    let levelIdUnit = Number(decode(d[di+2]));
    let levelId = levelIdDec*10+levelIdUnit;
    let entranceDec = Number(decode(d[di+3]));
    let entranceUnit = Number(decode(d[di+4]));
    let entranceId = entranceDec*10+entranceUnit;
    if (
      playerEntrance == entranceId && levelId == currentLevel && (!playerPlaced || playerWasPlaced)
    ) {
      console.log(playerEntrance,entranceId);
      console.log(levelId,currentLevel);
      playerCheckPointX = x;
      playerCheckPointY = y-0.5;
      playerX = x;
      playerY = y-0.5;
    }
    coinDoor(levelId,entranceId,coinNeed);
  }
}

// ENTRELACES

function initHEntrelaces(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let hEntrelaceX = x;
  let hEntrelaceY = y;
  if (c == 116) {
    let unit = Number(decode(d[di+2]));
    let dec = Number(decode(d[di+1]));
    let hEntrelaceId = dec*10+unit;
    if (hEntrelaces[hEntrelaceId]) {
      console.log(hEntrelaces[hEntrelaceId],hEntrelaceX,hEntrelaceY);
      let goX = hEntrelaces[hEntrelaceId][0];
      let goY = hEntrelaces[hEntrelaceId][1];
      hEntrelace(x,y,goX,goY);
      hEntrelace(goX,goY,x,y);
    } else {
      hEntrelaces[hEntrelaceId] = [hEntrelaceX,hEntrelaceY];
    }
  }
}

function initVEntrelaces(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let vEntrelaceX = x;
  let vEntrelaceY = y;
  if (c == 84) {
    let unit = Number(decode(d[di+2]));
    let dec = Number(decode(d[di+1]));
    let vEntrelaceId = dec*10+unit;
    console.log(vEntrelaceId);
    if (vEntrelaces[vEntrelaceId]) {
      console.log(vEntrelaces[vEntrelaceId],vEntrelaceX,vEntrelaceY);
      let goX = vEntrelaces[vEntrelaceId][0];
      let goY = vEntrelaces[vEntrelaceId][1];
      vEntrelace(x,y,goX,goY);
      vEntrelace(goX,goY,x,y);
    } else {
      vEntrelaces[vEntrelaceId] = [vEntrelaceX,vEntrelaceY];
    }
  }
}

// MOVING PLATFORMS

function initMovingPlatforms(){
  // TODO entity x,y,w(wRepeat),h(hRepeat)
  let movingPlatformX = x;
  let movingPlatformY = y;
  if (c == 80) {
    movingPlatform(
      ()=>Math.sin((f+movingPlatformY*10)/40)*6*(movingPlatformY%2?-1:1),
      ()=>0,
      ()=>0,
    );
  }
}


// BOOT

let entitiesArea, mirrors, vEntrelaces, hEntrelaces, entities, entitiesToRemove, levelDom, playerEntrance, background, foreground, playerTrail, groundList, toRestore, checkPoints, playerPlaced, playerWasPlaced, visitedLevels = [], visitedLevelsId = [];

let music = ()=>0;

let transitionIn = 0;
let transitionOut = 0;

function bootLevel(level,entrance){
  let level_number = isNaN(Number(level.slice(-2,-1))) ? level.slice(-1) : level.slice(-2);
  if (level_number == '1' && !visitedLevelsId.includes('1')) {
    msg = message(
      "Square on a door indicate\n"+
      "  an unexplored passage  \n"+
      "                         \n"+
      "           ┌[]┐          \n"+
      "           |  |          \n"+
      "           |__|          \n"+
      "                         \n"+
      "    Enter to continue    \n"
    );
  };
  if (level_number == '3' && !visitedLevelsId.includes('3')) {
    msg = message(
      "Arrow Up to jump \n"+
      "                 \n"+
      "      ↑          \n"+
      "                 \n"+
      "      ⇗   ⎾▔▔▔   \n"+
      "          ▏      \n"+
      "   ▔▔▔▔▔▔▔       \n"+
      "Enter to continue\n"
    );
  };
  if (level_number == '4' && !visitedLevelsId.includes('4')) {
    msg = message(
      "Arrow Down to get\n"+
      "through platforms\n"+
      "                 \n"+
      "        ↓        \n"+
      "     _______     \n"+
      "                 \n"+
      "        ⇓        \n"+
      "                 \n"+
      "Enter to continue\n"
    );
  };
  if (level_number == '5' && !visitedLevelsId.includes('5')) {
    msg = message(
      "You can jump while\n"+
      "      sliding     \n"+
      "                  \n"+
      "       ↑   /      \n"+
      "          /       \n"+
      "       ⇖ /        \n"+
      "        /         \n"+
      "                  \n"+
      "Enter to continue \n"
    );
  };
  if (level_number == '6' && !visitedLevelsId.includes('6')) {
    msg = message(
      "You can jump while\n"+
      "      falling     \n"+
      "                  \n"+
      "     ▕  ↑         \n"+
      "     ▕            \n"+
      "     ▕  ⇗  ____   \n"+
      "     ▕     ▏      \n"+
      "                  \n"+
      "Enter to continue \n"
    );
  };
  if (level_number == '7' && !visitedLevelsId.includes('7')) {
    msg = message(
      "Some doors need a\n"+
      "   key to open   \n"+
      "                 \n"+
      "         ┌[]┐    \n"+
      "     ⚴   |⚴ |    \n"+
      "     ⇑   |__|    \n"+
      "                 \n"+
      "                 \n"+
      "Enter to continue\n"
    );
  };
  if (level_number == '8' && !visitedLevelsId.includes('8')) {
    msg = message(
      " Some doors need \n"+
      "  coins to open  \n"+
      "                 \n"+
      "         ┌[]┐    \n"+
      "     ☺   |☺ |    \n"+
      "     ⇑   |__|    \n"+
      "                 \n"+
      "                 \n"+
      "Enter to continue\n"
    );
  };
  if (level_number == '9' && !visitedLevelsId.includes('9')) {
    msg = message(
      "If you are lost, look out\n"+
      "        for birds        \n"+
      "                         \n"+
      "             V           \n"+
      "          -    l         \n"+
      "            W            \n"+
      "                         \n"+
      "    Enter to continue    \n"
    );
  };
  visitedLevelsId.push(level_number);
  transitionIn = 100;
  transitionOut = 0;
  music = levelMusic[Number(level_number)];
  background = new Area(displayWidth,displayHeight);
  background.process(()=>32);
  foreground = new Area(displayWidth,displayHeight);
  playerTrail = new Area(sceneWidth,sceneHeight);
  playerEntrance = entrance;
  levelDom = document.getElementById(level);
  mirrors = [];
  vEntrelaces = [];
  hEntrelaces = [];
  doors = [];
  groundList = [];
  entities = [];
  entitiesToRemove = [];
  toRestore = [];
  checkPoints = [];
  entitiesArea = strToArea(levelDom.getElementsByClassName("entity")[0].textContent);
  coinIndex = 0;
  

  entitiesArea.process(()=>{
    initBats();
    initMirrors();
    initVEntrelaces();
    initHEntrelaces();
    initDoors();
    initLockedDoors();
    initCoinDoors();
    initMovingPlatforms();
    initCoins(level_number);
    if (c == 89) key();
    if (c == 69) checkPoint();
    if (c == 65 && !playerPlaced) {
      playerCheckPointX = x;
      playerCheckPointY = y;
      playerX = x;
      playerY = y;
      playerPlaced = true;
    }
    return c;
  });
  playerWasPlaced = true;
  fromLevel = currentLevel;
  currentLevel = level_number;
  bootGrounds();
  setColors(); 
}
function setColors(){
  // Set display background to black
  display.setBackground([
    levelDom.getAttribute("data-bg"),
  ])
  // Set channel colors
  display.setColor([
    levelDom.getAttribute("data-bright"),
    levelDom.getAttribute("data-soft"),
    levelDom.getAttribute("data-special"),
    levelDom.getAttribute("data-bitter"),
    levelDom.getAttribute("data-dark"),
  ])
} 

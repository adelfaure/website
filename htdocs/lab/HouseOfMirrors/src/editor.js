const ascii = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

const levelAreaName = [
  /*  0 */  "wall",
  /*  1 */  "danger",
  /*  2 */  "dangerAnimA",
  /*  3 */  "dangerAnimB",
  /*  4 */  "dangerAnimC",
  /*  5 */  "dangerAnimD",
  /*  6 */  "entity",
  /*  7 */  "softWallA",
  /*  8 */  "softWallB",
  /*  9 */  "softWallC",
  /* 10 */  "softWallAnimA",
  /* 11 */  "softWallAnimB",
  /* 12 */  "softWallAnimC",
  /* 13 */  "softWallAnimD",
  /* 14 */  "decor",
  /* 15 */  "upWind",
  /* 16 */  "rightWind",
  /* 17 */  "bottomWind",
  /* 18 */  "leftWind",
  /* 19 */  "vines",
  /* 20 */  "water",
];

let levelWidth, levelHeight;
let levelGround,levelDanger;
let cursorX,cursorY,cursorWidth,cursorHeight;
let marginLeft,marginTop,offsetLeft,offsetTop;
let entityMode,softAnimMode,dangerMode,softMode;
let outlineStyle, inlineStyle;
let mainInfo;
let groundChars;

// LEVEL AREA 

let levelArea = [];

function createLevelArea(){
  area = new Area(levelWidth,levelHeight);
  area.process(()=>64);
  levelArea.push(area);
}

function placeLevelArea(i){
  levelArea[i].place(
    marginLeft+offsetLeft,
    marginTop+offsetTop,
    0,0
  );
}

function calcDataIndex(x,y){
  x = Math.abs(x < 0 ? levelWidth + x%(levelWidth*-1) : x);
  y = Math.abs(y < 0 ? levelHeight + y%(levelHeight*-1) : y);
  return Math.ceil(x%levelWidth) + Math.ceil(y%levelHeight) * levelWidth;
}

function setLevelAreaData(i,data) {
  levelArea[i].data[calcDataIndex(x - marginLeft - offsetLeft ,y - marginTop - offsetTop)]=data;
}

function draw(name,data,exept){
  for (let i = 0; i < levelAreaName.length; i++) {
    if (exept.includes(levelAreaName[i])) continue;
    if (levelAreaName[i] == name) {
      setLevelAreaData(i,data);
    } else {
      // Clear other area
      setLevelAreaData(i,64);
    }
  }
}

function drawWithMode(names,mode,data,exept){
  for (let i = 0; i < levelAreaName.length; i++) {
    if (exept.includes(levelAreaName[i])) continue;
    let index = names.indexOf(levelAreaName[i]);
    if (index > -1 && index == mode) {
      setLevelAreaData(i,data);
    } else {
      // Clear other area
      setLevelAreaData(i,64);
    }
  }
}

function drawing(){
  // Replace mode for typing entity codes
  if (entityMode) {
    // type
    if (ascii.includes(last_key_down)){
      setLevelAreaData(
        levelAreaName.indexOf("entity"),
        encode(last_key_down)
      );
      // move cursor right after typing
      cursorX++;
      last_key_down = false;
    }
    // Erase but only entity area
    if (keys[" "]) {
      setLevelAreaData(
        levelAreaName.indexOf("entity"),
        64
      );
    }
  } else {
    // Erase everything
    if (keys[" "]){for (let i = 0; i < levelAreaName.length; i++)setLevelAreaData(i,64)};
    // Erase decor
    if (keys["m"]){setLevelAreaData(levelAreaName.indexOf("decor"),64)};
    // Draw decor dots
    if (
      keys["*"] 
      && (
        !(Math.floor(y/2)%2) ? !(x%8) : !((x+4)%8)
      )
      && (y%2)
    ){setLevelAreaData(levelAreaName.indexOf("decor"),46)};
    // Draw decor dots
    if (keys["ù"] && !(x%4) && (y%2)){setLevelAreaData(levelAreaName.indexOf("decor"),46)};
    // Draw decor dots
    if (keys["$"] && !(x%2)){setLevelAreaData(levelAreaName.indexOf("decor"),46)};
    // Draw decor double dots
    if (keys["Dead"]){setLevelAreaData(levelAreaName.indexOf("decor"),58)};
    // Draw decor rect edges
    if (keys["o"]){setLevelAreaData(levelAreaName.indexOf("decor"),32)};
    // Draw decor round edges
    if (keys["p"]){setLevelAreaData(levelAreaName.indexOf("decor"),35)};
    // Draw wall rect edges
    if (keys["a"]){draw("wall",32,["entity"])};
    // Draw wall round edges
    if (keys["e"]){draw("wall",35,["entity"])};
    // Draw soft rect edges
    if (keys["r"]) {drawWithMode(["softWallA","softWallB","softWallC"],softMode,32,["entity"])};
    // Draw soft round edges
    if (keys["t"]) {drawWithMode(["softWallA","softWallB","softWallC"],softMode,35,["entity"])};
    // Draw soft anim rect edges
    if (keys["y"]) {drawWithMode(["softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD"],softAnimMode,32,["entity"])};
    // Draw soft anim round edges
    if (keys["u"]) {drawWithMode(["softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD"],softAnimMode,35,["entity"])};
    // Draw up danger
    if (keys["z"]) {drawWithMode(
      ["danger","dangerAnimA","dangerAnimB","dangerAnimC","dangerAnimD"],
      dangerMode,
      9650,
      ["decor","softWallA","softWallB","softWallC","softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD","entity"]
    )};
    // Draw left danger
    if (keys["q"]) {drawWithMode(
      ["danger","dangerAnimA","dangerAnimB","dangerAnimC","dangerAnimD"],
      dangerMode,
      9664,
      ["decor","softWallA","softWallB","softWallC","softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD","entity"]
    )};
    // Draw right danger
    if (keys["d"]) {drawWithMode(
      ["danger","dangerAnimA","dangerAnimB","dangerAnimC","dangerAnimD"],
      dangerMode,
      9654,
      ["decor","softWallA","softWallB","softWallC","softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD","entity"]
    )};
    // Draw down danger
    if (keys["s"]) {drawWithMode(
      ["danger","dangerAnimA","dangerAnimB","dangerAnimC","dangerAnimD"],
      dangerMode,
      9660,
      ["decor","softWallA","softWallB","softWallC","softWallAnimA","softWallAnimB","softWallAnimC","softWallAnimD","entity"]
    )};
  }
  return[88,32,32,32,32];
}

function printLevelArea(i,printFunction,channelFunction){
  let printChannel = [32,32,32,32,32];
  let print = printFunction(i);
  if (print && print != 64) {
    if (
      false
      || x < marginLeft
      || y < marginTop
      || x >= marginLeft + 40
      || y >= marginTop + 20
    ) {
      if (
        (offsetTop%20)
        || (offsetLeft%40)
      ){
        printChannel[3] = print;
        return printChannel;
      }
      printChannel[4] = print;
      return printChannel;
    }
    printChannel[channelFunction()] = print;
    return printChannel;
  }
  return false;
}

// CURSOR FUNCTION

function fitCursor(){
  cursorX=Math.floor(cursorX/cursorWidth)*cursorWidth;  
  cursorY=Math.floor(cursorY/cursorHeight)*cursorHeight;
}

// KEYBINDING

function editorKeyBinding(){
  // shuffle colors
  if (keys["v"]){
    let lightMode = Math.random()>0.5;
    for(let i=0;i<display.colors.length-3;i++){
      display.colors[i]=Math.floor(Math.random()*16)*16;
    }
    display.colors[display.colors.length-3]=Math.floor(Math.random()*8)*8+(lightMode?127:0);
    display.colors[display.colors.length-2]=Math.floor(Math.random()*8)*8+(lightMode?127:0);
    display.colors[display.colors.length-1]=Math.floor(Math.random()*8)*8+(lightMode?127:0);
    display.updateColors();keys["v"]=false
  };
  // up red
  if (keys["b"]){display.colors[0]=display.colors[0] + cursorWidth < 255 ? display.colors[0] + cursorWidth : 255;display.updateColors();keys["b"]=false};
  // down red
  if (keys["n"]){display.colors[0]=display.colors[0] - cursorWidth > 0 ? display.colors[0] - cursorWidth : 0;display.updateColors();keys["n"]=false};
  // up green
  if (keys[","]){console.log("up green");display.colors[1]=display.colors[1] + cursorWidth < 255 ? display.colors[1] + cursorWidth : 255;display.updateColors();keys[","]=false};
  // down green
  if (keys[";"]){console.log("down green");display.colors[1]=display.colors[1] - cursorWidth > 0 ? display.colors[1] - cursorWidth : 0;display.updateColors();keys[";"]=false};
  // Load level
  if (keys["l"]){load(html=>importLevel(html));keys["l"]=false};
  // Move scene up
  if (keys["Control"] && keys["ArrowUp"]) {offsetTop-=cursorHeight;keys["ArrowUp"]=false};
  // Move scene down
  if (keys["Control"] && keys["ArrowDown"]) {offsetTop+=cursorHeight;keys["ArrowDown"]=false};
  // Move scene left
  if (keys["Control"] && keys["ArrowLeft"]) {offsetLeft-=cursorWidth;keys["ArrowLeft"]=false};
  // Move scene right
  if (keys["Control"] && keys["ArrowRight"]) {offsetLeft+=cursorWidth;keys["ArrowRight"]=false};
  // Reduce cursor rows
  if (keys["Shift"] && keys["ArrowUp"]) {cursorHeight = Math.floor(cursorHeight/2) > 0 ? Math.floor(cursorHeight/2) : cursorHeight; fitCursor(); keys["ArrowUp"]=false};
  // Increase cursor rows
  if (keys["Shift"] && keys["ArrowDown"]) {cursorHeight = Math.floor(cursorHeight*2) <= 20 ? Math.floor(cursorHeight*2) : cursorHeight; fitCursor(); keys["ArrowDown"]=false};
  // Reduce cursor cols
  if (keys["Shift"] && keys["ArrowLeft"]) {cursorWidth = Math.floor(cursorWidth/2) > 0 ? Math.floor(cursorWidth/2) : cursorWidth; fitCursor();keys["ArrowLeft"]=false};
  // Increase cursor cols
  if (keys["Shift"] && keys["ArrowRight"]) {cursorWidth = Math.floor(cursorWidth*2) <= 40 ? Math.floor(cursorWidth*2) : cursorWidth; fitCursor();keys["ArrowRight"]=false};
  // Move cursor right
  if (keys["ArrowRight"]) {cursorX+=cursorWidth;keys["ArrowRight"]=false};
  // Move cursor down
  if (keys["ArrowDown"]) {cursorY+=cursorHeight;keys["ArrowDown"]=false};
  // Move cursor left
  if (keys["ArrowLeft"]) {cursorX-=cursorWidth;keys["ArrowLeft"]=false};
  // Move cursor up
  if (keys["ArrowUp"]) {cursorY-=cursorHeight;keys["ArrowUp"]=false};
  // Export
  if (keys["Enter"]) {exportLevel();keys["Enter"] = false;}
  // Switch to entity mode
  if (keys["Escape"]) { entityMode = !entityMode; keys["Escape"] = false;}
  // Vary soft mode
  if (keys["f"]) { softMode = (softMode+1)%3; keys["f"] = false;}
  // Vary soft anim mode
  if (keys["h"]) { softAnimMode = (softAnimMode+1)%4; keys["h"] = false;}
  // Vary danger anim mode
  if (keys["w"]) { dangerMode = (dangerMode+1)%5; keys["w"] = false;}
}

// CHUNK

function groundChunk(area) {
  let editorGroundPlace = area.printAt(di,w);
  if (editorGroundPlace) {
    let top_left = area.printAt(di-w-1,w);
    let top = area.printAt(di-w,w);
    let top_right = area.printAt(di-w+1,w);
    let left = area.printAt(di-1,w);
    let right = area.printAt(di+1,w);
    let bottom_left = area.printAt(di+w-1,w);
    let bottom = area.printAt(di+w,w);
    let bottom_right = area.printAt(di+w+1,w);
    editorGroundPlace = (
      editorGroundPlace == 35 ? (
        (
          true
          && top == 64 
          && left == 64 
          && right == 64 
          && bottom == 64 
        ) ? 9647 :
        (
          true
          && top == 64 
          && right == 64 
          && bottom == 64 
        ) ? 8848 :
        (
          true
          && top == 64 
          && left == 64 
          && bottom == 64 
        ) ? 8847 :
        (
          true
          && top == 64 
          && left == 64 
          && right == 64 
        ) ? 8851 :
        (
          true
          && left == 64 
          && right == 64 
          && bottom == 64 
        ) ? 8852 :
        (
          true
          && top == 64 
          && bottom == 64 
        ) ? 8215 :
        (
          true
          && left == 64 
          && right == 64 
        ) ? 8214 :
        (
          true
          && top == 64 
          && left == 64 
        ) ? 47 :
        (
          true
          && top == 64 
          && right == 64 
        ) ? 92 :
        (
          true
          && bottom == 64 
          && left == 64 
        ) ? 92 :
        (
          true
          && bottom == 64 
          && right == 64 
        ) ? 47 :
        (
          true
          && top == 64 
        ) ? 9620 :
        (
          true
          && right == 64 
        ) ? 9621 :
        (
          true
          && bottom == 64 
        ) ? 95 :
        (
          true
          && left == 64 
        ) ? 9615 :
        32
      ) :
      editorGroundPlace == 32 ? (
        (
          true
          && top == 64 
          && left == 64 
          && right == 64 
          && bottom == 64 
        ) ? 9647 :
        (
          true
          && top == 64 
          && right == 64 
          && bottom == 64 
        ) ? 8848 :
        (
          true
          && top == 64 
          && left == 64 
          && bottom == 64 
        ) ? 8847 :
        (
          true
          && top == 64 
          && left == 64 
          && right == 64 
        ) ? 8851 :
        (
          true
          && left == 64 
          && right == 64 
          && bottom == 64 
        ) ? 8852 :
        (
          true
          && top == 64 
          && bottom == 64 
        ) ? 8215 :
        (
          true
          && left == 64 
          && right == 64 
        ) ? 8214 :
        (
          true
          && top == 64 
          && left == 64 
        ) ? 9150 :
        (
          true
          && top == 64 
          && right == 64 
        ) ? 9163 :
        (
          true
          && bottom == 64 
          && left == 64 
        ) ? 9151 :
        (
          true
          && bottom == 64 
          && right == 64 
        ) ? 9164 :
        (
          true
          && top == 64 
        ) ? 9620 :
        (
          true
          && right == 64 
        ) ? 9621 :
        (
          true
          && bottom == 64 
        ) ? 95 :
        (
          true
          && left == 64 
        ) ? 9615 :
        32
      ) :
      editorGroundPlace
    )
  }
  return editorGroundPlace;
}

// EXPORT

function exportLevelArea(i){
  levelArea[i].place(0,0,0,0);
  let exportArea = new Area(levelWidth+1,levelHeight);
  exportArea.process(()=>{
    // LINE FEED
    if (x == w-1) {
      return 10;
    }
    let levelAreaPlace = groundChunk(levelArea[i]);
    if (levelAreaPlace) return levelAreaPlace;
    return 32;
  });
  let exportElement = document.createElement("pre");
  exportElement.classList.add(levelAreaName[i]);
  exportElement.textContent = exportArea.render({"type":"text"});
  return exportElement;
}

function exportLevel(){
  let level = document.createElement("div");
  // 98 is the test level
  level.setAttribute("id","level98");
  level.setAttribute("data-bright","#ffffff");
  level.setAttribute("data-special","#20ff20");
  level.setAttribute("data-bitter","#ff2020");
  level.setAttribute("data-soft","#2020ff");
  level.setAttribute("data-dark","#505050");
  level.setAttribute("data-bg","#101010");

  for (let i = 0; i < levelAreaName.length; i++) {
    let exportElement = exportLevelArea(i);
    level.appendChild(exportElement);
  }
  let buffer = encoding.encoder.encode(level.outerHTML);
  save([buffer],"level98.html","text/html");
}

function importLevel(html){
  let parent = document.createElement("div");
  parent.innerHTML = html;
  let levelDiv = parent.children[0];
  display.getDomColors(levelDiv); 
  display.updateColors();
  for (let i = 0; i < levelDiv.children.length; i++){
    let levelStr = levelDiv.children[i].textContent;
    levelStr = levelStr.replaceAll('\n','');
    levelStr = levelStr.replaceAll('\\','#');
    levelStr = levelStr.replaceAll('/','#');
    for (let j = 0; j < groundChars.length; j++) {
      levelStr = levelStr.replaceAll(groundChars[j],' ');
    }
    levelArea[i].data = encode(levelStr);
  }
}

// INIT

function initEditor(w,h) {
  entityMode = false;
  outLineStyle = 0;
  softMode = 0;
  softAnimMode = 0;
  dangerMode = 0;
  levelWidth = w;
  levelHeight = h;
  groundChars = decode(new Uint16Array([9647,8848,8847,8851,8852,9150,9163,9151,9164,9620,9621,95,9615]));
 
  for (let i = 0; i < levelAreaName.length; i++) createLevelArea();
  
  cursorWidth = 4;
  cursorHeight = 2;
  cursorX = cursorWidth;
  cursorY = cursorHeight;
  offsetLeft = 0;
  offsetTop = 0;
  marginLeft = Math.floor(displayWidth/2 - 40/2);
  marginTop = Math.floor(displayHeight/2 - 20/2);
  fps = 24;
}

// EDITOR

function editor(){

  // INFO
  mainInfo = infoBox(
    "FPS "+animation_fps+'              \n'
    +"Danger mode (w) "+dangerMode+'   '
    +"Soft mode (f) "+softMode+'     '
    +"Soft anim mode (h) "+softAnimMode
  );
  mainInfo.place(0,0,displayWidth,displayHeight);

  // KEYBINDING
  editorKeyBinding();

  // Fit cursor position to scene
  cursorX = cursorX - cursorWidth < 0 ? cursorWidth : cursorX > 40 ? 40 : cursorX;
  cursorY = cursorY - cursorHeight < 0 ? cursorHeight : cursorY > 20 ? 20 : cursorY;
  
  // PLACE ALL LEVEL AREA
  for (let i = 0; i < levelAreaName.length; i++) placeLevelArea(i);
  
  // display
  display.process(()=>{
    // LINE FEED
    if (x == w-1) {
      return [10,10,10,10,10];
    }

    // INFO
    let infoPrint = mainInfo.printAt(di,w);
    if (infoPrint) return [infoPrint,32,32,32,32];

    // DRAWING
    if (
      // Draw in cursor zone
      true
      && cursorX > x - marginLeft
      && cursorY > y - marginTop
      && cursorX <= x - marginLeft + cursorWidth
      && cursorY <= y - marginTop + cursorHeight
    ) return drawing();

    // OFFSET SCENE INDICATOR
    let spacePrint = (
      !((x-marginLeft-offsetLeft+1)%40)
      || !((y-marginTop-offsetTop+1)%20)
      || !((x-marginLeft-offsetLeft)%40)
      || !((y-marginTop-offsetTop)%20)
    )?43:58
    if (
      false
      || x < marginLeft
      || y < marginTop
      || x >= marginLeft + 40
      || y >= marginTop + 20
    ) {
      if (spacePrint == 43) return [32,32,32,32,spacePrint];
    }

    // ENTITY
    let entityPrint = printLevelArea(6,i=>levelArea[i].printAt(di,w),()=>2);
    if (entityPrint) return entityPrint;
    // DANGER
    let dangerPrint = printLevelArea(1,i=>levelArea[i].printAt(di,w),()=>3);
    if (dangerPrint) return dangerPrint;
    // DANGER A
    let dangerAPrint = printLevelArea(2,i=>levelArea[i].printAt(di,w),()=>((Math.sin(f/fps)< 0) ? 4 : 3));
    if (dangerAPrint) return dangerAPrint;
    // DANGER B
    let dangerBPrint = printLevelArea(3,i=>levelArea[i].printAt(di,w),()=>((Math.sin(f/fps+Math.PI/2)< 0) ? 4 : 3));
    if (dangerBPrint) return dangerBPrint;
    // DANGER C
    let dangerCPrint = printLevelArea(4,i=>levelArea[i].printAt(di,w),()=>((Math.sin(f/fps+Math.PI)< 0) ? 4 : 3));
    if (dangerCPrint) return dangerCPrint;
    // DANGER D
    let dangerDPrint = printLevelArea(5,i=>levelArea[i].printAt(di,w),()=>((Math.sin(f/fps+Math.PI*1.5)< 0) ? 4 : 3));
    if (dangerDPrint) return dangerDPrint;
    // WALL
    let wallPrint = printLevelArea(0,i=>groundChunk(levelArea[i]),()=>1);
    if (wallPrint) return wallPrint;
    // DECOR
    let decorPrint = printLevelArea(14,i=>groundChunk(levelArea[i]),()=>4);
    if (decorPrint) return decorPrint;
    // SOFT WALL A
    let softWallAPrint = printLevelArea(7,i=>groundChunk(levelArea[i]),()=>2);
    if (softWallAPrint) return softWallAPrint;
    // SOFT WALL B
    let softWallBPrint = printLevelArea(8,i=>groundChunk(levelArea[i]),()=>2);
    if (softWallBPrint) return softWallBPrint;
    // SOFT WALL C
    let softWallCPrint = printLevelArea(9,i=>groundChunk(levelArea[i]),()=>2);
    if (softWallCPrint) return softWallCPrint;
    // SOFT WALL ANIM A
    let softWallAnimAPrint = printLevelArea(10,i=>groundChunk(levelArea[i]),()=>((Math.sin(f/fps)< 0) ? 4 : 2));
    if (softWallAnimAPrint) return softWallAnimAPrint;
    // SOFT WALL ANIM B
    let softWallAnimBPrint = printLevelArea(11,i=>groundChunk(levelArea[i]),()=>((Math.sin(f/fps+Math.PI/2)< 0) ? 4 : 2));
    if (softWallAnimBPrint) return softWallAnimBPrint;
    // SOFT WALL ANIM C
    let softWallAnimCPrint = printLevelArea(12,i=>groundChunk(levelArea[i]),()=>((Math.sin(f/fps+Math.PI)< 0) ? 4 : 2));
    if (softWallAnimCPrint) return softWallAnimCPrint;
    // SOFT WALL ANIM D
    let softWallAnimDPrint = printLevelArea(13,i=>groundChunk(levelArea[i]),()=>((Math.sin(f/fps+Math.PI*1.5)< 0) ? 4 : 2));
    if (softWallAnimDPrint) return softWallAnimDPrint;
    // EMPTY SPACE
    if (
      false
      || x < marginLeft
      || y < marginTop
      || x >= marginLeft + 40
      || y >= marginTop + 20
    ) {
      if (
        (offsetTop%20)
        || (offsetLeft%40)
      ){
        return [32,32,32,spacePrint,32];
      }
      return [32,32,32,32,spacePrint];
    }
    return [32,32,32,32,32];
  });
}


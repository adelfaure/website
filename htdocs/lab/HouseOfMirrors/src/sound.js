function touchGroundSfx(intensity){
  if (intensity == 2) {
    playSynth(
      [
        0.5,   // 0 max gain
        0.5,   // 1 gain
        1,   // 2 freq
        2,    // 3 smooth
        1,   // 4 noise
        20,    // 5 res
        0.001, // 6 indur
        0,  // 7 middur
        0.1, // 8 outdur
      ],
      [
        0,    // 9 echo count
        0,   // 10 echo latency
        0,   // 11 echo modifier
      ]
    );
  } else if (intensity == 1) {
    playSynth(
      [
        0.5,   // 0 max gain
        0.5,   // 1 gain
        1,   // 2 freq
        2,    // 3 smooth
        1,   // 4 noise
        10,    // 5 res
        0.001, // 6 indur
        0,  // 7 middur
        0.05, // 8 outdur
      ],
      [
        0,    // 9 echo count
        0,   // 10 echo latency
        0,   // 11 echo modifier
      ]
    );
  }
}
function lockedSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.3,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,5),   // 2 freq
      setSmooth(5),    // 3 smooth
      0,   // 4 noise
      40,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.25,   // 11 echo modifier
    ]
  );
}
function unlockSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.2,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,8),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.2,   // 4 noise
      16,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      200,   // 10 echo latency
      0.625,   // 11 echo modifier
    ]
  );
}

function keySfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.2,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,10),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.5,   // 11 echo modifier
    ]
  );
}
function coinSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.2,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,9),   // 2 freq
      setSmooth(3),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.5,   // 11 echo modifier
    ]
  );
}
function umbrellaOffSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.2,   // 1 gain
      notes[5]*Math.pow(2,11),   // 2 freq
      setSmooth(3),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.75,   // 11 echo modifier
    ]
  );
}
function umbrellaOnSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.2,   // 1 gain
      notes[5]*Math.pow(2,12),   // 2 freq
      setSmooth(3),    // 3 smooth
      0,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.75,   // 11 echo modifier
    ]
  );
}

function messageSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.25,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,8),   // 2 freq
      setSmooth(4),    // 3 smooth
      1,   // 4 noise
      16,    // 5 res
      0.025, // 6 indur
      0,  // 7 middur
      0.025, // 8 outdur
    ],
    [
      16,    // 9 echo count
      50,   // 10 echo latency
      0.7,   // 11 echo modifier
    ]
  );
}
function confirmSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.25,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,7),   // 2 freq
      setSmooth(4),    // 3 smooth
      1,   // 4 noise
      16,    // 5 res
      0.025, // 6 indur
      0,  // 7 middur
      0.025, // 8 outdur
    ],
    [
      16,    // 9 echo count
      50,   // 10 echo latency
      0.7,   // 11 echo modifier
    ]
  );
}
function doorSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.25,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,6),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.2, // 8 outdur
    ],
    [
      16,    // 9 echo count
      200,   // 10 echo latency
      0.6,   // 11 echo modifier
    ]
  );
}
function mirrorSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.25,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,9),   // 2 freq
      setSmooth(10),    // 3 smooth
      1,   // 4 noise
      8,    // 5 res
      0.025, // 6 indur
      0,  // 7 middur
      0.01, // 8 outdur
    ],
    [
      8,    // 9 echo count
      100,   // 10 echo latency
      0.5,   // 11 echo modifier
    ]
  );
}
function flickSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.1,   // 1 gain
      3+Math.floor(Math.random()*3)*Math.pow(2,13),   // 2 freq
      setSmooth(10),    // 3 smooth
      1,   // 4 noise
      0,    // 5 res
      0.003125, // 6 indur
      0,  // 7 middur
      0.00625, // 8 outdur
    ],
    [
      0,    // 9 echo count
      200,   // 10 echo latency
      0.33,   // 11 echo modifier
    ]
  );
}
function camSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.1,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,10),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      16,    // 5 res
      0.00625, // 6 indur
      0,  // 7 middur
      0.00625, // 8 outdur
    ],
    [
      4,    // 9 echo count
      200,   // 10 echo latency
      0.33,   // 11 echo modifier
    ]
  );
}
function entrelaceSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.25,   // 1 gain
      notes[[3,5,7][Math.floor(Math.random()*3)]]*Math.pow(2,8),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      8,    // 5 res
      0.05, // 6 indur
      0,  // 7 middur
      0.05, // 8 outdur
    ],
    [
      16,    // 9 echo count
      100,   // 10 echo latency
      0.6,   // 11 echo modifier
    ]
  );
}
function checkpointSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.3,   // 1 gain
      notes[3]*Math.pow(2,8),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.1, // 8 outdur
    ],
    [
      16,    // 9 echo count
      200,   // 10 echo latency
      0.5,   // 11 echo modifier
    ]
  );
  playSynth(
    [
      0.5,   // 0 max gain
      0.3,   // 1 gain
      notes[7]*Math.pow(2,8),   // 2 freq
      setSmooth(3),    // 3 smooth
      0.1,   // 4 noise
      8,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.1, // 8 outdur
    ],
    [
      16,    // 9 echo count
      200,   // 10 echo latency
      0.5,   // 11 echo modifier
    ]
  );
}

function deathSfx(){
  playSynth(
    [
      1,   // 0 max gain
      0.5,   // 1 gain
      notes[[1][Math.floor(Math.random()*1)]]*Math.pow(2,3),   // 2 freq
      5,    // 3 smooth
      0.5,   // 4 noise
      80,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.25, // 8 outdur
    ],
    [
      8,    // 9 echo count
      150,   // 10 echo latency
      0.65,   // 11 echo modifier
    ]
  );
  playSynth(
    [
      1,   // 0 max gain
      0.5,   // 1 gain
      notes[[1][Math.floor(Math.random()*1)]]*Math.pow(2,4),   // 2 freq
      10,    // 3 smooth
      0.5,   // 4 noise
      80,    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.25, // 8 outdur
    ],
    [
      8,    // 9 echo count
      150,   // 10 echo latency
      0.65,   // 11 echo modifier
    ]
  );
}

function jumpSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.5,   // 1 gain
      1,   // 2 freq
      2+Math.random()*2,    // 3 smooth
      1,   // 4 noise
      5+Math.floor(Math.random()*5),    // 5 res
      0.01, // 6 indur
      0,  // 7 middur
      0.025, // 8 outdur
    ],
    [
      4,    // 9 echo count
      40,   // 10 echo latency
      0.6,   // 11 echo modifier
    ]
  );
}

function moveSfx(){
  playSynth(
    [
      0.5,   // 0 max gain
      0.5,   // 1 gain
      1,   // 2 freq
      2+Math.random()*8,    // 3 smooth
      1,   // 4 noise
      20,    // 5 res
      0.001, // 6 indur
      0,  // 7 middur
      0.005, // 8 outdur
    ],
    [
      0,    // 9 echo count
      0,   // 10 echo latency
      0,   // 11 echo modifier
    ]
  );
}

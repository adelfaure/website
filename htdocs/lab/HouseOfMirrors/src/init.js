let log,messageShow,messageEvent,messageEventOut,messageTransition,doorTuto,msg,mirrorX,mirrorY,mirrorPlace,mirrorGo,playerData, colors;

function init(level){
  
  // LOG
  
  log = new Area(displayWidth,1);
  log.place(0,0,displayWidth,displayHeight);
  fps = 60;
  
  messageShow = true;
  messageEvent = true;
  messageEventOut = false;
  messageTransition = 100;
  doorTuto = false;
  
  msg = message(
    "Arrow Left and Arrow Right to move\n"+
    "                                  \n"+
    "            ←-      -→            \n"+
    "                                  \n"+
    "             ⇐      ⇒             \n"+
    "           ▔▔▔▔▔  ▔▔▔▔▔           \n"+
    "                                  \n"+
    "        Enter to continue         \n"
  );
  
  // ENTITIES PRINT
  
  entitiesPrint = new Area(sceneWidth,sceneHeight+1);
  entitiesIndexes = new Area(sceneWidth,sceneHeight);
  
  // ANIMATION
  mirrorX = -1;
  mirrorY = -1;
  mirrorPlace = 0;
  mirrorGo = false;
  playerData = 0;
  
  // BOOT
  
  bootLevel("level"+level,0);
  
}

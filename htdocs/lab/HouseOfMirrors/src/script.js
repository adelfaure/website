
let groundOrder = [
   0,/*  "wall",           */  
   1,/*  "danger",         */  
   2,/*  "dangerAnimA",    */  
   3,/*  "dangerAnimB",    */  
   4,/*  "dangerAnimC",    */  
   5,/*  "dangerAnimD",    */  
   6,/*  "entity",         */  
   7,/*  "softWallA",      */  
   8,/*  "softWallB",      */  
   9,/*  "softWallC",      */  
  10,/*  "softWallAnimA",  */  
  11,/*  "softWallAnimB",  */  
  12,/*  "softWallAnimC",  */  
  13,/*  "softWallAnimD",  */  
  14,/*  "decor",          */  
  15,/*  "upWind",         */  
  16,/*  "rightWind",      */  
  17,/*  "bottomWind",     */  
  18,/*  "leftWind",       */  
  19,/*  "vines",          */  
  20,/*  "water",          */  
];

let slowRdmValues = [];
let slowRdmIndex = [];
let slowRdmVariety = 100;
let slowRdmSpeed = 3;

function slowRdm(){
  return slowRdmValues[slowRdmIndex[(di+Math.floor(f/slowRdmSpeed))%slowRdmIndex.length]];
}

// REPEAT GAME SCREEN LIKE IN EDITOR ?

function game() {
  // PROCESS FRAME

  // SLOW RDM

  if (!(f%slowRdmSpeed)) {
    for (var i = 0; i < slowRdmVariety; i++){
      slowRdmValues[i] = Math.random()-Math.random();
      slowRdmIndex[i] = Math.floor(Math.random()*slowRdmVariety);
    }
  }
  
  // MUSIC
  if (keys["Enter"] && !audioContext) {audioContext = new AudioContext()};
  music();
  
  // LOG
  let log_content = encode("FPS "+animation_fps+' LVL '+currentLevel);
  log.process(()=>log_content[di]);
  

  // PLAYER
  // TODO Entity collision round with camera per entity ?
  let oldCamX = camX;
  let oldCamY = camY;
  playerMove();
  
  // EMPTY MOVING WALLS
  //movingPlatformGround.area.process(()=>0);
  
  // ENTITIES
  entitiesPrint.process(()=>0);
  entitiesIndexes.process(()=>0);

  while (entitiesToRemove.length){
    entities.splice(entitiesToRemove.pop(),1);
  }
  for (var i = 0; i < entities.length; i++) {
    let entity = entities[i][0];
    // TODO entity x,y,w(wRepeat),h(hRepeat)
    let entityX = entities[i][1]+entity.positionXFunction();
    let entityY = entities[i][2]+entity.positionYFunction();
    let entityW = entities[i][3];
    let entityH = entities[i][4];
    // TODO NOW entity position XY function
    let entityPosition = [
      (entityX + Math.abs((
        camX < 0 ? entitiesArea.width+camX%entitiesArea.width : camX
      )))%entitiesArea.width,
      (entityY + Math.abs((
        camY < 0 ? entitiesArea.height+camY%entitiesArea.height : camY
      )))%entitiesArea.height,
      entityW,
      entityH
    ];
    // TODO NOW entity animation function
    entity.animate(
      entity.animationFunction(entity),
      entityPosition
    );
    let indexList = entity.printOn(entitiesPrint);
    for (var j = 0; j < indexList.length; j++){
      entitiesIndexes.data[indexList[j]] = i;
    }
  }
  
  // GROUND
  for (var i = 0; i < groundList.length; i++) {
    //if (groundList[i].id == "softWallC") {
    //  if (Math.sin(f/fps) + 0.5 < 0) {
    //    // ADD SOME RANDOM
    //    groundList[i].channel = 4;
    //  } else {
    //    groundList[i].channel = 2;
    //  }
    //}
    //if (groundList[i].id == "softWallD") {
    //  if (Math.sin(f/fps+Math.PI/2) + 0.5 < 0) {
    //    groundList[i].channel = 4;
    //  } else {
    //    groundList[i].channel = 2;
    //  }
    //}
    //if (groundList[i].id == "softWallE") {
    //  if (Math.sin(f/fps+Math.PI) + 0.5 < 0) {
    //    groundList[i].channel = 4;
    //  } else {
    //    groundList[i].channel = 2;
    //  }
    //}
    groundList[i].place(camX,camY);
  }

  // PLAYER TRAIL
  if (!(f%6)) playerTrail.process(()=>(
    player.sheet.data.includes(c) ? 58 : 
    c==58 ? 46 : 
    c==46 ? 0 : 
    c
  ));

  // BACKGROUND

  background.process(()=>
    c > 32 && c < 128 ? (Math.random()>0.95 ? 32 : c) :
    (
      Math.random()>0.005 ? c : Math.random()>0.005 ? 32 : 
      Math.floor(32+Math.random()*(127-32))
    )
  );

  // MESSAGE

  if (messageShow) {
    if (messageTransition >= 100 && messageEventOut) {
      messageTransition = 100;
      messageEventOut = false;
      messageShow = false;
    }
    if (messageTransition <= 0) messageTransition = 0;
    msg.place(
      scene.width/2-msg.width/2,
      scene.height/2-msg.height/2,
      scene.width,
      scene.height
    );
    messageTransition = messageTransition + (
      messageEventOut ? (
        messageTransition < 100 ? 4 : 0
      ) : ( 
        messageTransition && messageEvent > 0 ? -4 : 0
      )
    );
    if (keys["Enter"] && !messageEventOut) {
      confirmSfx();
      messageEventOut = true;
      messageEvent = false;
    }
  }

  // PROCESS SCENE
  scene.process(()=>{
    if (Math.random()*100 < transitionIn) {
      return c;
    }
    if (messageShow) {
      let messagePrint = msg.printAt(di,w);
      if (messagePrint && Math.random()*100>messageTransition) {
        return [
          messagePrint == 32 || Math.random()*100 > messageTransition ? messagePrint : (
            32 + Math.random()*(127-32)
          ),
          32,32,32,32
        ];
      }
    }
    // MIRROR PLACE
    if (mirrorX == x && mirrorY == y && mirrorPlace) {
      mirrorGo = [x,y];
      return [32,32,32,32,mirrorPlace];
    }
    // PLAYER
    let player_place = player.printAt(di,w);
    let playerDownPlace = player.printAt(di+w,w);

    if (y < h-1 && playerDownPlace && umbrella) return [9730,32,32,32,32];
    if (y < h-1 && playerDownPlace && playerGotKey) return [9908,32,32,32,32];

    // ENTITIES
    let entity = entities[entitiesIndexes.data[di]] ? entities[entitiesIndexes.data[di]][0] : false;
    let entityPrint = entity ? entitiesPrint.data[di] : false;
    if (entityPrint && entityPrint != 64) {
      entity.printEvent(di);
    }
    if (entity && entity.type == "mirror" && playerDownPlace) {
      console.log(x,entity.x);
      mirrorX = entity.goX-(
        
        entity.x-x
      );
      mirrorY = entity.goY+1;
      mirrorPlace = entityPrint != 32 ? 0 : playerDownPlace;
      if (entityPrint != 32) mirrorGo = false;
      if (entityPrint == 32) {
      let print = [32,32,32,32,32];
      print[entities[entitiesIndexes.data[di]][0].channel(entityPrint,entities[entitiesIndexes.data[di]][0])] = playerDownPlace;
      return print;
      }
    }
    if (player_place && entityPrint && entityPrint != 64) {
      entities[entitiesIndexes.data[di]][0].playerInteractionFunction(
        entitiesIndexes.data[di],
        entities[entitiesIndexes.data[di]][0].sprite.element.pos[0],
        entities[entitiesIndexes.data[di]][0].sprite.element.pos[1],
      );
      if (!playerDead) {
        let mirroredPlayer = calcPlayerIndex(camX,camY) != di;
        return [playerDead || mirroredPlayer ? 32 : player_place,32,mirroredPlayer ? player_place : 32,playerDead && !mirroredPlayer ? player_place : 32,32];
      }
    } else if (entityPrint && entityPrint != 64) {
      let print = [32,32,32,32,32];
      //if (entities[entitiesIndexes.data[di]][0].type.includes("Entrelace")){
      //  let time = Math.floor(f/8-Math.sin(x)/Math.cos(y));
      //  entityPrint = (
      //    (time%(32-(y+x)%16)) ? entityPrint :
      //    32 + (time*x*y%(127-32))
      //  );
      //}
      print[entities[entitiesIndexes.data[di]][0].channel(entityPrint,entities[entitiesIndexes.data[di]][0])] = entityPrint
      return print;
    }

    // PLAYER PLACE
    if (player_place) {
      playerData = player_place;
      let mirroredPlayer = calcPlayerIndex(camX,camY) != di;
      if (!mirroredPlayer && !playerDead) playerTrail.data[di] = player_place;
      return [playerDead || mirroredPlayer ? 32 : player_place,32,mirroredPlayer ? player_place : 32,playerDead && !mirroredPlayer ? player_place : 32,32];
    }

    // PLAYER TRAIL
    let trailPlace = playerTrail.printAt(di,w);
    if (trailPlace) return [32,32,32,playerDead ? trailPlace : 32,playerDead ? 32 : trailPlace];
    
    // GROUND
    for (var i = 0; i < groundList.length; i++) {
      let groundPrint = false;
      //console.log(i,groundList[groundOrder[i]].id);
      //if (groundList[groundOrder[i]].id == "danger"){
      //  groundPrint = groundList[groundOrder[i]].printAt(di,w);
      //  if (groundPrint[3] == 82) {
      //    return [32,32,32,(
      //      rainDrop(x,y,w,h) ? 108 : 64
      //    ),32];
      //  }
      //} else if (groundList[groundOrder[i]].id == "softWallC") {
      //  if (!groundList[groundOrder[i]].area.printAt(di,w)) {
      //    groundPrint = 0;
      //  } else {
      //    if (Math.sin(f/fps) + slowRdm()/2 < 0) {
      //      groundPrint = [
      //        32,
      //        32,
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //      ];
      //    } else {
      //      groundPrint = [
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //        32,
      //        32
      //      ];
      //    }
      //  }
      //} else if (groundList[groundOrder[i]].id == "softWallD") {
      //  if (!groundList[groundOrder[i]].area.printAt(di,w)) {
      //    groundPrint = 0;
      //  } else {
      //    if (Math.sin(f/fps+Math.PI/2) + slowRdm()/2 < 0) {
      //      groundPrint = [
      //        32,
      //        32,
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //      ];
      //    } else {
      //      groundPrint = [
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //        32,
      //        32
      //      ];
      //    }
      //  }
      //} else if (groundList[groundOrder[i]].id == "softWallE") {
      //  if (!groundList[groundOrder[i]].area.printAt(di,w)) {
      //    groundPrint = 0;
      //  } else {
      //    if (Math.sin(f/fps+Math.PI) + slowRdm()/2 < 0) {
      //      groundPrint = [
      //        32,
      //        32,
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //      ];
      //    } else {
      //      groundPrint = [
      //        32,
      //        32,
      //        groundList[groundOrder[i]].area.printAt(di,w),
      //        32,
      //        32
      //      ];
      //    }
      //  }
      //} else {
        groundPrint = groundList[groundOrder[i]].printAt(di,w);
      //}
      if (groundPrint) return groundPrint;
    }

    // BACKGROUND
    let backgroundPrint = background.printAt(di,w);
    if (backgroundPrint) return [32,32,32,32,backgroundPrint];
    
    // EMPTY SPACE
    return [32,32,32,32,32];

  });

  // SCENE
  scene.place(display.width/2-scene.width/2,display.height/2-scene.height/2,displayWidth,displayHeight)

  // FOREGROUND
  if (playerDead > 1) {
    playerDead--;
    if (playerDead < playerDeadTransition) {
      playerX = playerCheckPointX;
      playerY = playerCheckPointY;
      while (toRestore.length) {
        let restore = toRestore.pop();
        if (!grabbedCoins[restore[0].coinLevel][restore[0].coinId]){
          entities.push(restore);
        }
      }
      if (!(f%3)) foreground.process(()=>[Math.floor(32+Math.random()*(127-32))]);
    }
  } else if (foreground.data[0]){
    playerDead = 0;
    foreground.process(()=>0);
  }

  // PROCESS DISPLAY
  display.process(()=>{
    // LINE FEED
    if (x == w-1) {
      return [10,10,10,10,10];
    }

    //if (transition) return [32,32,32,32,32];

    // LOG
    let log_place = log.printAt(di,w);
    if (log_place) {
      return [32,32,32,32,log_place];
    }

    // SCENE
    let scene_place = scene.printAt(di,w);
    if (scene_place) {
      // FOREGROUND
      if (foreground.data[di]) {
        if (playerDead > 1 && playerDead < playerDeadTransition) {
          return [32,32,32,foreground.data[di],32];
        }
      }
      scene_place[3] = scene_place[3] == 64 ? 32 : scene_place[3];
      return scene_place;
    }

    // scene outline
      if (scene.printAt(di+1,w))return [32,32,32,32,124];
      if (scene.printAt(di-1,w))return [32,32,32,32,124];
      if (scene.printAt(di+w,w))return [32,32,32,32,45];
      if (scene.printAt(di-w,w))return [32,32,32,32,45];
      if (scene.printAt(di+w+1,w))return [32,32,32,32,9484];
      if (scene.printAt(di+w-1,w))return [32,32,32,32,9488];
      if (scene.printAt(di-w-1,w))return [32,32,32,32,9496];
      if (scene.printAt(di-w+1,w))return [32,32,32,32,9492];
    return [32,32,32,32,background.data[di]];
    
  });
  // UMBRELLA

  if (!messageEvent) {
  umbrella = (()=>{
    return false;
    let value = (keys[keysBindings["Action"]]) ? !umbrella : umbrella;
    if (keys[keysBindings["Action"]] && value) umbrellaOnSfx();
    if (keys[keysBindings["Action"]] && !value) umbrellaOffSfx();
    keys[keysBindings["Action"]] = false;
    return value;
  })();
  }
  
  // DISPLAY
  
  display.print();
  //pause_animation = true;

  // FX
  if (transitionIn <= 0 && Math.random()>0.99)transitionIn = Math.floor(Math.random()*8);

  // IF CAM MOVED
  if (oldCamX != camX || oldCamY != camY) {
    if (transitionIn <= 0) {
      camSfx();
      transitionIn = 8;
    }
    playerTrail.process(()=>0);
  }
  // TRANSITION
  transitionIn = transitionIn > 0 ? transitionIn - 3 : transitionIn;
}

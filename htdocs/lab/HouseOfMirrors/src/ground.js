// GROUND

Ground = function(
  id,
  channelFunction,
  collideXEvent,
  collideYEvent,
  collideXYEvent,
  standEvent,
  frictionX,
  frictionY,
  gravityX,
  gravityY,
  maxJump,
  velocityXFunction,
  velocityYFunction
) {
  this.id = id;
  this.channelFunction = channelFunction;
  this.channel;
  this.area = strToArea(levelDom.getElementsByClassName(id)[0].textContent);
  this.area.process(()=>d[di] == 64 || d[di] == 38 ? 0 : d[di]);
  this.collideXEvent = collideXEvent;
  this.collideYEvent = collideYEvent;
  this.collideXYEvent = collideXYEvent;
  this.standEvent = standEvent;
  this.frictionX = !frictionX ? airFrictionX : frictionX;
  this.frictionY = !frictionY ? airFrictionY : frictionY;
  this.gravityX = gravityX;
  this.gravityY = gravityY;
  this.maxJump = maxJump;
  this.velocityXFunction = velocityXFunction ? velocityXFunction : ()=>0;
  this.velocityYFunction = velocityYFunction ? velocityYFunction : ()=>0;
  groundList.push(this);
}

Ground.prototype.stand = function(c){
  this.standEvent(this,c);
  return [this.frictionX,this.frictionY,this.gravityX,this.gravityY,this.maxJump,this];
}

Ground.prototype.collideX = function(){
  this.collideXEvent(this);
  return [this.frictionX,this.frictionY,this.gravityX,this.gravityY,this.maxJump,this];
}
Ground.prototype.collideY = function(print){
  this.collideYEvent(this,print);
  return [this.frictionX,this.frictionY,this.gravityX,this.gravityY,this.maxJump,this];
}
Ground.prototype.collideXY = function(){
  this.collideXYEvent(this);
  return [this.frictionX,this.frictionY,this.gravityX,this.gravityY,this.maxJump,this];
}

Ground.prototype.place = function(x,y) {
  flickerPlayed = false;
  this.area.place(x,y,0,0);
}
let flickerPlayed = false;
Ground.prototype.printAt = function(di,w){
  if (this.channel == -1) return 0;
  let nextChannel = this.channelFunction();
  let place = this.area.printAt(di,w);
  if (place && nextChannel != this.channel && !flickerPlayed){ flickSfx(); flickerPlayed = true;}
  this.channel = nextChannel;
  let channels = [32,32,32,32,32];
  channels[this.channel] = place;
  return place ? channels : 0;
}

function rainDrop(x,y,w,h){
  return !((Math.floor((f+(x*40))/2)-y+Math.floor(Math.sin(x*w)*160))%24);
}

function softWall(id){
  return new Ground(
    // pre id
    "softWall"+id,
    // channel
    ()=>2,
    // collide event
    ground =>false,
    (ground,print) =>{
      if ((!stand || stand[5] != ground) && playerVelocityY > 0 && !keys[keysBindings["Move down"]]) {
      if (print[ground.channel] == 92) {
        playerUnvolontaryVelocityX += airGravityY*5;
      }
      if (print[ground.channel] == 47) {
        playerUnvolontaryVelocityX -= airGravityY*5;
      }
        if (playerVelocityY > 0.2) {
        }
        if (playerVelocityY > 0) playerCanJump = true;
      touchGroundSfx(
        playerVelocityY > 0.5 ? 2:
        playerVelocityY > 0.25 ? 1:
        0
      );
      playerVelocityY = 0;
      }
    },
    ground =>{},
    // stand event
    ()=>false,
    // friction X
    0,
    // friction Y
    0,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    airMaxJump
  );
}
function dangerAnim(id){
  return new Ground(
    // pre id
    "dangerAnim"+id,
    // channel
    ()=>{
      let val = (Math.sin(f/fps+(
          id == 'A' ? Math.PI*0:
          id == 'B' ? Math.PI*0.5:
          id == 'C' ? Math.PI*1:
          id == 'D' ? Math.PI*1.5:
          0
        )) + slowRdm()/2)
      return (
        (val < 0) ? 4 :
        3
      );
    },
    // collide event
    ground =>0,
    ground =>0,
    ground =>0,
    // stand event
    (ground,c)=>{
      if (
        (Math.sin(f/fps+(
          id == 'A' ? Math.PI*0:
          id == 'B' ? Math.PI*0.5:
          id == 'C' ? Math.PI*1:
          id == 'D' ? Math.PI*1.5:
          0
        )) - 0.5 < 0)
      ) return;
      playerDie(x+camX,y+camY);
    },
    // friction X
    airFrictionX,
    // friction Y
    airFrictionY,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    airMaxJump,
  );
}
function softWallAnim(id){
  return new Ground(
    // pre id
    "softWallAnim"+id,
    // channel
    ()=>{
      let val = (Math.sin(f/fps+(
          id == 'A' ? Math.PI*0:
          id == 'B' ? Math.PI*0.5:
          id == 'C' ? Math.PI*1:
          id == 'D' ? Math.PI*1.5:
          0
        )) + slowRdm()/2); 
      return (
        (val < 0) ? 4 :
        2
      );
    },
    // collide event
    ground =>false,
    (ground,print) =>{
      if (
      !(
        (Math.sin(f/fps+(
          id == 'A' ? Math.PI*0:
          id == 'B' ? Math.PI*0.5:
          id == 'C' ? Math.PI*1:
          id == 'D' ? Math.PI*1.5:
          0
        )) + 0.5 < 0)
      ) && 

      (!stand || stand[5] != ground) && playerVelocityY > 0 && !keys[keysBindings["Move down"]]) {
      if (print[ground.channel] == 92) {
        playerUnvolontaryVelocityX += airGravityY*5;
      }
      if (print[ground.channel] == 47) {
        playerUnvolontaryVelocityX -= airGravityY*5;
      }
        if (playerVelocityY > 0) playerCanJump = true;
      touchGroundSfx(
        playerVelocityY > 0.5 ? 2:
        playerVelocityY > 0.25 ? 1:
        0
      );
      playerVelocityY = 0;
      }
    },
    ground =>{},
    // stand event
    ()=>false,
    // friction X
    0,
    // friction Y
    0,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    airMaxJump
  );
}

// GROUNDS

let wallGround, movingPlatformGround, softWallAGround, softWallBGround, softWallCGround, decorGround, rainGround;

let lastTouch = 0;


function bootGrounds(){
  wallGround = new Ground(
    // pre id
    "wall",
    // channel
    ()=>1,
    // collide event
    ground =>{
      playerVelocityX = 0;
    },
    (ground,print) =>{
      if (print[ground.channel] == 92) {
        playerUnvolontaryVelocityX += airGravityY*5;
      }
      if (print[ground.channel] == 47) {
        playerUnvolontaryVelocityX -= airGravityY*5;
      }
      if (playerVelocityY > 0) playerCanJump = true;
      touchGroundSfx(
        playerVelocityY > 0.5 ? 2:
        playerVelocityY > 0.25 ? 1:
        0
      );
      playerVelocityY = 0;
    },
    ground =>{
      if (!collideX && !collideY) {
        playerVelocityX = 0;
        playerVelocityY = 0;
        keys[keysBindings["Move right"]] = false;
        keys[keysBindings["Move left"]] = false;
      }
    },
    // stand event
    ()=>{
      playerX += Math.ceil(Math.abs(playerVelocityX))*Math.sign(playerVelocityX)*-1;
      playerY += Math.ceil(Math.abs(playerVelocityY))*Math.sign(playerVelocityY)*-1;
    },
    // friction X
    airFrictionX,
    // friction Y
    0,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    0,
  );
  dangerGround = new Ground(
    // pre id
    "danger",
    // channel
    ()=>3,
    // collide event
    ground =>0,
    ground =>0,
    ground =>0,
    // stand event
    (ground,c)=>{
      //let x = Math.ceil(playerX);
      //let y = Math.ceil(playerY);
      //let w = Math.floor(sceneWidth);
      //let h = Math.floor(sceneHeight);
      //// RAIN
      //if (c[3] == 82 ){
      //  if (rainDrop(x,y,w,h) && !umbrella){
      //  } else {
      //    return;
      //  }
      //}
      playerDie(x+camX,y+camY);
    },
    // friction X
    airFrictionX,
    // friction Y
    airFrictionY,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    airMaxJump,
  );
  dangerAnimAGround = dangerAnim('A');
  dangerAnimBGround = dangerAnim('B');
  dangerAnimCGround = dangerAnim('C');
  dangerAnimDGround = dangerAnim('D');

  softWallAGround = softWall('A');
  softWallBGround = softWall('B');
  softWallCGround = softWall('C');
  softWallAnimAGround = softWallAnim('A');
  softWallAnimBGround = softWallAnim('B');
  softWallAnimCGround = softWallAnim('C');
  softWallAnimDGround = softWallAnim('D');
/*
wall
danger
dangerAnimA
dangerAnimB
dangerAnimC
dangerAnimD
entity
softWallA
softWallB
softWallC
softWallAnimA
softWallAnimB
softWallAnimC
softWallAnimD
decor
upWind
rightWind
bottomWind
leftWind
vines
water
*/
  decorGround = new Ground(
    // pre id
    "decor",
    // channel
    ()=>4,
    // collide event
    ()=>false,
    ()=>false,
    ()=>false,
    // stand event
    ()=>false,
    // friction X
    0,
    // friction Y
    0,
    // gravity X
    airGravityX,
    // gravity Y
    airGravityY,
    // maxJump
    airMaxJump,
  );
}

  //movingPlatformGround = new Ground(
  //  // pre id
  //  "movingPlatform",
  //  // channel
  //  0,
  //  // collide event
  //  ground =>{},
  //  ground =>{
  //    if ((!stand || stand[5] != ground) && playerVelocityY > 0 && !keys[keysBindings["Move down"]]) {
  //      playerCanJump = true;
  //    if (playerVelocityY > 0.5) {
  //      playSynth(
  //        [
  //          0.5,   // 0 max gain
  //          0.5,   // 1 gain
  //          1,   // 2 freq
  //          2,    // 3 smooth
  //          1,   // 4 noise
  //          20,    // 5 res
  //          0.001, // 6 indur
  //          0,  // 7 middur
  //          0.1, // 8 outdur
  //        ],
  //        [
  //          0,    // 9 echo count
  //          0,   // 10 echo latency
  //          0,   // 11 echo modifier
  //        ]
  //      );
  //    } else if (playerVelocityY > 0.25) {
  //      playSynth(
  //        [
  //          0.5,   // 0 max gain
  //          0.5,   // 1 gain
  //          1,   // 2 freq
  //          2,    // 3 smooth
  //          1,   // 4 noise
  //          10,    // 5 res
  //          0.001, // 6 indur
  //          0,  // 7 middur
  //          0.05, // 8 outdur
  //        ],
  //        [
  //          0,    // 9 echo count
  //          0,   // 10 echo latency
  //          0,   // 11 echo modifier
  //        ]
  //      );
  //    }
  //      playerVelocityY = 0;
  //      playerUnvolontaryVelocityX = (
  //        ground.velocityXFunction(f,Math.floor(playerY+2))-ground.velocityXFunction(f-1,Math.floor(playerY+2))
  //      )/2;
  //    }
  //  },
  //  ground =>{},
  //  // stand event
  //  ground =>{},
  //  // friction X
  //  0,
  //  // friction Y
  //  0,
  //  // gravity X
  //  airGravityX,
  //  // gravity Y
  //  airGravityY,
  //  // maxJump
  //  airMaxJump,
  //  // velocityXFunction
  //  (f,y)=>Math.sin((f+y*10)/40)*6*(y%2?-1:1)
  //);
  //
  //

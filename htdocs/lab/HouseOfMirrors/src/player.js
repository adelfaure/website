// CAMERA
let camX = 0;
let camY = 0;

// PLAYER

let playerJump = 0;
let playerCanJump = false;
let playerCheckPointX = 0;
let playerCheckPointY = 0;
let playerX = 0;
let playerY = 0;
let playerSpeedX = 0.15;
let playerSpeedY = 0.075;
let playerVelocityX = 0;
let playerVelocityY = 0;
let playerUnvolontaryVelocityX = 0;
let playerUnvolontaryVelocityY = 0;
let playerDead = 0;
let playerDeadTransition = 0;
let umbrella = false;
//let mirroredX = true;

let airFrictionX = 1.5;
let airFrictionY = 1.25;
let airMaxJump = 0.28;
let airGravityX = 0;
let airGravityY = 0.125;
let air = [airFrictionX,airFrictionY,airGravityX,airGravityY,airMaxJump];

let stand,collideX,collideY,collideXY;

let player = new Sprite(
  strToArea(document.getElementById("player").textContent),
  9
);

// KEYS

let keysBindings = {
  "Move left" : "ArrowLeft",
  "Move right" : "ArrowRight",
  "Jump" : "ArrowUp",
  "Action" : "ArrowDown",
  "Move up" : "ArrowUp",
  "Move down" : "ArrowDown",
}; 

function calcPlayerIndex(x,y){
  return Math.ceil(playerX+x) + Math.ceil(playerY+y) * sceneWidth;
}

function playerMove(){
  if (playerDead > 1 && playerDead < playerDeadTransition) {
    player.animate(1,[playerX,playerY,sceneWidth-1,sceneHeight-1]);
    return;
  }
  // Stand
  let playerStand = calcPlayerIndex(camX,camY);
  for (var i = 0; i < groundList.length; i++) groundList[groundOrder[i]].place(camX,camY);
  stand = (()=>{
    for (var i = 0; i < groundList.length; i++) {
      let groundPrint = groundList[groundOrder[i]].printAt(playerStand,sceneWidth);
      if (groundPrint) return groundList[groundOrder[i]].stand(groundPrint);
    }
    return false;
  })();
  stand = !stand ? air : stand;
  
  // JUMP
  let maxJump = stand[4];
  if (!messageEvent) {
  if (playerCanJump && keys[keysBindings["Jump"]]) {
    playerJump = maxJump;
    playerCanJump = false;
    jumpSfx();
  }
  }
  
  let gravityX = stand[2];
  let gravityY = stand[3];

  // Apply gravity to player velocity
  playerVelocityX += gravityX;
  playerVelocityY += (gravityY/((umbrella && playerVelocityY > 0)?8:(playerJump > 0.001?4:1)))-playerJump;

 
  // Control player
  // TODO AIR GROUND ?
  if (!messageEvent) {
    playerVelocityX = (
      keys[keysBindings["Move left"]] ? playerVelocityX - playerSpeedX : 
      keys[keysBindings["Move right"]] ? playerVelocityX + playerSpeedX : 
      playerVelocityX
    );
    playerVelocityY = (
      //keys[keysBindings["Move up"]] ? playerVelocityY - playerSpeedY : 
      //keys[keysBindings["Move down"]] ? playerVelocityY + playerSpeedY : 
      playerVelocityY
    );
  }
  
  // Apply unvolontary velocity
  let playerBakUnvolontaryVelocityX = playerUnvolontaryVelocityX;
  let playerBakUnvolontaryVelocityY = playerUnvolontaryVelocityY;
  playerVelocityX += playerUnvolontaryVelocityX;
  playerVelocityY += playerUnvolontaryVelocityY;
  playerUnvolontaryVelocityX = 0;
  playerUnvolontaryVelocityY = 0;


  // Collide
  
  // Temporary cam to check ground if cam moved by velocity
  let tryCamX = (
    Math.floor(
      Math.ceil(playerX + playerVelocityX)/sceneWidth
    )*sceneWidth
  )*-1;
  let tryCamY = (
    Math.floor(
      Math.ceil(playerY + playerVelocityY)/sceneHeight
    )*sceneHeight
  )*-1;

  let playerCollideX = calcPlayerIndex(playerVelocityX+tryCamX,camY);
  let playerCollideY = calcPlayerIndex(camX,playerVelocityY+tryCamY);
  let playerCollideXY = calcPlayerIndex(playerVelocityX+tryCamX,playerVelocityY+tryCamY);
  
  for (var i = 0; i < groundList.length; i++) groundList[i].place(tryCamX,camY);

  collideX = (()=>{
    for (var i = 0; i < groundList.length; i++) {
      let groundPrint = groundList[i].printAt(playerCollideX,sceneWidth);
      if (groundPrint) {
        return groundList[i].collideX();
      }
    }
    return false;
  })();
  
  for (var i = 0; i < groundList.length; i++) groundList[i].place(camX,tryCamY);
  
  collideY = (()=>{
    for (var i = 0; i < groundList.length; i++) {
      let groundPrint = groundList[i].printAt(playerCollideY,sceneWidth);
      if (groundPrint) {
        return groundList[i].collideY(groundPrint);
      }
    }
    return false;
  })();
  
  for (var i = 0; i < groundList.length; i++) groundList[i].place(tryCamX,tryCamY);
  
  collideXY = (()=>{
    for (var i = 0; i < groundList.length; i++) {
      let groundPrint = groundList[i].printAt(playerCollideXY,sceneWidth);
      if (groundPrint) return groundList[i].collideXY();
    }
    return false;
  })();

  collideX = !collideX ? air : collideX;
  collideY = !collideY ? air : collideY
  collideXY = !collideXY ? air : collideXY;

  let frictionX = (stand[0]+collideX[0]+collideXY[0])/3;
  let frictionY = (stand[1]+collideX[1]+collideXY[1])/3;
  
  // Jump
  if (playerJump) {
    playerJump /= frictionY; 
  }

  // Apply friction to player velocity
  playerVelocityX /= frictionX;
  playerVelocityY /= frictionY;
 
  // SAVE POS
  let oldPlayerX = Math.floor(playerX);
 
  // Update player coordinates
  playerX = playerX + playerVelocityX;
  playerY = playerY + playerVelocityY;
  
  // Update camera coordinates
  camX = (
    Math.floor(
      Math.ceil(playerX)/sceneWidth
    )*sceneWidth
  )*-1;
  camY = (
    Math.floor(
      Math.ceil(playerY)/sceneHeight
    )*sceneHeight
  )*-1;
 
  // Place player
  
  let playerBaseVelocityX = playerVelocityX - playerBakUnvolontaryVelocityX;
  let playerBaseVelocityY = playerVelocityY - playerBakUnvolontaryVelocityY; 
  player.animate(
    // player animation
    (
      (playerBaseVelocityX < -0.1 && playerBaseVelocityY < -0.1) ? (-5) :
      (playerBaseVelocityX > 0.1 && playerBaseVelocityY < -0.1) ? (-6) :
      (playerBaseVelocityX > 0.1 && playerBaseVelocityY > 0.1) ? (-7) :
      (playerBaseVelocityX < -0.1 && playerBaseVelocityY > 0.1) ? (-8) :
      (playerBaseVelocityX < -0.1) ? (-1) :
      (playerBaseVelocityX > 0.1) ? (-2) :
      (playerBaseVelocityY < -0.1) ? (-3) :
      (playerBaseVelocityY > 0.1) ? (-4) :
      (0)
    ),
    [playerX,playerY,(
      /*!mirroredX ? */ sceneWidth-1/* : sceneWidth/2+3*/
    ),sceneHeight-1]
  );

  // SOUND
  if (
    Math.floor(playerX) != oldPlayerX
    && !playerBaseVelocityY 
    && (
      playerBaseVelocityX > 0.1 
      || playerBaseVelocityX < -0.1 
    )
  ) {
    moveSfx();
  }
}

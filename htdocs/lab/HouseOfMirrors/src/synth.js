let audioContext = false;

function synthProcess(i,gain,disto,freq,frame_rate,maxNoise){
  let noise = (Math.random()-0.5)*maxNoise;
  let signal = Math.sin( i * 2 * Math.PI * ((freq)/frame_rate))+noise;
  signal = Math.floor(signal*disto)/disto;
  let value = gain * signal;
  value = value > gain ? gain : value < gain*-1 ? gain*-1 : value;
  return value;
}

function synth(
  maxGain,
  gain,
  freq,
  dist,
  noise,
  res,
  inDur,
  midDur,
  outDur
){
  let rate = audioContext.sampleRate;
  let length = rate * (inDur + midDur + outDur);
  let buffer = audioContext.createBuffer(
    1,
    length,
    audioContext.sampleRate,
  );
  gain = gain > 1 ? 1 : gain;
  let data = buffer.getChannelData(0);
  let gainIndex = 0;
  let gainUp = inDur * rate;
  let gainMid = midDur * rate;
  let gainDown = outDur * rate;
  let last_value = 0;
  for (var i = 0; i < length; i++) {
    gainIndex = (
      i < gainUp ? gainIndex + 1/gainUp :
      i < gainMid ? gainIndex :
      gainIndex - 1/gainDown
    );
    gainIndex = gainIndex < 0 ? 0 : gainIndex;
    let value = synthProcess(i,gainIndex*gain*maxGain,dist,freq,rate,noise);
    last_value = !(i%res) ? value : last_value;
    data[i] = last_value;
  }
  return buffer
}

let note = [32.7,36.71,41.2,43.65,49,55,61.74,32.7*2];
let octave = [0,1,2,3,4,5,6,7,8];

//document.addEventListener("keydown",function(e){
//  if (!audioContext) audioContext = new AudioContext();
//  a=[0,3,5,7,10][b%3];
//  b++;
//  playSynth(
//    [
//      0.5,   // 0 max gain
//      0.3,   // 1 gain
//      setNote(a)*Math.pow(2,6),   // 2 freq
//      setSmooth(6),    // 3 smooth
//      0.1,   // 4 noise
//      10,    // 5 res
//      0.01, // 6 indur
//      2,  // 7 middur
//      0.01, // 8 outdur
//    ],
//    [
//      0,    // 9 echo count
//      400,   // 10 echo latency
//      0.9,   // 11 echo modifier
//    ]
//  );
//  //playSynth(
//  //  [
//  //    0.5,   // 0 max gain
//  //    0.15,   // 1 gain
//  //    setNote(a)*Math.pow(2,7),   // 2 freq
//  //    setSmooth(6),    // 3 smooth
//  //    0,   // 4 noise
//  //    8,    // 5 res
//  //    0.04, // 6 indur
//  //    0.5,  // 7 middur
//  //    0.2, // 8 outdur
//  //  ],
//  //  [
//  //    16,    // 9 echo count
//  //    400,   // 10 echo latency
//  //    0.9,   // 11 echo modifier
//  //  ]
//  //);
//  a=[0,3,5,7,10][b%4];
//  playSynth(
//    [
//      0.5,   // 0 max gain
//      0.2,   // 1 gain
//      setNote(a)*Math.pow(2,8),   // 2 freq
//      setSmooth(6),    // 3 smooth
//      0,   // 4 noise
//      8,    // 5 res
//      0.02, // 6 indur
//      0.5,  // 7 middur
//      0.2, // 8 outdur
//    ],
//    [
//      16,    // 9 echo count
//      600,   // 10 echo latency
//      0.9,   // 11 echo modifier
//    ]
//  );
//  //a=[0,3,5,7,10][b%5];
//  //playSynth(
//  //  [
//  //    0.5,   // 0 max gain
//  //    0.2,   // 1 gain
//  //    setNote(a)*Math.pow(2,8),   // 2 freq
//  //    setSmooth(3),    // 3 smooth
//  //    0,   // 4 noise
//  //    8,    // 5 res
//  //    0.01, // 6 indur
//  //    0,  // 7 middur
//  //    0.2, // 8 outdur
//  //  ],
//  //  [
//  //    16,    // 9 echo count
//  //    300,   // 10 echo latency
//  //    0.5,   // 11 echo modifier
//  //  ]
//  //);
//  //a=[0,3,7,10,12][b%5];
//  //playSynth(
//  //  [
//  //    0.5,   // 0 max gain
//  //    0.2,   // 1 gain
//  //    setNote(a)*Math.pow(2,9),   // 2 freq
//  //    setSmooth(3),    // 3 smooth
//  //    0,   // 4 noise
//  //    8,    // 5 res
//  //    0.01, // 6 indur
//  //    0,  // 7 middur
//  //    0.2, // 8 outdur
//  //  ],
//  //  [
//  //    16,    // 9 echo count
//  //    100,   // 10 echo latency
//  //    0.5,   // 11 echo modifier
//  //  ]
//  //);
//});

let b = 0;

function playSynth(synthOptions,echoOptions){
  if (
    !audioContext
    || audioContext.state != "running"
  ) return;
  let buffer = synth(...synthOptions);
  playBuffer(buffer);
  echo(...[buffer].concat(echoOptions));
}

function playBuffer(buffer){
  let source = audioContext.createBufferSource();
  source.buffer = buffer;
  source.connect(audioContext.destination);
  source.start();
}

function echo(buffer,count,every,modifier){
  if (!count) return;
  let data = buffer.getChannelData(0);
  for (var i = 0; i < data.length; i++) {
    data[i] *= modifier;
  }
  playBuffer(buffer);
  setTimeout(function(){
    echo(buffer,count-1,every,modifier);
  },every);
}

let a = 0;

function setSmooth(value){
  return 0.1 * Math.pow(value+1,1.75);
}

function setNote(value){
  return notes[value%notes.length];
}

let notes = [
1.0219375,
1.08275,
1.147125 ,
1.2153125,
1.2875625 ,
1.3641875,
1.44525,
1.5311875,
1.62225,
1.71875,
1.8209375,
1.92925
];

/*


"',.:;`


[]

  do nothing

_ save

~ load

() repeat
{} every

! play

? octave

$ max gain
@ gain
# freq
& smooth
% noise
* res
< indur
= middur
> outdur
/ echo count
| echo latency
\ echo modifier

0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ set 

+ increase 
- decrease
^ randomize

 

*/

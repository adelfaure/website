<!doctype html>
<html>
<head>
<meta charset="utf-16" />
<style>
  *{
    margin:0;
    padding:0;
    overflow:hidden;
    -webkit-font-smoothing: none;
    -webkit-user-select: none; /* Safari */
    -ms-user-select: none; /* IE 10 and IE 11 */
    user-select: none; /* Standard syntax */
  }
  @font-face{
    font-family:"jgs";
    src:url("fonts/Jgs-SingleLine.ttf");
  }
  pre {
    font-family:"jgs";
  }
  body{
    width: 100%;
  }
  #assets,#levels{
    display:none;
  }
</style>
<script defer src="src/animation.js"></script>
<script defer src="src/textor.js"></script>
<script defer src="src/ui.js"></script>
<script defer src="src/ground.js"></script>
<script defer src="src/synth.js"></script>
<script defer src="src/entities.js"></script>
<script defer src="src/levels.js"></script>
<script defer src="src/sound.js"></script>
<script defer src="src/music.js"></script>
<script defer src="src/display.js"></script>
<script defer src="src/events.js"></script>
<script defer src="src/editor.js"></script>
<script defer src="src/colors.js"></script>
<script defer src="src/init.js"></script>
<script defer src="src/script.js"></script>
<script defer src="src/mainEditor.js"></script>
</head>
<body>
</body>
</html>

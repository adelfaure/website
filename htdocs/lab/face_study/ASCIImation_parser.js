console.log("ASCIImation_parser");

function add_frame_to_stack(frame,stack,length) {
  if (!frame.length) return;
  for (var l = 0; l < length; l++) {
    let frame_copy = [];
    for (var i = 0; i < frame.length; i++) {
      frame_copy[i] = frame[i];
    }
    stack.push([]);
    while(frame_copy.length) {
      stack[stack.length-1].push(frame_copy.shift());
    }
  }
  while(frame.length) frame.pop();
}

function animascii_to_stack(str) {
  let lines = str.split('\n');
  if (lines[0] != "ASCIImation") return false;
  let bungee = lines[1] == "BUNGEE" ? true : false;
  let stack = [];
  let frame = [];
  let length = 0;
  let first_length = 0;
  for (var i = 1; i < lines.length; i++) {
    let tokens = lines[i].split(' ');
    if (tokens[0] == "LEN") {
      add_frame_to_stack(frame,stack,length);
      length = Number(tokens[1]);
      if (!first_length) first_length = length;
    } else {
      frame.push(lines[i]);
    }
  }
  if (bungee) {
    let anim_length = stack.length;
    add_frame_to_stack(frame,stack,length);
    while(anim_length--) {
      stack.push(stack[anim_length]);
      if (anim_length == first_length) break;
    }
  } else {
    add_frame_to_stack(frame,stack,length);
  }
  return stack;
}

function stack_frame_to_string(stack,frame) {
  frame = frame - Math.floor(frame/stack.length)*stack.length;
  if (!stack[frame]) return false;
  let str = "";
  for (var i = 0; i < stack[frame].length; i++) {
    str += stack[frame][i] + '\n';
  }
  return str;
}

// Create a pre element as log display
let log_el = document.createElement("pre");
document.body.appendChild(log_el);

// Add string ending with new line to log
function log(str) {
  if (str === false) return false;
  log_el.textContent += String(str) +'\n';
}

// Empty log
function clear() {
  log_el.textContent = '';
}

function D6() {
  return Math.ceil(Math.random()*6);
}

function roll(N) {
  let roll_result = [];
  while (N--) {
    roll_result.push(D6());
  }
  return roll_result;
}

function total(roll) {
  let result = 0;
  for (var i = 0; i < roll.length; i++) {
    result += roll[i];
  }
  return result;
}

function re_roll(roll,selection) {
  while (selection.length) {
    roll[selection.pop()] = D6();
  }
}
// TOTEST
function select(roll,N) {
  let selection = [];
  for (var i = 0; i < roll.length; i++) {
    if (roll[i] == N) selection.push(i);
  }
  return selection;
}
// TODO
//function multi_select(multi_N,roll) {
//  let multi_selection = [];
//  for (var i = 0; i < multi_N.length; i++) {
//    
//  }
//}

function even(roll) {
  let even_indexes = [];
  for (var i = 0; i < roll.length; i++) {
    if (Number.isInteger(roll[i]/2)){
      even_indexes.push(i);
    }
  }
  return even_indexes;
}

function odd(roll) {
  let odd_indexes = [];
  for (var i = 0; i < roll.length; i++) {
    if (!Number.isInteger(roll[i]/2)){
      odd_indexes.push(i);
    }
  }
  return odd_indexes;
}

function kind(roll) {
  let kind_result = [];
  for (var i = 1; i < 7; i++) {
    kind_result.push(0);
    for (var j = 0; j < roll.length; j++) {
      if (roll[j] == i) {
        kind_result[kind_result.length-1]++;
      }
    }
  }
  return kind_result;
}

function N_of_kind(N,kind_result) {
  return kind_result.includes(N);
}

function count(roll,N) {
  let count_result = 0;
  for (var i = 0; i < roll.length; i++) {
    if (roll[i] == N) count_result++;
  }
  return count_result;
}

function sequence(roll) {
  let all_sequence_result = [];
  for (var i = 1; i < 7; i++) {
    if (!roll.includes(i)) continue;
    var value = i;
    var sequence_result = [];
    sequence_result.push(value);
    var found_next = roll.includes(value+1);
    while (found_next) {
      value++;
      sequence_result.push(value);
      if (sequence_result.length) {
        let is_new = true;
        for (var j = 0 ; j < all_sequence_result.length; j++) {
          if (String(all_sequence_result[j]) == String(sequence_result)) {
            is_new = false;
          }
        }
        if (is_new) {
          let result_copy = [];
          for (var j = 0; j < sequence_result.length; j++) {
            result_copy.push(sequence_result[j]);
          }
          all_sequence_result.push(result_copy);
        }
      }
      found_next = roll.includes(value+1);
    }
  }
  return all_sequence_result;
}



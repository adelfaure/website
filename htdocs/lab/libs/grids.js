// convert grid index to grid x y position
function pos(index,width,data_length) {
  let true_index = Math.floor(index/data_length);
  let line = Math.floor(true_index/width);
  return [true_index - line * width, line];
}

// convert grid x y position to grid index
function idx(pos,width,data_length) {
  return pos[1]*data_length * width + pos[0]*data_length;
}

// verify if pos is inside grid limits
function verif_pos(pos,grid) {
  return pos[0] < wh(grid) && pos[0] >= 0 && pos[1] < ht(grid) && pos[1] >= 0;
}

// make grid
function make_grid(width,height,data_length) {
  let grid = [];
  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      for (var d = 0; d < data_length; d++) {
        grid.push(0); // Default data filler to verify get_data output
      }
    }
  }
  grid.push(data_length);
  grid.push(height);
  grid.push(width);
  return grid;
}

// return grid width
function wh(grid) {
  return (grid[grid.length-1]);
}

// return grid height
function ht(grid) {
  return (grid[grid.length-2]);
}

// return grid data length
function dl(grid) {
  return (grid[grid.length-3]);
}

// get grid data from x y pos
function get_data(grid,pos) {
  if (!verif_pos(pos,grid)) return false;
  let data = [];
  let index = idx(pos,wh(grid),dl(grid));
  for (var d = 0; d < dl(grid); d++) {
    data.push(grid[index+d]);
  }
  return data;
}

// set grid data from x y pos
function set_data(grid,pos,data) {
  if (!verif_pos(pos,grid)) return false;
  let index = idx(pos,wh(grid),dl(grid));
  for (var d = 0; d < dl(grid); d++) {
    grid[index+d] = data[d];
  }
  return true;
}

// shift data
function shift(grid,from,to,pop) {
  let shifted_data = extract(grid,from,to);
  if (pop) {
    shifted_data.unshift(shifted_data.pop());
  } else {
    shifted_data.push(shifted_data.shift());
  }
  for (var i = 0; i < shifted_data.length; i++) {
    set_data(grid,pos(idx(from,wh(grid),dl(grid))+i,wh(grid),dl(grid)),shifted_data[i]);
  } 
}

// extract data
function extract(grid,from,to) {
  let from_idx = idx(from,wh(grid),dl(grid));
  let to_idx = idx(to,wh(grid),dl(grid));
  let extracted_data = [];
  let distance = to_idx - from_idx;
  for (var i = 0; i < distance; i++) {
    extracted_data.push(get_data(grid,pos(from_idx+i,wh(grid),dl(grid))));
  }
  return extracted_data;
}

// check if data is empty
function is_empty(grid,data) {
  console.log(data);
  for (var d = 0; d < dl(grid); d++) {
    if (data[d]) return false;
  }
  return true;
}

// resize grid
function resize(grid,new_width,new_height) {
  if (new_width <= 0 || new_height <= 0) return grid;
  let new_grid = make_grid(new_width,new_height,dl(grid));
  for (var y = 0; y < ht(grid); y++) {
    for (var x = 0; x < wh(grid); x++) {
      set_data(new_grid,[x,y],get_data(grid,[x,y]));
    }
  }
  return new_grid;
}

// convert text into grid
function text_to_grid(str) {
  console.log(str);
}

console.log("grids.js");

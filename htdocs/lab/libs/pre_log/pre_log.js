// Create a pre element as log display
let log_el = document.createElement("pre");
document.body.appendChild(log_el);

var log_number = 0;

// Add string ending with new line to log
function log(str,print_log_number) {
  if (str === false) return false;
  log_el.textContent += (print_log_number ? log_number + '  ' : '') + String(str) +'\n';
  log_number++;
}

// Empty log
function clear() {
  log_el.textContent = '';
}

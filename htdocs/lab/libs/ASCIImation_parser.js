// ASCIImation cheatsheet

/*
:: 
:: Comment
 O
/|\
/ \
:: Some ASCII art
::F
:: Compile frame
_O_
 | 
/ \
::F
\O/
 | 
/ \
::FR 10
:: Set frame compilation repeat to 10
::F
::GBF
:: Set group compilation to back and forth mode, reset to false after group
:: compilation
::GV
:: Set group compilation to reverse mode, reset to false after group compilation
::GH
:: Set group compilation to hide mode, reset to false after group compilation
::GR 2
:: Set group compilation repeat to 10
::X 1 1
:: Set X offset modifier for group compilation by 1 every 1 frames, reset to
:: false after group compilation
::Y 0 1
:: Set Y offset modifier for group compilation by 1 every 1 frames, reset to
:: false after group compilation
::G
:: Compile Group


   F -> G -> L -> A (implicit)
   |    |    |   
   |_R  |_R  |_R 
        |_BF |_BF
        |_V  |_V 
        |_H  |_X
             |_Y
                 
          
FR,GR,GBF,GV,H,LR,LBF,LV,X,Y
F,G,L

*/

// READER

function read_asciimation(str) {
  let lines = str.split('\n');

  // line index
  let li = -1;

  while (li++ < lines.length-1) {
    let tokens = lines[li].split(' ');

    // Read comments
    if (tokens[0] == '::') continue;

    // Read frame repeat setting
    if (tokens[0] == '::FR') {
      set_frame_repeat(tokens[1]);
      continue;
    }
    
    // Read frame compilation request
    if (tokens[0] == '::F') {
      compile(frame,group,frame_repeat);
      continue;
    }
    
    // Read group repeat setting
    if (tokens[0] == '::GR') {
      set_group_repeat(tokens[1]);
      continue;
    }
    
    // Read group back and forth setting
    if (tokens[0] == '::GBF') {
      set_group_back_and_forth();
      continue;
    }
    
    // Read group compilation request
    if (tokens[0] == '::G') {
      //compile_group();
      continue;
    }
    
    // Read layer compilation request
    if (tokens[0] == '::L') {
      //compile_layer();
      continue;
    }

    // Read ASCII art lines
    frame.push(lines[li]);
  }
}

// COMPILATIONS

// Variables

var frame = [];
var group = [];
var layer = [];
var animascii = [];

// Functions

function compile(from,to,repeat,mode) {

  // repeat index
  let ri = -1;
  to.push([])                    
  
  while (ri++ < repeat-1) {
    
    to[to.length-1].push([])                    

    // from index
    let fi = -1;
    while (fi++ < from.length-1) {
      to[to.length-1][to[to.length-1].length-1].push(from[fi]);
    }
  }

  while(from.length)from.pop();
}

// function compile_frame() {
// 
//   // frame repeat index
//   let fri = -1;
// 
//   // insert frame repeat as sub list to group to keep track of it for group
//   // compilation modes
//   group.push([])                    
// 
//   while (fri++ < frame_repeat-1) {
//     
//     // insert frame as sub list to frame repeat to keep track of it for
//     // rebuilding text at animation compilation
//     group[group.length-1].push([])                    
// 
//     // frame index
//     let fi = -1;
//     while (fi++ < frame.length-1) {
//       group[group.length-1][group[group.length-1].length-1].push(frame[fi]);
//     }
//   }
// 
//   // empty frame after compilation
//   while(frame.length)frame.pop();
// }
// 
// function compile_group() {
//   // group repeat index
//   let gri = -1;
// 
//   // insert group repeat as sub list of layer to keep track of it for layer
//   // compilation modes
//   layer.push([])                    
// 
//   while (gri++ < group_repeat-1) {
// 
//     // group index
//     let gi = -1;
//     while (gi++ < group.length-1) {
//       layer[layer.length-1].push(group[gi]);
//     }
//   }
// 
//   // empty group after compilation
//   while(group.length)group.pop();
// }
// 
// function compile_layer() {
//   console.log(String(layer));
// }
// 
// function compile_animascii() {
// }

// SETTINGS

// Variables

var frame_repeat = 1;
var group_repeat = 1;
var group_back_and_forth = false;
var group_reverse = false;
var group_hide = false;
var layer_repeat = 1;
var layer_back_and_forth = true;
var layer_reverse = true;
var move_x_by = 0;
var move_x_every = 0;
var move_y_by = 0;
var move_y_every = 0;

// Functions

function set_frame_repeat(val) {
  frame_repeat = val;
}

function set_group_repeat(val) {
  group_repeat = val;
}

function set_group_back_and_forth() {
  group_back_and_forth = true;
}

function set_group_reverse() {
  group_reverse = true;
}

function set_group_hide() {
  group_hide = true;
}

function set_layer_repeat(val) {
  layer_repeat = val;
}

function set_layer_back_and_forth() {
  layer_back_and_forth = true;
}

function set_layer_reverse() {
  layer_reverse = true;
}

function set_move_x(by,every) {
  move_x_by = by;
  move_x_every = every;
}

function set_move_y(by,every) { 
  move_y_by = by;
  move_y_every = every;
}

/*

::ABF
:: animascii back and forth
::AV
:: animascii reverse
::X 0 1
:: When group frame compile, move layer X offset by 0 every 1 frames
::Y 0 1
:: When group frame compile, Move layer Y offset by 0 every 1 frames
::F 0
:: Frame, repeat 0
::GR 0
:: Group repeat 0
::GBF
:: Group back and forth, reset to false every group compilation
::GV
:: Group reverse, reset to false every group compilation
::GH
:: Hide group, reset to false every group compilation
::G
:: Compile group
::LR
:: Layer repeat
::L
:: Compile layer
*/

//  // GLOBAL
//  
//  var global_back_and_forth = false;
//  var global_reverse = false;
//  var global_repeat_multiplier = 1;
//  var global_every_multiplier = 1;
//  var global_animascii = [];
//  
//  function set_global_repeat_multiplier(repeat_mutliplier) {
//    global_repeat_multiplier = repeat_mutliplier;
//  }
//  
//  function set_global_every_multiplier(every_mutliplier) {
//    global_every_multiplier = every_mutliplier;
//  }
//  
//  function set_global_back_and_forth() {
//    global_back_and_forth = true;
//  }
//  
//  function set_global_reverse() {
//    global_reverse = true;
//  }
//  
//  // LAYER
//  
//  var layer = [];
//  var layer_repeat = 1;
//  var layer_x_offset = 0;
//  var layer_y_offset = 0;
//  var layer_frame_index = 0;
//  
//  function compile_layer() {
//    layer_x_offset = 0;
//    layer_y_offset = 0;
//    group_move_x_by = 0;
//    group_move_x_every = 0;
//    group_move_y_by = 0;
//    group_move_y_every = 0;
//    layer_frame_index = 0;
//  }
//    //layer_repeat = repeat ? repeat : layer_repeat;
//  
//  // GROUP
//  
//  var group = [];
//  var group_repeat = 1;
//  var group_back_and_forth = false;
//  var group_reverse = false;
//  var group_move_x_by = 0;
//  var group_move_x_every = 0;
//  var group_move_y_by = 0;
//  var group_move_y_every = 0;
//  
//  function compile_group() {
//    let gri = group_repeat;
//    while(gri--) {
//      if (group_reverse) {
//        let fri = group.length;                     // frame repeat index
//        while (fri--) {
//          let fi = -1;                              // frame index
//          while (fi++ < group[fri].length-1) {
//            add_offset_to_frame(group[fri][fi]);
//          }
//        }
//        continue;
//      }
//      let fri = -1;                                 // frame repeat index
//      while (fri++ < group.length-1) {
//        let fi = -1;                                // frame index
//        while (fi++ < group[fri].length-1) {
//          add_offset_to_frame(group[fri][fi]);
//        }
//      }
//      if (group_back_and_forth) {
//        let fri = group.length-1;                   // frame repeat index
//        while (fri-- > 1) {
//          let fi = -1;                              // frame index
//          while (fi++ < group[fri].length-1) {
//            add_offset_to_frame(group[fri][fi]);
//          }
//        }
//      }
//    }
//    while(group.length) group.pop();
//    group_back_and_forth = false;
//    group_reverse = false;
//  }
//  //group_repeat = repeat ? repeat : group_repeat;
//  
//  function back_and_forth() {
//    group_back_and_forth = true;
//  }
//  
//  function reverse() {
//    group_reverse = true;
//  }
//  
//  function move_x(by,every) {
//    group_move_x_by = Number(by);
//    group_move_x_every = every;
//  }
//  
//  function move_y(by,every) {
//    group_move_y_by = Number(by);
//    group_move_y_every = every;
//  }
//  
//  // FRAME
//  
//  var frame = [];
//  var frame_repeat = 1;
//  
//  function compile_frame() {
//    if (frame.length) {
//      group.push([]);
//      let fri = frame_repeat * global_repeat_multiplier;
//      while(fri--) {
//        group[group.length-1].push(frame.join('\n'));
//      }
//      while(frame.length) frame.pop();
//    }
//  }
//  //frame_repeat = repeat ? repeat : frame_repeat;
//  
//  function add_offset_to_frame(frame) {
//    let frame_lines = frame.split('\n');
//    let fli = -1;                                   // frame lines index
//    let lyoi = layer_y_offset;                      // layer y offset index
//    while (lyoi--) {
//      frame_lines.unshift('');
//    }
//    while (fli++ < frame_lines.length-1) {
//      let lxoi = layer_x_offset;                    // layer x offset index
//      while (lxoi--) {
//        frame_lines[fli] = ' ' + frame_lines[fli];
//      }
//    }
//    layer.push(frame_lines.join('\n'));
//    layer_frame_index ++;
//    if (Number.isInteger(
//      layer_frame_index/(group_move_x_every*global_every_multiplier)
//    )) {
//      layer_x_offset += group_move_x_by;
//    }
//    if (Number.isInteger(
//      layer_frame_index/(group_move_y_every*global_every_multiplier)
//    )) {
//      console.log(frame);
//      layer_y_offset += group_move_y_by;
//    }
//  }
//  
//  // ASCIIMATION PARSER
//  
//  function asciimation(str) {
//    let lines = str.split('\n');
//    let li = -1;                          // lines index
//    while (li++ < lines.length-1) {
//      let tokens = lines[li].split(' ');
//      if (tokens[0] == '::') continue;    // comment
//      if (tokens[0] == '::ARM') {
//        set_global_repeat_multiplier(tokens[1]);
//        continue;
//      }
//      if (tokens[0] == '::AEM') {
//        set_global_every_multiplier(tokens[1]);
//        continue;
//      }
//      if (tokens[0] == '::ABF') {
//        set_global_back_and_forth();
//        continue;
//      }
//      if (tokens[0] == '::AR') {
//        set_global_reverse();
//        continue;
//      }
//      if (tokens[0] == '::L') {
//        compile_layer(tokens[1]);
//        continue;
//      }
//      if (tokens[0] == '::G') {
//        compile_group(tokens[1]);
//        continue;
//      }
//      if (tokens[0] == '::BF') {
//        back_and_forth();
//        continue;
//      }
//      if (tokens[0] == '::R') {
//        reverse();
//        continue;
//      }
//      if (tokens[0] == '::X') {
//        move_x(tokens[1],tokens[2]);
//        continue;
//      }
//      if (tokens[0] == '::Y') {
//        move_y(tokens[1],tokens[2]);
//        continue;
//      }
//      if (tokens[0] == '::F') {
//        compile_frame(tokens[1]);
//        continue;
//      }
//      frame.push(lines[li]);
//    }
//    console.log(debug());
//    return layer;
//  }
//  
//  function print_asciimation_frame(asciimation,frame_index){
//    frame_index = frame_index > asciimation.length-1 ? frame_index - Math.floor(frame_index/asciimation.length) * asciimation.length : frame_index;
//    return asciimation[frame_index];
//  }
//  
//  // DEBUG
//  
//  function debug() {
//    let str = "";
//    str += "GLOBAL"+'\n';
//    str += "  global_back_and_forth "+global_back_and_forth+'\n';
//    str += "  global_reverse "+global_reverse+'\n';
//    str += "  global_repeat_mutliplier "+global_repeat_multiplier+'\n';
//    str += "  global_every_mutliplier "+global_every_multiplier+'\n';
//    str += "  global_animascii "+global_animascii+'\n';
//    str += "LAYER"+'\n';
//    str += "  layer "+layer+'\n';
//    str += "  layer_repeat "+layer_repeat+'\n';
//    str += "  layer_x_offset "+layer_x_offset+'\n';
//    str += "  layer_y_offset "+layer_y_offset+'\n';
//    str += "  layer_frame_index "+layer_frame_index+'\n';
//    str += "GROUP"+'\n';
//    str += "  group "+group+'\n';
//    str += "  group_repeat "+group_repeat+'\n';
//    str += "  group_back_and_forth "+group_back_and_forth+'\n';
//    str += "  group_reverse "+group_reverse+'\n';
//    str += "  group_move_x_by "+group_move_x_by+'\n';
//    str += "  group_move_x_every "+group_move_x_every+'\n';
//    str += "  group_move_y_by "+group_move_y_by+'\n';
//    str += "  group_move_y_every "+group_move_y_every+'\n';
//    str += "FRAME"+'\n';
//    str += "  frame "+frame+'\n';
//    str += "  frame_repeat "+frame_repeat+'\n';
//    return str;
//  }
//  
//  console.log("ASCIImation_parser");

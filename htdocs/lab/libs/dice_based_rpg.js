// need roll_engine.js 
// need pre_log.js 

// STATS

// Player

var player_name, player_base_speed, player_base_attack, player_base_defense, player_base_vitality, player_base_intellect, player_speed, player_attack, player_defense, player_vitality, player_intellect, player_hit_points, player_mana_points, player_speed_roll, player_attack_roll, player_skills;

// Enemy

var enemy_name, enemy_speed, enemy_attack, enemy_defense, enemy_vitality, enemy_hit_points, enemy_speed_roll, enemy_attack_roll;

// NEW GAME

function new_game() {
  player_name             = "Doe";
  player_base_speed       = 1;
  player_base_attack      = 1;
  player_base_defense     = 1;
  player_base_vitality    = 1;
  player_base_intellect   = 1;
  player_speed            = 1;
  player_attack           = 1;
  player_defense          = 1;
  player_vitality         = 1;
  player_intellect        = 1;
  player_hit_points       = player_base_vitality*6;
  player_mana_points      = player_base_intellect*6;
  player_attack_roll      = false;
  player_speed_roll       = false;
  player_skills           = [];
  enemy_name              = "Slime";
  enemy_speed             = 1;
  enemy_attack            = 1;
  enemy_defense           = 1;
  enemy_vitality          = 1;
  enemy_hit_points        = enemy_vitality*6;
  enemy_speed_roll        = false;
  enemy_attack_roll       = false;
}

menu = [
  [round()],
];

// ROUND(S)

function round(){
  clear();
  if (player_dead()) {
    game_over();
    log(game_over_log());
    new_game();
    return;
  }
  if (enemy_dead()) {
    fight_over();
    log(fight_over_log());
    new_game();
    return;
  }
  if (!player_speed_roll) {
    speed_round();
    log(speed_round_log());
  } else if (!player_attack_roll) {
    attack_round();
    log(attack_round_log());
  } else {
    end_round();
    log(end_round_log());
    new_round();
  }
}

function speed_round() {
  player_speed_roll = roll(player_speed);  
  enemy_speed_roll = roll(enemy_speed);
  if (no_attack()) {
    player_attack_roll = true;
    enemy_attack_roll = true;
  } 
}

function attack_round() {
  player_attack_roll = roll(
    total(player_speed_roll) > total(enemy_speed_roll) ? player_attack : player_defense
  );  
  enemy_attack_roll = roll(
    total(player_speed_roll) > total(enemy_speed_roll) ? enemy_defense : enemy_attack
  );  
}

function end_round() {
  if (player_attack_sucess()) {
    enemy_hit_points -= total(player_attack_roll) - total(enemy_attack_roll);
  } 
  if (enemy_attack_sucess()) { 
    player_hit_points -= total(enemy_attack_roll) - total(player_attack_roll);
  }
}

function new_round() {
  player_speed_roll = false;
  enemy_speed_roll = false;
  player_attack_roll = false;
  enemy_attack_roll = false;
}

// OVER

function game_over() {
}

function fight_over() {
}

// SITUATIONS

function player_dead() {
  return player_hit_points <= 0 ? true : false;
}

function enemy_dead() {
  return enemy_hit_points <= 0 ? true : false;
}

function player_faster() {
  return total(player_speed_roll) > total(enemy_speed_roll) ? true : false;
}

function enemy_faster() {
  return total(player_speed_roll) < total(enemy_speed_roll) ? true : false;
}

function no_attack() {
  return total(player_speed_roll) == total(enemy_speed_roll) ? true : false;
}

function player_attack_sucess() {
  return player_faster() && total(player_attack_roll) > total(enemy_attack_roll) ? true : false;
}

function enemy_attack_sucess() {
  return enemy_faster() && total(player_attack_roll) < total(enemy_attack_roll) ? true : false;
}

function player_dodge() {
  return enemy_faster() && total(player_attack_roll) == total(enemy_attack_roll) ? true : false;
}

function enemy_dodge() {
  return player_faster() && total(player_attack_roll) == total(enemy_attack_roll) ? true : false;
}

function player_parry() {
  return enemy_faster() && total(enemy_attack_roll) < total(player_attack_roll) ? true : false;
}

function enemy_parry() {
  return player_faster() && total(enemy_attack_roll) > total(player_attack_roll) ? true : false;
}

// LOGS

function speed_round_log() {
  let str = "";
  str += "NEW ROUND\n";
  str += '\n';
  str += player_name + " (player) HP " + player_hit_points + '/' + player_vitality * 6 + " MP " + player_mana_points + '/' + player_intellect*6+'\n';
  str += '\n';
  str += "Speed roll " + player_speed_roll + '\n';
  str += "Total " + total(player_speed_roll) + '\n';
  str += '\n';
  str += "VERSUS\n";
  str += '\n';
  str += enemy_name + " (enemy) HP "+enemy_hit_points+'/'+enemy_vitality*6 + '\n';
  str += '\n';
  str += "Speed roll " + enemy_speed_roll + '\n';
  str += "Total " + total(enemy_speed_roll) + '\n';
  str += '\n';
  str += '---\n';
  str += '\n';
  str += no_attack() ? "No one will attack." : 
     ( player_faster() ? player_name : enemy_name ) + " will attack next !\n";
  return str;
}

function attack_round_log() {
  let str = "";
  str +=
    (player_faster() ? player_name : enemy_name) + " ATTACK\n";
  str += '\n';
  str += player_name + " (player) HP "+player_hit_points+'/'+player_vitality*6+" MP "+player_mana_points+'/'+player_intellect*6 +'\n';
  str += '\n';
  str +=
    player_faster() ? "Attack" : "Defense" + " roll " + player_attack_roll + '\n';
  str += "Total " + total(player_attack_roll) + '\n';
  str += '\n';
  str += 'VERSUS\n';
  str += '\n';
  str += enemy_name + " (enemy) HP " + enemy_hit_points + '/' + enemy_vitality * 6 + "\n";
  str += '\n';
  str +=
    player_faster() ? "Defense" : "Attack" +
    " roll " + enemy_speed_roll + '\n';
  str += "Total " + total(enemy_attack_roll) + '\n';
  str += '\n';
  str += '---\n';
  str += '\n';
  str +=
    player_attack_sucess() ? player_name + " will hit " + enemy_name  + " for " + ( total(player_attack_roll) - total(enemy_attack_roll)  ) + " damages !" :
    enemy_attack_sucess()  ? enemy_name  + " will hit " + player_name + " for " + ( total(enemy_attack_roll)  - total(player_attack_roll) ) + " damages !" :
    player_dodge()         ? player_name + " will dodge !"                                                                                                 : 
    enemy_dodge()          ? enemy_name  + " will dodge !"                                                                                                 : 
    player_parry()         ? player_name + " will parry !"                                                                                                 : 
                             enemy_name  + " will parry !\n"                                                                                               ;
  return str;
}

function end_round_log() {
  let str = "";
  if (no_attack()) {
    str += "NO ONE ATTACK\n";
  }
  if (player_attack_sucess()) {
    str += player_name + " HIT " + enemy_name + " FOR " + (total(player_attack_roll) - total(enemy_attack_roll)) + " DAMAGES\n";
  }
  if (enemy_attack_sucess()) {
    str += enemy_name + " HIT " + player_name + " FOR " + (total(enemy_attack_roll) - total(player_attack_roll))+ " DAMAGES\n";
  }
  if (player_parry()) {
    str += player_name + " PARRY THE ATTACK\n";
  }
  if (enemy_parry()) {
    str += player_name + " PARRY THE ATTACK\n";
  }
  if (player_dodge()) {
    str += player_name + " DODGE THE ATTACK\n";
  }
  if (enemy_dodge()) {
    str += player_name + " DODGE THE ATTACK\n";
  }
  str += '\n';
  str += player_name + " (player) HP "+player_hit_points+'/'+player_vitality*6+" MP "+player_mana_points+'/'+player_intellect*6 + '\n';
  str += '\n';
  str += enemy_name + " (enemy) HP " + enemy_hit_points + '/' + enemy_vitality * 6 + '\n';
  return str;
}

function game_over_log() {
  let str = "";
  str += "GAME OVER\n";
  str += '\n';
  str += player_name + " was slain by " + enemy_name + '\n';
  return str;
}

function fight_over_log() {
  let str = "";
  str += player_name + " slayed " + enemy_name;
  return str;
}

/*
  Simple compilation

  [ A B C ] -> [ [ A B C ] ] ]
  |_ From      | |_ Frame 
               |___ Repeats

  Repeat compilation

  [ A B C ] -> [ [ A B C ] [ A B C ] ] 
  |_ From      | |_ Frame  |_ Frame 
               |___ Repeats

  Reverse compilation

  [ A B C ] -> [ [ C B A ] ]
  |_ From      | |_ Frame 
               |___ Repeats
  
  Back and forth compilation

  [ A B C ] -> [ [ A B C B ] ]
  |_ From      | |_ Frame 
               |___ Repeats
  
  Reverse + back and forth compilation

  [ A B C ] -> [ [ C B A B ] ] ]
  |_ From      | |_ Frame 
               |___ Repeats

  Repeat + back and forth compilation

  [ A B C ] -> [ [ A B C B ] [ A B C B ] ]
  |_ From      | |_ Frame    |_ Frame 
               |___ Repeats

*/

function compile(from,repeat,reverse,back_and_forth) {
  let repeats = [];

  // repeat index
  let ri = -1;
  while(ri++<repeat-1){

    let frame = [];
    
    // from index
    let fi = -1;
    while(fi++<from.length-1)[
      
      frame.push(from[fi]);

    }

    repeats.push(frame);
  }

  return repeats;  
}

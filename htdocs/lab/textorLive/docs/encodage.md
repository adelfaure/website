## Encodage des tuiles

La manière dont Textor encode les tuiles est similaire à [l'encodage de
caractères](https://fr.wikipedia.org/wiki/Codage_des_caract%C3%A8res). Le
principe est d'assigner une valeur numérique à une combinaison de forme, de
couleur de forme et de couleur de fond de tuile.

![Exemple de tuile avec comme forme 'A', noir comme couleur de fond et blanc comme couleur de forme](../docs/assets/images/A_blanc_sur_noir.png)

Les valeurs de tuiles sont rangées par forme puis par combinaisons de couleur
de forme et de couleur de fond. Le système d'encodage de Textor varie selon le
nombre de formes et de couleurs de tuiles définies par les classes `TileSet` et
`Palette` selon la formule suivante:

```
valeur de tuile = (
  forme de tuile
  + nombre de tuiles * couleur de fond
  + nombre de tuiles * nombre de couleurs * couleur de forme
)
```

### Exemple

Prenons l'exemple de l'encodage obtenu par le `TileSet` `ascii8x16` combiné avec la `Palette` `base1`:

![Ordre des tuiles encodées par la combinaison `ascii8x16` - `base1` disposé par lignes de 32 tuiles.](../docs/assets/images/ascii8x16_et_base1.png)

`ascii8x16` contient 128 formes de tuiles correspondant à l'encodage
[ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange).

Les 32 premiers caractères ASCII correspondent à des [caractères de
contrôle](https://fr.wikipedia.org/wiki/Caract%C3%A8re_de_contr%C3%B4le). Ces
derniers n'étant pas censés être affichés ils n'ont pas de forme et se
confondent ici avec la forme d'un espace. Ils sont dans l'image ces bandes
vides au début de chaque changement de couleur.

`base1` contient 2 couleurs, le noir et le blanc, correspondant à une [profondeur de couleur 1-bit](https://en.wikipedia.org/wiki/Color_depth#1-bit_color).

La combinaison des deux produit `128*2*2` tuiles pour un total de 512.

On peux observer dans l'image la manière dont sont rangées les tuiles par le
système d'encodage. D'abord 128 tuiles noir sur fond noir, le noir étant la
première couleur de la palette, produisant un rectangle noir sur 4 lignes.
Ensuite 128 tuiles noir sur fond blanc, le blanc étant la deuxième couleur de
la palette. Puis 128 tuiles blanc sur fond noir, les combinaisons de couleur de
fond étant épuisées c'est la couleur de forme qui change tandis le fond
retourne à la valeur initiale. Viens enfin 128 tuiles en blanc sur fond blanc
marquant la fin des combinaisons possibles et produisant un rectangle blanc sur
4 lignes.

Pour retrouver la valeur d'un 'A' noir sur fond blanc nous pouvons reprendre la
formule citée plus haut.

```
valeur de tuile = (
  65
  + 128 * 1
  + 128 * 1 * 0
)
```

65 étant la valeur de la forme de la tuile, la 65e forme sur les 128 stockées
dans `ascii8x16`.

128 étant le total des formes de tuiles.

1 étant la valeur de couleur de fond de la tuile, correspondant au blanc,
deuxième valeur de couleur sur les 2 stockées dans `base1`. (Il faut compter à
partir de 0)

1 étant le total de couleurs stockées dans `base1`.

0 étant la valeur de couleur de forme de la tuile, correspondant au noir,
première valeur de couleur sur les 2 stockées dans `base1`.

Le résultat étant 193 nous pouvons retrouver dans l'image le 'A' noir sur fond
blanc en 193e position. Il est possible de vérifier en s'aidant des lignes, 'A'
noir sur fond blanc étant à la 6e ligne après la tuile '@' soit `6*32+1 = 192`.

Changer le `tileSet` ou la `palette` fait varier l'encodage selon le nombre de
tuiles et de couleurs.

Conservons le `TileSet` `ascii8x16` mais changeons de `Palette` avec `base3`
aura comme resultat l'encodage suivant:

![Ordre des tuiles encodées par la combinaison `ascii8x16` - `base3` disposé par lignes de 64 tuiles.](../docs/assets/images/ascii8x16_et_base3.png)

`base3` contient 4 couleurs, correspondant à une [profondeur de couleur
3-bit](https://en.wikipedia.org/wiki/Color_depth#3-bit_color).

Combiné avec `ascii8x16` ont obtient `128*4*4` tuiles pour un total de 2048.

Avec ce changement de `Palette` la valeur 193 ne tombe plus sur 'A' noir sur
fond blanc mais sur 'A' noir sur fond gris sombre, le nouveau calcul étant:

```
valeur de tuile = (
  65
  + 128 * 3
  + 128 * 3 * 0
)
```

Le premier 3 étant la valeur de couleur de fond de la tuile, correspondant au
blanc, 4e valeur de couleur sur les 4 stockées dans `base3`.

Le deuxième 3 étant le nombre total de couleur stockées dans `base3`.

Le résultat étant 449 nous pouvons retrouver dans l'image le 'A' noir sur fond
blanc en 449e position. Il est possible de vérifier en s'aidant des lignes, 'A'
noir sur fond blanc étant à la 7e ligne après la tuile '@' soit `7*64+1 = 449`.

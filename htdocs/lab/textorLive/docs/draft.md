<!--
## Frame program

Instructions within a `frame program` will be performed at each frame of the
animation process handled by an `Animation` class instance.

A `frame program` used as construction argument of a new `Animation` instance
will be automatically feeded by a package of information through a
`frameContext` argument.

The `Animation` instance will read from `frame program` output, awaiting for a
`true` or `false` value for performing next frame or stopping animation
process.

`EndInterface` instance will feed `frame program` context with user input
values and values useful for painting pixels on screen.

Theses values includes `EndInterface` `BitmapInterface` instance value along
its width and height, user mouse down, mouse x and y coordinates and user
pressed keys.

`Animation` instance will feed `frame program` context with current time, frame
and frame rate values.

```mjs

// frameContext object automatically build and sended as frame program argument
const frameContext = {
  t,    // animation time
  f,    // animation frame
  fps,  // animation frame rate
  m,    // mouse down
  x,    // mouse x
  y,    // mouse y
  k,    // keys
  w,    // end interface width
  h,    // end interface width
  o     // end interface bitmapInterface
}

// Frame program function structure
function frame(frameContext){

  // Must return 1 or 0 value, requesting next frame execution or stopping
  // animation process
  return foo;
}
```

## Fragment program

Inspired how
<a href="https://play.ertdfgcvb.xyz/">Anreas Gysin's Play</a> 
use GLSL programming style for processing text, Textor way of processing tiles
is close to how GLSL fragment shaders process pixels.  Where fragment shaders
calculate color of pixels individually through a program returning four float
values (float4), Textor fragments return single integer value corresponding to
colored tile code.

A `fragment program` used as `process` method argument of `TiledBitmap` or
`Matrix` instance will be automatically feeded by a package of information
through a `fragmentContext` argument.

`Matrix` instance will read from `fragment program` output for defining and/or
updating its stored data.

`Tileset` instance will feed `fragment program` context with availables tiles
number, tile width and tile width.


### Fragment context

```mjs
fragmentContext.i;      // tile data index
fragmentContext.x;      // tile x coordinate
fragmentContext.y;      // tile y coordinate
fragmentContext.w;      // tile width
fragmentContext.h;      // tile height
fragmentContext.di;     // tile data at index
fragmentContext.d;      // tiles data
fragmentContext.t;      // tileSet length
fragmentContext.tw;     // tileSet tile width
fragmentContext.th;     // tileSet tile height
fragmentContext.c;      // palette length
fragmentContext.fg();   // fg
fragmentContext.bg();   // bg
fragmentContext.gfg();  // get fg 
fragmentContext.gbg();  // get bg 
fragmentContext.gt();   // get tile
```

-->
## Encodage des tuiles

La manière dont Textor encode les tuiles est similaire à [l'encodage de
caractères](https://fr.wikipedia.org/wiki/Codage_des_caract%C3%A8res). Le
principe est d'assigner une valeur numérique à une combinaison de forme, de
couleur de forme et de couleur de fond de tuile.

![Exemple de tuile avec comme forme 'A', noir comme couleur de fond et blanc comme couleur de forme](./assets/images/A_blanc_sur_noir.png)

Les valeurs de tuiles sont rangées par forme puis par combinaisons de couleur
de forme et de couleur de fond. Le système d'encodage de Textor varie selon le
nombre de formes et de couleurs de tuiles définies par les classes `TileSet` et
`Palette` selon la formule suivante:

```
valeur de tuile = (
  forme de tuile
  + nombre de tuiles * couleur de fond
  + nombre de tuiles * nombre de couleurs * couleur de forme
)
```

### Exemple

Prenons l'exemple de l'encodage obtenu par le `TileSet` `ascii8x16` combiné avec la `Palette` `base1`:

![Ordre des tuiles encodées par la combinaison `ascii8x16` - `base1` disposé par lignes de 32 tuiles.](./assets/images/ascii8x16_et_base1.png)

`ascii8x16` contient 128 formes de tuiles correspondant à l'encodage
[ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange).

Les 32 premiers caractères ASCII correspondent à des [caractères de
contrôle](https://fr.wikipedia.org/wiki/Caract%C3%A8re_de_contr%C3%B4le). Ces
derniers n'étant pas censés être affichés ils n'ont pas de forme et se
confondent ici avec la forme d'un espace. Ils sont dans l'image ces bandes
vides au début de chaque changement de couleur.

`base1` contient 2 couleurs, le noir et le blanc, correspondant à une [profondeur de couleur 1-bit](https://en.wikipedia.org/wiki/Color_depth#1-bit_color).

La combinaison des deux produit `128*2*2` tuiles pour un total de 512.

On peux observer dans l'image la manière dont sont rangées les tuiles par le
système d'encodage. D'abord 128 tuiles noir sur fond noir, le noir étant la
première couleur de la palette, produisant un rectangle noir sur 4 lignes.
Ensuite 128 tuiles noir sur fond blanc, le blanc étant la deuxième couleur de
la palette. Puis 128 tuiles blanc sur fond noir, les combinaisons de couleur de
fond étant épuisées c'est la couleur de forme qui change tandis le fond
retourne à la valeur initiale. Viens enfin 128 tuiles en blanc sur fond blanc
marquant la fin des combinaisons possibles et produisant un rectangle blanc sur
4 lignes.

Pour retrouver la valeur d'un 'A' noir sur fond blanc nous pouvons reprendre la
formule citée plus haut.

```
valeur de tuile = (
  65
  + 128 * 1
  + 128 * 1 * 0
)
```

65 étant la valeur de la forme de la tuile, la 65e forme sur les 128 stockées
dans `ascii8x16`.

128 étant le total des formes de tuiles.

1 étant la valeur de couleur de fond de la tuile, correspondant au blanc,
deuxième valeur de couleur sur les 2 stockées dans `base1`. (Il faut compter à
partir de 0)

1 étant le total de couleurs stockées dans `base1`.

0 étant la valeur de couleur de forme de la tuile, correspondant au noir,
première valeur de couleur sur les 2 stockées dans `base1`.

Le résultat étant 193 nous pouvons retrouver dans l'image le 'A' noir sur fond
blanc en 193e position. Il est possible de vérifier en s'aidant des lignes, 'A'
noir sur fond blanc étant à la 6e ligne après la tuile '@' soit `6*32+1 = 192`.

Changer le `tileSet` ou la `palette` fait varier l'encodage selon le nombre de
tuiles et de couleurs.

Conservons le `TileSet` `ascii8x16` mais changeons de `Palette` avec `base3`
aura comme resultat l'encodage suivant:

![Ordre des tuiles encodées par la combinaison `ascii8x16` - `base3` disposé par lignes de 64 tuiles.](./assets/images/ascii8x16_et_base3.png)

`base3` contient 4 couleurs, correspondant à une [profondeur de couleur
3-bit](https://en.wikipedia.org/wiki/Color_depth#3-bit_color).

Combiné avec `ascii8x16` ont obtient `128*4*4` tuiles pour un total de 2048.

Avec ce changement de `Palette` la valeur 193 ne tombe plus sur 'A' noir sur
fond blanc mais sur 'A' noir sur fond gris sombre, le nouveau calcul étant:

```
valeur de tuile = (
  65
  + 128 * 3
  + 128 * 3 * 0
)
```

Le premier 3 étant la valeur de couleur de fond de la tuile, correspondant au
blanc, 4e valeur de couleur sur les 4 stockées dans `base3`.

Le deuxième 3 étant le nombre total de couleur stockées dans `base3`.

Le résultat étant 449 nous pouvons retrouver dans l'image le 'A' noir sur fond
blanc en 449e position. Il est possible de vérifier en s'aidant des lignes, 'A'
noir sur fond blanc étant à la 7e ligne après la tuile '@' soit `7*64+1 = 449`.

## Afficher une tuile en particulier

<!--
## Exemple

Prenons l'exemple d'un 'A' blanc sur fond noir.


Ceux-ci sont importés depuis les module `tileSets.mjs` et `palettes.mjs` du
dossier `textor/assets` et sont attribués à l'objet `fragmentContext` donné en
argument de la fonction `fragment` (voir [fragments](#fragments)).


Également par défaut, le sketch appliquera blanc comme couleur de forme et noir
comme couleur de fond quelque soit la valeur de tuile.

Dans ce contexte, pour obtenir un 'A' blanc sur fond noir il suffit de
renseigner la valeur ASCII correspondant au 'A' soit 65, via l'export d'une
fonction `fragment`.

```
export function fragment(){
  return 65;
} 
```

![A blanc sur fond noir]("./assets/A_blanc_sur_fond_noir.png)

Pour choisir librement la valeur colorée d'une tuile il faut exporter la
variable `freeColor` assignée à une valeur positive.

```
export const freeColor = 1;
export function fragment(){
  return 65;
} 
```

Dans ce cas la valeur 65 ne sera pas modifiée et apparaitra noir sur fond noir,
noir étant la première couleur de la palette.

![A noir sur fond noir]("./assets/A_noir_sur_fond_noir.png)

Pour obtenir à nouveau un 'A' blanc sur fond noir il faut trouver la valeur
d'encodage correspondante selon le calcul donné le diagram plus haut

```
Valeur de tuile = (
  Valeur de forme
  + Valeur de couleur de fond
  + Valeur de couleur de forme
)
```

soit

```
v = (
  v%t
  + [v/(t*c)]%c
  + [v/t]%c
)
```

Nous savons que la forme 'A' correspond à 65, que `ascii8x16` contient 128 tuiles et que `base1` contient 2 couleurs.

```
v = (
  65
  + [v/(128*2)]%2
  + [v/128]%2
)
```

par défaut = 65
ascii8x16 + base1 = 449 soit 65 + fg(1)(384) + bg(0)(0)
semigraphRegular8x8 + base1 = 1313 soit 65 + fg(1) + bg(0)(0)
ascii8x16 + base3 = 449 soit 65 + fg(1)(384) + bg(0)(0)
semigraphRegular8x8 + base3 = 1313 soit 65 + fg(1) + bg(0)(0)


### Tile sets

```mjs
TileSets.semigraphRegular8x8;
TileSets.semigraphBold8x8;
TileSets.semigraphHeavy8x8;
TileSets.ascii4x8;
TileSets.ascii6x12;
TileSets.ascii8x16;
TileSets.liblab4x8;
```

### Palettes

```mjs
Palettes.base1;
Palettes.base3;
Palettes.base4;
```

-->

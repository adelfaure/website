# Fonction `fragment`

Cette fonction s'execute à chacune des tuiles qui composent l'affichage
définissant le visuel en sortie de programme.

Pour être appliquée en sortie une fonction `fragment` doit être définie
précédée de l'instruction `export` et doit retourner une valeur décimale via
l'instruction `return`.

Les valeurs envoyées de cette manière seront interprétées en formes et en
couleurs sur chaque tuile de la grille qui compose l'affichage (voir
[encodage](#encodage)). L'envoie de la valeur 35 par exemple remplira la grille
d'affichage d'un '#'.

```mjs
export function fragment(){ 
  return 35;
}
```

# Objet contexte d'un `fragment`

Lorsqu'une fonction `fragment` est traitée, un objet contenant des propriétées
spécifiques à son contexte d'execution lui est envoyé en premier argument.
Pour récupérer cet objet il suffit d'attribuer un nom de variable en premier
argument lors de la définition de la function `fragment` décrite précédement.

```mjs
export function fragment(context){
  var text = context.write(Object.keys(context));
  return text ? text : 32;
}
```

Cet objet contient différents types d'informations et d'outils; il apporte des
données sur la position de la tuile en cours de traitement, sur les dimensions
et le contenu de la grille d'affichage en sortie ainsi sur le set de tuiles
(voir [`tileSet`](#tileset)) et la palette de couleur (voir
[`palette`](#palette)) en cours d'utilisation.  Il contient également des
fonctions se servant du système d'encodage pour modifier les couleurs des
tuiles ou convertir du texte ou des valeurs hexadecimales en valeur décimales.

Il contient par exemple la méthode `write` permettant d'écrire du texte en
sortie.

```mjs
export function fragment(fragmentContext){
  var text = fragmentContext.write("Sortie du fragment");
  return text ? text : 35;
}
```

À noté qu'il est possible de nommer librement l'argument et de créer des
raccourcis vers ses propriétées.

```mjs
export function fragment(ctx){
  var {write,y} = ctx;
  var text = write("Ligne "+y,0,y);
  return text ? text : 32;
}
```

# Index des propritées du contexte de `fragment`

- [`fragmentContext.write(str,x,y,fg,bg)`](#fragmentcontext.writestrxyfgbg)
- [`fragmentContext.i`](#fragmentcontext.i)
- [`fragmentContext.x`](#fragmentcontext.x)
- [`fragmentContext.y`](#fragmentcontext.y)
- [`fragmentContext.w`](#fragmentcontext.w)
- [`fragmentContext.h`](#fragmentcontext.h)
- [`fragmentContext.di`](#fragmentcontext.di)
- [`fragmentContext.d`](#fragmentcontext.d)
- [`fragmentContext.t`](#fragmentcontext.t)
- [`fragmentContext.tw`](#fragmentcontext.tw)
- [`fragmentContext.th`](#fragmentcontext.th)
- [`fragmentContext.c`](#fragmentcontext.c)
- [`fragmentContext.cp(str)`](#fragmentcontext.cpstr)
- [`fragmentContext.hex(str)`](#fragmentcontext.hexstr)
- [`fragmentContext.fg(value)`](#fragmentcontext.fgvalue)
- [`fragmentContext.bg(value)`](#fragmentcontext.bgvalue)
- [`fragmentContext.gt(value)`](#fragmentcontext.gtvalue)
- [`fragmentContext.gfg(value)`](#fragmentcontext.gfgvalue)
- [`fragmentContext.gbg(value)`](#fragmentcontext.gbgvalue)

### `fragmentContext.write(str,x,y,fg,bg)`

Converti une chaine de caractères en valeur decimales et les placent aux
coordonées indiquées en appliquant une la couleur de forme et de fond.  Les
arguments de coordonées (x,y) et de couleur (fg,bg) sont optionnels. Par défaut
le text se place en 0,0 sans couleur définie.

```mjs
export function fragment(fragmentContext) {
  var text = fragmentContext.write("hello world");
  return text ? text : 32;
}
```

### `fragmentContext.i`

`index`

Renvoie la valeur d'index de la tuile en cours de traitement.

```mjs
export function fragment(fragmentContext) {
  var {i,y,write} = fragmentContext;
  var text = fragmentContext.write(
    i+" index du debut de ligne "+y,0,y
  );
  return text ? text : 32;
}
```

### `fragmentContext.x`

`x`

Renvoie la position sur l'axe horizontal de la tuile en cours de traitement.

```mjs
export function fragment(fragmentContext) {
  return !(fragmentContext.x%4) ? 35 : 32;
}
```

### `fragmentContext.y`

`y`

Renvoie la position sur l'axe vertical de la tuile en cours de traitement.

```mjs
export function fragment(fragmentContext) {
  return !(fragmentContext.y%4) ? 35 : 32;
}
```

### `fragmentContext.w`

`width`

Renvoie le nombre de colonnes de l'affichage.

```mjs
export function fragment(fragmentContext) {
  var {x,w} = fragmentContext;
  return x > w/2 ? 35 : 32;
}
```

### `fragmentContext.h`

`height`

Renvoie le nombre de lignes de l'affichage.

```mjs
export function fragment(fragmentContext) {
  var {y,h} = fragmentContext;
  return y > h/2 ? 35 : 32;
}
```

### `fragmentContext.di`

`data index`

Renvoie la valeur actuelle de la tuile en cours de traitement.

```mjs
export function fragment(fragmentContext) {
  return fragmentContext.di + 1;
}
```

## `fragmentContext.d`

`data`

Renvoie la liste du total des valeurs de tuiles.

```mjs
export function fragment(fragmentContext) {
  var {i,di,d} = fragmentContext;
  return (
    !i
    ? di
    : d[i-1]+1
  );
}
```

## `fragmentContext.t`

`tiles`

Renvoie la taille du set de tuiles.

```mjs
export function fragment(fragmentContext) {
  var {i,t} = fragmentContext;
  return i < t ? i : 32;
}
```

## `fragmentContext.tw`

`tile width`

Renvoie la largeur en pixel des tuiles du set de tuiles.

## `fragmentContext.th`

`tile height`

Renvoie la hauteur en pixel des tuiles du set de tuiles.

## `fragmentContext.c`

`colors`

Renvoie le nombre de couleurs de la palette.

```mjs
import {base4} from "textor/assets/palettes.mjs"
var {random,floor} = Math;
export var palette = base4;
export var freeColor = 1;
export function fragment(fragmentContext) {
  var {t,c} = fragmentContext;
  return floor(random()*t*c*c);
}
```

## `fragmentContext.cp(str)`

`code point`

Converti une chaine de caractères en valeur decimales. Renvoi une valeur seule
ou une liste selon si la chaine comporte un ou plusieurs caractères.

```mjs
export function fragment(fragmentContext) {
  return fragmentContext.cp('A');
}
```

## `fragmentContext.hex(str)`

`hexadecimal`

Converti une chaine de caractères composées de valeurs hexadecimales séparées
un esapce en valeur decimales. Renvoi une valeur seule ou une liste selon si la
chaine comporte une ou plusieurs valeurs hexadecimales.

À savoir qu'il est possible d'afficher ou cacher un tableau des valeurs
hexadecimales correspondant aux tuiles du `tileSet` en cours d'utilisation via
la touche `F1`.

```mjs
export function fragment(fragmentContext) {
  return fragmentContext.hex('41');
}
```

## `fragmentContext.fg(value)`

`foreground`

Retourne l'index d'encodage de la couleur de forme correspondant à la valeur
donnée. Cet index, ajouté à une valeur de forme de tuile permet de définir sa
couleur de forme.

```mjs
import {base3} from "textor/assets/palettes.mjs"
export var palette = base3;
export var freeColor = 1;
export function fragment(fragmentContext) {
  var {cp,fg} = fragmentContext;
  return cp('A')+fg(1);
}
```

## `fragmentContext.bg(value)`

`background`

Retourne l'index d'encodage de la couleur de fond correspondant à la valeur
donnée. Cet index, ajouté à une valeur de forme de tuile permet de définir sa
couleur de fond.

```mjs
import {base3} from "textor/assets/palettes.mjs"
export var palette = base3;
export var freeColor = 1;
export function fragment(fragmentContext) {
  var {cp,fg,bg} = fragmentContext;
  return cp('A')+fg(1)+bg(2);
}
```

## `fragmentContext.gt(value)`

`get tile`

Retourne la forme de tuile correspondante à la valeur donnée. 

```mjs
var {random,floor} = Math;
var tile = floor(random()*128);
export function fragment(fragmentContext) {
  var {write,d,gt,di,cp} = fragmentContext;
  var text = write(
    gt(d[0])+" est la couleur de forme choisie au hasard",
    1,
    0
  );
  return (
    text
    ? text
    : !di 
    ? tile
    : di
  );
}
```

## `fragmentContext.gfg(value)`

`get foreground`

Retourne la couleur de forme correspondant à la valeur donnée. 

```mjs
import {base4} from "textor/assets/palettes.mjs"
var {random,floor} = Math;
export var palette = base4;
export var freeColor = 1;
var color = floor(random()*palette.length);
export function fragment(fragmentContext) {
  var {write,di,d,c,cp,fg,gfg} = fragmentContext;
  var text = write(
    gfg(d[0])+" est la couleur de forme choisie au hasard",
    1,
    0
  );
  return (
    text
    ? text+fg(c-1)
    : !di 
    ? cp('A') + fg(color)
    : di
  );
}
```

## `fragmentContext.gbg(value)`

`get background`

Retourne la couleur de fond correspondant à la valeur donnée. 

```mjs
import {base4} from "textor/assets/palettes.mjs"
var {random,floor} = Math;
export var palette = base4;
export var freeColor = 1;
var color = floor(random()*palette.length);
export function fragment(fragmentContext) {
  var {write,di,d,c,cp,fg,gfg} = fragmentContext;
  var text = write(
    gbg(d[0])+" est la couleur de forme choisie au hasard",
    1,
    0
  );
  return (
    text
    ? text+bg(c-1)
    : !di 
    ? cp('A') + bg(color)
    : di
  );
}
```


<!--
  i;    // tile data index
  x;    // tile x coordinate
  y;    // tile y coordinate
  w;    // matrix width
  h;    // matrix height
  di;   // tile data at index
  d;    // tiles data
  t;    // tileSet length
  tw;   // tileSet tile width
  th;   // tileSet tile height
  c;    // palette length
  cp = (input) => codePoint(input); // codePoint shortcut function
  hex = (input) => codePoint(input); // codePoint shortcut function
  fg = (value) => this.foreground(value);  // fg shortcut function
  bg = (value) => this.background(value);  // bg shortcut function
  gfg = (value) => this.getForeground(value);  // fg shortcut function
  gbg = (value) => this.getBackground(value);  // bg shortcut function
  gt = (value) => this.getTile(value);  // bg shortcut function
  write = (str,x,y) => write(str,x,y); // write shortcut

```mjs
export function fragment(
  fragmentContext
) {
  var {write} = fragmentContext;
  var text = write("Sortie du fragment");
  return text ? text : 32;
}
```
-->

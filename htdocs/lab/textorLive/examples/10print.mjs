/**
@title 10print
@year 2025
@author adl
@desc Remake of the famous Commodore program 
10 PRINT CHR$(205.5+RND(1)); : GOTO 10 
see https://10print.org
*/

import {semigraphBold8x8} from "textor/assets/tileSets.mjs";
export var tileSet = semigraphBold8x8;

export function frame(){
  return 0;
}

export function fragment(fragmentContext){
  var {cp} = fragmentContext;
  return (
    Math.random() > 0.5 
    ? cp('/')
    : cp('\\')
  )
}

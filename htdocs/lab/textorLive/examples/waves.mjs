import {hex} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";

export var scale = 2;
export var fps = 30;
export var tileSet = semigraphRegular8x8;

var {floor,sin} = Math;

var density = hex(
  "81 141 14a 153 20"
);

var denstityRes = 4;

function wave(f,x,width,height){
  return floor(sin((f+x)/width)*height)
}

export function fragment(fragCtx,frameCtx) {
  var {x,y,w,h} = fragCtx;
  var {f} = frameCtx;
  var index = floor(
    y > floor(h/2+10) + wave(f,x,10,4) - wave(f/2,x,9,4)
    ? 0
    : y > floor(h/2) + wave(f,x,5,3) - wave(f/2,x,9,3)
    ? 1
    : y > floor(h/2-10) + wave(f,x,3,3) - wave(f/3,x,9,5)
    ? 2
    : y > floor(h/2-20) + wave(f,x,6,4) - wave(f/3,x,9,6)
    ? 3
    : 4
  );
  return density[index];
}

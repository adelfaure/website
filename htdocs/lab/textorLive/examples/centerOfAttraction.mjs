/**
@title Center of attraction
@year 2025
@author adl
@desc Distance from a point
*/

import {hex} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";

export var scale = 3;
export var fps = 30;
export var tileSet = semigraphRegular8x8;

var {floor} = Math;

var density = hex(
  "81 141 14a 153 20"
);

function dist(a,b){
  return a > b ? a - b : b - a;
}

var denstityRes = 4;

export function fragment(fragCtx,frameCtx) {
  var {x,y,w,h} = fragCtx;
  var pX = w/2;
  var pY = h/2;
  var motion = frameCtx.f/2;
  var index = floor((
    dist(x,pX)
    + dist(y,pY)
    + motion
  )/denstityRes)%density.length;
  return density[index];
}

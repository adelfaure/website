/**
@title Center of attraction
@year 2025
@author adl
@desc Distance from a point
*/

import {hex} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";

export var scale = 3;
export var fps = 30;
export var tileSet = semigraphRegular8x8;

var {floor} = Math;

var density = hex(
  "81 141 14a 153 20"
);

function dist(a,b){
  return a > b ? a - b : b - a;
}

var pX = 0;
var pY = 0;
var motion = 0;
var denstityRes = 4;

export function frame(ctx){
  var {f,mx,my} = ctx;
  pX = mx/tileSet.height;
  pY = my/tileSet.width;
  motion = f/2;
  return 1;
}

export function fragment(ctx) {
  var {x,y} = ctx;
  var index = floor((
    dist(x,pX)
    + dist(y,pY)
    + motion
  )/denstityRes)%density.length;
  return density[index];
}

/**
@title Crossing rows
@year 2025
@author adl
@desc Textor adaptation of ertdfgcvb, "Coordinates: x, y"
*/

// Import hex to decimal converter
import {hex} from "textor/textor.mjs";
// Import semigraphic font
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";

// Define pixel size
export var scale = 3;
// Define framerate
export var fps = 12;
// Define tile set
export var tileSet = semigraphRegular8x8;

// Create a gradient from semigraphic characters
// Use "Show tileset" command for looking at the corresponding
// hexadecimal positions
var density = hex("81 160 141 168 14a 170 153 158 3a 20");

// Inspired by GLSL fragment shader, fragment function will be
// executed on each output cell, defining a moving picture made
// of tiles
export function fragment(
  // TODO continue commenting
  fragCtx,
  frameCtx
) {

  // Shortcuts for width, x and y axis position
  var {w,x,y} = fragCtx;
  // Shortcut for current frame
  var {f} = frameCtx;

  // -1 for even lines, 1 for odd lines
  var sign = y % 2 * 2 - 1;
  var baseIndex = (w + y + x * sign + f);
  var patternScale = 2;
  var index = (
    Math.floor(baseIndex/patternScale)
    % density.length
  );

  return density[index];
}

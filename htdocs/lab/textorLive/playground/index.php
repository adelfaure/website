<!doctype html>
<html>
<head>
<title>Textor playground</title>
<style>
  @font-face{
    font-family:"jgs";
    src:url("../src/Jgs-SingleLine-regular.ttf");
  }
  @media(max-width:1279px){
    main,
    body {
      flex-direction:column-reverse;
    }
    main > * {
      width:100%;
    }
    #textor {
      height:100Vh;
    }
  }
  @media(min-width:1280px){
    aside {
      height:100vh;
      overflow-y:auto;
      max-width:720px;
      min-width:720px;
    }
    #menu{
      max-width:240px;
      min-width:240px;
    }
    body{
      overflow:hidden;
    }
    #content{
      max-width:720px;
      min-width:720px;
    }
    main {
      max-width:calc(100vw - 240px);
    }
    #textor{
      flex:1;
      height:100vh;
    }
  }
  @media(min-width:1600px){
    aside {
      display:flex;
      flex-direction:row-reverse;
      max-width:960px;
      min-width:960px;
    }
    #menu{
      max-width:240px;
      min-width:240px;
    }
    body{
      overflow:hidden;
    }
    #content{
      max-width:720px;
      min-width:720px;
    }
    main {
      max-width:calc(100vw - 240px);
    }
    aside,
    #menu,
    #content,
    #textor{
      flex:1;
      height:100vh;
    }
  }
  * {
    font-family:"jgs"!important;
    font-size:18px;
    line-height:24px;
    box-sizing:border-box;
  }
  body{
    margin:0;
  }
  body,
  main{
    display: flex;
  }
  main {
      flex: 1;
  }
  #menu{
    overflow-y:auto;
    background-color:#FFF;
    color:#6c6c6c;
    padding-left:12px;
  }
  #menu * {
    margin:0;
    font-weight:normal;
  }
  #content{
    overflow:auto;
    background-color:#fff;
  }
  #textor{
    overflow: hidden;
    background-color: #000;
  }
  #menu,
  #content{
    padding:12px;
  }
  #content{
    padding-top:0px;
  }
  #codeMirror{
    padding-top:12px;
  }
  .cm-content{
    padding:0 !important;
  }
  .cm-gutters {
    border: none !important;
    background-color: white !important;
  }
  .cm-focused,
  *:focus{
    outline:none !important;
  }
  #errorLog{
    width:100%;
    background-color:#C04040;
    color:white;
    font-weight: bold;
    padding-left:5px;
    padding-right:5px;
    position:fixed;
    top: 0px;
    z-index:100;
  }

  /* button */

  .button {
    color:#00C000;
  }
  .button:hover{
    background-color:#F0F0F0;
    cursor:pointer;
  }
  
  /* link */

  .link {
    color:#0000FF;
  }
  .link:hover{
    background-color:#F0F0F0;
    cursor:pointer;
  }

  /* doc */
  #fragmentDoc,
  #encodingDoc,
  #back{
    display:none;
  }
  #back{
    position:sticky;
    top: 0px;
    padding-top:12px;
    background-color:#fff;
  }

  /* article */

article img{
  max-width:100%;
}

article > *:not(h1,h2,h3) {
  margin-left: 18px;
}

article pre, 
article code{
  color:#404040;
  background-color:#F0F0F0;
}

article pre {
  padding:18px;
}

article a {
  color:#00F;
  text-decoration:none;
}

article h1,
article h2,
article h3
{
  margin-top:-24px;
  padding-top:48px;
}

article a code{
  color:#00F;
}

article ul{
  padding:0;
}

article li{
  list-style:none;
}
</style>
<script type="importmap">
	{
		"imports": {
			"codemirror/": "../codemirror-esm/esm/",
      "textor/": "../../../tools/textor/src/modules/"
		}
	}
</script>
<script async type="module">
	import {basicSetup, EditorView} from "codemirror/codemirror/dist/index.js";
	import {javascript } from "codemirror/lang-javascript/dist/index.js";
  import {indentWithTab} from "codemirror/commands/dist/index.js";
  import {keymap} from "codemirror/view/dist/index.js";
  import {initLive, updateLive, log, showTileSetMap} from "textor/utils/live.mjs";

  const autoSave = localStorage.getItem("textorPlaygroundAutosave");
	const editor = new EditorView({
		doc: (
      autoSave == null 
      ? (
      "export function fragment(fragmentContext){\n"
      +"  const text = fragmentContext.write('Welcome to Textor playground');\n"
      +"  return text ? text : 32;\n"
      +"}\n"
      )
      : autoSave
    ),
		extensions: [basicSetup, javascript(), keymap.of([indentWithTab])],
		parent: document.getElementById("codeMirror"),
	});
  
  document.getElementById("errorLogWrapper").appendChild(log);


  function getLiveSource(){
    const text = editor.state.doc.toString();
    localStorage.setItem("textorPlaygroundAutosave",text);
    return text;
  }

  initLive(document.getElementById("textor"),getLiveSource);

  document.getElementById("update").addEventListener(
    "click",
    ()=>updateLive(getLiveSource)
  );
  
  document.getElementById("showTileSet").addEventListener(
    "click",
    ()=>{showTileSetMap[0] = !showTileSetMap[0]}
  );
  
  document.getElementById("back").addEventListener(
    "click",
    ()=>{
      document.getElementById("codeMirror").style.display = "block";
      document.getElementById("back").style.display = "none";
      document.getElementById("fragmentDoc").style.display = "none";
      document.getElementById("encodingDoc").style.display = "none";
    }
  );
  
  document.getElementById("fragmentDocLink").addEventListener(
    "click",
    ()=>{
      document.getElementById("codeMirror").style.display = "none";
      document.getElementById("encodingDoc").style.display = "none";
      document.getElementById("back").style.display = "block";
      document.getElementById("fragmentDoc").style.display = "block";
    }
  );
  
  document.getElementById("encodingDocLink").addEventListener(
    "click",
    ()=>{
      document.getElementById("codeMirror").style.display = "none";
      document.getElementById("fragmentDoc").style.display = "none";
      document.getElementById("back").style.display = "block";
      document.getElementById("encodingDoc").style.display = "block";
    }
  );

  document.getElementById("crossingRowsLink").addEventListener(
    "click",
    async ()=>{
      const response = await fetch("../examples/crossingRows.mjs");
      const text = await response.text();
      editor.dispatch({
        changes: {from: 0, to: editor.state.doc.length, insert: text}
      })
    }
  );
  
  document.getElementById("centerOfAttractionLink").addEventListener(
    "click",
    async ()=>{
      const response = await fetch("../examples/centerOfAttraction.mjs");
      const text = await response.text();
      editor.dispatch({
        changes: {from: 0, to: editor.state.doc.length, insert: text}
      })
    }
  );
 
  function example(name){ 
    document.getElementById(name+"Link").addEventListener(
      "click",
      async ()=>{
        const response = await fetch("../examples/"+name+".mjs");
        const text = await response.text();
        editor.dispatch({
          changes: {from: 0, to: editor.state.doc.length, insert: text}
        })
      }
    );
  }

  example("helloWorld");
  example("10print");
  example("waves");

  document.addEventListener("keydown",function(e){
    console.log(e.key);
    if (e.key == "F2"){
      updateLive(getLiveSource);
    }
    if (e.key == "F1"){
      showTileSetMap[0] = !showTileSetMap[0];
    }
  });

</script>
</head>
<body>
<aside>
  <div class="col" id="content">
    <div id="codeMirror"></div>
    <div id="back"><a class="link" id="showCode" >Retour</a></div>
    <article id="fragmentDoc">
    <?php include "../docs/fragment.html" ?>
    </article>
    <article id="encodingDoc">
    <?php include "../docs/encodage.html" ?>
    </article>
  </div>
  <div class="col" id="menu">
<pre>
Textor playground     
  About

Commands 
  <a class="button" id="showTileSet" >Show tileSet</a> ....... F1
  <a class="button" id="update" >Update</a> ............. F2
  <!--<a class="button" id="find" >Find / Replace</a> ..... F3
  <a class="button" id="fullscreen" >Fullscreen</a> ......... F4
-->
Documentation
  <a class="link" id="fragmentDocLink" >Fragment</a>
  Frame
  TileSets
  Palettes
  Imports
  Exports
  Math tips
  <a class="link" id="encodingDocLink" >Encoding</a>

Examples
  <a class="link" id="helloWorldLink">Hello world</a>
  <a class="link" id="10printLink">10print</a>
  <a class="link" id="crossingRowsLink">Crossing Rows</a>
  <a class="link" id="centerOfAttractionLink">Center of Attraction</a>
  <a class="link" id="wavesLink">Waves</a>
</pre>
  </div>
</aside>
<main>
  <div class="col" id="textor">
    <div id="errorLogWrapper"></div>
  </div>
</main>
</body>
</html>

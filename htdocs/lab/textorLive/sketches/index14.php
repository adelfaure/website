<!doctype html>
<html>
  <head>
    <?php include "./importMap.html"; ?>
    <link rel="stylesheet" href="style.css"/>
    <script type="module">
      const url = "<?=(!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];?>:9001/p/live14/export/txt";
      <?php include "./script.mjs"; ?>
    </script>
  </head>
  <body>
  <div id="errorLogWrapper"></div>
  </body>
</html>

console.log(url);

import {initLive, updateLive, log, showTileSetMap} from "textor/utils/live.mjs";

document.getElementById("errorLogWrapper").appendChild(log);

async function getLiveSource(){
  const response = await fetch(url);
  const text = await response.text();
  return text;
}

initLive(document.body,getLiveSource);

document.addEventListener("keydown",function(e){
  if (e.key == "Enter") {
    updateLive(getLiveSource);
  }
});

import {
  BitmapInterface,
  TiledBitmap,
  EndInterface,
  Animation
} from "textor/textor.mjs";

import {ascii8x16} from "textor/assets/tileSets.mjs";
import {base1} from "textor/assets/palettes.mjs";

const encoder = new TextEncoder();
var freeColor = 0;
var tileSet = ascii8x16;
var palette = base1;
var scale = 2;
var liveText = "";
var fragment = ()=>0;
var frame = ()=>1;
var bitmapInterface = 0; 
var tiledBitmap = 0;
var endInterface = 0;

async function externalLive(url){
  try {
    const response = await fetch(url);
    const text = await response.text();
    if (text == liveText) return 0;
    liveText = text;
    const blob = new Blob([encoder.encode(text)],{"type":"text/javascript"});
    const module = URL.createObjectURL(blob);
    return await import(module).then((module) => {
      return module;
    });
  } catch (e){
    console.log(e);
    return 0;
  }
}

async function internalLive(sourceFunction){
  try {
    const text = sourceFunction();
    const blob = new Blob([encoder.encode(text)],{"type":"text/javascript"});
    const module = URL.createObjectURL(blob);
    return await import(module).then((module) => {
      return module;
    });
  } catch (e){
    console.log(e);
    return 0;
  }
}

async function liveFrame(frameContext,source){
  const {f,k,t} = frameContext;
  var frameValue = 1;
  try {
    if (!(f%60)) {
      const module = await (
        typeof source == "string"
        ? externalLive(source)
        : initLive(source)
      );
      if (module) {
        fragment = module.fragment != undefined ? module.fragment : fragment;
        frame = module.frame != undefined ? module.frame : frame;
        const newScale = module.scale != undefined ? module.scale : scale;
        const newPalette = module.palette != undefined ? module.palette : palette;
        const newTileSet = module.tileSet != undefined ? module.tileSet : tileSet;
        freeColor = module.freeColor != undefined ? module.freeColor : freeColor;
        var updateTiledBitmap = false;
        if (newScale != scale) {
          scale = newScale;
          bitmapInterface.scale(window.innerWidth,window.innerHeight,scale);
          updateTiledBitmap = true;
        }
        if (newTileSet != tileSet) {
          tileSet = newTileSet;
          updateTiledBitmap = true;
        }
        if (newPalette != palette) {
          palette = newPalette;
          updateTiledBitmap = true;
        }
        if (updateTiledBitmap) {
          tiledBitmap = new TiledBitmap(
            tileSet,
            palette,
            bitmapInterface
          );
        }
      }
    }
    frameValue = frame(frameContext);
    if (freeColor) {
      tiledBitmap.process(fragment);
    } else {
      tiledBitmap.process((frag,frame)=>{
        const {fg,c,t} = frag;
        return fragment(frag,frame)%t+fg(c-2);
      });
    }
    tiledBitmap.update();
  } catch (e){
    console.log(e);
  }
  return (frameValue && !k[232]);
}

export function initLive(parent,source){
  const rect = parent.getBoundingClientRect();
  console.log(parent);
  bitmapInterface = new BitmapInterface(
    rect.width,
    rect.height,
    scale
  );
  tiledBitmap = new TiledBitmap(
    tileSet,
    palette,
    bitmapInterface
  );
  endInterface = new EndInterface(
    bitmapInterface,
    parent
  );
  new Animation(60,
    async function(frameContext){
      return liveFrame(frameContext,source);
    }
  );
}

console.log("convert.js");

function convert(value,from,to) {
  return (
    from == 'km' && to == 'm' ? value * 1000 :
    from == 'm' && to == 'km' ? value / 1000 :
    from == 'h' && to == 'min' ? value * 60 :
    from == 'h' && to == 's' ? value * 60 * 60 :
    from == 'h' && to == 'ms' ? value * 60 * 60 * 1000 :
    from == 'min' && to == 'h' ? value / 60 :
    from == 'min' && to == 's' ? value * 60 :
    from == 'min' && to == 'ms' ? value * 60 * 1000 :
    from == 's' && to == 'h' ? value / 60 /60 :
    from == 's' && to == 'min' ? value / 60 :
    from == 's' && to == 'ms' ? value * 1000 :
    from == 'ms' && to == 'h' ? value / 60 / 60 / 1000 :
    from == 'ms' && to == 'min' ? value / 1000 / 60 :
    from == 'ms' && to == 's' ? value / 1000 :
    from == 'kg' && to == 'N' ? value * 9.80665 :
    from == 'N' && to == 'kg' ? value / 9.80665 :
    from == 'km/h' && to == 'm/s' ? value * 1000 / 60 / 60 :
    from == 'm/s' && to == 'km/h' ? value * 60 * 60 / 1000 :
    from == 'km/h' && to == 'm/ms' ? value * 1000 / 60 / 60 / 1000 :
    from == 'm/ms' && to == 'km/h' ? value * 1000 * 60 * 60 / 1000 :
    value
  );
}

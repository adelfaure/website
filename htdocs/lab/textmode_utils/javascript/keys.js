console.log("keys.js");

let keydown = false;
const keys = {};
let keys_functions = {};
let keyup_functions = [];
let keydown_functions = [];
let on_keydown_functions = [];
let last_key = false;

function keys_process(frame){
  for (var i in keys) {
    if (keys[i] && keys_functions[i]) {
      keys_functions[i]();
    }
  }
  console.log(keydown);
  if (keydown && last_key) {
    for (var i = 0; i < on_keydown_functions.length; i++) {
      on_keydown_functions[i](frame,last_key);
    }
  }
}

document.addEventListener("keydown",function(e){
  keys[e.key] = true;
  keydown = true;
  if (e.key.length == 1){
    last_key = e.key;
  }
  for (var i = 0; i < keydown_functions.length; i++) {
    keydown_functions[i](e.key);
  }
});

document.addEventListener("keyup",function(e){
  keys[e.key] = false;
  if (e.key.length == 1){
    last_key = false;
  }
  let all_up = true;
  for (var i in keys) {
    if (keys[i]) all_up = false;
  }
  if (all_up) {
    keydown = false;
    for (var i = 0; i < keyup_functions.length; i++) {
      keyup_functions[i](e.key);
    }
  }
});

console.log(keys);

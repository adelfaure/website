//let animation_start_x = 0;
//let animation_start_y = 0;
//let animation_w = 2;
//let animation_h = 2;
//let animation_frame_w = 1;
//let animation_frame_h = 1;
//let animation_show_x = 0;
//let animation_show_y = 0;

var animation_frames = [];
var frame_index = 0;

function init_animation(){
  if (!UF(matrix.height()/matrix.width()-1)) return;
  animation_frames = [];
  let frame_y = -1;
  while(frame_y++ < UF(matrix.height()/matrix.width())-1) {
    frame = new Matrix(matrix.width(),matrix.width(),3);
    UA(
      0,
      frame_y*matrix.width(),
      matrix.width(),matrix.width(),
      function(x,y) {
        let glyph = matrix.get(x,y);
        if(!glyph) return;
        frame.set(x,y-frame_y*matrix.width(),glyph);
      }
    );
    animation_frames.push(frame);
  }
}

start_animation_event(10,function(){
  init_animation();
});
start_animation_event(5,function(){
  frame_index = frame_index < animation_frames.length-1 ? frame_index + 1 : 0;
});
start_animation_event(1,function(){
  if (!UF(matrix.height()/matrix.width()-1) ||
      !animation_frames[frame_index]) return;
  UA(0,0,animation_frames[frame_index].width(),animation_frames[frame_index].height(),
    function(x,y) {
      let glyph = animation_frames[frame_index].get(x,y);
      if (!glyph) return;
      overlay_matrix.set(overlay_matrix.width()-animation_frames[frame_index].width()+x,1+y,glyph);
    }
  );
});

function _base64ToArrayBuffer(base64) {
  var binary_string = window.atob(base64.split(',')[1]);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

let scale = 3;
let char_scale = 8;
var matrix = new Matrix(32,32,3);
matrix.fullset([0,4,0]);
var back_memory = [new Uint16Array(matrix.buffer)];
var forth_memory = [];

var screen = new Screen(window.innerWidth,window.innerHeight,scale);
var w = UF(window.innerWidth/scale/char_scale);
var h = UF(window.innerHeight/scale/char_scale);
var screen_matrix = new Matrix(UF(w),UF(h),3);
screen_matrix.fullset([-1,-1,-1]);
var overlay_matrix = new Matrix(UF(w),UF(h),3);

function infos() {
  overlay_matrix.clear();
  overlay_matrix.rect(0,0,w,1,[0,fg_color,bg_color]);
  overlay_matrix.rect(0,h-1,w,1,[0,fg_color,bg_color]);
  overlay_matrix.write(
    0,0,
    //("Default semigraphics  ")+
    (fill_mode ? 'FILL [ctrl+f]  ':'')+
    (hide_mode ? 'HIDE [ctrl+h]  ':'')+
    (size_step ? 'SIZED STEP [ctrl+z]  ':'')+
    (write_mode ? 'WRITE [ctrl+e]  ':'')+
    '',fg_color,bg_color
  );
  let glyph = matrix.get(x_cursor-x_cam,y_cursor-y_cam);
  overlay_matrix.write(
    0,h-1,
    "FPS "+real_fps+
    '  cursor '+(x_cursor-x_cam)+','+(y_cursor-y_cam)+','+(w_cursor)+','+(h_cursor)+
    '  data '+glyph+' '+patterns_names[glyph[0]]+
    '  camera '+x_cam+','+y_cam+
    '  canvas '+matrix.width()+','+matrix.height()+
    '',fg_color,bg_color
  );
  if (!write_mode) {
    let glyph_index = (keys["Shift"] || capslock) ? 40 : 0;
    for (var y = 0; y < 4; y++) {
      for (var x = 0; x < 10; x++) {
        overlay_matrix.write(x,y*2+1,azerty[glyph_index],fg_color,bg_color);
        overlay_matrix.set(
          x,y*2+2,
          [
            keys["Tab"] ? 244 : semigraphic[glyph_index+80*sheet_index],
            keys["Tab"] ? (glyph_index < colors.length-1 ? glyph_index : 0) : last_typed == glyph_index ? bg_color : fg_color,
            last_typed == glyph_index ? fg_color : bg_color,
          ]
        );
        glyph_index++;
      }
    }
  }
  if (!hide_mode) {
    if (keys["Control"]) {
      overlay_matrix.write(0,13, 
        "[control] + \n"+
        "  [0-9] change sheet \n"+
        "  [a] appply color \n"+
        "  [arrow keys] move camera \n"+
        "  [c] copy \n"+
        "  [d] randomize \n"+
        "  [e] write mode \n"+
        "  [f] fill mode \n"+
        "  [h] hide keys \n"+
        "  [i] zoom in \n"+
        "  [k] pick color \n"+
        "  [l] load file \n"+
        "  [o] zoom out \n"+
        "  [p] paste \n"+
        "  [r] redo \n"+
        "  [s] save file \n"+
        "  [shift] + [arrows keys] change scene size \n"+
        "  [u] undo \n"+
        "  [v] reverse \n"+
        "  [x] execute \n"+
        "  [z] sized step mode \n"+
      '',fg_color,bg_color);
    }
    overlay_matrix.write(
      0,9,
      "[arrows keys] move cursor \n"+
      "[shift] + [arrows keys] change cursor size \n"+
      "[control] show control functions \n"+
      "[tab] + [key] change color ",
      fg_color,bg_color
    );
  }
}

let glob = {};
let x_cursor = 0;
let y_cursor = 0;
let w_cursor = 1;
let h_cursor = 1;
let clipboard = false;
let x_cam = 0;
let y_cam = 0;
let fg_color = 4;
let bg_color = 0;
let last_typed = -1;
let capslock = false;
let size_step = false;
let hide_mode = false;
let write_mode = false;
let fill_mode = false;
let sheet_index = 0;

let semigraphic = [
  101,103,104,105,274,275,254,255,256,257,
    0,282,107,106,273,276,253,108,109,258,
  265,266,267,268,280,277,264,111,110,259,
  272,271,270,269,279,278,263,262,261,260,

  119,115,120,125,126,249,248,250,242,243,
  114,116,112,124,123,246,102,245,241,240,
  118,113,117,121,122,252,247,251,324,325,
  238,237,319,175,239,244,326,321,323,322,

  292,293,294,295,318,312,173,170,171,174,
  291,131,132,296,317,311,161,169,162,166,
  302,134,133,297,178,179,168,167,165,164,
  301,300,299,298,177,176,130,172,160,163,

  287,286,288,144,141,145,150,148,152,154,
  284,129,283,140,137,138,147,146,128,157,
  290,285,289,143,139,142,151,149,159,158,
  303,304,305,306,135,136,281,156,153,155,

  336,337,338,339,357,356,229,225,227,226,
  335,184,185,340,358,355,216,224,217,221,
  346,187,186,341,233,234,223,222,220,219,
  345,344,343,342,232,231,183,228,215,218,

  331,330,332,199,196,200,205,203,207,209,
  328,182,327,195,192,193,202,201,181,212,
  334,329,333,198,194,197,206,204,214,213,
  347,348,349,350,190,191,320,211,208,210,

    0,378,  0,381,364,382,385,368,386,  0,
  377,  0,375,363,235,365,367,236,369,  0,
    0,376,  0,380,366,379,384,370,383,  0,
  392,414,410,406,391,413,409,405,  0,  0,

  389,372,390,  0,402,  0,  0,398,  0,  0,
  371,230,373,401,127,399,397,180,395,  0,
  388,374,387,  0,400,  0,  0,396,  0,  0,
  394,412,408,404,393,411,407,403,188,189,
];
//si = 236;
//while(si++ < 362){
//  semigraphic.push(si);
//}



document.addEventListener("keyup",function(e) {
})
document.addEventListener("keydown",function(e) {

  e.preventDefault();
  e.stopPropagation();
  if (e.key == "CapsLock") capslock = !capslock;
  if (keys["Control"] && keys["Shift"]) {
    let matrix_content = new Matrix(matrix.width(),matrix.height(),3,matrix.buffer);
    if (e.key == "ArrowRight") {
      matrix = new Matrix(matrix.width()+(size_step ? w_cursor : 1),matrix.height(),3);
      matrix.fullset([0,4,0]);
      UA(0,0,matrix_content.width(),matrix_content.height(),
        function(x,y) {
          let glyph = matrix_content.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "ArrowLeft") {
      matrix = new Matrix(matrix.width()-(size_step ? w_cursor : 1),matrix.height(),3);
      matrix.fullset([0,4,0]);
      UA(0,0,matrix_content.width(),matrix_content.height(),
        function(x,y) {
          let glyph = matrix_content.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "ArrowDown") {
      matrix = new Matrix(matrix.width(),matrix.height()+(size_step ? h_cursor : 1),3);
      matrix.fullset([0,4,0]);
      UA(0,0,matrix_content.width(),matrix_content.height(),
        function(x,y) {
          let glyph = matrix_content.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "ArrowUp") {
      matrix = new Matrix(matrix.width(),matrix.height()-(size_step ? h_cursor : 1),3);
      matrix.fullset([0,4,0]);
      UA(0,0,matrix_content.width(),matrix_content.height(),
        function(x,y) {
          let glyph = matrix_content.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
  } else if (keys["Control"]) {
    let number_index = numbers.indexOf(e.key);
    if (number_index > -1) {
      sheet_index = number_index;
    }
    if (e.key == "ArrowRight") {
      x_cam += (size_step ? w_cursor : 1);
    }
    if (e.key == "ArrowLeft") {
      x_cam -= (size_step ? w_cursor : 1);
    }
    if (e.key == "ArrowDown") {
      y_cam += (size_step ? h_cursor : 1);
    }
    if (e.key == "ArrowUp") {
      y_cam -= (size_step ? h_cursor :1);
    }
    if (e.key == "x") {
      let to_eval = '';
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if(patterns_names[glyph[0]].length > 1 ||
             !glyph) return;
          to_eval += patterns_names[glyph[0]];
        }
      );
      try {
        eval(to_eval);
      } catch(e) {
      }
    }
    if (e.key == 's') {
      let blob = new Blob([matrix.buffer]);
      let blobUrl = URL.createObjectURL(blob); 
      let downloadLink = document.createElement('a');
      downloadLink.setAttribute('download', "text.tile");
      downloadLink.setAttribute('href', blobUrl);
      downloadLink.click();
    }
    if (e.key == 'l') {
      let input = document.createElement("input");
      input.type = "file";
      input.addEventListener("change",function(e){
        let file = input.files[0];
        let fr = new FileReader();
        fr.addEventListener("load", function(){
          matrix.buffer = new Uint16Array(_base64ToArrayBuffer(this.result));
          forth_memory = [];
          back_memory.push(new Uint16Array(matrix.buffer));
        }, false);
        fr.readAsDataURL(file); 
      });
      input.click();
    }
    if (e.key == "a") {
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,[glyph[0],fg_color,bg_color]);
        }
      );
      x_cursor += (size_step ? w_cursor : 1);
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "c") {
      clipboard = new Matrix(w_cursor,h_cursor,3);
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          clipboard.set(x-x_cursor+x_cam,y-y_cursor+y_cam,glyph);
        }
      );
    }
    if (e.key == "!") {
      var to_mirror = [];
      while (to_mirror.length < h_cursor) {
        to_mirror.push([]);
      }
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          to_mirror[y-y_cursor+y_cam].push(glyph);
        }
      );
      to_mirror.reverse();
      to_mirror = to_mirror.flat();
      
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          let to_mirror_glyph = to_mirror.shift();
          let to_mirror_name = patterns_names[to_mirror_glyph[0]];
          if (to_mirror_name.includes("top")) {
            to_mirror_name = to_mirror_name.replace("top","bottom");
          } else if (to_mirror_name.includes("bottom")) {
            to_mirror_name = to_mirror_name.replace("bottom","top");
          }
          if (to_mirror_name.includes("ascent")) {
            to_mirror_name = to_mirror_name.replace("ascent","descent");
          } else if (to_mirror_name.includes("descent")) {
            to_mirror_name = to_mirror_name.replace("descent","ascent");
          }
          to_mirror_glyph[0] = patterns_names.indexOf(to_mirror_name);
          matrix.set(x,y,to_mirror_glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "m") {
      var to_mirror = [];
      while (to_mirror.length < h_cursor) {
        to_mirror.push([]);
      }
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          to_mirror[y-y_cursor+y_cam].push(glyph);
        }
      );
      for (var i = 0; i < to_mirror.length; i++) {
        to_mirror[i].reverse();
      }
      to_mirror = to_mirror.flat();
      
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          let to_mirror_glyph = to_mirror.shift();
          let to_mirror_name = patterns_names[to_mirror_glyph[0]];
          if (to_mirror_name.includes("left")) {
            to_mirror_name = to_mirror_name.replace("left","right");
          }else if (to_mirror_name.includes("right")) {
            to_mirror_name = to_mirror_name.replace("right","left");
          }
          if (to_mirror_name.includes("ascent")) {
            to_mirror_name = to_mirror_name.replace("ascent","descent");
          } else if (to_mirror_name.includes("descent")) {
            to_mirror_name = to_mirror_name.replace("descent","ascent");
          }
          to_mirror_glyph[0] = patterns_names.indexOf(to_mirror_name);
          matrix.set(x,y,to_mirror_glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "d") {
      let to_randomize = [];
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          to_randomize.push(glyph);
        }
      );
      to_randomize = US(to_randomize);
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          let glyph = matrix.get(x,y);
          if (!glyph) return;
          matrix.set(x,y,to_randomize.pop());
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == 'f') {
      fill_mode = !fill_mode;
    }
    if (e.key == "h") {
      hide_mode = !hide_mode;
    }
    if (e.key == "p") {
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          if(!clipboard) return;
          let glyph = clipboard.get(x-x_cursor+x_cam,y-y_cursor+y_cam);
          if (!glyph) return;
          matrix.set(x,y,glyph);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
    }
    if (e.key == "e") {
      write_mode = !write_mode;
    }
    if (e.key == "z") {
      size_step = !size_step;
    }
    if (e.key == "k") {
      let glyph = matrix.get(x_cursor-x_cam,y_cursor-y_cam);
      if (!glyph) return;
      fg_color = glyph[1];
      bg_color = glyph[2];
    }
    if (e.key == "u" && back_memory.length > 1) {
      forth_memory.push(back_memory.pop());
      matrix.buffer = new Uint16Array(back_memory[back_memory.length-1]);
    }
    if (e.key == "r" && forth_memory.length) {
      back_memory.push(forth_memory.pop());
      matrix.buffer = new Uint16Array(back_memory[back_memory.length-1]);
    }
    if (e.key == "v") {
      let new_fg = bg_color;
      bg_color = fg_color;
      fg_color = new_fg;
    }
    if (e.key == "i") {
      let x_offset = UF((window.innerWidth/(scale+1)/char_scale - w)/2);
      let y_offset = UF((window.innerHeight/(scale+1)/char_scale - h)/2);
      x_cam += x_offset;
      y_cam += y_offset;
      x_cursor += x_offset;
      y_cursor += y_offset;
      scale++;
      document.body.removeChild(screen.canvas);
      screen = new Screen(window.innerWidth,window.innerHeight,scale);
      w = UF(window.innerWidth/scale/char_scale);
      h = UF(window.innerHeight/scale/char_scale);
      screen_matrix = new Matrix(UF(w),UF(h),3);
      overlay_matrix = new Matrix(UF(w),UF(h),3);
    }
    if (e.key == "o") {
      if (scale <= 1) return;
      let x_offset = UF((window.innerWidth/(scale-1)/char_scale - w)/2);
      let y_offset = UF((window.innerHeight/(scale-1)/char_scale - h)/2);
      x_cam += x_offset;
      y_cam += y_offset;
      x_cursor += x_offset;
      y_cursor += y_offset;
      scale--;
      document.body.removeChild(screen.canvas);
      screen = new Screen(window.innerWidth,window.innerHeight,scale);
      w = UF(window.innerWidth/scale/char_scale);
      h = UF(window.innerHeight/scale/char_scale);
      screen_matrix = new Matrix(UF(w),UF(h),3);
      overlay_matrix = new Matrix(UF(w),UF(h),3);
    }
  } else {
    if (keys["Shift"]) {
      if (e.key == "ArrowRight") {
        w_cursor += (size_step ? w_cursor : 1);
      }
      if (e.key == "ArrowLeft") {
        w_cursor -= (size_step ? w_cursor : 1);
        w_cursor = w_cursor <= 0 ? 1 : w_cursor;
      }
      if (e.key == "ArrowDown") {
        h_cursor += (size_step ? h_cursor : 1);
      }
      if (e.key == "ArrowUp") {
        h_cursor -= (size_step ? h_cursor : 1);
        h_cursor = h_cursor <= 0 ? 1 : h_cursor;
      }
    } else {
      if (e.key == "ArrowRight") {
        x_cursor += (size_step ? w_cursor : 1);
      }
      if (e.key == "ArrowLeft") {
        x_cursor -= (size_step ? w_cursor : 1);
      }
      if (e.key == "ArrowDown") {
        y_cursor += (size_step ? h_cursor : 1);
      }
      if (e.key == "ArrowUp") {
        y_cursor -= (size_step ? h_cursor : 1);
      }
    }
    if (e.key == "Backspace") {
      x_cursor -= w_cursor;
      UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
        function(x,y) {
          matrix.set(x,y,[0,fg_color,bg_color]);
        }
      );
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
      last_typed = -1;
    }
    if (e.key == " " && !write_mode) {
      if (fill_mode) {
        let x = x_cursor-x_cam;
        let y = y_cursor-y_cam;
        let from = matrix.get(x,y);
        let to = [0,fg_color,bg_color];
        matrix.floodfill(
          [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
          function (x,y,options) {
            if (
              !options.matrix.get(x,y) ||   
              !options.matrix.compare(x,y,options.from) ||
              options.matrix.compare(x,y,options.to)
            ) return false;
            options.matrix.set(x,y,options.to);
            return true;
          },
          {"matrix":matrix,"from":from,"to":to}
        );
      } else {
        UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
          function(x,y) {
            matrix.set(x,y,[0,fg_color,bg_color]);
          }
        );
      }
      forth_memory = [];
      back_memory.push(new Uint16Array(matrix.buffer));
      last_typed = -1;
      x_cursor += w_cursor;
    }
    let type = write_mode ? patterns_names.indexOf(e.key) : azerty.indexOf(e.key);
    if (type > -1) {
      if (keys["Tab"]) {
        fg_color = type < colors.length ? type : fg_color;
      } else {
        if (fill_mode) {
          let x = x_cursor-x_cam;
          let y = y_cursor-y_cam;
          let from = matrix.get(x,y);
          let to = [write_mode ? type : semigraphic[type+80*sheet_index],fg_color,bg_color];
          matrix.floodfill(
            [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
            function (x,y,options) {
              if (
                !options.matrix.get(x,y) ||   
                !options.matrix.compare(x,y,options.from) ||
                options.matrix.compare(x,y,options.to)
              ) return false;
              options.matrix.set(x,y,options.to);
              return true;
            },
            {"matrix":matrix,"from":from,"to":to}
          );
        } else {
          UA(x_cursor-x_cam,y_cursor-y_cam,w_cursor,h_cursor,
            function(x,y) {
              matrix.set(x,y,[write_mode ? type : semigraphic[type+80*sheet_index],fg_color,bg_color]);
            }
          );
        }
        forth_memory = [];
        back_memory.push(new Uint16Array(matrix.buffer));
        last_typed = type;
        x_cursor += (size_step ? w_cursor : 1);
      }
    }
  }
});


let fps = 60;
start_animation(fps); 

start_animation_event(1,function(frame){
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      if (
        x >= x_cursor && y >= y_cursor &&
        x < x_cursor+w_cursor && y < y_cursor+h_cursor
      ) {
        if (
          x < 0 + x_cam ||
          y < 0 + y_cam ||
          x > matrix.width()-1 + x_cam ||
          y > matrix.height()-1 + y_cam
        ) {
          screen.print_pattern(
            patterns[127],
            colors[3],
            colors[4],
            x*char_scale,
            y*char_scale
          );
          screen_matrix.set(x,y,[127,3,4]);
          continue;
        }
        let glyph = matrix.get(x-x_cam,y-y_cam);
        screen.print_pattern(
          patterns[glyph[0]],
          colors[glyph[2]],
          colors[glyph[1]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,[glyph[0],glyph[2],glyph[1]]);
        continue;
      }
      if (!UCO(overlay_matrix.get(x,y),[0,0,0])){
        let glyph = overlay_matrix.get(x,y);
        if (!patterns[glyph[0]]) continue;
        screen.print_pattern(
          patterns[glyph[0]],
          colors[glyph[1]],
          colors[glyph[2]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,glyph);
        continue;
      }
      if (
        x < 0 + x_cam ||
        y < 0 + y_cam ||
        x > matrix.width()-1 + x_cam ||
        y > matrix.height()-1 + y_cam
      ) {
        if(UCO(screen_matrix.get(x,y),[127,1,2])) continue;
        screen.print_pattern(
          patterns[127],
          colors[1],
          colors[2],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,[127,1,2]);
        continue;
      }
      let printed_glyph = screen_matrix.get(x,y);
      if (!matrix.compare(x-x_cam,y-y_cam,printed_glyph)){
        let glyph = matrix.get(x-x_cam,y-y_cam);
        screen.print_pattern(
          patterns[glyph[0]],
          colors[glyph[1]],
          colors[glyph[2]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,glyph);
      }
    }
  }
  infos();
  screen.print();
});

let real_fps = 0;
start_animation_event(10,function(){
  let time_elapsed = eval_time_elapsed();
  real_fps = UF(60/17*time_elapsed/10);
});



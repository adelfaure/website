console.log("webaudio.js");
// Audio Context
var ctx;

let C  = [16.35, 32.7, 65.41, 130.81, 261.63, 523.25, 1046.5, 2093.0, 4186.01];
let Db = [17.32, 34.65, 69.3, 138.59, 277.18, 554.37, 1108.73, 2217.46, 4434.92];
let D  = [18.35, 36.71, 73.42, 146.83, 293.66, 587.33, 1174.66, 2349.32, 4698.64];
let Eb = [19.45, 38.89, 77.78, 155.56, 311.13, 622.25, 1244.51, 2489.02, 4978.03];
let E  = [20.6, 41.2, 82.41, 164.81, 329.63, 659.26, 1318.51, 2637.02];
let F  = [21.83, 43.65, 87.31, 174.61, 349.23, 698.46, 1396.91, 2793.83];
let Gb = [23.12, 46.25, 92.5, 185.0, 369.99, 739.99, 1479.98, 2959.96];
let G  = [24.5, 49.0, 98.0, 196.0, 392.0, 783.99, 1567.98, 3135.96];
let Ab = [25.96, 51.91, 103.83, 207.65, 415.3, 830.61, 1661.22, 3322.44];
let A  = [27.5, 55.0, 110.0, 220.0, 440.0, 880.0, 1760.0, 3520.0];
let Bb = [29.14, 58.27, 116.54, 233.08, 466.16, 932.33, 1864.66, 3729.31];
let B  = [30.87, 61.74, 123.47, 246.94, 493.88, 987.77, 1975.53, 3951.07];

function play(note,gain,in_dur,mid_dur,out_dur,res,noise,flat,dist) {
  if (!ctx) ctx = new(window.AudioContext || window.webkitAudioContext)();
  if(!gain  || !note || (!in_dur && !mid_dur && !out_dur)) return;
  if(!res   || res   < 0) res = 10000;
  if(!noise || noise < 0) noise = 0;
  if(!flat  || flat  < 0) flat = 1;
  if(!dist  || dist  < 0) dist = 1;
  
  // Gain
  const gainNode = ctx.createGain();
  gainNode.gain.setValueAtTime(in_dur ? 0 : gain, ctx.currentTime);
  if (in_dur) gainNode.gain.linearRampToValueAtTime(gain, ctx.currentTime + in_dur);
  if (mid_dur) gainNode.gain.linearRampToValueAtTime(gain, ctx.currentTime + in_dur + mid_dur);
  if (out_dur) gainNode.gain.linearRampToValueAtTime(0, ctx.currentTime + in_dur + mid_dur + out_dur);

  // Oscillator
  const oscNode = noise_oscillator(ctx,note,in_dur+mid_dur+out_dur,res,noise,flat,dist);

  // Play
  oscNode.connect(gainNode);
  gainNode.connect(ctx.destination);
  oscNode.start(ctx.currentTime);
  oscNode.stop(ctx.currentTime+in_dur+mid_dur+out_dur);
}

let buffers = {};

function noise_oscillator(ctx, note, dur, res, noise, flat, dist){
  let pattern = [];
  let i = 0;
  let buffer_key = 
    String(Math.floor(note *1   ))+'.'+
    String(Math.floor(dur  *100 ))+'.'+
    String(Math.floor(res  *1   ))+'.'+
    String(Math.floor(noise*100 ))+'.'+
    String(Math.floor(flat *1   ))+'.'+
    String(Math.floor(dist *10  ));
  var buffer;
  if (buffers[buffer_key]) {
    buffer = buffers[buffer_key];
  } else {
    while (i < ctx.sampleRate/note) {
      let rdm_val = Math.random() - Math.random();
      let val = (Math.sin(2*Math.PI*note * (i/ctx.sampleRate)) + rdm_val * noise) * dist;
      val = Math.floor(val * res)/res;
      for (var r = 0; r < flat; r++) {
        pattern.push(val > 1 ? 1 : val < -1 ? -1 : val);
        i++;
      }
    }
    buffer = ctx.createBuffer(1, ctx.sampleRate*dur, ctx.sampleRate)
    for (let ch=0; ch<buffer.numberOfChannels; ch++) {
      let samples = buffer.getChannelData(ch);
      for (let s=0; s<buffer.length; s++) {
        samples[s] = pattern[s > pattern.length-1 ? s - Math.floor(s/pattern.length) * pattern.length : s ];
      }
    }
    buffers[buffer_key] = buffer;
  }
  return new AudioBufferSourceNode(ctx, {buffer:buffer})
}

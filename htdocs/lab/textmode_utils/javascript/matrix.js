console.log("matrix.js");
Matrix = function(width,height,depth,init_buffer) {
  this.buffer = new Uint16Array(init_buffer ? init_buffer : (width*height*depth+3));
  this.buffer[this.buffer.length-1] = depth;
  this.buffer[this.buffer.length-2] = height;
  this.buffer[this.buffer.length-3] = width;
}

Matrix.prototype.width = function() {
  return this.buffer[this.buffer.length-3];
}

Matrix.prototype.height = function() {
  return this.buffer[this.buffer.length-2];
}

Matrix.prototype.depth = function() {
  return this.buffer[this.buffer.length-1];
}

Matrix.prototype.infos = function() {
  return [
    this.width(),this.height(),this.depth()
  ];
}

Matrix.prototype.target = function(x,y,di) {
  if (
    x >= this.width() || 
    y >= this.height() ||
    x < 0 || 
    y < 0
  ) return undefined;
  return (y*this.width()+x)*this.depth()+di;
}

Matrix.prototype.compare = function(x,y,to) {
  let di = -1;
  while (di++ < to.length-1) {
    let target = this.target(x,y,di);
    if (this.buffer[target] != to[di]) return false;
  }
  return true;
}

Matrix.prototype.get = function(x,y) {
  let data = [];
  let di = -1;
  while (di++ < this.depth()-1) {
    let target = this.target(x,y,di);
    if (target == undefined) return false;
    data.push(this.buffer[target]);
  }
  return data;
}

Matrix.prototype.set = function(x,y,value) {
  let di = -1;
  while (di++ < this.depth()-1) {
    let target = this.target(x,y,di);
    if (target == undefined) return false;
    this.buffer[target] = value[di];
  }
}

Matrix.prototype.fullset = function(values) {
  for (var y = 0; y < this.height(); y++){
    for (var x = 0; x < this.width(); x++){
      this.set(x,y,values);
    }
  }
}

Matrix.prototype.clear = function() {
  for (var i = 0; i < this.buffer.length-3; i++){
    this.buffer[i] = 0;
  }
}

// TEXT

Matrix.prototype.write = function(x,y,str,fg,bg) {
  let ci = -1;
  let x_offset = 0;
  while (ci++ < str.length-1) {
    if (str[ci] == '\n') {
      y++;
      x_offset = 0;
      continue;
    }
    let code = patterns_names.indexOf(str[ci]);
    this.set(x+x_offset,y,[code,fg,bg,0]);
    x_offset++;
  }
}
Matrix.prototype.erase = function(x,y,str,fg,bg) {
  let ci = -1;
  let x_offset = 0;
  while (ci++ < str.length-1) {
    if (str[ci] == '\n') {
      y++;
      x_offset = 0;
      continue;
    }
    let code = character_set.indexOf(str[ci]);
    this.set(x+x_offset,y,[0,0,0,0]);
    x_offset++;
  }
}

// GEOMETRY

Matrix.prototype.rect = function(x,y,w,h,value) {
  let yi = -1;
  while (yi++ < h-1) {
    let xi = -1;
    while (xi++ < w-1) {
      this.set(x+xi,y+yi,value);
    }
  }
}

// FLOODFILL

Matrix.prototype.floodfill = function(flood,dirs,fill_condition,fill_options) {
  while ((flood_dirs = flood.shift())) {
    let more_flood = this.fill(flood_dirs[0],flood_dirs[1],dirs,fill_condition,fill_options);
    if (more_flood) {
      flood = flood.concat(more_flood);
    }
  }
}

Matrix.prototype.dynfloodfill = function(flood,dirs,fill_condition,fill_options,speed) {
  while ((flood_dirs = flood.shift())) {
    let more_flood = this.fill(flood_dirs[0],flood_dirs[1],dirs,fill_condition,fill_options);
    if (more_flood) {
      setTimeout(function(){
        this.dynfloodfill(more_flood,dirs,fill_condition,fill_options,speed);
      }.bind(this),speed);
    }
  }
}

Matrix.prototype.fill = function(x,y,dirs,fill_condition,fill_options) {
  if (!fill_condition(x,y,fill_options)) return false;
  let next_dirs = [];
  for (var i = 0; i < dirs.length; i++) {
    next_dirs.push([
      x + dirs[i][0],
      y + dirs[i][1],
    ]);
  }
  return next_dirs;
}

/* 

**********************
* Floodfill examples *
**********************

Matrix.prototype.base_floodfill = function(x,y,value) {
  this.floodfill(
    [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
    function (x,y,options) {
      if (
        !options.matrix.get(x,y) ||   
        options.matrix.compare(x,y,options.value)
      ) return false;
      options.matrix.set(x,y,options.value);
      return true;
    },
    {"matrix":this,"value":value}
  );
}

Matrix.prototype.replace_floodfill = function(x,y,from,to) {
  this.floodfill(
    [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
    function (x,y,options) {
      if (
        !options.matrix.get(x,y) ||   
        !options.matrix.compare(x,y,options.from) ||
        options.matrix.compare(x,y,options.to)
      ) return false;
      options.matrix.set(x,y,options.to);
      return true;
    },
    {"matrix":this,"from":from,"to":to}
  );
}

Matrix.prototype.limited_replace_floodfill = function(x,y,from,to,limit) {
  this.floodfill(
    [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
    function (x,y,options) {
      if (
        options.limit <= 0 ||
        !options.matrix.get(x,y) ||   
        !options.matrix.compare(x,y,options.from) ||
        options.matrix.compare(x,y,options.to)
      ) return false;
      options.limit--;
      options.matrix.set(x,y,options.to);
      return true;
    },
    {"matrix":this,"from":from,"to":to,"limit":limit}
  );
}

Matrix.prototype.limited_replace_dynfloodfill = function(x,y,from,to,limit,speed) {
  this.dynfloodfill(
    [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
    function (x,y,options) {
      if (
        options.limit <= 0 ||
        !options.matrix.get(x,y) ||   
        !options.matrix.compare(x,y,options.from) ||
        options.matrix.compare(x,y,options.to)
      ) return false;
      options.limit--;
      options.matrix.set(x,y,options.to);
      return true;
    },
    {"matrix":this,"from":from,"to":to,"limit":limit},
    speed
  );
}

Matrix.prototype.limited_floodfill = function(x,y,value,limit) {
  this.floodfill(
    [[x,y]],[[1,0],[0,1],[-1,0],[0,-1]],
    function (x,y,options) {
      if (
        options.limit <= 0 ||
        !options.matrix.get(x,y) ||   
        options.matrix.compare(x,y,options.value)
      ) return false;
      options.limit--;
      options.matrix.set(x,y,options.value);
      return true;
    },
    {"matrix":this,"value":value,"limit":limit}
  );
}

*/

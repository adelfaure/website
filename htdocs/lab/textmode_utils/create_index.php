<!DOCTYPE html>
<?php
  function ascii_assets() {
    $path = "./art/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<pre class=\"ascii\" id=\"$path$file\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</pre>\n";
      }
    }
  }
  function js_assets() {
    $path = "./javascript/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<script src=\"$path$file\"></script>\n";
      }
    }
  }
?>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
    <title></title>
    <style></style>
  </head>
  <body>
    <?php ascii_assets(); ?>
  </body>
  <?php js_assets(); ?>
  <script>
  </script>
</html>

// render

let render_output = document.getElementById("render_output");
let render_width = 0;
let render_height = 0;
let render = [];
let free_render_character = '█';
let transparent_character = '░';

// setup_render;

function setup_render() {
  render_width = Math.floor(window.innerWidth/char_width);
  render_height = Math.floor(window.innerHeight/char_height);
  render = [];
  for (var i = 0; i < render_width*render_height; i++) {
    render.push(Number.isInteger((i+1)/render_width) ? '\n' : free_render_character);
  }
}

// get render position
function get_render_position(x,y,rw){
  return Math.floor(x)+Math.floor(y)*rw;
}

// clear render process
function clear_render_process() {
  if (window.innerWidth/10.5 != render_width) {
    setup_render();
  }
  for (var i = 0; i < render.length; i++) {
    if (render[i] == '\n') continue;
    render[i] = free_render_character;
  }
}

// render_process
function render_process() {
  //render_sprite("Total elements: "+sprites_renders,0,1);
  //render_sprite("Chunk: "+view_chunk_index,0,2);
  //render_sprite("Rendering: "+sprites_renders_list.length,0,3);
  //render_sprites();
  for (var i = 0; i < render.length; i++) {
    if (render[i] != free_render_character) continue;
		//let y = Math.trunc((i+1)/render_width);
    render[i] = ' ' //Math.floor(y-render_height/2 + view_y) > 0 ? ':' : Math.floor(y-render_height/2) == 0 ? '\'' : ' ';
  }
  let render_content = render.join('');
  if (render_content != render_output.textContent) {
    render_output.textContent = render_content;
  }
}

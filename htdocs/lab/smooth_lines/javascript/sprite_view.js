	// SPRITE VIEW

let view_x = 0;
let view_y = 0;
let view_chunk_size = Math.floor(window.innerWidth/14)/2;
let view_chunks = [];
let view_chunk_index = -1;
let chunk_explored = [];
let to_spatialize_list = [];

sort_view_list = function(view_list) {
  if (!view_list) return;
  let result = [];
  let z_view_list = [];
  let z_indexes = [];
  let z_order = [];
  for (var i = 0; i < view_list.length; i++){
    z_view_list.push(view_list[i][3]);
  }
  z_view_list.sort(function (a, b) {
    return a - b;
  });
  while (z_view_list.length) {
    let z_index = z_view_list.shift()
    if (z_indexes.includes(z_index)) continue;
    z_indexes.push(z_index);
    z_order.push([]);
    for (var i = 0; i < view_list.length; i++){
      let el = view_list[i]
      if (el[3] == z_index && !z_order[z_order.length-1].includes(el)) {
        z_order[z_order.length-1].push(el);
      }
    }
  }
  for (var i = 0; i < z_order.length; i++) {
    let y_view_list = [];
    let y_indexes = [];
    for (var j = 0; j < z_order[i].length; j++){
      y_view_list.push(z_order[i][j][2]);
    }
    y_view_list.sort(function (a, b) {
      return a - b;
    });
    while (y_view_list.length) {
      let y_index = y_view_list.pop()
      if (y_indexes.includes(y_index)) continue;
      y_indexes.push(y_index);
      for (var j = 0; j < z_order[i].length; j++){
        let el = z_order[i][j]
        if (el[2] == y_index && !result.includes(el)) {
          result.push(el);
        }
      }
    }
  }
  return result;
}

function add_to_view_chunks(sprite_name,x,y,z) {
  let sprite_render = new_sprite_render(sprite_name,0,0);
  let sprite_view = [sprite_render,x,y,z];
  let sprite_width = sprites_width_list[sprite_name];
  for (var chunk_spread = Math.floor(z*-1); chunk_spread < z; chunk_spread++) {
    let min_view_chunks_index = Math.floor(x/view_chunk_size)+chunk_spread;
    let max_view_chunks_index = Math.floor((x+(sprite_width+1)*z)/view_chunk_size)+chunk_spread;
    if (max_view_chunks_index < 0) continue;
    while (view_chunks.length-1 < max_view_chunks_index+1) {
      view_chunks.push([]);
    }
    for (var i = min_view_chunks_index; i < max_view_chunks_index+1; i++) {
      if (i<0) continue
      view_chunks[i].push(sprite_view);
    }
  }
}

// update view
function update_view_content(view_x) {
	// check chunk index change
  if (Math.floor(view_x/view_chunk_size) != view_chunk_index) {
  	view_chunk_index = Math.floor(view_x/view_chunk_size);
		
		// if chunk is unexplored, generate its content
  	if (!chunk_explored.includes(view_chunk_index)) {
			generate_chunk_content(view_x);
  	  chunk_explored.push(view_chunk_index);
  	}

		// setup view update
  	sprites_renders_list = [];
  	to_spatialize_list = [];
  	let view_list = [];
  	
		// update view
		if (view_chunks[view_chunk_index]) {
  	  for (var i = 0; i < view_chunks[view_chunk_index].length; i++) {
  	    view_list.push(view_chunks[view_chunk_index][i]);
  	    to_spatialize_list.push(view_chunks[view_chunk_index][i]);
  	  }
  	}
  	if (view_chunks[view_chunk_index-1]) {
  	  for (var i = 0; i < view_chunks[view_chunk_index-1].length; i++) {
  	    view_list.push(view_chunks[view_chunk_index-1][i]);
  	    to_spatialize_list.push(view_chunks[view_chunk_index-1][i]);
  	  }
  	}
  	view_list = sort_view_list(view_list);

		// update sprite render queue with new view
  	for (var i = 0; i < view_list.length; i++) {
  	  sprites_renders_list.push(view_list[i][0]);
  	}
	}
}

function spatialize(vx,vy,x,y,z,w,h,rw,rh) {
	return [
    // spatialize x
    rw/2 - w/2 + (x - vx) / z,
    // spatialize y
    rh/2 - h + (y - vy) / z
	];
}

// process_view
function view_process() {
	if (!render_width) return;
	
	// generate root content
	if (!chunk_explored.length) {
		for (var i = 0; i < 100; i++) {
			update_view_content(view_x-render_width*i);
		}
	}

  update_view_content(view_x);

	// spatialize visible sprites
  for (var i = 0; i < to_spatialize_list.length; i++) {
    let to_spatialize = to_spatialize_list[i];
		let spatialised_pos = spatialize(
			view_x,
			view_y,
			to_spatialize[1],
			to_spatialize[2],
			to_spatialize[3],
			to_spatialize[0][3], 
			to_spatialize[0][4], 
			render_width, 
			render_height
		);
    to_spatialize[0][1] = spatialised_pos[0];
    to_spatialize[0][2] = spatialised_pos[1];
  }
}



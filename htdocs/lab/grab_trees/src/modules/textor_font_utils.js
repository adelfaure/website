// Semigraphics

let graphic_names = [
  "NUL","SOH","STX","ETX","EOT","ENQ","ACK","BEL","BS","HT","LF","VT","FF","CR","SO","SI",
  "DLE","DC1","DC2","DC3","DC4","NAK","SYN","ETB","CAN","EM","SUB","ESC","FS","GS","RS","US",
  "full","edge","half","quarter","path","cross","fork","turn","diagonal","diag-cross","diag-fork","diag-turn","round-turn","round-quarter","round-big-quarter","round-big-turn","slope",
  "pike","big-pike","bump","big-bump","soft-slope","small-square","small-circle","medium-square","small-losange","medium-circle","square","circle","big-circle","end","corner",
  "tunnel","circle-outline","square-outline","medium-circle-outline","big-checkers","checkers","half-checkers","shade","half-shade","low-density","arch","round-arch","small-arch","double-lines","crossed-lines","corner-dot",
  "edge-dots","turn-dots","four-ots","two-dots","three-dots","five-dots","six-dots","arrow","turning-arrow","thin-lines","note","double-note","heart","losange","clover","spade",
  "character","character-walking","character-angry","smiley","filled-smiley","star","drop","fire","ice","skull","sun","moon","life","diamond","cherry","apple",
  "tree","pine","winter-tree","young-tree","herbs","plant","roses","branch","flowers","flower","mold","ground-flowers","ground","mushroom","water","DEL",
  "PAD","HOP","BPH","NBH","IND","NEL","SSA","ESA","HTS","HTJ","LTS","PLD","PLU","RI","SS2","SS3",
  "DCS","PU1","PU2","STS","CCH","MW","SPA","EPA","SOS","SGCI","SCI","CSI","ST","OSC","PM","APC",
  "NBSP","mushrooms",
  "cow","wolf","cat","rat","lizard","rabbit","snake","worm","turtle","flies","snail","crab","deer","bat","bird","chicken",
  "vase","coffer","opened-coffer","empty-shelf","shelf","empty-table","table","bed","door","opened-door","window","broken-window","computer","torch","key","chair",
  "human","human-walking","human-angry","human-armed","human-armed-walking","human-armed-angry","human-armed-aiming","human-sitting","human-hat","human-hat-walking","human-hat-angry","human-hat-armed","human-hat-armed-walking","human-hat-armed-angry","human-hat-armed-aiming","human-hat-sitting"
];

function graphic(str){
  return graphic_names.indexOf(str)+256;
}

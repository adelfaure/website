Floodfill = function(
  queue,
  target,
  replacement,
  dirs,
  pace,
  end_queue_event,
){

  // MANUAL
  this.queue = queue;
  this.target =  target;
  this.replacement = replacement;
  this.dirs = dirs;
  this.pace = pace;
  this.end_queue_event = end_queue_event;
  
  // AUTO
  this.f = 0;
  this.next_queue = [];
}

// Work only in area render shader function  
Floodfill.prototype.process = function(){
  if (f%this.pace) return -1;
  if (f != this.f) {
    for (var qe = 0; qe < this.next_queue.length; qe++){
      this.queue.push(this.next_queue[qe]);
    }
    if (!this.queue.length) {
      this.end_queue_event(this);
    }
    this.next_queue = [];
    this.f = f;
  }
  let queue_index, queue_element;
  queue_index = this.queue.indexOf(i);
  queue_element = queue_index > -1 ? this.queue.splice(queue_index,1)[0] : queue_index;
  if (queue_element > -1) {
    for (var d = 0; d < this.dirs.length; d++) {
      if (
        dn[i+this.dirs[d]] != this.replacement
        && x + this.dirs[d]%w > -1 
        && x + this.dirs[d]%w < w 
        && this.target.includes(dn[i+this.dirs[d]])
      ) {
        this.next_queue.push(queue_element+this.dirs[d]);
      }
    }
  }
  return (
    queue_element > -1 && this.target.includes(dn[i]) ? this.replacement : 
    -1
  );
}

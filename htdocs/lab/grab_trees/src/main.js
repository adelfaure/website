// CONTROLS
let keys = {};
document.addEventListener("keydown",function(e){
  keys[e.key] = true;
});
document.addEventListener("keyup",function(e){
  keys[e.key] = false;
});

// GAME

// cam
let cam_x = 0;
let cam_y = 0;

// player
let player_action = 0;
let player_x = 0;
let player_y = 0;
let player_speed = 1/6;
let player_vel_x = 0;
let player_vel_y = 0;
let player_move = false;
let player_dc = 0;
let will_step_on_x; 
let will_step_on_y;
let will_step_on_xy;
let new_player_di_x;
let new_player_di_y;
let new_player_di_xy;
let inventory = [];
let inventory_ui;

// decor
let decor_x = 128;
let decor_y = 128;

// MAIN
function main() {

  let decor = new Area(256,256);
  let fx = new Area(scene.width,scene.height);
  let grab_fx_dc = [
    render.font.encoding.mode(
      graphic(
        "small-losange"
      ),{
        "fg":"bright",
      }
    ),
    render.font.encoding.mode(
      graphic(
        "small-circle"
      ),{
        "fg":"bright",
      }
    ),
    render.font.encoding.mode(
      render.font.encoding.descrToDec({"name":'·'}),{
        "fg":"bright",
      }
    ),
  ];
  let move_fx_dc = [
    render.font.encoding.mode(
      graphic(
        "small-losange"
      ),{
        "fg":"soft",
      }
    ),
    render.font.encoding.mode(
      graphic(
        "small-circle"
      ),{
        "fg":"soft",
      }
    ),
    render.font.encoding.mode(
      render.font.encoding.descrToDec({"name":'·'}),{
        "fg":"soft",
      }
    ),
  ];
  // A game where you pick up mushrooms and flowers ?
  decor.render(
    {"type":"no-display"},
    function(){
      let tree = mr() > 0.8;
      let flower = mr() > 0.9;
      let mushroom = mr() > 0.99;
      return (
        x == 0 && y == 0 ? 0 :
        x == w-1 && y == 0 ? 0 :
        x == w-1 && y == h-1 ? 0 :
        x == 0 && y == h-1 ? 0 :
        y == 0 ? render.font.encoding.mode(graphic(
            "diag-turn"
          ),{
            fg:"special"
          }
        ):
        y == h-1 ? render.font.encoding.mode(graphic(
            "diag-turn"
          ),{
            'fg':"special",
            'rotation':'180',
          }
        ):
        x == 0 ? render.font.encoding.mode(graphic(
            "diag-turn"
          ),{
            'fg':"special",
            'rotation':'270',
          }
        ):
        x == w-1 ? render.font.encoding.mode(graphic(
            "diag-turn"
          ),{
            'fg':"special",
            'rotation':'90',
          }
        ):
        mr()> 0.7 ? render.font.encoding.mode(graphic(
          tree ? (
            mr () > 0.5 ? (
              mr () > 0.5 ? "tree" : "winter-tree"
            ) : (
              mr() > 0.5 ? "pine" : "young-tree"
            )
          ) : 
          mushroom ? mr() > 0.5 ? "mushroom" : "mushrooms" : 
          flower ? "ground-flowers" : 
          "ground"
        ),{
          "fg": (
            (flower || mushroom) && !tree ? ["soft","bitter","special","bright"][mf(mr()*4)] : 
            !tree && mr() > 0.9 ? mr() > 0.25 ? "special" : "bright" : 
            "sweet"
          ),
          "rotation": tree || mushroom ? "0" : rotation[mf(mr()*rotation.length)],
          "mirror": tree ? "none" : (
            mushroom ? ["none","horizontal"][mf(mr()*2)]:
            mirror[mf(mr()*mirror.length)]
          ),
        }) : 
        0
      );
    }
  );

  let view = new Area(view_size,view_size);
  player_x = view.width/2;
  player_y = view.height/2;

  // Obstacles
  let obstacles = ["tree","winter-tree","young-tree","pine","diag-turn"];
 
  // UI
  let text;
  let will_step_on_text;
  let collide_with_text = '';

  frameStart = function(){

    // Player move
    player_vel_x = (
      keys["q"] || keys["ArrowLeft"] ? player_speed*-1 :
      keys["d"] || keys["ArrowRight"] ? player_speed : 
      0
    );
    player_vel_y = (
      keys["z"] || keys["ArrowUp"]? player_speed*-1 :
      keys["s"] || keys["ArrowDown"] ? player_speed : 
      0
    );
    player_move = (player_vel_x != 0 || player_vel_y != 0);

    let new_player_x = player_x + player_vel_x;
    let new_player_y = player_y + player_vel_y;

    new_player_di_x = mf(new_player_x-cam_x) + mf(player_y-cam_y)*view.width;
    new_player_di_y = mf(player_x-cam_x) + mf(new_player_y-cam_y)*view.width;
    new_player_di_xy = mf(new_player_x-cam_x) + mf(new_player_y-cam_y)*view.width;

    will_step_on_x = decor.place(view,new_player_di_x,cam_x*-1-decor_x,cam_y*-1-decor_y,0,0);
    will_step_on_y = decor.place(view,new_player_di_y,cam_x*-1-decor_x,cam_y*-1-decor_y,0,0);
    will_step_on_xy = decor.place(view,new_player_di_xy,cam_x*-1-decor_x,cam_y*-1-decor_y,0,0);

    let will_step_on_x_descr = render.font.encoding.decToDescr(
      will_step_on_x
    );
    let will_step_on_y_descr = render.font.encoding.decToDescr(
      will_step_on_y
    );
    let will_step_on_xy_descr = render.font.encoding.decToDescr(
      will_step_on_xy
    );

    let str_x = graphic_names[render.font.encoding.rules_sub_keys[0].indexOf(will_step_on_x_descr["name"])];
    let str_y = graphic_names[render.font.encoding.rules_sub_keys[0].indexOf(will_step_on_y_descr["name"])];
    let str_xy = graphic_names[render.font.encoding.rules_sub_keys[0].indexOf(will_step_on_xy_descr["name"])];


    let grab = -1; 
    if (!obstacles.includes(str_xy)) {
      will_step_on_text = str_xy == 'NUL' ? '' : str_xy;
      collide_with_text = '';
      grab = keys[' '] ? new_player_di_xy : -1;
      player_y = grab < 0 ? new_player_y : player_y;
      player_x = grab < 0 ? new_player_x : player_x;
    } else if (!obstacles.includes(str_x)) {
      will_step_on_text = str_x == 'NUL' ? '' : str_x;
      collide_with_text = '';
      grab = keys[' '] ? new_player_di_x : -1;
      player_x = grab < 0 ? new_player_x : player_x;
    } else if (!obstacles.includes(str_y)) {
      will_step_on_text = str_y == 'NUL' ? '' : str_y;
      collide_with_text = '';
      grab = keys[' '] ? new_player_di_y : -1;
      player_y = grab < 0 ? new_player_y : player_y;
    }
    if (obstacles.includes(str_xy)){
      collide_with_text = str_xy;
      grab = keys[' '] ? new_player_di_xy : -1;
    } else if (obstacles.includes(str_x)){
      collide_with_text = str_x;
      grab = keys[' '] ? new_player_di_x : -1;
    } else if (obstacles.includes(str_y)){
      collide_with_text = str_y;
      grab = keys[' '] ? new_player_di_y : -1;
    }

    if (grab > -1) {
      keys = {};
      let item = decor.replace(view,grab,cam_x*-1-decor_x,cam_y*-1-decor_y,0,0,0);
      if (item != 0) {
        inventory.push(item);
        player_action = f+10;
        fx.replace(view,grab,0,0,0,0,grab_fx_dc[0]);
      }
    }

    let new_cam_x = mf(player_x/(view.width-2))*(view.width-2)-1;
    let new_cam_y =  mf(player_y/(view.height-2))*(view.height-2)-1;
    if (new_cam_x != cam_x || new_cam_y != cam_y) {
      fx.render(
        render,
        ()=>0
      );
    }
    cam_x = new_cam_x;
    cam_y = new_cam_y;
    player_dc = render.font.encoding.mode(
      graphic(
        f < player_action ? "human-angry" :
        player_move ? "human-walking" :
        "human"
      ),{
        "fg":f<player_action-5 ? "bright" : "bitter",
        "rotation":(
          player_vel_x > 0 && player_vel_y > 0 ? "90" :
          player_vel_x < 0 && player_vel_y > 0 ? "270" :
          player_vel_x > 0 && player_vel_y < 0 ? "270" :
          player_vel_x < 0 && player_vel_y < 0 ? "90" :
          "0"
        ),
        "mirror":(
          player_vel_x < 0 ? "horizontal" :
          player_vel_x == 0 && player_move && mf(player_y)%2 ? "horizontal" :
          "none"
        )
      }
    );

    // FX
    fx.render(
      render,
      function(){
        return (
          x == mf(player_x-cam_x) && y == mf(player_y-cam_y) ? move_fx_dc[0] :
          !(f%5) && dn[di] == move_fx_dc[0] ? move_fx_dc[1] :
          !(f%5) && dn[di] == move_fx_dc[1] ? move_fx_dc[2] :
          !(f%5) && dn[di] == move_fx_dc[2] ? 0 :
          !(f%5) && dn[di] == grab_fx_dc[0] ? grab_fx_dc[1] :
          !(f%5) && dn[di] == grab_fx_dc[1] ? grab_fx_dc[2] :
          !(f%5) && dn[di] == grab_fx_dc[2] ? 0 :
          dn[di]
        )
      }
    );

    // VIEW
    view.render(
      {"type":"no-display"},
      function(){
        let decor_place = (
          (
            x + cam_x + decor_x > -1 
            && x + cam_x + decor_x < decor.width
            && y + cam_y + decor_y > -1 
            && y + cam_y + decor_y < decor.height 
          ) ? (
            decor.place(view,di,cam_x*-1-decor_x,cam_y*-1-decor_y,0,0)
          ) : 
          0
        );
        let player_place = x == mf(player_x-cam_x) && y == mf(player_y-cam_y);
        if (player_place) {
          //console.log(decor_place);
        }
        let fx_place = fx.place(view,di,0,0,0,0);
        let cd = (
          player_place ? player_dc :
          fx_place ? fx_place : 
          decor_place ? decor_place : 
          0
        );
        return cd;
      }
    );

    // UI
    text = render.font.encoding.strToArea(
      ///animation_fps + '\n' +
      (collide_with_text.length ? collide_with_text : will_step_on_text) + '\n'
    );
    inventory_ui = new Area(aside_size-2,aside_size-2);
    inventory_ui.render(
      {"type":"no-display"},
      function(){
        return inventory[di] ? inventory[di] : 
        render.font.encoding.mode(
          graphic("small-square"),{'fg':"soft"}
        ); 
      }
    );
  }
  
  let view_x = mf((scene.width-aside_size)/2 - view.width/2);
  let view_y = mf(scene.height/2 - view.height/2);
  
  fps = 60;
  shader = function(){
    let text_place = text.place(scene,di,(
      scene.width-aside_size+1
    ),(
      1
    ),scene.width,scene.height);
    let inventory_place = inventory_ui.place(scene,di,(
      scene.width-aside_size+1
    ),(
      3
    ),scene.width,scene.height);
    let not_view = (
      x < view_x
      || y < view_y
      || x > view_x + view.width-1
      || y > view_y + view.height-1 
    );
    return (
      inventory_place ? inventory_place :
      text_place ? (
        render.font.encoding.mode(text_place,{"fg":"bright"})
      ) :
      not_view ? (
        ( 
          y == view_y-1 && x == view_x + view.width
        ) ? render.font.encoding.mode(graphic("turn"),{'fg':"soft"}) : 
        ( 
          y == view_y + view.height && x == view_x + view.width
        ) ? render.font.encoding.mode(graphic("turn"),
          {'fg':"soft",'rotation':'90'}
        ) : 
        ( 
          y == view_y + view.height && x == view_x-1
        ) ? render.font.encoding.mode(graphic("turn"),
          {'fg':"soft",'rotation':'180'}
        ) : 
        ( 
          y == view_y-1 && x == view_x-1
        ) ? render.font.encoding.mode(graphic("turn"),
          {'fg':"soft",'rotation':'270'}
        ) : 
        ( 
          y == view_y-1 && x >= view_x && x < view_x + view.width
          || y == view_y+view.height && x >= view_x && x < view_x + view.width
        ) ? render.font.encoding.mode(graphic("path"), {'fg':"soft"} ) : 
        ( 
          x == view_x-1 && y >= view_y && y < view_y + view.height
          || x == view_x+view.width && y >= view_y && y < view_y + view.height
        ) ? render.font.encoding.mode(graphic("path"),
          {'fg':"soft",'rotation':"90",}
        ) :
        x == w-aside_size-1 ? render.font.encoding.mode(
          graphic("path"), {'fg':"soft",'rotation':"90"} 
        ) : 
        (
          x < scene.width-aside_size 
        ) ? render.font.encoding.mode(
          graphic("ground"),{'fg':"soft"}
        ) : 
        0
      ) : 
      view.place(scene,di,view_x,view_y,scene.width,scene.height)
    );
  }
  
  frameEnd = function(){console.log(animation_fps)};
}

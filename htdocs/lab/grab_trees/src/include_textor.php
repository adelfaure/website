<!-- TEXTOR FILES: start -->

<!-- ASCII art -->
<?php 
$path = $root."/res/ascii_art/";
$dir = scandir($path);
foreach ($dir as $file) {
  if (filetype("$path$file") == "file") {
    echo "<pre style=\"display: none;\" id=\"$file\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</pre>\n";
  }
}
?>

<!-- Bitmap fonts -->
<?php
$path = $root."/res/bitmap_fonts/";
$dir = scandir($path);
foreach ($dir as $file) {
  if (filetype("$path$file") == "file") {
    echo "<img style=\"display: none;\" id=\"$file\" src= \"$path$file\"/>\n";
  }
}
?>

<!-- Bitmap fonts keys redirections -->
<?php
$path = $root."/res/bitmap_fonts_keys_redirections/";
$dir = scandir($path);
foreach ($dir as $file) {
  if (filetype("$path$file") == "file") {
    echo "<pre style=\"display: none;\" id=\"$file\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</pre>\n";
  }
}
?>

<!-- Core -->
<?php
$path = $root."/src/core/";
$dir = scandir($path);
foreach ($dir as $file) {
  if (filetype("$path$file") == "file") {
    echo "<script defer src=\"".$path.$file."\"></script>\n";
  }
}
?>

<!-- Modules -->
<?php
$path = $root."/src/modules/";
$dir = scandir($path);
foreach ($dir as $file) {
  if (filetype("$path$file") == "file") {
    echo "<script defer src=\"".$path.$file."\"></script>\n";
  }
}
?>

<!-- Script -->
<script defer src="./src/init.js"></script>
<script defer src="./src/main.js"></script>

<!-- Load -->
<script>
  window.onload = function() {
    init();
    main();
    // first frame is made at load before animation start
    frameStart();
    scene.render(
      render,
      shader,
    );
    frameEnd();
    if (animation) start_animation();
  }
</script>

<!-- TEXTOR FILES: end -->

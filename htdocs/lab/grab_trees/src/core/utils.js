//  Textor, a javascript text mode engine.
//  Copyright (C) 2024 Adel Faure
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Save 
function save(data, filename, type) {
  var file = new Blob(data, {type: type});
  var a = document.createElement("a"), url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);  
  }, 0); 
}

// Micro notation
function ma(val){
  return Math.abs(val);
}
function mf(val){
  return Math.floor(val);
}
function mc(val){
  return Math.ceil(val);
}
function ms(val){
  return Math.sin(val);
}
function mn(val){
  return Math.round(val);
}
function mi(val){
  return Math.sign(val);
}
function mo(val){
  return Math.cos(val);
}
function mr(){
  return Math.random();
}

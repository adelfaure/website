/*

  Textor, a javascript text mode engine
  Copyright (C) 2024 Adel Faure
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// TEXTOR BITMAP FONT
// ------------------

// TODO better length system

BitmapFont = function(width,height,encoding,source,redirections,autoLoadCharacterData) {
  this.width = width;
  this.height = height;
  this.encoding = encoding;
  this.source = source;
  this.canvas = document.createElement("canvas");
  this.context = this.canvas.getContext("2d");
  this.canvas.width = this.source.width;
  this.canvas.height = this.source.height;
  this.type;
  if (source.tagName == "IMG") {
    this.context.drawImage(this.source,0,0);
    this.type = "IMG";
  } else {
    this.context.putImageData(this.source,0,0);
    this.type = "imageData";
  }
  this.nameLength = this.encoding.rules.body.name.length;
  this.mapWidth = this.source.width / width;
  this.mapHeight = this.source.height / height;
  this.mapLength = this.mapWidth * this.mapHeight;
  this.fullLength = this.encoding.length;
  this.characterData = [];
  this.redirections = redirections == undefined ? false : redirections;
  this.autoLoadCharacterData = autoLoadCharacterData == undefined ? false : autoLoadCharacterData;
}

// TODO REDIRECTION INCOMPATIBLE

BitmapFont.prototype.loadAllCharacterData = function(){
  for (var i = 0; i < this.mapLength; i++) {
    if (this.characterData[i]) continue;
    this.characterData[i] = this.getCharacterData(i);
  }
}

BitmapFont.prototype.loadCharacterData = function(cd){
  this.characterData[cd] = this.getCharacterData(cd);
}

BitmapFont.prototype.getCharacterData = function(
  cd,  // Character decimal
){
  //// REDIRECTION
  //if (this.redirections) {
  //  cd = this.redirections[cd*2+1];
  //}

  if (this.characterData[cd]) return this.characterData[cd];

  // Character top-left x-axis coordinate inside image font source
  let cx = (
    (cd % this.mapWidth) * this.width
  );
  // Character top-left y-axis coordinate inside image font source
  let cy = (
    Math.floor(cd / this.mapWidth) * this.height
  )
  // Character width
  let cw = this.width
  // Character height
  let ch = this.height

  let characterData = this.context.getImageData(cx,cy,cw,ch);
  characterData.density = 0;
  for (var i = 0; i < characterData.data.length; i++){
    characterData.density += characterData.data[i];
  }

  this.characterData[cd] = characterData;

  return characterData;
}

BitmapFont.prototype.toAreaList = function(
  colors
) {
  let areaList = [];
  while (areaList.length < this.mapLength) {
    areaList.push(this.characterToArea(areaList.length,colors));
  }
  return areaList;
}

// TODO? REPLACE WITH compressAreaList()
//BitmapFont.prototype.toCompressedAreaList = function(
//  colors
//) {
//  let areaList = [];
//  let strDataList = [];
//  let redirections = [];
//  for (var i = 0; i < this.mapLength; i++) {
//    let area = this.characterToArea(i,colors);
//    let area_str_data = area.data.join(',');
//    let redirection_index = strDataList.indexOf(area_str_data)
//    if (redirection_index < 0) {
//      redirections[i] = areaList.length;
//      areaList.push(area);
//      strDataList.push(area_str_data);
//    } else {
//      redirections[i] = redirection_index;
//    }
//  }
//  return [areaList,redirections];
//}

BitmapFont.prototype.characterToArea = function(
  cd,  // Character decimal
  colors
){
  if (!this.characterData[cd]) {
    this.loadCharacterData(cd);
  }
  let bitmapCharacter = this.characterData[cd];
  let area = new Area(bitmapCharacter.width,bitmapCharacter.height);
  for (var i = 0; i < bitmapCharacter.data.length; i+=4){
    for (var j = 0; j < colors.length; j+=4) {
      if (
        bitmapCharacter.data[i] == colors[j]
        && bitmapCharacter.data[i+1] == colors[j+1]
        && bitmapCharacter.data[i+2] == colors[j+2]
        && bitmapCharacter.data[i+3] == colors[j+3]
      ) {
        area.data[i/4] = j/4;
      } else {
        continue;
      }
    }
  }
  return area;
}

BitmapFont.prototype.redirectTo = function(
  cd // character decimal
) {
  return this.redirections[cd];
}
    

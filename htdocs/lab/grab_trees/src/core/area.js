//  Textor, a javascript text mode engine.
//  Copyright (C) 2024 Adel Faure
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// TEXTOR AREA
// -----------

// Area represents data of an area

Area = function(
  width,  // area width
  height, // area height
){
  this.width = width;
  this.height = height;
  this.data = new Uint32Array(width*height); // character area data
}

Area.prototype.place = function(area,i,pos_x,pos_y,gap_x,gap_y,transparency) {
  // origin x
  let ox = (pos_x*-1)%(this.width + gap_x) + this.width + gap_x;
  // origin y
  let oy = (pos_y*-1)%(this.height + gap_y) + this.height + gap_y;
  // area x
  let ax = i%area.width + ox;
  // area y
  let ay = Math.floor(i/area.width) + oy;
  // data x
  let dx = ax%(this.width + gap_x);
  // data y
  let dy = ay%(this.height + gap_y)*this.width;
  // data index
  let di = dx+dy;
  return (
    dx >= this.width ? 0 :
    this.data[di] == transparency ? 0 :
    this.data[di]
  )
}

Area.prototype.replace = function(area,i,pos_x,pos_y,gap_x,gap_y,replace_with,transparency) {
  // origin x
  let ox = (pos_x*-1)%(this.width + gap_x) + this.width + gap_x;
  // origin y
  let oy = (pos_y*-1)%(this.height + gap_y) + this.height + gap_y;
  // area x
  let ax = i%area.width + ox;
  // area y
  let ay = Math.floor(i/area.width) + oy;
  // data x
  let dx = ax%(this.width + gap_x);
  // data y
  let dy = ay%(this.height + gap_y)*this.width;
  // data index
  let di = dx+dy;
  
  if (
    !(dx >= this.width) && !(this.data[di] == transparency)
  ) {
    let data = this.data[di];
    this.data[di] = replace_with;
    return data;
  }
}


Area.prototype.get_rotation_index = function(i){
  return (i+(this.width-1)*(i+1))%(this.width*this.height+1);
}

Area.prototype.rotate = function(){
  let rotated_data = new Uint32Array(this.width*this.height);
  for (var i = 0; i < this.data.length; i++){
    rotated_data[this.get_rotation_index(i)] = this.data[i];
  }
  return rotated_data;
}

Area.prototype.get_vertical_mirror_index = function(i){
  let x = i%this.width;
  let y = Math.floor(i/this.width);
  return this.height*this.width-y*this.width+x-this.width;
}

Area.prototype.vertical_mirror = function(){
  let mirrored_data = new Uint32Array(this.width*this.height);
  for (var i = 0; i < this.data.length; i++){
    mirrored_data[this.get_vertical_mirror_index(i)] = this.data[i];
  }
  return mirrored_data;
}

Area.prototype.get_horizontal_mirror_index = function(i){
  let x = i%this.width;
  let y = Math.floor(i/this.width);
  return this.width-x+y*this.width-1;
}

Area.prototype.horizontal_mirror = function(){
  let mirrored_data = new Uint32Array(this.width*this.height);
  for (var i = 0; i < this.data.length; i++){
    mirrored_data[this.get_horizontal_mirror_index(i)] = this.data[i];
  }
  return mirrored_data;
}

Area.prototype.fitCanvas = function(
  scale,
  canvas,
  font
) {
  canvas.width = this.width * font.width;
  canvas.height = this.height * font.height;

  canvas.style.transformOrigin = "top left";
  canvas.style.transform = "scale("+scale+")";
}

Area.prototype.render = function(
  options, // Render options
  shader,  // A shader function to apply
  before_state,
) {

  // Iterate through data area
  for (di = 0; di < this.data.length; di++) {

    // Area x-axis coordinate
    x = di % this.width
    // Area y-axis coordinate
    y = Math.floor(di / this.width)

    let before_state = options["before_state"];

    // Apply shader to data
    if (shader) {
      dn = this.data;
      db = before_state ? before_state.data : 0;
      w = this.width;
      h = this.height;
      this.data[di] = shader();
    }

    // Look at before data, if same continue because there is no change to render
    if (before_state && this.data[di] == before_state.data[di]) continue;
    
    // Apply changes to before area for next render optimization
    if (before_state) {
      before_state.data[di] = this.data[di];
    }

    if (options["type"] == "no-display") continue;
    
    // TODO REPHRASE
    // Positionned data will be interpreted as character decimal. This data
    // will correspond to source sub-image character shape
    let cd = this.data[di];

    this[options["type"]](...[di,x,y,cd,options["context"],options["font"]]);
  }
}

// TODO REPHRASE
// Area.bitmapRender method paints data as  font characters onto
// given Web Canvas

Area.prototype.bitmapCharacterRender = function(
  i,              // Data index
  x,              // Area x-axis
  y,              // Area y-axis
  cd,             // Character decimal
  context,        // Web Canvas API CanvasRenderingContext2D
  font,           // BitmapFont
){

  // REDIRECTION
  if (font.redirections) {
    cd = font.redirectTo(cd);
  }

  // Character sub-image source (font ImageBitmap)
  // x-axis coordinate
  let sx = (
    (cd % font.mapWidth) * font.width
  );
  // y-axis coordinate
  let sy = (
    Math.floor(cd / font.mapWidth) * font.height
  )
  // sub-rectangle width
  let sw = font.width
  // sub-rectangle height
  let sh = font.height

  // Character sub-image destination (context Canvas). Character bitmap
  // render position will correspond to data area position scaled by font
  // size and ratio
  // x-axis coordinate
  let dx = x * font.width
  // y-axis coordinate
  let dy = y * font.height
  // sub-rectangle width
  let dw = font.width
  // sub-rectangle height
  let dh = font.height

  // Render character sub-image into context
  if (font.autoLoadCharacterData) {
    context.putImageData(font.getCharacterData(cd),dx,dy);
  } else {
    if (font.type == "IMG") {
      context.drawImage(font.source,sx,sy,sw,sh,dx,dy,dw,dh);
    } else if (font.type == "imageData") {
      // TODO
      context.putImageData(font.source,dx-sx,dy-sy,sx,sy,sw,sh);
    } else {
      console.log("Can't render bitmapCharacter : font type not supported");
    }
  }
}

Area.prototype.txtExport = function(font,file_name){
  // export as lone surrogates chars between 0 and 65535
  let txt = String.fromCharCode(this.width,this.height);
  let last_charCode = false;
  let charCode_repeat = 0;
  for (var i = 0; i < this.data.length; i++){
    let charCode = font.redirectTo(this.data[i]);
    txt += String.fromCharCode(charCode);
    if (charCode != last_charCode) {
      console.log(charCode_repeat);
    }
    charCode_repeat = charCode == last_charCode ? charCode_repeat + 1 : 0;
    last_charCode = charCode;
  }
  console.log(txt);
  //save([txt],file_name,"text/plain");
}

Area.prototype.dataToColorData = function(colors) {
  let color_data = new Uint8ClampedArray(this.width*this.height*4);
  for (var j = 0; j < this.data.length; j++){
    color_data[j*4]   = colors[this.data[j]*4+0];
    color_data[j*4+1] = colors[this.data[j]*4+1];
    color_data[j*4+2] = colors[this.data[j]*4+2];
    color_data[j*4+3] = colors[this.data[j]*4+3];
  }
  return color_data;
}

Area.prototype.dataToImageData = function(colors) {
  return new ImageData(this.dataToColorData(colors),this.width,this.height);
}

// AREA LIST

function areaListToImageData(areaList,width,colors) {
  let area_width = areaList[0].width;
  let area_height = areaList[0].height;
  let imageData = new ImageData(width*area_width,Math.ceil(areaList.length/width)*area_height);
  for (var i = 0; i < areaList.length; i++) {
    let area_x = (i%width)*area_width;
    let area_y = Math.floor(i/width)*area_height*(area_width*width);
    let area_color_data = areaList[i].dataToColorData(colors);
    for (var j = 0; j < area_color_data.length/4; j++){
      let data_x = j%area_width;
      let data_y = Math.floor(j/area_width)*(area_width*width);
      let data_index = (area_x+area_y+data_x+data_y)*4;
      let color_index = j*4;
      for (var k = 0; k < 4; k++) {
        imageData.data[(area_x+area_y+data_x+data_y)*4+k] = area_color_data[color_index+k];
      }
    }
  }
  return imageData;
}

function compressAreaList(areaList) {
  let redirections = [];
  let strDataList = [];
  let compressedAreaList = []
  for (var i = redirections.length; i < areaList.length; i++) {
    let area = areaList[i];
    let area_str_data = area.data.join(',');
    let redirection_index = strDataList.indexOf(area_str_data);
    if (redirection_index < 0) {
      redirections[i] = compressedAreaList.length;
      compressedAreaList.push(area);
      strDataList.push(area_str_data);
    } else {
      redirections[i] = redirection_index;
    }
  }
  return [compressedAreaList,redirections,strDataList];
}

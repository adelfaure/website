//  Textor, a javascript text mode engine.
//  Copyright (C) 2024 Adel Faure
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ENCODING UTILS
// TODO Seriously need some explaination

// TODO code, combinaison or mode ?
function code_to_dec(code_indexes,modes) {
  let dec = 0;
  for (var i = 0; i < code_indexes.length; i++) {
    dec += code_indexes[i] * modes[i];
  }
  return dec;
}

// TODO NOT WORKING
function dec_to_code(dec,modes) {
  //console.log(dec,modes);
  let code = [];
  for (var i = 0; i < modes.length; i++) {
    let code_index = Math.floor(dec/modes[i]);
    code.push(code_index);
    dec -= code_index * modes[i]
  }
  //console.log(dec,code);
  return code;
}

function modes_from_rules(rules) {
  let modes = [1];
  for (var i in rules) {
    modes.push(rules[i].length * modes[modes.length-1]);
  }
  return modes;
}

Encoding = function(rules){
  this.rules = rules;
  this.rules_keys = Object.keys(this.rules.body);
  this.rules_sub_keys = [];
  for (var i = 0; i < this.rules_keys.length; i++) {
    this.rules_sub_keys.push(this.rules.body[this.rules_keys[i]]);
  }
  this.modes = modes_from_rules(this.rules.body);
  this.length = this.modes[this.modes.length-1];
  this.name_length = this.modes[1];
}

Encoding.prototype.decToCode = function(dec){
  return dec_to_code(dec,this.modes);
}

Encoding.prototype.codeToDec = function(code){
  return code_to_dec(code,this.modes);
}

Encoding.prototype.descrToCode = function(descr){
  let code = [];
  for (var i = 0; i < this.rules_keys.length; i++) {
    for (var j in descr) {
      if (j == this.rules_keys[i]) {
        code.push(this.rules_sub_keys[i].indexOf(descr[j]));
      }
    }
  }
  return code;
}

Encoding.prototype.descrToDec = function(descr){
  return this.codeToDec(this.descrToCode(descr));
}

Encoding.prototype.codeToDescr = function(code){
  let descr = {};
  for (var i = 0; i < this.rules_keys.length; i++){
    descr[this.rules_keys[i]] = this.rules_sub_keys[i][code[i]];
  }
  return descr;
}


// TODO NOT WORKING
Encoding.prototype.decToDescr = function(dec){
  let descr = {}
  for (var i = 0; i < this.rules_keys.length; i++) {
    let key = this.rules_sub_keys[i][mf((dec%this.modes[i+1])/this.modes[i])];
    descr[this.rules_keys[i]] = key;
  }
  return descr;
}

Encoding.prototype.strToData = function(str,mode){
  let data = [];
  let descr = {};
  for (var i in mode) {
    descr[i] = mode[i];
  }
  for (var i = 0; i < str.length; i++){
    descr["name"] = str[i];
    data.push(this.descrToDec(descr));
  }
  return data;
}

Encoding.prototype.strToArea = function(str){
  let lines = str.split('\n');
  let width = 0;
  for (var i = 0; i < lines.length; i++){
    width = lines[i].length > width ? lines[i].length : width;
  }
  let area = new Area(width,lines.length);
  for (var i = 0; i < lines.length; i++){
    for (var j = 0; j < lines[i].length; j++){
      area.data[i*area.width+j] = this.descrToDec({"name":lines[i][j]});
    }
  }
  return area;
}

Encoding.prototype.mode = function(dec,mode_descr){
  let shift = 0;
  for (var i in mode_descr) {
    let mode_index = this.rules_keys.indexOf(i);
    let mode_param = this.rules_sub_keys[mode_index].indexOf(mode_descr[i])
    shift += this.modes[mode_index] * mode_param;
  }
  return dec + shift;
}

//  Textor, a javascript text mode engine.
//  Copyright (C) 2024 Adel Faure
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Render related
let canvas, context, scale, scene;
let render = {
  "type":undefined,    // Character rendering type
  "context":undefined, // API context, example : Web Canvas API CanvasRenderingContext2D
  "font":undefined,    // Font object, example : TextorBitmapFont
  "before state":false,    // An optionnal Area used for render optimization
}
        
let shader = () => dn[di];
let frameStart = () => false;
let frameEnd = () => false;

// Micro variables names for render shader function use
// 
// Area data now, area data index, area data before, x-axis area data, x-axis
// area data, area width, area height
let dn,di,db,x,y,w,h;

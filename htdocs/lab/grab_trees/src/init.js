// pre init game related
let view_size = mf(29/1.6);
let aside_size = mf(18/1.6);
let vertical_format = false;//window.innerWidth < window.innerHeight;

function init() {
  
  // Render config
  canvas = document.getElementById("canvas");
  render.type = "bitmapCharacterRender";
  render.context = canvas.getContext("2d");
  let bitmap_font = document.getElementById("textor8x8.png");
  let encoding = new Encoding(textor_rules);
  render.font = new BitmapFont(
    8,8,encoding,bitmap_font,
    JSON.parse('['+document.getElementById("textor8x8_redirections.txt").textContent+']'),
    true
  );

  // SCENE


  // scene scale
  let scale_y = (
    vertical_format ? mf(window.innerHeight/render.font.height/(view_size+aside_size+2)) :
    mf(window.innerHeight/render.font.height/(view_size+2))
  );
  let scale_x = (
    vertical_format ? mf(window.innerWidth/render.font.width/(view_size+2)) :
    mf(window.innerWidth/render.font.width/(view_size+aside_size+2))
  );
  scale = scale_x < scale_y ? scale_x > 0 ? scale_x : 1 : scale_y > 0 ? scale_y : 1 ;
  console.log(scale_x,scale_y,scale);

  // scene height
  let sh = mf(window.innerHeight/render.font.height/scale);
  
  // scene width
  let sw = mf(window.innerWidth/render.font.width/scale);

  // define scene
  scene = new Area(sw,sh);
  render["before_state"] = new Area(sw,sh);
  render["before_state"].render(
    {"type":"no-display"},
    ()=>1
  );

  // fit render output to scene size and scale according to used font caracteristics
  scene.fitCanvas(scale,canvas,render.font);

  // define animation
  animation = function() {
  
    // Start of frame process 
    frameStart();
    
    // Render scene
    scene.render(

      // render options
      render,

      // shader function
      shader,
    );
    
    // End of frame process
    frameEnd();
  };
}

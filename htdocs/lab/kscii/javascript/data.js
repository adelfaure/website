console.log("data.js");
/*

Patterns
--------

0-94 ascii

0  SP    10  *    20  4    30  >    40  H    50  R    60  \    70  f    80  p    90  z
1  !     11  +    21  5    31  ?    41  I    51  S    61  ]    71  g    81  q    91  {
2  "     12  ,    22  6    32  @    42  J    52  T    62  ^    72  h    82  r    92  |
3  1     13  _    23  7    33  A    43  K    53  U    63  _    73  i    83  s    93  }
4  $     14  .    24  8    34  B    44  L    54  V    64  `    74  j    84  t    94  ~
5  %     15  /    25  9    35  C    45  M    55  W    65  a    75  k    85  u
6  &     16  0    26  :    36  D    46  N    56  X    66  b    76  l    86  v
7  '     17  1    27  ;    37  E    47  O    57  Y    67  c    77  m    87  w
8  (     18  2    28  <    38  F    48  P    58  Z    68  d    78  n    88  u
9  )     19  3    29  =    39  G    49  Q    59  [    69  e    79  o    89  y

100-374 graphic

100 empty                           140 bold-right-pointing               180 thin-full                         220 thin-path-top-horizontal
101 full                            141 bold-bottom-pointing              181 thin-square                       221 thin-path-top-vertical
102 circle                          142 bold-top-left-pointing            182 thin-circle                       222 thin-path-top-right
103 diamond                         143 bold-top-right-pointing           183 thin-diamond                      223 thin-path-top
104 top-left-diamond                144 bold-bottom-right-pointing        184 top-left-circle-thin-line         224 thin-path-right-vertical
105 top-right-diamond               145 bold-bottom-left-pointing         185 top-right-circle-thin-line        225 thin-path-bottom-right
106 bottom-right-diamond            146 bold-line-left                    186 bottom-right-circle-thin-line     226 thin-path-bottom-left
107 bottom-left-diamond             147 bold-line-vertical-sides          187 bottom-left-circle-thin-line      227 thin-path-bottom-horizontal
108 top-left-circle                 148 bold-line-top-left                188 thin-ascents                      228 thin-path-right
109 top-right-circle                149 bold-line-bottom-left             189 thin-descents                     229 thin-path-bottom
110 bottom-right-circle             150 bold-line-top-vertical-sides      190 thin-ascent                       230 thin-dot
111 bottom-left-circle              151 bold-line-bottom-vertical-sides   191 thin-descent                      231 thin-dot-top-left
112 half-left                       152 bold-line-top                     192 thin-X                            232 thin-dot-top-right
113 half-top                        153 bold-line-horizontal-sides        193 thin-left-pointing                233 thin-dot-bottom-right
114 half-right                      154 bold-line-top-right               194 thin-top-pointing                 234 thin-dot-bottom-left
115 half-bottom                     155 bold-line-right-horizontal-sides  195 thin-right-pointing               235 full-thin-dot
116 quarter                         156 bold-line-left-horizontal-sides   196 thin-bottom-pointing              236 partial-thin-dot
117 quarter-top-left                157 bold-line-right                   197 thin-top-left-pointing            237 small-square
118 quarter-top-right               158 bold-line-bottom-right            198 thin-top-right-pointing           238 medium-square
119 quarter-bottom-right            159 bottom-bold-line                  199 thin-bottom-right-pointing        239 small-circle
120 quarter-bottom-left             160 horizontal-bold-line              200 thin-bottom-left-pointing         240 top-left-small-circle
121 quarter-ascent                  161 vertical-bold-line                201 thin-line-left                    241 top-right-small-circle
122 quarter-descent                 162 bold-cross                        202 thin-line-vertical-sides          242 bottom-right-small-circle
123 negative-quarter-top-left       163 bold-path-left                    203 thin-line-top-left                243 bottom-left-small-circle
124 negative-quarter-top-right      164 bold-path-top-left                204 thin-line-bottom-left             244 medium-circle
125 negative-quarter-bottom-right   165 bold-path-top-horizontal          205 thin-line-top-vertical-sides      245 left-circle
126 negative-quarter-bottom-left    166 bold-path-top-vertical            206 thin-line-bottom-vertical-sides   246 right-circle
127 bold-full                       167 bold-path-top-right               207 thin-line-top                     247 top-circle
128 bold-square                     168 bold-path-top                     208 thin-line-horizontal-sides        248 bottom-circle
129 bold-circle                     169 bold-path-right-vertical          209 thin-line-top-right               249 top-left-circle-quarter
130 bold-diamond                    170 bold-path-bottom-right            210 thin-line-right-horizontal-sides  250 top-right-circle-quarter
131 top-left-circle-bold-line       171 bold-path-bottom-horizontal       211 thin-line-left-horizontal-sides   251 bottom-right-circle-quarter
132 top-right-circle-bold-line      172 bold-path-right                   212 thin-line-right                   252 bottom-left-circle-quarter
133 bottom-right-circle-bold-line   173 bold-path-bottom                  213 thin-line-bottom-right            253 top-left-low-big-circle-part
134 bottom-left-circle-bold-line    174 bold-path-bottom-left             214 thin-line-bottom                  254 top-left-medium-big-circle-part
135 bold-ascent                     175 bold-dot                          215 horizontal-thin-line              255 top-left-high-big-circle-part
136 bold-descent                    176 bold-dot-top-left                 216 vertical-thin-line                256 top-right-high-big-circle-part
137 bold-X                          177 bold-dot-top-right                217 thin-cross                        257 top-right-medium-big-circle-part
138 bold-left-pointing              178 bold-dot-bottom-right             218 thin-path-left                    258 top-right-low-big-circle-part
139 bold-top-pointing               179 bold-dot-bottom-left              219 thin-path-top-left                259 bottom-right-low-big-circle-part

260 bottom-right-medium-big-circle-part            300 bold-bottom-left-high-big-circle-part               340 thin-top-right-low-big-circle-part
261 bottom-right-high-big-circle-part              301 bold-bottom-left-medium-big-circle-part             341 thin-bottom-right-low-big-circle-part
262 bottom-left-high-big-circle-part               302 bold-bottom-left-low-big-circle-part                342 thin-bottom-right-medium-big-circle-part
263 bottom-left-medium-big-circle-part             303 bold-top-left-low-big-horizontal-diamond-part       343 thin-bottom-right-high-big-circle-part
264 bottom-left-low-big-circle-part                304 bold-top-left-high-big-horizontal-diamond-part      344 thin-bottom-left-high-big-circle-part
265 top-left-low-big-horizontal-diamond-part       305 bold-top-right-high-big-horizontal-diamond-part     345 thin-bottom-left-medium-big-circle-part
266 top-left-high-big-horizontal-diamond-part      306 bold-top-right-low-big-horizontal-diamond-part      346 thin-bottom-left-low-big-circle-part
267 top-right-high-big-horizontal-diamond-part     307 bold-bottom-right-low-big-horizontal-diamond-part   347 thin-top-left-low-big-horizontal-diamond-part
268 top-right-low-big-horizontal-diamond-part      308 bold-bottom-right-high-big-horizontal-diamond-part  348 thin-top-left-high-big-horizontal-diamond-part
269 bottom-right-low-big-horizontal-diamond-part   309 bold-bottom-left-high-big-horizontal-diamond-part   349 thin-top-right-high-big-horizontal-diamond-part
270 bottom-right-high-big-horizontal-diamond-part  310 bold-bottom-left-low-big-horizontal-diamond-part    350 thin-top-right-low-big-horizontal-diamond-part
271 bottom-left-high-big-horizontal-diamond-part   311 bold-top-left-low-big-vertical-diamond-part         351 thin-bottom-right-low-big-horizontal-diamond-part
272 bottom-left-low-big-horizontal-diamond-part    312 bold-top-left-high-big-vertical-diamond-part        352 thin-bottom-right-high-big-horizontal-diamond-part
273 top-left-low-big-vertical-diamond-part         313 bold-top-right-high-big-vertical-diamond-part       353 thin-bottom-left-high-big-horizontal-diamond-part
274 top-left-high-big-vertical-diamond-part        314 bold-top-right-low-big-vertical-diamond-part        354 thin-bottom-left-low-big-horizontal-diamond-part
275 top-right-high-big-vertical-diamond-part       315 bold-bottom-right-low-big-vertical-diamond-part     355 thin-top-left-low-big-vertical-diamond-part
276 top-right-low-big-vertical-diamond-part        316 bold-bottom-right-high-big-vertical-diamond-part    356 thin-top-left-high-big-vertical-diamond-part
277 bottom-right-low-big-vertical-diamond-part     317 bold-bottom-left-high-big-vertical-diamond-part     357 thin-top-right-high-big-vertical-diamond-part
278 bottom-right-high-big-vertical-diamond-part    318 bold-bottom-left-low-big-vertical-diamond-part      358 thin-top-right-low-big-vertical-diamond-part
279 bottom-left-high-big-vertical-diamond-part     319 thin-small-square                                   359 thin-bottom-right-low-big-vertical-diamond-part
280 bottom-left-low-big-vertical-diamond-part      320 thin-medium-square                                  360 thin-bottom-right-high-big-vertical-diamond-part
281 medium-square                                  321 thin-small-circle                                   361 thin-bottom-left-high-big-vertical-diamond-part
282 bold-medium-circle                             322 thin-top-left-small-circle                          362 thin-bottom-left-low-big-vertical-diamond-part
283 bold-left-circle                               323 thin-top-right-small-circle                         363 left-triangle        
284 bold-right-medium-circle                       324 thin-bottom-right-small-circle                      364 top-triangle         
285 bold-top-medium-circle                         325 thin-bottom-left-small-circle                       365 right-triangle       
286 bold-bottom-medium-circle                      326 thin-medium-circle                                  366 bottom-triangle      
287 bold-top-left-circle-quarter                   327 thin-left-circle                                    367 bold-left-triangle   
288 bold-top-right-circle-quarter                  328 thin-right-circle                                   368 bold-top-triangle    
289 bold-bottom-right-circle-quarter               329 thin-top-circle                                     369 bold-right-triangle  
290 bold-bottom-left-circle-quarter                330 thin-bottom-circle                                  370 bold-bottom-triangle 
291 bold-top-left-low-big-circle-part              331 thin-top-left-circle-quarter                        371 thin-left-triangle   
292 bold-top-left-medium-big-circle-part           332 thin-top-right-circle-quarter                       372 thin-top-triangle    
293 bold-top-left-high-big-circle-part             333 thin-bottom-right-circle-quarter                    373 thin-right-triangle  
294 bold-top-right-high-big-circle-part            334 thin-bottom-left-circle-quarter                     374 thin-bottom-triangle 
295 bold-top-right-medium-big-circle-part          335 thin-top-left-low-big-circle-part                 x 375 left-half-diamond
296 bold-top-right-low-big-circle-part             336 thin-top-left-medium-big-circle-part                376 top-half-diamond
297 bold-bottom-right-low-big-circle-part          337 thin-top-left-high-big-circle-part                  377 right-half-diamond
298 bold-bottom-right-medium-big-circle-part       338 thin-top-right-high-big-circle-part                 378 bottom-half-diamond
299 bold-bottom-right-high-big-circle-part         339 thin-top-right-medium-big-circle-part               379 top-left-rounded-corner

380 top-right-rounded-corner
381 bottom-right-rounded-corner
382 bottom-left-rounded-corner
383 bold-top-left-rounded-path
384 bold-top-right-rounded-path
385 bold-bottom-right-rounded-path
386 bold-bottom-left-rounded-path
387 thin-top-left-rounded-path
388 thin-top-right-rounded-path
389 thin-bottom-right-rounded-path
390 thin-bottom-left-rounded-path
391 bold-vertical-lines
392 bold-horizontal-lines
393 thin-vertical-lines
394 thin-horizontal-lines
395 thin-left-shade
396 thin-top-shade
397 thin-right-shade
398 thin-bottom-shade
399 bold-left-shade
400 bold-top-shade
401 bold-right-shade
402 bold-bottom-shade
403 thin-vertical-waves
404 thin-horizontal-waves
405 bold-vertical-waves
406 bold-horizontal-waves
407 thin-vertical-medium-waves
408 thin-horizontal-medium-waves
409 bold-vertical-medium-waves
410 bold-horizontal-medium-waves
411 thin-vertical-large-waves
412 thin-horizontal-large-waves
413 bold-vertical-large-waves
414 bold-horiontal-large-waves












Colors
------

0-4 gray   5-11 red        12-18 green       19-25 blue       26-32 yellow
                                                              
0 black    5 darker red    12 darker green   19 darker blue   26 darker yellow
1 dark     6 dark red      13 dark green     20 dark blue     27 dark yellow
2 grey     7 medium red    14 medium green   21 medium blue   28 medium yellow
3 silver   8 red           15 green          22 blue          29 yellow
4 white    9 light red     16 light green    23 light blue    30 light yellow
           10 lighter red  17 lighter green  24 lighter blue  31 lighter yellow
           11 bright red   18 bright green   25 bright blue   32 bright yellow

33-39 cyan      40-46 magenta      47-49 orange     50-52 pink     53-55 lime
                                                                   
33 darker cyan  40 darker magenta  47 dark orange   50 dark pink   53 dark lime
34 dark cyan    41 dark magenta    48 orange        51 pink        54 lime
35 medium cyan  42 medium magenta  49 light orange  52 light pink  55 light lime
36 cyan         43 magenta
37 light cyan   44 light magenta
38 lighter cyan 45 lighter magenta
39 bright cyan  46 bright magenta

56-58 emerald     59-61 purple     62-64 bluesky
                                   
56 dark emerald   59 dark purple   62 dark bluesky
57 emerald        60 purple        63 bluesky
58 light emerald  61 light purple  64 light bluesky

*/

let semigraphic_map = [
  101,103,104,105,274,275,254,255,256,257,
    0,282,107,106,273,276,253,108,109,258,
  265,266,267,268,280,277,264,111,110,259,
  272,271,270,269,279,278,263,262,261,260,

  119,115,120,125,126,249,248,250,242,243,
  114,116,112,124,123,246,102,245,241,240,
  118,113,117,121,122,252,247,251,324,325,
  238,237,319,175,239,244,326,321,323,322,

  292,293,294,295,318,312,173,170,171,174,
  291,131,132,296,317,311,161,169,162,166,
  302,134,133,297,178,179,168,167,165,164,
  301,300,299,298,177,176,130,172,160,163,

  287,286,288,144,141,145,150,148,152,154,
  284,129,283,140,137,138,147,146,128,157,
  290,285,289,143,139,142,151,149,159,158,
  303,304,305,306,135,136,281,156,153,155,

  336,337,338,339,357,356,229,225,227,226,
  335,184,185,340,358,355,216,224,217,221,
  346,187,186,341,233,234,223,222,220,219,
  345,344,343,342,232,231,183,228,215,218,

  331,330,332,199,196,200,205,203,207,209,
  328,182,327,195,192,193,202,201,181,212,
  334,329,333,198,194,197,206,204,214,213,
  347,348,349,350,190,191,320,211,208,210,

    0,378,  0,381,364,382,385,368,386,  0,
  377,  0,375,363,235,365,367,236,369,  0,
    0,376,  0,380,366,379,384,370,383,  0,
  392,414,410,406,391,413,409,405,  0,  0,

  389,372,390,  0,402,  0,  0,398,  0,  0,
  371,230,373,401,127,399,397,180,395,  0,
  388,374,387,  0,400,  0,  0,396,  0,  0,
  394,412,408,404,393,411,407,403,188,189,
];

let patterns_names = [
' ',
'!',
'"',
'#',
'$',
'%',
'&',
'\'',
'(',
')',
'*',
'+',
',',
'-',
'.',
'/',
'0',
'1',
'2',
'3',
'4',
'5',
'6',
'7',
'8',
'9',
':',
';',
'<',
'=',
'>',
'?',
'@',
'A',
'B',
'C',
'D',
'E',
'F',
'G',
'H',
'I',
'J',
'K',
'L',
'M',
'N',
'O',
'P',
'Q',
'R',
'S',
'T',
'U',
'V',
'W',
'X',
'Y',
'Z',
'[',
'\\',
']',
'^',
'_',
'`',
'a',
'b',
'c',
'd',
'e',
'f',
'g',
'h',
'i',
'j',
'k',
'l',
'm',
'n',
'o',
'p',
'q',
'r',
's',
't',
'u',
'v',
'w',
'x',
'y',
'z',
'{',
'|',
'}',
'~',
'é',
'è',
'ç',
'à',
'§',
"empty",
"full",
"circle",
"diamond",
"top-left-diamond",
"top-right-diamond",
"bottom-right-diamond",
"bottom-left-diamond",
"top-left-circle",
"top-right-circle",
"bottom-right-circle",
"bottom-left-circle",
"half-left",
"half-top",
"half-right",
"half-bottom",
"quarter",
"quarter-top-left",
"quarter-top-right",
"quarter-bottom-right",
"quarter-bottom-left",
"quarter-ascent",
"quarter-descent",
"negative-quarter-top-left",
"negative-quarter-top-right",
"negative-quarter-bottom-right",
"negative-quarter-bottom-left",
"bold-full",
"bold-square",
"bold-circle",
"bold-diamond",
"top-left-circle-bold-line",
"top-right-circle-bold-line",
"bottom-right-circle-bold-line",
"bottom-left-circle-bold-line",
"bold-ascent",
"bold-descent",
"bold-X",
"bold-left-pointing",
"bold-top-pointing",
"bold-right-pointing",
"bold-bottom-pointing",
"bold-top-left-pointing",
"bold-top-right-pointing",
"bold-bottom-right-pointing",
"bold-bottom-left-pointing",
"bold-line-left",
"bold-line-vertical-sides",
"bold-line-top-left",
"bold-line-bottom-left",
"bold-line-top-vertical-sides",
"bold-line-bottom-vertical-sides",
"bold-line-top",
"bold-line-horizontal-sides",
"bold-line-top-right",
"bold-line-right-horizontal-sides",
"bold-line-left-horizontal-sides",
"bold-line-right",
"bold-line-bottom-right",
"bottom-bold-line",
"horizontal-bold-line",
"vertical-bold-line",
"bold-cross",
"bold-path-left",
"bold-path-top-left",
"bold-path-top-horizontal",
"bold-path-top-vertical",
"bold-path-top-right",
"bold-path-top",
"bold-path-right-vertical",
"bold-path-bottom-right",
"bold-path-bottom-horizontal",
"bold-path-right",
"bold-path-bottom",
"bold-path-bottom-left",
"bold-dot",
"bold-dot-top-left",
"bold-dot-top-right",
"bold-dot-bottom-right",
"bold-dot-bottom-left",
"thin-full",
"thin-square",
"thin-circle",
"thin-diamond",
"top-left-circle-thin-line",
"top-right-circle-thin-line",
"bottom-right-circle-thin-line",
"bottom-left-circle-thin-line",
"thin-ascents",
"thin-descents",
"thin-ascent",
"thin-descent",
"thin-X",
"thin-left-pointing",
"thin-top-pointing",
"thin-right-pointing",
"thin-bottom-pointing",
"thin-top-left-pointing",
"thin-top-right-pointing",
"thin-bottom-right-pointing",
"thin-bottom-left-pointing",
"thin-line-left",
"thin-line-vertical-sides",
"thin-line-top-left",
"thin-line-bottom-left",
"thin-line-top-vertical-sides",
"thin-line-bottom-vertical-sides",
"thin-line-top",
"thin-line-horizontal-sides",
"thin-line-top-right",
"thin-line-right-horizontal-sides",
"thin-line-left-horizontal-sides",
"thin-line-right",
"thin-line-bottom-right",
"thin-line-bottom",
"horizontal-thin-line",
"vertical-thin-line",
"thin-cross",
"thin-path-left",
"thin-path-top-left",
"thin-path-top-horizontal",
"thin-path-top-vertical",
"thin-path-top-right",
"thin-path-top",
"thin-path-right-vertical",
"thin-path-bottom-right",
"thin-path-bottom-left",
"thin-path-bottom-horizontal",
"thin-path-right",
"thin-path-bottom",
"thin-dot",
"thin-dot-top-left",
"thin-dot-top-right",
"thin-dot-bottom-right",
"thin-dot-bottom-left",
"full-thin-dot",
"partial-thin-dot",
"small-square",
"medium-square",
"small-circle",
"top-left-small-circle",
"top-right-small-circle",
"bottom-right-small-circle",
"bottom-left-small-circle",
"medium-circle",
"left-circle",
"right-circle",
"top-circle",
"bottom-circle",
"top-left-circle-quarter",
"top-right-circle-quarter",
"bottom-right-circle-quarter",
"bottom-left-circle-quarter",
"top-left-low-big-circle-part",
"top-left-medium-big-circle-part",
"top-left-high-big-circle-part",
"top-right-high-big-circle-part",
"top-right-medium-big-circle-part",
"top-right-low-big-circle-part",
"bottom-right-low-big-circle-part",
"bottom-right-medium-big-circle-part",
"bottom-right-high-big-circle-part",
"bottom-left-high-big-circle-part",
"bottom-left-medium-big-circle-part",
"bottom-left-low-big-circle-part",
"top-left-low-big-horizontal-diamond-part",
"top-left-high-big-horizontal-diamond-part",
"top-right-high-big-horizontal-diamond-part",
"top-right-low-big-horizontal-diamond-part",
"bottom-right-low-big-horizontal-diamond-part",
"bottom-right-high-big-horizontal-diamond-part",
"bottom-left-high-big-horizontal-diamond-part",
"bottom-left-low-big-horizontal-diamond-part",
"top-left-low-big-vertical-diamond-part",
"top-left-high-big-vertical-diamond-part",
"top-right-high-big-vertical-diamond-part",
"top-right-low-big-vertical-diamond-part",
"bottom-right-low-big-vertical-diamond-part",
"bottom-right-high-big-vertical-diamond-part",
"bottom-left-high-big-vertical-diamond-part",
"bottom-left-low-big-vertical-diamond-part",
"medium-square",
"bold-medium-circle",
"bold-left-circle",
"bold-right-medium-circle",
"bold-top-medium-circle",
"bold-bottom-medium-circle",
"bold-top-left-circle-quarter",
"bold-top-right-circle-quarter",
"bold-bottom-right-circle-quarter",
"bold-bottom-left-circle-quarter",
"bold-top-left-low-big-circle-part",
"bold-top-left-medium-big-circle-part",
"bold-top-left-high-big-circle-part",
"bold-top-right-high-big-circle-part",
"bold-top-right-medium-big-circle-part",
"bold-top-right-low-big-circle-part",
"bold-bottom-right-low-big-circle-part",
"bold-bottom-right-medium-big-circle-part",
"bold-bottom-right-high-big-circle-part",
"bold-bottom-left-high-big-circle-part",
"bold-bottom-left-medium-big-circle-part",
"bold-bottom-left-low-big-circle-part",
"bold-top-left-low-big-horizontal-diamond-part",
"bold-top-left-high-big-horizontal-diamond-part",
"bold-top-right-high-big-horizontal-diamond-part",
"bold-top-right-low-big-horizontal-diamond-part",
"bold-bottom-right-low-big-horizontal-diamond-part",
"bold-bottom-right-high-big-horizontal-diamond-part",
"bold-bottom-left-high-big-horizontal-diamond-part",
"bold-bottom-left-low-big-horizontal-diamond-part",
"bold-top-left-low-big-vertical-diamond-part",
"bold-top-left-high-big-vertical-diamond-part",
"bold-top-right-high-big-vertical-diamond-part",
"bold-top-right-low-big-vertical-diamond-part",
"bold-bottom-right-low-big-vertical-diamond-part",
"bold-bottom-right-high-big-vertical-diamond-part",
"bold-bottom-left-high-big-vertical-diamond-part",
"bold-bottom-left-low-big-vertical-diamond-part",
"thin-small-square",
"thin-medium-square",
"thin-small-circle",
"thin-top-left-small-circle",
"thin-top-right-small-circle",
"thin-bottom-right-small-circle",
"thin-bottom-left-small-circle",
"thin-medium-circle",
"thin-left-circle",
"thin-right-circle",
"thin-top-circle",
"thin-bottom-circle",
"thin-top-left-circle-quarter",
"thin-top-right-circle-quarter",
"thin-bottom-right-circle-quarter",
"thin-bottom-left-circle-quarter",
"thin-top-left-low-big-circle-part",
"thin-top-left-medium-big-circle-part",
"thin-top-left-high-big-circle-part",
"thin-top-right-high-big-circle-part",
"thin-top-right-medium-big-circle-part",
"thin-top-right-low-big-circle-part",
"thin-bottom-right-low-big-circle-part",
"thin-bottom-right-medium-big-circle-part",
"thin-bottom-right-high-big-circle-part",
"thin-bottom-left-high-big-circle-part",
"thin-bottom-left-medium-big-circle-part",
"thin-bottom-left-low-big-circle-part",
"thin-top-left-low-big-horizontal-diamond-part",
"thin-top-left-high-big-horizontal-diamond-part",
"thin-top-right-high-big-horizontal-diamond-part",
"thin-top-right-low-big-horizontal-diamond-part",
"thin-bottom-right-low-big-horizontal-diamond-part",
"thin-bottom-right-high-big-horizontal-diamond-part",
"thin-bottom-left-high-big-horizontal-diamond-part",
"thin-bottom-left-low-big-horizontal-diamond-part",
"thin-top-left-low-big-vertical-diamond-part",
"thin-top-left-high-big-vertical-diamond-part",
"thin-top-right-high-big-vertical-diamond-part",
"thin-top-right-low-big-vertical-diamond-part",
"thin-bottom-right-low-big-vertical-diamond-part",
"thin-bottom-right-high-big-vertical-diamond-part",
"thin-bottom-left-high-big-vertical-diamond-part",
"thin-bottom-left-low-big-vertical-diamond-part",
"left-triangle",
"top-triangle",
"right-triangle",
"bottom-triangle",
"bold-left-triangle",
"bold-top-triangle",
"bold-right-triangle",
"bold-bottom-triangle",
"thin-left-triangle",
"thin-top-triangle",
"thin-right-triangle",
"thin-bottom-triangle",
"left-half-diamond",
"top-half-diamond",
"right-half-diamond",
"bottom-half-diamond",
"top-left-rounded-corner",
"top-right-rounded-corner",
"bottom-right-rounded-corner",
"bottom-left-rounded-corner",
"bold-top-left-rounded-path",
"bold-top-right-rounded-path",
"bold-bottom-right-rounded-path",
"bold-bottom-left-rounded-path",
"thin-top-left-rounded-path",
"thin-top-right-rounded-path",
"thin-bottom-right-rounded-path",
"thin-bottom-left-rounded-path",
"bold-vertical-lines",
"bold-horizontal-lines",
"thin-vertical-lines",
"thin-horizontal-lines",
"thin-left-shade",
"thin-top-shade",
"thin-right-shade",
"thin-bottom-shade",
"bold-left-shade",
"bold-top-shade",
"bold-right-shade",
"bold-bottom-shade",
"thin-vertical-waves",
"thin-horizontal-waves",
"bold-vertical-waves",
"bold-horizontal-waves",
"thin-vertical-medium-waves",
"thin-horizontal-medium-waves",
"bold-vertical-medium-waves",
"bold-horizontal-medium-waves",
"thin-vertical-large-waves",
"thin-horizontal-large-waves",
"bold-vertical-large-waves",
"bold-horiontal-large-waves",
'ô'
]

let patterns = [
// 0-94 @ascii
// 0 @SP
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 1 @!
[
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 2 @"
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 3 @1
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 4 @$
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,1,1,1,0,],
  [0,0,1,0,1,0,0,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,1,0,0,0,],
],
// 5 @%
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 6 @&
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,0,0,0,0,],
  [0,1,0,0,1,0,0,0,],
  [0,0,1,1,0,0,0,0,],
  [0,1,0,0,1,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,1,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 7 @'
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 8 @(
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 9 @)
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 10 @*
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,0,1,0,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,1,1,1,1,1,1,1,],
  [0,0,0,1,1,1,0,0,],
  [0,0,1,0,1,0,1,0,],
  [0,0,0,0,1,0,0,0,],
],
// 11 @+
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 12 @,
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 13 @_
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 14 @.
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 15 @/
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 16 @0
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,1,1,1,0,],
  [0,1,1,1,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 17 @1
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 18 @2
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,1,1,0,0,],
  [0,0,1,1,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 19 @3
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 20 @4
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 21 @5
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 22 @6
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 23 @7
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 24 @8
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 25 @9
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 26 @:
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 27 @;
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 28 @<
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
],
// 29 @=
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 30 @>
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 31 @?
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,1,1,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 32 @@
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,1,1,1,1,0,],
  [0,1,0,1,0,0,1,0,],
  [0,1,0,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
],
// 33 @A
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 34 @B
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 35 @C
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 36 @D
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 37 @E
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 38 @F
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 39 @G
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 40 @H
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 41 @I
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 42 @J
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 43 @K
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,1,1,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 44 @L
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 45 @M
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,0,0,1,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 46 @N
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,0,0,0,1,0,],
  [0,1,0,1,0,0,1,0,],
  [0,1,0,0,1,0,1,0,],
  [0,1,0,0,0,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 47 @O
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 48 @P
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 49 @Q
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,1,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,1,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 50 @R
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 51 @S
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 52 @T
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 53 @U
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 54 @V
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 55 @W
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,1,0,0,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 56 @X
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 57 @Y
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 58 @Z
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 59 @[  
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 60 @\
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 61 @]
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 62 @^
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 63 @_
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
],
// 64 @`
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 65 @a
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 66 @b
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 67 @c
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 68 @d
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 69 @e
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 70 @f
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 71 @g
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 72 @h
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 73 @i
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 74 @j
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 75 @k
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 76 @l
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 77 @m
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,0,0,1,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 78 @n
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 79 @o
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 80 @p
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
],
// 81 @q
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
],
// 82 @r
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 83 @s
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 84 @t
[
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 85 @u
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 86 @v
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 87 @w
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,1,0,0,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 88 @x
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 89 @y
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 90 @z
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 91 @{
[
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
],
// 92 @|
[
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
],
// 93 @}
[
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
],
// 94 @~
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,0,0,1,0,],
  [0,1,0,0,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 95-99 @extended-ascii
// 95 @é
[
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 96 @è
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 97 @ç
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,1,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 98 @à
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 99 @§
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 100-180 @graphic
// 100 @empty
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 101 @full
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 102 @circle
[
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 103 @diamond
[
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 104 @top-left-diamond
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 105 @top-right-diamond
[
  [1,0,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
],
// 106 @bottom-right-diamond
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 107 @bottom-left-diamond
[
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,0,1,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,0,1,],
],
// 108 @top-left-circle
[
  [0,0,0,0,0,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 109 @top-right-circle
[
  [1,1,1,0,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 110 @bottom-right-circle
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,0,0,0,0,0,],
],
// 111 @bottom-left-circle
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,0,1,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,1,1,1,],
],
// 112 @half-left
[
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
],
// 113 @half-top
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 114 @half-right
[
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
],
// 115 @half-bottom
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 116 @quarter
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 117 @quarter-top-left
[
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 118 @quarter-top-right
[
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 119 @quarter-bottom-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
],
// 120 @quarter-bottom-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
],
// 121 @quarter-ascent
[
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
],
// 122 @quarter-descent
[
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
],
// 123 @negative-quarter-top-left
[
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 124 @negative-quarter-top-right
[
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 125 @negative-quarter-bottom-right
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
],
// 126 @negative-quarter-bottom-left
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
],
// 127 @bold-full
[
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [0,0,1,1,0,0,1,1,],
  [0,0,1,1,0,0,1,1,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [0,0,1,1,0,0,1,1,],
  [0,0,1,1,0,0,1,1,],
],
// 128 @bold-square
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 129 @bold-circle
[
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 130 @bold-diamond
[
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 131 @top-left-circle-bold-line
[
  [0,0,0,0,0,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,1,1,1,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,1,1,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 132 @top-right-circle-bold-line
[
  [1,1,1,0,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,0,1,1,0,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 133 @bottom-right-circle-bold-line
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,1,1,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,0,0,0,0,0,],
],
// 134 @bottom-left-circle-bold-line
[
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [0,1,1,0,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,1,1,1,],
],
// 135 @bold-ascent
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 136 @bold-descent
[
  [1,1,0,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 137 @bold-X
[
  [1,1,0,0,0,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 138 @bold-left-pointing
[
  [1,1,0,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 139 @bold-top-pointing
[
  [1,1,0,0,0,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 140 @bold-right-pointing
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 141 @bold-bottom-pointing
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 142 @bold-top-left-pointing
[
  [1,1,0,0,0,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 143 @bold-top-right-pointing
[
  [1,1,0,0,0,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 144 @bold-bottom-right-pointing
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,1,1,1,],
  [0,0,0,0,1,1,1,0,],
  [0,0,0,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 145 @bold-bottom-left-pointing
[
  [1,1,0,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [0,1,1,1,0,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 146 @bold-line-left
[
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 147 @bold-line-vertical-sides
[
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 148 @bold-line-top-left
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 149 @bold-line-bottom-left
[
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 150 @bold-line-top-vertical-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
],
// 151 @bold-line-bottom-vertical-sides
[
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,0,0,0,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 152 @bold-line-top
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 153 @bold-line-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 154 @bold-line-top-right
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 155 @bold-line-right-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 156 @bold-line-left-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 157 @bold-line-right
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 158 @bold-line-bottom-right
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 159 @bottom-bold-line
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
],
// 160 @horizontal-bold-line
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 161 @vertical-bold-line
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 162 @bold-cross
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 163 @bold-path-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 164 @bold-path-top-left
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 165 @bold-path-top-horizontal
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 166 @bold-path-top-vertical
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 167 @bold-path-top-right
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 168 @bold-path-top
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 169 @bold-path-right-vertical
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 170 @bold-path-bottom-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 171 @bold-path-bottom-horizontal
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 172 @bold-path-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 173 @bold-path-bottom
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 174 @bold-path-bottom-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 175 @bold-dot
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 176 @bold-dot-top-left
[
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 177 @bold-dot-top-right
[
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 178 @bold-dot-bottom-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,1,1,],
  [0,0,0,0,0,0,1,1,],
],
// 179 @bold-dot-bottom-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
  [1,1,0,0,0,0,0,0,],
],
// 180 @thin-full
[
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
],
// 181 @thin-square
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,],
],
// 182 @thin-circle
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
],
// 183 @thin-diamond
[
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
],
// 184 @top-left-circle-thin-line
[
  [0,0,0,0,0,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 185 @top-right-circle-thin-line
[
  [1,1,1,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
],
// 186 @bottom-right-circle-thin-line
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,0,0,0,0,0,],
],
// 187 @bottom-left-circle-thin-line
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,1,1,1,],
],
// 188 @thin-ascents
[
  [0,0,0,1,0,0,0,1,],
  [0,0,1,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,1,],
  [0,0,1,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
],
// 189 @thin-descents
[
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,1,0,0,0,1,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,1,0,0,0,1,],
],
// 190 @thin-ascent
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 191 @thin-descent
[
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,],
],
// 192 @thin-X
[
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
],
// 193 @thin-left-pointing
[
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 194 @thin-top-pointing
[
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 195 @thin-right-pointing
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,],
],
// 196 @thin-bottom-pointing
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
],
// 197 @thin-top-left-pointing
[
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 198 @thin-top-right-pointing
[
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,],
],
// 199 @thin-bottom-right-pointing
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
],
// 200 @thin-bottom-left-pointing
[
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
],
// 201 @thin-line-left
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 202 @thin-line-vertical-sides
[
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
],
// 203 @thin-line-top-left
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 204 @thin-line-bottom-left
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
],
// 205 @thin-line-top-vertical-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
],
// 206 @thin-line-bottom-vertical-sides
[
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,],
],
// 207 @thin-line-top
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 208 @thin-line-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
],
// 209 @thin-line-top-right
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
],
// 210 @thin-line-right-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,],
],
// 211 @thin-line-left-horizontal-sides
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
],
// 212 @thin-line-right
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
],
// 213 @thin-line-bottom-right
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,],
],
// 214 @thin-line-bottom
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
],
// 215 @horizontal-thin-line
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 216 @vertical-thin-line
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 217 @thin-cross
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 218 @thin-path-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 219 @thin-path-top-left
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 220 @thin-path-top-horizontal
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 221 @thin-path-top-vertical
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 222 @thin-path-top-right
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 223 @thin-path-top
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 224 @thin-path-right-vertical
[
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 225 @thin-path-bottom-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 226 @thin-path-bottom-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 227 @thin-path-bottom-horizontal
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 228 @thin-path-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 229 @thin-path-bottom
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
],
// 230 @thin-dot
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 231 @thin-dot-top-left
[
  [1,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 232 @thin-dot-top-right
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 233 @thin-dot-bottom-right
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,1,],
],
// 234 @thin-dot-bottom-left
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
// 235 @full-thin-dot
[
  [1,0,1,0,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,1,0,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,1,0,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,1,0,1,0,1,0,],
  [0,0,0,0,0,0,0,0,],
],
// 236 @partial-thin-dot
[
  [1,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
// 237 @small-square
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 238 @medium-square
[
[0,0,0,0,0,0,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,0,0,0,0,0,0,],
],
// 239 @small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 240 @top-left-small-circle
[
[0,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 241 @top-right-small-circle
[
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 242 @bottom-right-small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,0,],
],
// 243 @bottom-left-small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
],
// 244 @medium-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 245 @left-circle
[
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 246 @right-circle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 247 @top-circle
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 248 @bottom-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 249 @top-left-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
],
// 250 @top-right-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
],
// 251 @bottom-right-circle-quarter
[
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 252 @bottom-left-circle-quarter
[
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 253 @top-left-low-big-circle-part
[
[0,0,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 254 @top-left-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
],
// 255 @top-left-high-big-circle-part
[
[0,0,0,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 256 @top-right-high-big-circle-part
[
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 257 @top-right-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,1,0,0,],
],
// 258 @top-right-low-big-circle-part
[
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 259 @bottom-right-low-big-circle-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,0,0,],
],
// 260 @bottom-right-medium-big-circle-part
[
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 261 @bottom-right-high-big-circle-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,0,0,0,],
],
// 262 @bottom-left-high-big-circle-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
],
// 263 @bottom-left-medium-big-circle-part
[
[0,0,1,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 264 @bottom-left-low-big-circle-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
],
// 265 @top-left-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 266 @top-left-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 267 @top-right-high-big-horizontal-diamond-part
[
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 268 @top-right-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,1,1,],
],
// 269 @bottom-right-low-big-horizontal-diamond-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 270 @bottom-right-high-big-horizontal-diamond-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 271 @bottom-left-high-big-horizontal-diamond-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 272 @bottom-left-low-big-horizontal-diamond-part
[
[1,1,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 273 @top-left-low-big-vertical-diamond-part
[
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 274 @top-left-high-big-vertical-diamond-part
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
],
// 275 @top-right-high-big-vertical-diamond-part
[
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
],
// 276 @top-right-low-big-vertical-diamond-part
[
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 277 @bottom-right-low-big-vertical-diamond-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
],
// 278 @bottom-right-high-big-vertical-diamond-part
[
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 279 @bottom-left-high-big-vertical-diamond-part
[
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
],
// 280 @bottom-left-low-big-vertical-diamond-part
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
],
// 281 @medium-square
[
[0,0,0,0,0,0,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,0,0,0,0,0,0,],
],
// 282 @bold-medium-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 283 @bold-left-circle
[
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,1,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 284 @bold-right-medium-circle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,1,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 285 @bold-top-medium-circle
[
[1,1,0,0,0,0,1,1,],
[1,1,1,0,0,1,1,1,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 286 @bold-bottom-medium-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[1,1,1,0,0,1,1,1,],
[1,1,0,0,0,0,1,1,],
],
// 287 @bold-top-left-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,0,],
[0,0,0,0,1,1,0,0,],
],
// 288 @bold-top-right-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,1,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
],
// 289 @bold-bottom-right-circle-quarter
[
[0,0,1,1,0,0,0,0,],
[0,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 290 @bold-bottom-left-circle-quarter
[
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,1,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 291 @bold-top-left-low-big-circle-part
[
[0,0,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 292 @bold-top-left-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,0,0,0,0,],
],
// 293 @bold-top-left-high-big-circle-part
[
[0,0,0,1,1,1,1,1,],
[0,1,1,1,1,1,1,1,],
[1,1,1,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 294 @bold-top-right-high-big-circle-part
[
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,1,1,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 295 @bold-top-right-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,1,1,0,0,],
],
// 296 @bold-top-right-low-big-circle-part
[
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 297 @bold-bottom-right-low-big-circle-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,0,0,],
],
// 298 @bold-bottom-right-medium-big-circle-part
[
[0,0,0,0,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 299 @bold-bottom-right-high-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,1,1,1,],
[1,1,1,1,1,1,1,0,],
[1,1,1,1,1,0,0,0,],
],
// 300 @bold-bottom-left-high-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,1,1,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
],
// 301 @bold-bottom-left-medium-big-circle-part
[
[0,0,1,1,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 302 @bold-bottom-left-low-big-circle-part
[
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
],
// 303 @bold-top-left-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
],
// 304 @bold-top-left-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 305 @bold-top-right-high-big-horizontal-diamond-part
[
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 306 @bold-top-right-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
],
// 307 @bold-bottom-right-low-big-horizontal-diamond-part
[
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 308 @bold-bottom-right-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 309 @bold-bottom-left-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 310 @bold-bottom-left-low-big-horizontal-diamond-part
[
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 311 @bold-top-left-low-big-vertical-diamond-part
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 312 @bold-top-left-high-big-vertical-diamond-part
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
],
// 313 @bold-top-right-high-big-vertical-diamond-part
[
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
],
// 314 @bold-top-right-low-big-vertical-diamond-part
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 315 @bold-bottom-right-low-big-vertical-diamond-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 316 @bold-bottom-right-high-big-vertical-diamond-part
[
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 317 @bold-bottom-left-high-big-vertical-diamond-part
[
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
],
// 318 @bold-bottom-left-low-big-vertical-diamond-part
[
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 319 @thin-small-square
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 320 @thin-medium-square
[
[0,0,0,0,0,0,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,0,0,0,0,0,0,],
],
// 321 @thin-small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 322 @thin-top-left-small-circle
[
[0,1,1,0,0,0,0,0,],
[1,0,0,1,0,0,0,0,],
[1,0,0,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 323 @thin-top-right-small-circle
[
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,0,0,1,],
[0,0,0,0,1,0,0,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 324 @thin-bottom-right-small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,0,0,1,],
[0,0,0,0,1,0,0,1,],
[0,0,0,0,0,1,1,0,],
],
// 325 @thin-bottom-left-small-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,0,0,1,0,0,0,0,],
[1,0,0,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
],
// 326 @thin-medium-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 327 @thin-left-circle
[
[1,1,0,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 328 @thin-right-circle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,0,1,1,],
],
// 329 @thin-top-circle
[
[1,0,0,0,0,0,0,1,],
[1,0,0,0,0,0,0,1,],
[0,1,0,0,0,0,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 330 @thin-bottom-circle
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,0,0,0,0,1,0,],
[1,0,0,0,0,0,0,1,],
[1,0,0,0,0,0,0,1,],
],
// 331 @thin-top-left-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
],
// 332 @thin-top-right-circle-quarter
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
],
// 333 @thin-bottom-right-circle-quarter
[
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 334 @thin-bottom-left-circle-quarter
[
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 335 @thin-top-left-low-big-circle-part
[
[0,0,1,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 336 @thin-top-left-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
],
// 337 @thin-top-left-high-big-circle-part
[
[0,0,0,1,1,1,1,1,],
[0,1,1,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 338 @thin-top-right-high-big-circle-part
[
[1,1,1,1,1,0,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 339 @thin-top-right-medium-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,0,0,],
],
// 340 @thin-top-right-low-big-circle-part
[
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
],
// 341 @thin-bottom-right-low-big-circle-part
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,1,0,0,],
],
// 342 @thin-bottom-right-medium-big-circle-part
[
[0,0,0,0,0,1,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 343 @thin-bottom-right-high-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,1,1,0,],
[1,1,1,1,1,0,0,0,],
],
// 344 @thin-bottom-left-high-big-circle-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,1,1,0,0,0,0,0,],
[0,0,0,1,1,1,1,1,],
],
// 345 @thin-bottom-left-medium-big-circle-part
[
[0,0,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,1,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 346 @thin-bottom-left-low-big-circle-part
[
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
],
// 347 @thin-top-left-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 348 @thin-top-left-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 349 @thin-top-right-high-big-horizontal-diamond-part
[
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 350 @thin-top-right-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
],
// 351 @thin-bottom-right-low-big-horizontal-diamond-part
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 352 @thin-bottom-right-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 353 @thin-bottom-left-high-big-horizontal-diamond-part
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
],
// 354 @thin-bottom-left-low-big-horizontal-diamond-part
[
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 355 @thin-top-left-low-big-vertical-diamond-part
[
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 356 @thin-top-left-high-big-vertical-diamond-part
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
],
// 357 @thin-top-right-high-big-vertical-diamond-part
[
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
],
// 358 @thin-top-right-low-big-vertical-diamond-part
[
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
],
// 359 @thin-bottom-right-low-big-vertical-diamond-part
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
],
// 360 @thin-bottom-right-high-big-vertical-diamond-part
[
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 361 @thin-bottom-left-high-big-vertical-diamond-part
[
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,1,0,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,1,0,],
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,0,1,],
],
// 362 @thin-bottom-left-low-big-vertical-diamond-part
[
[1,0,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,1,0,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
],
// 363 @left-triangle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,0,1,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 364 @top-triangle
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
],
// 365 @right-triangle
[
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 366 @bottom-triangle
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,0,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 367 @bold-left-triangle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 368 @bold-top-triangle
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[1,1,0,0,0,0,1,1,],
[1,1,0,0,0,0,1,1,],
],
// 369 @bold-right-triangle
[
[1,1,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,1,1,1,1,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 370 @bold-bottom-triangle
[
[1,1,0,0,0,0,1,1,],
[1,1,0,0,0,0,1,1,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 371 @thin-left-triangle
[
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
],
// 372 @thin-top-triangle
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,1,0,0,1,0,0,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[1,0,0,0,0,0,0,1,],
[1,0,0,0,0,0,0,1,],
],
// 373 @thin-right-triangle
[
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
],
// 374 @thin-bottom-triangle
[
[1,0,0,0,0,0,0,1,],
[1,0,0,0,0,0,0,1,],
[0,1,0,0,0,0,1,0,],
[0,1,0,0,0,0,1,0,],
[0,0,1,0,0,1,0,0,],
[0,0,1,0,0,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 375 @left-half-diamond
[
[1,0,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,0,0,0,0,0,0,0,],
],
// 376 @top-half-diamond
[
[1,1,1,1,1,1,1,1,],
[0,1,1,1,1,1,1,0,],
[0,0,1,1,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 377 @right-half-diamond
[
[0,0,0,0,0,0,0,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,0,1,],
],
// 378 @bottom-half-diamond
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,1,1,0,0,],
[0,1,1,1,1,1,1,0,],
[1,1,1,1,1,1,1,1,],
],
// 379 @top-left-rounded-corner
[
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 380 @top-right-rounded-corner
[
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 381 @bottom-right-rounded-corner
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
[0,0,0,1,1,1,1,1,],
],
// 382 @bottom-left-rounded-corner
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
[1,1,1,1,1,0,0,0,],
],
// 383 @bold-top-left-rounded-path
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,1,1,1,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 384 @bold-top-right-rounded-path
[
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,1,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 385 @bold-bottom-right-rounded-path
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,1,1,1,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 386 @bold-bottom-left-rounded-path
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,1,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
[0,0,0,1,1,0,0,0,],
],
// 387 @thin-top-left-rounded-path
[
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,1,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 388 @thin-top-right-rounded-path
[
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 389 @thin-bottom-right-rounded-path
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,1,1,1,],
[0,0,0,0,1,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
],
// 390 @thin-bottom-left-rounded-path
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,1,0,0,0,0,0,],
[0,0,0,1,0,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
[0,0,0,0,1,0,0,0,],
],
// 391 @bold-vertical-lines
[
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
],
// 392 @bold-horizontal-lines
[
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 393 @thin-vertical-lines
[
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
[1,0,1,0,1,0,1,0,],
],
// 394 @thin-horizontal-lines
[
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
],
// 395 @thin-left-shade
[
[1,0,1,0,0,0,0,0,],
[0,1,0,1,0,0,0,0,],
[1,0,1,0,0,0,0,0,],
[0,1,0,1,0,0,0,0,],
[1,0,1,0,0,0,0,0,],
[0,1,0,1,0,0,0,0,],
[1,0,1,0,0,0,0,0,],
[0,1,0,1,0,0,0,0,],
],
// 396 @thin-top-shade
[
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 397 @thin-right-shade
[
[0,0,0,0,1,0,1,0,],
[0,0,0,0,0,1,0,1,],
[0,0,0,0,1,0,1,0,],
[0,0,0,0,0,1,0,1,],
[0,0,0,0,1,0,1,0,],
[0,0,0,0,0,1,0,1,],
[0,0,0,0,1,0,1,0,],
[0,0,0,0,0,1,0,1,],
],
// 398 @thin-bottom-shade
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
],
// 399 @bold-left-shade
[
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[1,1,0,0,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
[0,0,1,1,0,0,0,0,],
],
// 400 @bold-top-shade
[
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[0,0,1,1,0,0,1,1,],
[0,0,1,1,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 401 @bold-right-shade
[
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,1,1,0,0,],
[0,0,0,0,0,0,1,1,],
[0,0,0,0,0,0,1,1,],
],
// 402 @bold-bottom-shade
[
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[0,0,1,1,0,0,1,1,],
[0,0,1,1,0,0,1,1,],
],
// 403 @thin-vertical-waves
[
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
],
// 404 @thin-horizontal-waves
[
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,0,1,0,1,0,1,0,],
[0,1,0,1,0,1,0,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 405 @bold-vertical-waves
[
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
],
// 406 @bold-horizontal-waves
[
[1,0,1,0,1,0,1,0,],
[1,1,1,1,1,1,1,1,],
[0,1,0,1,0,1,0,1,],
[0,0,0,0,0,0,0,0,],
[1,0,1,0,1,0,1,0,],
[1,1,1,1,1,1,1,1,],
[0,1,0,1,0,1,0,1,],
[0,0,0,0,0,0,0,0,],
],
// 407 @thin-vertical-medium-waves
[
[1,0,0,0,1,0,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[0,1,0,0,0,1,0,0,],
[1,0,0,0,1,0,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[0,1,0,0,0,1,0,0,],
],
// 408 @thin-horizontal-medium-waves
[
[1,1,0,0,1,1,0,0,],
[0,0,1,1,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,1,1,0,0,],
[0,0,1,1,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 409 @bold-vertical-medium-waves
[
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
],
// 410 @bold-horizontal-medium-waves
[
[1,1,0,0,1,1,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,1,1,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,1,1,0,0,1,1,],
[0,0,0,0,0,0,0,0,],
],
// 411 @thin-vertical-large-waves
[
[1,0,0,0,1,0,0,0,],
[1,0,0,0,1,0,0,0,],
[1,0,0,0,1,0,0,0,],
[1,0,0,0,1,0,0,0,],
[0,1,0,0,0,1,0,0,],
[0,1,0,0,0,1,0,0,],
[0,1,0,0,0,1,0,0,],
[0,1,0,0,0,1,0,0,],
],
// 412 @thin-horizontal-large-waves
[
[1,1,1,1,0,0,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[0,0,0,0,0,0,0,0,],
],
// 413 @bold-vertical-large-waves
[
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[1,1,0,0,1,1,0,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
[0,1,1,0,0,1,1,0,],
],
// 414 @bold-horiontal-large-waves
[
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
[1,1,1,1,0,0,0,0,],
[1,1,1,1,1,1,1,1,],
[0,0,0,0,1,1,1,1,],
[0,0,0,0,0,0,0,0,],
],
// 415 @ô
[
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
],
/*
// 108 @left-half-block
[
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],
],
// 109 @top-half-block
[
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],
],
// 102 @right-half-block
[
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],
],
// 103 @bottom-half-block
[
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],
],
// 104 @top-left-quarter-block
[
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],
],
// 105 @top-right-quarter-block
[
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],
],
// 106 @bottom-right-quarter-block
[
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
],
// 107 @bottom-left-quarter-block
[
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [0,0,0,0,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
],
// 108 @two-quarter-half-block
[
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
],
// 109 @top-left-diamond
[
  [0,0,0,0,0,0,0,1,],,
  [0,0,0,0,0,0,1,1,],,
  [0,0,0,0,0,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,1,1,1,1,1,],,
  [0,0,1,1,1,1,1,1,],,
  [0,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],
],
// 110 @top-right-diamond 
[
  [1,0,0,0,0,0,0,0,],,
  [1,1,0,0,0,0,0,0,],,
  [1,1,1,0,0,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,1,1,0,0,0,],,
  [1,1,1,1,1,1,0,0,],,
  [1,1,1,1,1,1,1,0,],,
  [1,1,1,1,1,1,1,1,],
],
// 111 @bottom-left-diamond
[
  [1,1,1,1,1,1,1,1,],,
  [0,1,1,1,1,1,1,1,],,
  [0,0,1,1,1,1,1,1,],,
  [0,0,0,1,1,1,1,1,],,
  [0,0,0,0,1,1,1,1,],,
  [0,0,0,0,0,1,1,1,],,
  [0,0,0,0,0,0,1,1,],,
  [0,0,0,0,0,0,0,1,],
],
// 112 @bottom-right-diamond
[
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,0,],,
  [1,1,1,1,1,1,0,0,],,
  [1,1,1,1,1,0,0,0,],,
  [1,1,1,1,0,0,0,0,],,
  [1,1,1,0,0,0,0,0,],,
  [1,1,0,0,0,0,0,0,],,
  [1,0,0,0,0,0,0,0,],
],
// 113 @top-left-circle
[
  [0,0,0,0,0,1,1,1,],,
  [0,0,0,1,1,1,1,1,],,
  [0,0,1,1,1,1,1,1,],,
  [0,1,1,1,1,1,1,1,],,
  [0,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],,
  [1,1,1,1,1,1,1,1,],
],
// 114 @top-right-circle
[
  [1,1,1,0,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,]
],
// 115 @bottom-left-circle
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,1,],
  [0,0,1,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,1,1,1,]
],
// 116 @bottom-right-circle
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,0,0,0,0,0,]
],
// 117 @descent-diag
[
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,]
],
// 118 @ascent-diag
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,]
],
// 119 @top-left-circle-outline
[
  [0,0,0,0,0,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,]
],
// 120 @top-right-circle-outline 
[
  [1,1,1,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,]
],
// 121 @bottom-right-circle-outline 
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,0,1,0,],
  [0,0,0,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,0,0,0,0,0,]
],
// 122 @bottom-left-circle-outline 
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,1,0,0,0,0,0,0,],
  [0,0,1,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,1,1,1,]
],
// 123 @outline-block
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,]
],
// 124 @left-line
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,]
],
// 125 @top-line
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 126 @right-line
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,]
],
// 127 @bottom-line
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,]
],
// 128 @top-left-line-corner 
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,]
],
// 129 @top-right-line-corner 
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,]
],
// 130 @bottom-right-line-corner 
[
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,]
],
// 131 @bottom-left-line-corner 
[
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,]
],
// 132 @left-line-deadend
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,]
],
// 133 @top-line-deadend
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,]
],
// 134 @right-line-deadend
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [0,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,]
],
// 135 @bottom-line-deadend
[
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,]
],
// 136 @vertical-double-line
[
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,]
],
// 137 @horizontal-double-line
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,]
],
// 138 @square
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,]
],
// 139 @small-square
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 140 @outline-square
[
  [0,0,0,0,0,0,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,0,0,0,0,0,0,]
],
// 141 @outline-small-square
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 142 @maze
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,1,1,1,1,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,1,1,1,1,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,1,1,1,1,1,1,1,]
],
// 143 @circle
[
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,]
],
// 144 @medium-circle
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 145 @small-circle
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 146 @centered-dot
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 147 @outline-circle
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,]
],
// 148 @outline-medium-circle
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 149 @outline-small-circle
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 150 @bubble
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,1,1,0,0,1,],
  [1,0,1,1,1,1,0,1,],
  [1,0,1,1,1,1,0,1,],
  [1,0,0,1,1,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,]
],
// 151 @outline-bubble
[
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [1,0,0,1,1,0,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,0,1,1,0,0,1,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,]
],
// 152 @medium-bubble
[
  [0,0,0,0,0,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,0,0,0,0,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,0,1,1,0,1,0,],
  [0,1,0,0,0,0,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 153 @horizontal-double-line
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 154 @vertical-double-line
[
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,]
],
// 155 @crossed-double-line
[
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,0,1,1,0,0,],
  [1,1,0,0,1,1,0,0,]
],
// 156 @horizontal-lines
[
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,]
],
// 157 @vertical-lines
[
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,],
  [1,0,1,0,1,0,1,0,]
],
// 158 @crossed-lines
[
  [1,1,1,1,1,1,1,1,],
  [1,0,1,0,1,0,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,0,1,0,1,0,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,0,1,0,1,0,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,0,1,0,1,0,1,0,]
],
// 159 @ascent-lines
[
  [0,0,0,1,0,0,0,1,],
  [0,0,1,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,0,0,1,0,0,0,1,],
  [0,0,1,0,0,0,1,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,]
],
// 160 @descent-lines
[
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,1,0,0,0,1,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,1,0,0,0,1,]
],
// 161 @half-pattern
[
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,]
],
// 162 @light-pattern
[
  [1,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,1,0,0,0,1,0,],
  [0,0,0,0,0,0,0,0,]
],
// 163 @odd-horizontal-lines
[
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,0,0,0,0,],
  [0,0,0,0,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 164 @odd-vertical-lines
[
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,]
],
// 165 @horizontal-waves
[
  [1,1,0,0,1,1,0,0,],
  [0,0,1,1,0,0,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,0,0,1,1,0,0,],
  [0,0,1,1,0,0,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 166 @vertical-waves
[
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [0,1,0,0,0,1,0,0,]
],
// 167 @horiontal-stitches
[
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,0,1,0,1,0,1,0,],
  [0,1,0,1,0,1,0,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 168 @vertical-stitches
[
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,],
  [1,0,0,0,1,0,0,0,],
  [0,1,0,0,0,1,0,0,]
],
// 169 @vertical-path 
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 170 @horizontal-path
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 171 @crossed-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 172 @left-top-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 173 @left-top-right-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 174 @top-right-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,]
],
// 175 @top-right-bottom-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 176 @right-bottom-path
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 177 @right-bottom-left-path
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 178 @bottom-left-path
[
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
// 179 @bottom-left-top-path
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [1,1,1,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,]
],
/*
[
  [0,0,0,1,1,0,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,0,0,1,1,0,],
  [0,1,1,0,0,1,1,0,],
],
[
  [0,1,1,0,0,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
  [0,0,1,1,1,1,0,0,],
  [0,0,0,1,1,0,0,0,],
],
[
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,0,0,1,1,0,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,1,1,1,0,0,],
],
[
  [0,1,1,0,0,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,1,1,0,1,1,],
  [0,1,1,0,0,1,1,0,],
  [0,1,1,0,0,1,1,0,],
  [1,1,0,1,1,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,0,0,1,1,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,0,0,1,0,0,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [1,1,1,1,1,1,1,1,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
  [1,0,1,0,0,0,1,0,],
  [1,0,0,0,0,0,0,0,],
],
[
  [0,1,1,1,1,1,1,0,],
  [1,0,0,0,0,0,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,0,1,1,0,0,1,],
  [1,0,0,0,0,0,0,1,],
  [0,1,1,1,1,1,1,0,],
],
[
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,1,1,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,1,1,0,1,1,],
  [1,1,1,0,0,1,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
],
[
  [0,1,1,1,1,1,1,0,],
  [1,0,0,0,0,0,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,0,0,0,0,0,1,],
  [1,0,0,1,1,0,0,1,],
  [1,0,1,0,0,1,0,1,],
  [1,0,0,0,0,0,0,1,],
  [0,1,1,1,1,1,1,0,],
],
[
  [0,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,1,],
  [1,1,0,1,1,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [1,1,1,0,0,1,1,1,],
  [1,1,0,1,1,0,1,1,],
  [1,1,1,1,1,1,1,1,],
  [0,1,1,1,1,1,1,0,],
],
[
  [0,0,1,1,1,1,0,0,],
  [0,1,1,1,0,0,1,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [1,1,1,0,0,0,0,0,],
  [0,1,1,1,0,0,1,0,],
  [0,0,1,1,1,1,0,0,]
],
[
  [0,0,1,0,0,1,0,0,],
  [0,0,0,1,1,0,0,0,],
  [1,0,1,1,1,1,0,1,],
  [0,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,1,0,],
  [1,0,1,1,1,1,0,1,],
  [0,0,0,1,1,0,0,0,],
  [0,0,1,0,0,1,0,0,]
],
[
  [0,0,1,1,1,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [1,1,1,1,1,1,1,0,],
  [1,1,1,1,1,1,1,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
[
  [0,1,1,0,1,1,0,0,],
  [1,0,1,1,1,0,1,0,],
  [0,1,1,1,1,1,0,0,],
  [1,0,0,1,0,0,1,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
],
[
  [0,0,0,1,0,0,0,0,],
  [0,0,1,1,1,0,0,0,],
  [0,0,0,1,0,0,0,0,],
  [0,1,1,1,1,1,0,0,],
  [0,0,0,1,0,0,0,0,],
  [1,1,1,1,1,1,1,0,],
  [0,0,0,1,0,0,0,0,],
  [0,0,0,0,0,0,0,0,],
]
],
 CHARACTERS
[
 Right walker
  [
    [0,0,0,0,1,1,0,0,],
    [0,0,0,0,1,1,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,1,0,1,1,1,0,0,],
    [1,0,1,1,0,0,1,0,],
    [0,0,1,1,1,0,0,0,],
    [0,0,1,0,0,1,0,0,],
    [0,1,0,0,0,1,0,0,],
  ],
 Left walker
  [
    [0,1,1,0,0,0,0,0,],
    [0,1,1,0,0,0,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,1,1,1,0,1,0,0,],
    [1,0,0,1,1,0,1,0,],
    [0,0,1,1,1,0,0,0,],
    [0,1,0,0,1,0,0,0,],
    [0,1,0,0,0,1,0,0,],
  ],
 Right standing
  [
    [0,0,0,0,1,1,0,0,],
    [0,0,0,0,1,1,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,1,1,1,1,0,0,0,],
    [0,1,1,1,1,1,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,0,1,0,1,0,0,0,],
    [0,0,1,0,1,0,0,0,],
  ],
 Left standing
  [
    [0,1,1,0,0,0,0,0,],
    [0,1,1,0,0,0,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,0,1,1,1,1,0,0,],
    [0,1,1,1,1,1,0,0,],
    [0,0,1,1,1,0,0,0,],
    [0,0,1,0,1,0,0,0,],
    [0,0,1,0,1,0,0,0,],
  ],
]
   */ 
]


// generate_colors

let colors = [];

function add_color(r,v,b) {
  colors.push([r*64-(r>0?1:0),v*64-(v>0?1:0),b*64-(b>0?1:0),255]);
}

// GRAY
let r = 0;
let v = 0;
let b = 0;
while (r < 5) {
  add_color(r,v,b);
  r++;
  v++;
  b++;
}
// RED
r = 1;
v = 0;
b = 0;
while (r < 5) {
  add_color(r,v,b);
  r++;
}
r = 4;
v = 1;
b = 1;
while (v < 4) {
  add_color(r,v,b);
  v++;
  b++;
}
// GREEN
r = 0;
v = 1;
b = 0;
while (v < 5) {
  add_color(r,v,b);
  v++;
}
r = 1;
v = 4;
b = 1;
while (r < 4) {
  add_color(r,v,b);
  r++;
  b++;
}
// BLUE
r = 0;
v = 0;
b = 1;
while (b < 5) {
  add_color(r,v,b);
  b++;
}
r = 1;
v = 1;
b = 4;
while (r < 4) {
  add_color(r,v,b);
  r++;
  v++;
}
// YELLOW
r = 1;
v = 1;
b = 0;
while (r < 5) {
  add_color(r,v,b);
  r++;
  v++;
}
r = 4;
v = 4;
b = 1;
while (b < 4) {
  add_color(r,v,b);
  b++;
}
// CYAN
r = 0;
v = 1;
b = 1;
while (v < 5) {
  add_color(r,v,b);
  v++;
  b++;
}
r = 1;
v = 4;
b = 4;
while (r < 4) {
  add_color(r,v,b);
  r++;
}
// MAGENTA
r = 1;
v = 0;
b = 1;
while (r < 5) {
  add_color(r,v,b);
  r++;
  b++;
}
r = 4;
v = 1;
b = 4;
while (v < 4) {
  add_color(r,v,b);
  v++;
}
// ORANGE
r = 2;
v = 1;
b = 0;
while (r < 5) {
  add_color(r,v,b);
  r+=2;
  v++;
}
r = 4;
v = 3;
b = 1;
while (v < 4) {
  add_color(r,v,b);
  v++;
  b++;
}
// PINK
r = 2;
v = 0;
b = 1;
while (r < 5) {
  add_color(r,v,b);
  r+=2;
  b++;
}
r = 4;
v = 1;
b = 3;
while (b < 4) {
  add_color(r,v,b);
  v++;
  b++;
}
// LIME
r = 1;
v = 2;
b = 0;
while (v < 5) {
  add_color(r,v,b);
  v+=2;
  r++;
}
r = 3;
v = 4;
b = 1;
while (r < 4) {
  add_color(r,v,b);
  r++;
  b++;
}
// EMERALD
r = 0;
v = 2;
b = 1;
while (v < 5) {
  add_color(r,v,b);
  v+=2;
  b++;
}
r = 1;
v = 4;
b = 3;
while (b < 4) {
  add_color(r,v,b);
  r++;
  b++;
}
// PURPLE
r = 1;
v = 0;
b = 2;
while (b < 5) {
  add_color(r,v,b);
  b+=2;
  r++;
}
r = 3;
v = 1;
b = 4;
while (r < 4) {
  add_color(r,v,b);
  r++;
  v++;
}
// BLUESKY
r = 0;
v = 1;
b = 2;
while (b < 5) {
  add_color(r,v,b);
  b+=2;
  v++;
}
r = 1;
v = 3;
b = 4;
while (v < 4) {
  add_color(r,v,b);
  r++;
  v++;
}

let character_set = [
// ASCII
" ",'!','"','#','$','%','&','\\','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\'',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}','~',
// CUSTOM
'é','è','ç','à','§'
];

let numbers = ['&','é','"','\'','(','-','è','_','ç','à',')','='];

// AZERTY
let azerty = [
  '&',  'é',  '"', '\'',  '(',  '-',  'è',  '_',  'ç',  'à',
  'a',  'z',  'e',  'r',  't',  'y',  'u',  'i',  'o',  'p',
  'q',  's',  'd',  'f',  'g',  'h',  'j',  'k',  'l',  'm',
  'w',  'x',  'c',  'v',  'b',  'n',  ',',  ';',  ':',  '!',

  '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
  'A',  'Z',  'E',  'R',  'T',  'Y',  'U',  'I',  'O',  'P',
  'Q',  'S',  'D',  'F',  'G',  'H',  'J',  'K',  'L',  'M',
  'W',  'X',  'C',  'V',  'B',  'N',  '?',  '.',  '/',  '§'
];

// Aube AZERTY
let aube_azerty = [
  '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
  'A',  'Z',  'E',  'R',  'T',  'Y',  'U',  'I',  'O',  'P',
  'Q',  'S',  'D',  'F',  'G',  'H',  'J',  'K',  'L',  'M',
  'W',  'X',  'C',  'V',  'B',  'N',  '?',  '.',  '/',  '§',

  '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
  'A',  'Z',  'E',  'R',  'T',  'Y',  'U',  'I',  'O',  'P',
  'Q',  'S',  'D',  'F',  'G',  'H',  'J',  'K',  'L',  'M',
  'W',  'X',  'C',  'V',  'B',  'N',  '?',  '.',  '/',  '§'
];


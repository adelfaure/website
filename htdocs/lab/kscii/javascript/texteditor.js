console.log("texteditor.js");

// TEXT

TextEditor = function(max_lines) {
  this.x = 0;
  this.y = 0;
  this.max_lines = max_lines;
  this.lines = [new Line([],[])];
}

TextEditor.prototype.send = function(key) {
  var line = this.lines[this.y];

  // New line
  if (key == "Enter" && this.lines.length < this.max_lines) {
    let new_lines = [];
    for (var i = 0; i < this.lines.length; i++) {
      line = this.lines[i];
      if (i == this.y) {
        let before = [];
        while (l = line.before.shift()) {
          before.push(l);
        }
        let after = [];
        while (l = line.after.shift()) {
          after.push(l);
        }
        new_lines.push(new Line(before,[]));
        new_lines.push(new Line([],after));
      } else {
        new_lines.push(line);
      }
    }
    this.lines = new_lines;
    this.y++;
    this.x = 0;
  
  // Go up
  } else if (key == "ArrowUp" && this.y > 0) {
    let prev_line = this.lines[this.y-1];
    while (prev_line.before.length > this.x && prev_line.before.length) {
      prev_line.after.unshift(prev_line.before.pop());
    }
    while (prev_line.before.length < this.x && prev_line.after.length) {
      prev_line.before.push(prev_line.after.shift());
    }
    this.y--; 
  
  // Go down
  } else if (key == "ArrowDown" && this.y < this.lines.length-1) {
    let next_line = this.lines[this.y+1];
    while (next_line.before.length > this.x && next_line.before.length) {
      next_line.after.unshift(next_line.before.pop());
    }
    while (next_line.before.length < this.x && next_line.after.length) {
      next_line.before.push(next_line.after.shift());
    }
    this.y++; 
  
  // Go start of line
  } else if (key == "Home") {
    line.send(key);
    this.x = 0;
  
  // Delete line
  } else if (key == "Backspace" && line.before.length == 0 && this.y > 0) {
    let new_lines = [];
    for (var i = 0; i < this.lines.length; i++) {
      line = this.lines[i];
      if (i == this.y-1) {
        line.before = line.before.concat(line.after);
        line.after = this.lines[this.y].after;
        new_lines.push(line);
      } else if (i != this.y){
        new_lines.push(line);
      }
    }
    this.lines = new_lines;
    this.y--;

  } else {
    line.send(key);
    this.x = line.before.length;
  }
}
TextEditor.prototype.print = function() {
  let str = '';
  for (var i = 0; i < this.lines.length; i++) {
    str += this.lines[i].before.join('')+this.lines[i].after.join('')+'\n';
  }
  return str;
}

TextEditor.prototype.clear = function(){
  this.lines = [new Line([],[])];
}

TextEditor.prototype.pretty_print = function() {
  let str = '';
  let caret_line = '    ^';
  while (caret_line.length-5 < this.lines[this.y].before.length) {
    caret_line = ' ' + caret_line;
  }
  for (var i = 0; i < this.lines.length; i++) {
    str += (i < 10 ? '  '+i : i < 100 ? ' '+i : i) + ' ' +this.lines[i].before.join('')+this.lines[i].after.join('')+(i == this.y ? '\n'+caret_line : '')+'\n';
  }
  return str+this.x+','+this.y;
}

// LINE

Line = function(before,after) {
  this.before = before;
  this.after = after;
}

Line.prototype.send = function(key) {

  // Type key
  if (key.length == 1) {
    this.before.push(key);

  // Go left
  } else if (key == "ArrowLeft" && this.before.length) {
    this.after.unshift(this.before.pop());

  // Go right
  } else if (key == "ArrowRight" && this.after.length) {
    if (!this.after.length) return;
    this.before.push(this.after.shift());

  // Go start of line
  } else if (key == "Home") {
    while (ch = this.before.pop()) {
      this.after.unshift(ch);
    }

  // Go end of line
  } else if (key == "End") {
    while (ch = this.after.shift()) {
      this.before.push(ch);
    }

  // Delete
  } else if (key == "Backspace") {
    this.before.pop();

  }
}

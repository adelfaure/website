console.log("floodfill.js");

function fill(screen,x,y,dirs,from,to,memory,lifespan) {
  if (
    !screen[y] ||
    !screen[y][x] ||
    screen[y][x] != from && from != 'any' ||
    screen[y][x] == to || 
    memory[x+','+y] != undefined
  ) return false;
  screen[y][x] = to != 'any' ? to : screen[y][x];
  memory[x+','+y] = lifespan;
  let next_dirs = [];
  for (var i = 0; i < dirs.length; i++) {
    next_dirs.push([
      x + dirs[i][0],
      y + dirs[i][1],
    ]);
  }
  return next_dirs;
}
function floodfill(screen,flood,dirs,from,to,lifespan,memory) {
  while ((flood_dirs = flood.shift()) && lifespan) {
    let more_flood = fill(screen,flood_dirs[0],flood_dirs[1],dirs,from,to,memory,lifespan);
    if (more_flood) {
      flood = flood.concat(more_flood);
      lifespan--;
    }
  }
}
function dynfloodfill(screen,flood,dirs,from,to,lifespan,speed,memory) {
  if (lifespan <= 0) return;
  for (var i = 0; i < flood.length; i++) {
    let more_flood = fill(screen,flood[i][0],flood[i][1],dirs,from,to,memory,lifespan);
    if (more_flood) {
      lifespan[0]--;
      setTimeout(function(){
        dynfloodfill(screen,more_flood,dirs,from,to,lifespan,speed,memory);
      },speed);
    }
  }
}
function pathfinder(screen,from,to,limit) {
  let memory = {};
  let flood = [from];
  let step = 0;
  let from_value = screen[from[1]][from[0]];
  let dirs = [[1,0],[-1,0],[0,1],[0,-1]];
  while ((flood_dirs = flood.shift()) && !memory[String(to)] && step < limit) {
    let more_flood = fill(
      screen,
      flood_dirs[0],
      flood_dirs[1],
      dirs,
      from_value,
      "any",
      memory,
      step
    );
    if (more_flood) {
      flood = flood.concat(more_flood);
      step++;
    }
  }
  if (!memory[String(to)]) return false;
  let path = [];
  let min_val = Infinity;
  let val = to;
  while (min_val > 0) {
    let next_val;
    for (var i = 0; i < dirs.length; i++) {
      if (memory[(val[0]+dirs[i][0])+','+(val[1]+dirs[i][1])] < min_val){
        next_val = [(val[0]+dirs[i][0]),(val[1]+dirs[i][1])];
        min_val = memory[(val[0]+dirs[i][0])+','+(val[1]+dirs[i][1])];
      }
    }
    path.push(val);
    val = next_val;
  }
  path.push(from);
  return path;
}

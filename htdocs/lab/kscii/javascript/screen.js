console.log("screen.js (to rename to something else");
let filler = '▒';

function Textscreen(w,h,fill) {
  let screen = [];
  while (screen.length < h) {
    screen.push([]);
    while (screen[screen.length-1].length < w) {
      screen[screen.length-1].push(fill);
    }
  }
  return screen;
}

function string_to_screen(string) {
  let screen = [];
  string = string.split('\n');
  for (var i = 0; i < string.length; i++) {
    screen.push(string[i].split(''));
  }
  return screen;
}

function add_to_screen(parent_screen,child_screen,x,y,interactive_screen,fun) {
  if (!child_screen) return;
  for (var row = 0; row < child_screen.length; row++) {
    if (row + y > parent_screen.length-1
    ||  row + y < 0 ) continue;
    for (var col = 0; col < child_screen[row].length; col++) {
      if (col + x > parent_screen[row + y].length-1
      ||  col + x < 0 
      ||  child_screen[row    ][col    ] == '░'
      ||  parent_screen[ row + y][col + x] != filler ) continue;
      parent_screen[     row + y][col + x] = child_screen[row][col];
      if (interactive_screen) {
        interactive_screen[     row + y][col + x] = fun;
      }
    }
  }
}

function screen_to_string(screen) {
  let string = [];
  for (var i = 0; i < screen.length; i++) {
    string.push(screen[i].join(''));
  }
  return string.join('\n');
}

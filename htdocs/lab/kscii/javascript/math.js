console.log("math.js (rename to utils.js");

// Random
function UR() {
  return Math.random();
}

// Random between
function URB(min,max) {
  return UR() * (max - min) + min;
}

//Random index from array
function URIA(array) {
  return UF(UR()*array.length);
}

// Random from array
function URA(array) {
  return array[URIA(array)];
}

// Pick from array (or delete)
function UPA(array,index) {
  let pick = array.splice(index, 1);
  return pick[0];
}

// Random pick from array
function URPA(array) {
  return UPA(array,URIA(array));
}

// Luck
function UL(difficulty) {
  difficulty = difficulty == undefined ? 0.5 : difficulty;
  return UR() > difficulty;
}

// Shuffle array
function US(array) {
  let new_array = [];
  while (array.length) {
    new_array.push(URPA(array));
  }
  return new_array;
}

// Move array
function UM(array,dir) {
  if (dir) {
    array.push(array.shift());
  } else {
    array.unshift(array.pop());
  }
}

// Floor
function UF(val) {
  return Math.floor(val);
}

// Ceil
function UC(val) {
  return Math.ceil(val);
}

// Random dirs

function URD(min_length,max_length,min_value,max_value) {
  let dirs = [];
  let dirs_length = URB(min_length,max_length);
  while (dirs.length < dirs_length) {
    dirs.push([
      UF(URB(min_value,max_value)),
      UF(URB(min_value,max_value))
    ]);
  }
  return dirs;
}

// Compare
function UCO(from,to) {
  let i = -1;
  while (i++ < to.length-1) {
    if (from[i] != to[i]) return false;
  }
  return true;
}

// Blink
function UB(frame,value) {
  return Number.isInteger(UF(frame/value)/2);
}

// Apply on 
function  UA(x,y,w,h,fn) {
  let yi = -1;
  while (yi++ < h-1) {
    let xi = -1;
    while (xi++ < w-1) {
      fn(x+xi,y+yi);
    }
  }
}

// NEST
UN = function(parent){
  this.parent = parent;
  this.arr = [];
}

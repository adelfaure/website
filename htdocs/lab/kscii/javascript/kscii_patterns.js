///////////////////////////////////////////////

// utils
function _base64ToArrayBuffer(base64) {
  var binary_string = window.atob(base64.split(',')[1]);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

// COMMAND PATTERNS FOR KSCII

// Variables

// Command

function add(a, b) { return a + b; }
function sub(a, b) { return a - b; }

Command = function (execute, undo, value, target, key, matrix) {
  this.execute = execute;
  this.undo = undo;
  this.value = value;
  this.target = target;
  this.key = key;
  this.matrix = matrix;
}

function execute (command) {
  if (command.matrix) {
    command.target.matrix.buffer[command.key] = command.execute(command.target.matrix.buffer[command.key], command.value);
  } else {
    command.target[command.key] = command.execute(command.target[command.key], command.value);
  }
  command.target.commands.push(command);
  log = command.key + "+=" + command.value + ", "+log ;
}

function undo (target) {
  if (!target.commands.length) return false;
  var command = target.commands.pop();
  if (command.matrix) {
    command.target.matrix.buffer[command.key] = command.undo(command.target.matrix.buffer[command.key], command.value);
  } else {
    target[command.key] = command.undo(target[command.key], command.value);
  }
  log = command.key + "-=" + command.value + ", "+log ;
}

// Editor

Kscii = function () {
  this.matrix = new Matrix(32,32,3);
  this.matrix.fullset([0,0,4]);
  this.prompt = new TextEditor(1);
  // TODO PROMPT MODE, PROMPT MEMORY ?
  this.scale = 3; 
  this.x_cursor = UF(screen_matrix.width()/2); 
  this.y_cursor = UF(screen_matrix.height()/2); 
  this.w_cursor = 1; 
  this.h_cursor = 1; 
  this.w_size = 32; 
  this.h_size = 32; 
  this.sheet = 0; 
  this.x_cam = UF(screen_matrix.width()/2); 
  this.y_cam = UF(screen_matrix.height()/2);
  this.fg_color = 0; 
  this.bg_color = 4; 
  this.char_clipboard = 0; 
  this.color_clipboard = 0; 
  this.background_clipboard = 0; 
  this.recording = false;
  this.records = records;
  this.commands = [];
  this.command_codes = [];
}

Kscii.prototype.change_key_val = function(key,value) {
  execute(new Command(add, sub, value - this[key], this, key));
}

Kscii.prototype.change_character_val = function(x,y,key,code) {
  let character = this.matrix.buffer[this.matrix.target(x,y,key)];
  execute(new Command(add, sub, code-character, this, this.matrix.target(x,y,key), true));
}

Kscii.prototype.action = function(command) {
  return command.execute.toString().split(' ')[1].split('(')[0].split('_').join(' ');
}

Kscii.prototype.undo = function() {
  undo(this);
}

Kscii.prototype.resolve_nested_command = function(arr) {
  var to_resolve;
  while(arr.length) {
    let part = arr.shift();
    if (typeof part == "string"){
      this.resolve_command_code(part);
    } else {
      let next_part_bits = arr[0].split(' ');
      let repeat_instructions = next_part_bits.shift().split(',');
      let iterations = repeat_instructions[0];
      let delay = repeat_instructions[1];
      arr[0] = next_part_bits.join(' ');
      while(iterations--){
        let editor = this;
        setTimeout(function(){
          editor.resolve_nested_command(structuredClone(part));
        },delay*iterations);
      }
    }
  }
}

Kscii.prototype.resolve_command_code = function(command_code){
  let function_letters = command_code.split(' ');
  while (command = function_letters.shift()) {
    let command_args = command.split(':');
    // TODO CHANGE THAT
    let option = command_args.length == 1 ? 1 : command_args[1];
    let key = command_args[0];
    let editor = this;
    if(key=="wait") {
      setTimeout(function(){
        editor.command(function_letters.join(' '));
      },Number(option));
      break;
    }
    if(key=="if_char") {
      let code = this.matrix.buffer[this.matrix.target(this.x_cursor,this.y_cursor,0)];
      if (code != Number(option)) break;
      continue;
    }
    if(key=="if_color") {
      let code = this.matrix.buffer[this.matrix.target(this.x_cursor,this.y_cursor,1)];
      if (code != Number(option)) break;
      continue;
    }
    if(key=="if_background") {
      let code = this.matrix.buffer[this.matrix.target(this.x_cursor,this.y_cursor,2)];
      if (code != Number(option)) break;
      continue;
    }
    if(key=="break") {
      break;
    }
    if(key.slice(0,1) == '$') {
      this.command(this.records[key.slice(1)]);
      continue;
    }
    if(option=="screen_w"){
      option = screen_matrix.width()-1;
    }
    if(option=="screen_h"){
      option = screen_matrix.height()-1;
    }
    if(option=="editor_w"){
      option = this.matrix.width()-1;
    }
    if(option=="editor_h"){
      option = this.matrix.height()-1;
    }
    if(option=="editor_color"){
      option = this.fg_color;
    }
    if(option=="editor_background"){
      option = this.bg_color;
    }
    console.log(key,option);
    command_code_functions[key](this,option);
    if (key == "start_recording" || 
        key == "prompt_command"  || 
        key == "stop_recording") continue;
    if (this.recording) {
      if (!this.records[this.recording]) this.records[this.recording] = '';
      this.records[this.recording] +=  key +':'+option+' ';
    }
  }
}

Kscii.prototype.command = function(command_code) {
  let chars = command_code.split('');
  let mother_nest = new UN(false);
  let current_nest = mother_nest;
  let content = '';
  while (c = chars.shift()) {
    if (c == '(') {
      if (content.length) current_nest.arr.push(content);
      content = '';
      let nest = new UN(current_nest);
      current_nest.arr.push(nest.arr);
      current_nest = nest;
    } else if (c == ')') {
      if (content.length) current_nest.arr.push(content);
      content = '';
      current_nest = current_nest.parent;
    } else {
      content += c;
    }
  }
  if (content.length) current_nest.arr.push(content);
  this.resolve_nested_command(mother_nest.arr); 
}


// COMMAND CODE

let command_code_functions = {
  'save' : function(kscii,option){
    option = option == 1 ? 'file' : option;
    let blob = new Blob([kscii.matrix.buffer]);
    let blobUrl = URL.createObjectURL(blob); 
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('download', option+".kscii");
    downloadLink.setAttribute('href', blobUrl);
    downloadLink.click();
  },
  'play' : function(kscii,option){
  },
  'w_size' : function(kscii,option){
  },
  'load' : function(kscii,option){
    let input = document.createElement("input");
    input.type = "file";
    input.addEventListener("change",function(e){
      let file = input.files[0];
      let fr = new FileReader();
      fr.addEventListener("load", function(){
        let temp_editor = new Kscii();
        temp_editor.matrix.buffer = new Uint16Array(_base64ToArrayBuffer(this.result));
        temp_editor.command("select:0,0,"+temp_editor.matrix.width()+','+temp_editor.matrix.height()+" copy:import");
        kscii.records["import"] = temp_editor.records["import"];
        kscii.command("select:0,0,1,1 $import");
     }, false);
     fr.readAsDataURL(file); 
   });
   input.click();
  },
  'pick_color' : function(kscii,option){
    let glyph = kscii.matrix.get(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam);
    kscii.change_key_val("fg_color",glyph[1]);
  },
  'pick_background' : function(kscii,option){
    let glyph = kscii.matrix.get(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam);
    kscii.change_key_val("bg_color",glyph[2]);
  },
  'pick' : function(kscii,option){
    let glyph = kscii.matrix.get(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam);
    kscii.change_key_val("fg_color",glyph[1]);
    kscii.change_key_val("bg_color",glyph[2]);
  },
  'prompt_mode' : function(kscii,option){
    prompt_mode = !prompt_mode;
  },
  'prompt_command' : function(kscii,option){
    log = 'prompt_command:'+kscii.prompt.print().replace('\n','') + ', ' + log;
    kscii.command(kscii.prompt.print().replace('\n',''));
  },
  'reverse' : function(kscii,option) {
    let color = kscii.fg_color;
    let background = kscii.bg_color;
    kscii.change_key_val("fg_color",background);
    kscii.change_key_val("bg_color",color);
  },
  'previous_sheet' : function(kscii,option) {
    if (kscii.sheet <= 0) return;
    kscii.change_key_val("sheet",kscii.sheet-1);
  },
  'next_sheet' : function(kscii,option) {
    if (kscii.sheet >= 7) return;
    kscii.change_key_val("sheet",kscii.sheet+1);
  },
  'sheet' : function(kscii,option) {
    kscii.change_key_val("sheet",Number(option));
  },
  'x_cursor' : function(kscii,option) {
    kscii.change_key_val("x_cursor",Number(option));
  },
  'y_cursor' : function(kscii,option) {
    kscii.change_key_val("y_cursor",Number(option));
  },
  'w_cursor' : function(kscii,option) {
    if (Number(option) < 1) return;
    kscii.change_key_val("w_cursor",Number(option));
  },
  'h_cursor' : function(kscii,option) {
    if (Number(option) < 1) return;
    kscii.change_key_val("h_cursor",Number(option));
  },
  'w_grow' : function(kscii,option) {
    kscii.change_key_val("w_cursor",kscii.w_cursor + Number(option));
  },
  'h_grow' : function(kscii,option) {
    kscii.change_key_val("h_cursor",kscii.h_cursor + Number(option));
  },
  'w_shrink' : function(kscii,option) {
    if (kscii.w_cursor - Number(option) < 1) return;
    kscii.change_key_val("w_cursor",kscii.w_cursor - Number(option));
  },
  'h_shrink' : function(kscii,option) {
    if (kscii.h_cursor - Number(option) < 1) return;
    kscii.change_key_val("h_cursor",kscii.h_cursor - Number(option));
  },
  'copy' : function(kscii,option) {
    option = option == 1 ? "paste" : option;
    kscii.records[option] = '';
    console.log(option);
    let prev_x = -1;
    let prev_y = -1;
    let move_x = 0;
    let move_y = 0;
    let total_move_x = 0;
    let total_move_y = 0;
    UA(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,kscii.w_cursor,kscii.h_cursor,function(x,y){
      let char_code = kscii.matrix.get(x,y);
      if (prev_x != -1 && prev_y != -1) { 
        move_x = x-prev_x;
        move_y = y-prev_y;
        total_move_x += move_x;
        total_move_y += move_y;
      }
      kscii.records[option] += "right:"+move_x+" down:"+move_y+" char:"+char_code[0]+" color:"+char_code[1]+" background:"+char_code[2]+' ';
      prev_x = x;
      prev_y = y;
    });
    kscii.records[option] += "left:"+total_move_x+" up:"+total_move_y+' ';
  },
  'select' : function(kscii,option) {
    let zone = option.split(',');
    kscii.change_key_val("x_cursor",Number(zone[0])+kscii.x_cam);
    kscii.change_key_val("y_cursor",Number(zone[1])+kscii.y_cam);
    kscii.change_key_val("w_cursor",Number(zone[2]));
    kscii.change_key_val("h_cursor",Number(zone[3]));
  },
  'left' : function(kscii,option) {
    if(kscii.x_cursor <= 0) return;
    kscii.change_key_val("x_cursor",kscii.x_cursor - Number(option));
  },
  'up' : function(kscii,option) {
    if(kscii.y_cursor <= 0) return;
    kscii.change_key_val("y_cursor",kscii.y_cursor - Number(option));
  },
  'right' : function(kscii,option) {
    if(kscii.x_cursor >= w-1) return;
    kscii.change_key_val("x_cursor",kscii.x_cursor + Number(option));
  },
  'down' : function(kscii,option) {
    if(kscii.y_cursor >= h-1) return;
    kscii.change_key_val("y_cursor",kscii.y_cursor + Number(option));
  },
  'cam_left' : function(kscii,option) {
    if(kscii.x_cam <= 1 && (kscii.matrix.width() - w + kscii.x_cam <= -1)) return;
    kscii.change_key_val("x_cam",kscii.x_cam - Number(option));
  },
  'cam_up' : function(kscii,option) {
    if(kscii.y_cam <= 2 && (kscii.matrix.height() - h + kscii.y_cam <= -3)) return;
    kscii.change_key_val("y_cam",kscii.y_cam - Number(option));
  },
  'cam_right' : function(kscii,option) {
    if(kscii.x_cam >= w-(kscii.matrix.width()+1) && (kscii.x_cam>=1)) return;
    kscii.change_key_val("x_cam",kscii.x_cam + Number(option));
  },
  'cam_down' : function(kscii,option) {
    if(kscii.y_cam >= h-(kscii.matrix.height()+3) && (kscii.y_cam>=2)) return;
    kscii.change_key_val("y_cam",kscii.y_cam + Number(option));
  },
  'char' : function(kscii,option) {
    if(!patterns[Number(option)])return;
    kscii.change_character_val(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,0,Number(option))
  },
  'color' : function(kscii,option) {
    if(!colors[Number(option)])return;
    kscii.change_character_val(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,1,Number(option))
  },
  'background' : function(kscii,option) {
    if(!colors[Number(option)])return;
    kscii.change_character_val(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,2,Number(option))
  },
  'LEFT' : function(kscii,option) {
    kscii.change_key_val("x_cursor",kscii.x_cursor - Number(option)*kscii.w_cursor);
  },
  'UP' : function(kscii,option) {
    kscii.change_key_val("y_cursor",kscii.y_cursor - Number(option)*kscii.h_cursor);
  },
  'RIGHT' : function(kscii,option) {
    kscii.change_key_val("x_cursor",kscii.x_cursor + Number(option)*kscii.w_cursor);
  },
  'DOWN' : function(kscii,option) {
    kscii.change_key_val("y_cursor",kscii.y_cursor + Number(option)*kscii.h_cursor);
  },
  'CAM_LEFT' : function(kscii,option) {
    kscii.change_key_val("x_cam",kscii.x_cam - Number(option)*kscii.w_cursor);
  },
  'CAM_UP' : function(kscii,option) {
    kscii.change_key_val("y_cam",kscii.y_cam - Number(option)*kscii.h_cursor);
  },
  'CAM_RIGHT' : function(kscii,option) {
    kscii.change_key_val("x_cam",kscii.x_cam + Number(option)*kscii.w_cursor);
  },
  'CAM_DOWN' : function(kscii,option) {
    kscii.change_key_val("y_cam",kscii.y_cam + Number(option)*kscii.h_cursor);
  },
  'CHAR' : function(kscii,option) {
    if(!patterns[Number(option)])return;
    UA(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,kscii.w_cursor,kscii.h_cursor,function(x,y){
      kscii.change_character_val(x,y,0,Number(option));
    });
  },
  'COLOR' : function(kscii,option) {
    if(!colors[Number(option)])return;
    UA(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,kscii.w_cursor,kscii.h_cursor,function(x,y){
      kscii.change_character_val(x,y,1,Number(option));
    });
  },
  'BACKGROUND' : function(kscii,option) {
    if(!colors[Number(option)])return;
    UA(kscii.x_cursor-kscii.x_cam,kscii.y_cursor-kscii.y_cam,kscii.w_cursor,kscii.h_cursor,function(x,y){
      kscii.change_character_val(x,y,2,Number(option));
    });
  },
  'W_GROW' : function(kscii,option) {
    kscii.change_key_val("w_cursor",kscii.w_cursor + Number(option)*kscii.w_cursor);
  },
  'H_GROW' : function(kscii,option) {
    kscii.change_key_val("h_cursor",kscii.h_cursor + Number(option)*kscii.h_cursor);
  },
  'W_SHRINK' : function(kscii,option) {
    kscii.change_key_val("w_cursor",kscii.w_cursor - Number(option)*kscii.w_cursor);
  },
  'H_SHRINK' : function(kscii,option) {
    kscii.change_key_val("h_cursor",kscii.h_cursor - Number(option)*kscii.h_cursor);
  },
  'start_recording' : function(kscii,option) {
    log = 'start_recording:'+option+', '+ log;
    kscii.records[option] = false;
    kscii.recording = option;
  },
  'stop_recording' : function(kscii,option) {
    log = 'stop_recording'+log;
    kscii.recording = false;
  },
  'color_editor' : function(kscii,option) {
    if(!colors[Number(option)])return;
    kscii.change_key_val("fg_color",Number(option));
  },
  'background_editor' : function(kscii,option) {
    if(!colors[Number(option)])return;
    kscii.change_key_val("bg_color",Number(option));
  },
  'zoom_in' : function(kscii,option) {
    if (kscii.scale >= 6) return;
    kscii.change_key_val("scale",kscii.scale + Number(option));
  },
  'write_mode' : function(kscii,option) {
    write_mode = !write_mode;
  },
  'zoom_out' : function(kscii,option) {
    if (kscii.scale <= 1) return;
    kscii.change_key_val("scale",kscii.scale - Number(option));
  },
  //'shift_mode' : function(kscii,option) {
  //  option = Number(option);
  //  shift_mode = option;
  //},
  'undo' : function(kscii,option) {
    option = Number(option);
    while(option--) kscii.undo();
  },
};

// KEYBOARD

let control_map = {
  "Escape"      : "prompt_mode",
  "Enter"       : "prompt_command",
  "ArrowLeft"   : "left",
  "ArrowRight"  : "right",
  "ArrowDown"   : "down",
  "ArrowUp"     : "up",
  "Home"        : "x_cursor:0",
  "End"         : "x_cursor:screen_w",
  "PageUp"      : "y_cursor:0",
  "PageDown"    : "y_cursor:screen_h",
  "Backspace"   : "left CHAR:0 COLOR:editor_color BACKGROUND:editor_background",
  " "           : "CHAR:0 COLOR:editor_color BACKGROUND:editor_background right",
  "F1"          : "$F1",
  "F2"          : "$F2",
  "F3"          : "$F3",
  "F4"          : "$F4",
  "F5"          : "$F5",
  "F6"          : "$F6",
  "F7"          : "$F7",
  "F8"          : "$F8",
  "F9"          : "$F9",
  "F10"         : "$F10",
  "F11"         : "$F11",
  "Alt"         : "previous_sheet",
  "AltGraph"    : "next_sheet",
  "Shift"     : {
    "ArrowLeft" : "w_shrink",
    "ArrowRight": "w_grow",
    "ArrowDown" : "h_grow",
    "ArrowUp"   : "h_shrink",
    "F1"        : "start_recording:F1",
    "F2"        : "start_recording:F2",
    "F3"        : "start_recording:F3",
    "F4"        : "start_recording:F4",
    "F5"        : "start_recording:F5",
    "F6"        : "start_recording:F6",
    "F7"        : "start_recording:F7",
    "F8"        : "start_recording:F8",
    "F9"        : "start_recording:F9",
    "F10"       : "start_recording:F10",
    "F11"       : "start_recording:F11",
    "F12"       : "stop_recording",
  },
  "Control"   : {
    'a'         : "COLOR:editor_color BACKGROUND:editor_background",
    'p'         : "pick",
    'r'         : "write_mode",
    'i'         : "zoom_in",
    'o'         : "zoom_out",
    'z'         : "undo",
    'c'         : "copy",
    'v'         : "$paste",
    'e'         : "reverse",
    's'         : "save",
    'l'         : "load",
    "ArrowLeft" : "cam_left",
    "ArrowRight": "cam_right",
    "ArrowDown" : "cam_down",
    "ArrowUp"   : "cam_up",
    "&"         : "sheet:0",
    "é"         : "sheet:1",
    "\""        : "sheet:2",
    "'"         : "sheet:3",
    "("         : "sheet:4",
    "-"         : "sheet:5",
    "è"         : "sheet:6",
    "_"         : "sheet:7",
    "ç"         : "sheet:8",
    "à"         : "sheet:9",
  },
  "Prompt"    : {
    "Escape"    : "prompt_mode",
    "Enter"     : "prompt_command"
  }
};

let key_map = azerty;
let control_mode = false;
let tab_mode = false;
let shift_mode = false;
let prompt_mode = false;
let write_mode = false;
let last_typed = -1;

document.addEventListener("keydown",function(e){
  e.preventDefault();
  e.stopPropagation();
  console.log(e.key);
  if (key_map.indexOf(e.key)) {
    last_typed = key_map.indexOf(e.key);
    console.log(key_map.indexOf(e.key)+(40*editor.sheet));
  }
  if (prompt_mode) {
    if (control_map["Prompt"][e.key])editor.command(control_map["Prompt"][e.key]);
    editor.prompt.send(e.key);
    console.log(editor.prompt.print());
    return;
  };
  if (e.key == "Shift") {
    shift_mode = true;
  //editor.command("shift_mode:1");
    return;
  }
  if (e.key == "Control") {
    control_mode = true;
    return;
  }
  if (e.key == "Tab") {
    tab_mode = !tab_mode;
    return;
  }
  if (tab_mode && key_map.indexOf(e.key) > -1) {
    if (control_mode) {
      if (control_map["Control"][e.key])editor.command(control_map["Control"][e.key]);
      return;
    }
    editor.command("color_editor:"+key_map.indexOf(e.key));
    return;
  }
  if (shift_mode) {
    if (control_map["Shift"][e.key]) {
      editor.command(control_map["Shift"][e.key]);
    } else {
      editor.command("CHAR:"+(write_mode? patterns_names.indexOf(e.key) : semigraphic_map[key_map.indexOf(e.key)+(40*editor.sheet)])+" COLOR:editor_color BACKGROUND:editor_background right");
    }
    return;
  }
  if (control_mode) {
    if (control_map["Control"][e.key])editor.command(control_map["Control"][e.key]);
    return;
  }
  if (key_map.indexOf(e.key) > -1) {
    editor.command("CHAR:"+(write_mode? patterns_names.indexOf(e.key) : semigraphic_map[key_map.indexOf(e.key)+(40*editor.sheet)])+" COLOR:editor_color BACKGROUND:editor_background right");
  }
  if (control_map[e.key]) {
    editor.command(control_map[e.key]);
  }
});

document.addEventListener("keyup",function(e){
  e.preventDefault();
  e.stopPropagation();
  if (e.key == "Control") {
    control_mode = false;
    return;
  }
  if (e.key == "Shift") {
    //editor.command("shift_mode:0");
    shift_mode = false;
    return;
  }
});

// DISPLAY

let char_scale = 8, scale = 10;

var screen = new Screen(window.innerWidth,window.innerHeight,scale);
var w = UF(window.innerWidth/scale/char_scale);
var h = UF(window.innerHeight/scale/char_scale);
var screen_matrix = new Matrix(UF(w),UF(h),3);
var editor = new Kscii();
var overlay_matrix;

function setup_display() {
  if (screen.canvas) document.body.removeChild(screen.canvas);
  screen = new Screen(window.innerWidth,window.innerHeight,editor.scale);
  w = UF(window.innerWidth/editor.scale/char_scale);
  h = UF(window.innerHeight/editor.scale/char_scale);
  screen_matrix = new Matrix(UF(w),UF(h),3);
  screen_matrix.fullset([-1,-1,-1]);
  overlay_matrix = new Matrix(UF(w),UF(h),3);
}

function zoom_in() {
  setup_display();
}


// UI
let log = "";
function infos() {
  overlay_matrix.clear();
//let control_mode = false;
//let tab_mode = false;
//let shift_mode = false;
//let prompt_mode = false;
//let write_mode = true;
  let offset = 0;
  overlay_matrix.write(0,0,"écrire  ",(write_mode?14:2),0);
  offset+= "écrire  ".length;
  overlay_matrix.write(offset,0,"tabulation  ",(tab_mode?14:2),0);
  offset+= "tabulation  ".length;
  overlay_matrix.write(offset,0,"majuscule  ",(shift_mode?4:2),0);
  offset+= "majuscule  ".length;
  overlay_matrix.write(offset,0,"contrôle  ",(control_mode?4:2),0);
  offset+= "controle  ".length;
  overlay_matrix.write(offset,0,"commande",(prompt_mode?14:2),0);
  if (!write_mode || write_mode && tab_mode) {
    let glyph_index = (shift_mode) ? 40 : 0;
    for (var y = 0; y < 4; y++) {
      for (var x = 0; x < 10; x++) {
        overlay_matrix.write(x,y*2+1,aube_azerty[glyph_index],editor.fg_color,editor.bg_color);
        overlay_matrix.set(
          x,y*2+2,
          [
            tab_mode ? 244 : semigraphic_map[glyph_index+40*editor.sheet],
            tab_mode ? (glyph_index < colors.length-1 ? glyph_index : 0) : last_typed == glyph_index ? editor.bg_color : editor.fg_color,
            last_typed == glyph_index ? editor.fg_color : editor.bg_color,
          ]
        );
        glyph_index++;
      }
    }
    overlay_matrix.write(0,9,"feuille:"+editor.sheet,2,0);
  }
  log = log.slice(0,w-17);
  overlay_matrix.write(0,h-1,"memoire: "+editor.commands.length+' ['+log+'...]',2,0);
  let before = editor.prompt.lines[0].before.join('');
  let after = editor.prompt.lines[0].after.join('');
  overlay_matrix.write(0,h-3,'>',prompt_mode ? 14 : 2,0);
  overlay_matrix.write(1,h-3,before,prompt_mode ? 4 : 2,0);
  overlay_matrix.write(before.length+1,h-3,after.slice(0,1),0,prompt_mode ? 4 : 2);
  let glyph = editor.matrix.get(editor.x_cursor-editor.x_cam,editor.y_cursor-editor.y_cam);
  overlay_matrix.write(before.length+(after.length ? 2 : 1),h-3,after.length ? after.slice(1) : ' ',after.length ? prompt_mode ? 4 : 2 : 0,after.length ? 0 : prompt_mode ? 4 : 2);
  overlay_matrix.write(0,h-2,"curseur:"+(editor.x_cursor-editor.x_cam)+','+(editor.y_cursor-editor.y_cam)+','+(editor.w_cursor)+','+(editor.h_cursor)+'  caractère:'+glyph[0]+','+glyph[1]+','+glyph[2]+"  camera:"+(editor.x_cam)+','+(editor.y_cam)+','+editor.scale,2,0);
}

// RENDER

let fps = 60;
start_animation(fps); 

start_animation_event(1,function(frame){
  if (editor.scale != scale) {
    let x_offset =UF((window.innerWidth/(editor.scale)/char_scale - w)/2);
    let y_offset =UF((window.innerHeight/(editor.scale)/char_scale - h)/2);
    //if(!editor.commands.length) {
    //if (scale-editor.scale > 0) {
    //  x_offset = UF(x_offset);
    //  y_offset = UF(y_offset);
    //} else {
    //  x_offset = UC(x_offset);
    //  y_offset = UC(y_offset);
    //}
      editor.x_cam += x_offset;
      editor.y_cam += y_offset;
      editor.x_cursor += x_offset;
      editor.y_cursor += y_offset;
    //}else{
    //  editor.command("cam_right:"+x_offset);
    //  editor.command("cam_down:"+y_offset);
    //  editor.command("right:"+x_offset);
    //  editor.command("down:"+y_offset);
    //}
    scale = editor.scale;
    setup_display();
  }
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      if (
        x >= editor.x_cursor && y >= editor.y_cursor &&
        x < editor.x_cursor+editor.w_cursor && y < editor.y_cursor+editor.h_cursor
      ) {
        if (
          x < 0 + editor.x_cam ||
          y < 0 + editor.y_cam ||
          x > editor.matrix.width()-1 + editor.x_cam ||
          y > editor.matrix.height()-1 + editor.y_cam
        ) {
          screen.print_pattern(
            patterns[127],
            colors[3],
            colors[4],
            x*char_scale,
            y*char_scale
          );
          screen_matrix.set(x,y,[127,3,4]);
          continue;
        }
        let glyph = editor.matrix.get(x-editor.x_cam,y-editor.y_cam);
        screen.print_pattern(
          patterns[glyph[0]],//(glyph[0] == 0 || glyph[0] == 101 )? 213 : glyph[0]],
          colors[glyph[2]],
          colors[glyph[1]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,[/*(glyph[0] == 0 || glyph[0] == 101 )? 213 : glyph[0]*/glyph[0],glyph[2],glyph[1]]);
        continue;
      }
      if (!UCO(overlay_matrix.get(x,y),[0,0,0])){
        let glyph = overlay_matrix.get(x,y);
        if (!patterns[glyph[0]]) continue;
        screen.print_pattern(
          patterns[glyph[0]],
          colors[glyph[1]],
          colors[glyph[2]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,glyph);
        continue;
      }
      if (
        x < 0 + editor.x_cam ||
        y < 0 + editor.y_cam ||
        x > editor.matrix.width()-1 + editor.x_cam ||
        y > editor.matrix.height()-1 + editor.y_cam
      ) {
        if(UCO(screen_matrix.get(x,y),[127,1,2])) continue;
        screen.print_pattern(
          patterns[127],
          colors[1],
          colors[2],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,[127,1,2]);
        continue;
      }
      let printed_glyph = screen_matrix.get(x,y);
      if (!editor.matrix.compare(x-editor.x_cam,y-editor.y_cam,printed_glyph)){
        let glyph = editor.matrix.get(x-editor.x_cam,y-editor.y_cam);
        screen.print_pattern(
          patterns[glyph[0]],
          colors[glyph[1]],
          colors[glyph[2]],
          x*char_scale,
          y*char_scale
        );
        screen_matrix.set(x,y,glyph);
      }
    }
  }
  infos();
  screen.print();
});

editor.x_cam -= UF((editor.matrix.width()-1)/2);
editor.y_cam -= UF((editor.matrix.height()-1)/2);

let real_fps = 0;
start_animation_event(10,function(){
  let time_elapsed = eval_time_elapsed();
  real_fps = UF(60/17*time_elapsed/10);
});

// Init

function run() {


    editor.change_character_val(0,0,0,65);
    editor.change_character_val(1,0,0,65);
    editor.change_character_val(1,1,0,65);
    editor.change_character_val(0,0,0,100);


    editor.undo();
    console.log(editor);
}

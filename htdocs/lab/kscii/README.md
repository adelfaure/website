# KSCII

KSCII is a textmode graphics editor designed to be used entirely and solely with the keyboard without any menu interactions.

KSCII render engine is based on Javascript Canvas API associated with a custom code standard dedicated to describing colored semigraphic characters.

In KSCII semigraphic code system, each character is described as triplets `a,b,c`, `a` refering to character shape, `b` color of the character and `c` background color of the character. For example `65,4,0` in KSCII code refer to a white on black `a` letter.

KSCII is currently in early developement and I didn't write much documentation about it. It is under GNU/GPL3 license. You can try it online on my website here [adelfaure.net/lab/textmode_utils](https://adelfaure.net/lab/textmode_utils/).

The tool currently contains no documentation for using it but keys are directly listed on interface.

Javascript scripts associated with KSCII in the `javascript/` folder are mainly textmode utils suited for my own projects.

All code specific to KSCII editor is on `javascript/kscii.js` including key mapping.

If you are looking for KSCII semigraphic code system specification you can look at the commented section at the top of `javascript/data.js` file.

I hope you will be able to use this tool, have fun drawing with it or making forks from on its current drafty state.

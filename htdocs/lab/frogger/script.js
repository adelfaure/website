// TESTS

let scene_w = Math.floor(window.innerWidth/12);
let scene_h = Math.floor(window.innerHeight/24);

let display = new Display({
  "type":"text",
  "channels":5,
  "width":scene_w,
  "height":scene_h,
  "dom_destination":document.body
});

display.set_color([
  "#F00",
  "#0C0",
  "#00F",
  "#FC0",
  "#A9A",
])

// FROG

let frog_sprite = assetToArea("frog");
let frog = new Area(frog_sprite.width-1,4);
frog.place(scene_w/2-frog.width/2,scene_h-frog.height,0,0);
let frog_jump = 0;
let frog_speed = 16/fps;
let frog_dead = false;
let frog_dead_x = -1;
let frog_dead_y = -1;

// FROG TONGUE

let frog_tongue_sprite = assetToArea("frog_tongue");
let frog_tongue = new Area(frog_tongue_sprite.width-1,3);
let frog_tongue_action = 0;
let frog_caught_fly = false;
let frog_ate = false;

// FLY

let fly_sprite = assetToArea("fly");
let fly = new Area(1,1);

// CAR

let car_sprite = assetToArea("car_right");
let car = new Area(car_sprite.width-1,car_sprite.height/4);

// WHEELS

let wheels_sprite = assetToArea("wheels_right");
let wheels = new Area(wheels_sprite.width-1,wheels_sprite.height/3);
let wheels_blood = [];

// LEFT CAR

let left_car_sprite = assetToArea("car_left");
let left_car = new Area(left_car_sprite.width-1,left_car_sprite.height/4);

// WHEELS

let left_wheels_sprite = assetToArea("wheels_left");
let left_wheels = new Area(left_wheels_sprite.width-1,left_wheels_sprite.height/3);
let left_wheels_blood = [];

// LOG

let log = new Area(scene_w,1);
log.place(0,0,scene_w,scene_h);

// ANIMATION

animation = function() {

  // FROG BEHAVIOUR
  let frog_walk = false;
  if (!frog_dead) {
    if (frog_caught_fly && ! frog_tongue_action) frog_ate = true;

    if (keys["ArrowLeft"]) {
      frog_walk = true;
      frog.pos[0] -= frog_speed;
    }
    if (keys["ArrowRight"]) {
      frog_walk = true;
      frog.pos[0] += frog_speed;
    }
    if (keys["ArrowDown"]) {
      frog_walk = true;
      frog.pos[1] += frog_speed/3;
    }
    if (keys["ArrowUp"] && !frog_jump && !frog_tongue_action) {
      frog_jump = frog.height;
      keys["ArrowUp"] = false;
    }
    if (keys[" "] && !frog_tongue_action && !frog_jump) {
      frog_tongue_action = 5;
      keys[" "] = false;
    }
    if (frog_jump) {
      frog.pos[1] -= 0.5;
      frog_jump -= 0.5;
    }
    if (frog_tongue_action && f%2) {
      frog_tongue_action-=0.5;
    }
  }

  // FROG ANIMATION
  frog_dead = frog_dead > 0 ? frog_dead + 1 : frog_dead;
  console.log(frog_dead);
  frog.place(frog.pos[0],frog.pos[1],scene_w-frog.width-1,scene_h-frog.height);
  frog_sprite.place(
    0,
    (
      frog_dead ? frog.height * -8 :
      frog_jump ? frog.height * -1 : 
      frog_tongue_action ? frog.height * -7 : 
      frog_walk ? frog.height * (
        Math.floor(frog.origin[0]/2+frog.origin[1])%2 ? 2 : 3
      )*-1 :
      !(  (Math.floor(f))%(fps*5)) ? frog.height * -6 :
      !((Math.floor(f+1))%(fps*5)) ? frog.height * -5 :
      !((Math.floor(f+2))%(fps*5)) ? frog.height * -5 :
      !((Math.floor(f+3))%(fps*5)) ? frog.height * -5 :
      !((Math.floor(f+4))%(fps*5)) ? frog.height * -4 :
      0  
    ),
    0,0
  );
  frog.process(()=>frog_sprite.printAt(di));

  // FROG TONGUE ANIMATION
  frog_tongue.place(
    frog.pos[0]+3,
    frog.pos[1]-frog_tongue.height+1,
    scene_w-frog_tongue.width-1,
    scene_h-frog_tongue.height
  );
  frog_tongue_sprite.place(
    0,
    (
      frog_tongue.height * Math.floor(frog_tongue_action)
    )*-1,
    0,0
  );
  frog_tongue.process(()=>frog_tongue_sprite.printAt(di));
  
  // FLY ANIMATION
  if (frog_caught_fly) {
    fly.place(
      frog_tongue.pos[0],
      frog_tongue.pos[1]+frog_tongue.height-[0,1,2,3,2,1][Math.floor(frog_tongue_action)],
      scene_w-2,scene_h-1
    );
  } else {
    fly.place(scene_w/2+Math.sin(f/30)*8-4,scene_h/2+Math.cos(f/30)*4-2,scene_w-2,scene_h-1);
  }
  fly_sprite.place(
    0,
    (
      (f/4)%fly_sprite.height
    ),
    0,0
  );
  fly.process(()=>fly_sprite.printAt(di));

  // CAR ANIMATION
  car_sprite.place(0,car.height*(Math.floor(f/8)%4)*-1,0,0);
  car.process(()=>car_sprite.printAt(di));
  car.place(f*1.25,scene_h/4*3-car.height+2,scene_w*2-1-car.width,scene_h);
  
  // WHEELS ANIMATION
  wheels_sprite.place(0,wheels.height*(Math.floor(f/6)%3)*-1,0,0);
  wheels.place(f*1.25,scene_h/4*3-car.height+3,scene_w*2-1-wheels.width,scene_h);
  wheels.process(()=>wheels_sprite.printAt(di));
  
  // LEFT CAR ANIMATION
  left_car_sprite.place(0,left_car.height*(Math.floor(f/8)%4)*-1,0,0);
  left_car.process(()=>left_car_sprite.printAt(di));
  left_car.place(f*1.25*-1+scene_w-left_car.width,scene_h/4*2-left_car.height-2,scene_w*1.5-1-left_car.width,scene_h);
  
  // LEFT WHEELS ANIMATION
  left_wheels_sprite.place(0,left_wheels.height*(Math.floor(f/6)%3)*-1,0,0);
  left_wheels.place(f*1.25*-1+scene_w-left_car.width,scene_h/4*2-left_car.height-2,scene_w*1.5-1-left_wheels.width,scene_h);
  left_wheels.process(()=>left_wheels_sprite.printAt(di));

  // LOG
  let log_content = encode("FPS "+animation_fps+ " [ arrows keys ] move, [ space ] use tongue");
  log.process(()=>log_content[di]);
  
  // PROCESS
  let fly_index = -1;
  display.process(()=>{

    // LINE FEED
    if (x == w-1) {
      return [10,10,10,10,10];
    }

    // LOG
    let log_place = log.printAt(di);
    if (log_place) {
      return [32,32,32,log_place,32];
    }
    
    // FROG
    let frog_place = frog.printAt(di);
    
    
    // LEFT WHEELS
    let left_wheels_place = left_wheels.printAt(di);
    
    if (left_wheels_place && left_wheels_place != 35){
      if (frog_place && frog_place != 35 && !frog_dead) {
        frog_dead = 1;
        frog_dead_x = frog_dead_x > -1 ? frog_dead_x : x;
        frog_dead_y = frog_dead_y > -1 ? frog_dead_y : y;
      }
      if (
        frog_dead && frog_dead < 100
        && left_wheels_blood.indexOf(di) == -1
        && (
          frog.printAt(di+1)
          || left_wheels_blood.indexOf(di+1) > -1 && Math.random()>0.75
        )
      ) {
        left_wheels_blood.push(di);
      }
    }

    // WHEELS
    let wheels_place = wheels.printAt(di);
    
    if (wheels_place && wheels_place != 35){
      if (frog_place && frog_place != 35 && !frog_dead) {
        frog_dead = 1;
        frog_dead_x = frog_dead_x > -1 ? frog_dead_x : x;
        frog_dead_y = frog_dead_y > -1 ? frog_dead_y : y;
      }
      if (
        frog_dead && frog_dead < 100
        && wheels_blood.indexOf(di) == -1
        && (
          frog.printAt(di-1)
          || wheels_blood.indexOf(di-1) > -1 && Math.random()>0.75
        )
      ) {
        wheels_blood.push(di);
      }
    }
    
    
    // CAR
    let car_place = car.printAt(di);
    if (car_place && car_place != 35){
      return [32,32,32,32,car_place];
    }
    
    // WHEELS
    if (wheels_place && wheels_place != 35){
      return [32,32,32,32,wheels_place];
    }
    
    // FROG TONGUE & FLY
    let frog_tongue_place = frog_tongue.printAt(di);
    let fly_place = fly.printAt(di);
    
    if (fly_place) {
      fly_index = di;
    }    

    if (frog_tongue_place && frog_tongue_place != 35) {
      if (di == fly_index) {
        frog_caught_fly = true;
      }
    }

    if (fly_place && !frog_ate) {
      if (frog_caught_fly && !frog_tongue_action) frog_ate = true;
      return [32,32,fly_place,32,32];
    }
    
    // FROG TONGUE
    if (frog_tongue_place && frog_tongue_place != 35) {
      return [frog_tongue_place,32,32,32,32];
    }
    
    // LEFT CAR
    let left_car_place = left_car.printAt(di);
    if (left_car_place && left_car_place != 35){
      return [32,32,32,32,left_car_place];
    }
    
    // LEFT WHEELS
    if (left_wheels_place && left_wheels_place != 35){
      return [32,32,32,32,left_wheels_place];
    }


    // FROG
    if (frog_place == 111) {
      return [32,32,32,frog_place,32];
    }
    if (frog_place && frog_place != 35) {
      let blood = (
        wheels_blood.indexOf(di) > -1
        || left_wheels_blood.indexOf(di) > -1
      );
      return [blood ? frog_place : 32,blood? 32 : frog_place,32,32,32];
    }

    // GROUND

    if ( (
        y > h - 7
        || y < 6
      ) && !((x+y*4)%8) ) {
      return [32,(
        (Math.floor((
          x+y*8
        )/2)%5) ? 45 : 
        32
      ),32,32,32];
    }
    
    if (y == Math.floor(h/2) ) {
      return [32,32,32,32,(
        !(Math.floor(x/10)%3) ? (
          !(x%10) || !((x+1)%10) ? 108 : 61
        ) : 32
      )];
    }

    if (y == 6 || y == h-7) {
      return [32,32,32,32,x%2?58:46];
    }
    
    if (y > 6 && y < h-7) {
      return [(
        wheels_blood.indexOf(di) > -1  ? 58 : 
        left_wheels_blood.indexOf(di) > -1  ? 58 : 
        32
      ),32,32,32,(
        !((x+y*y*y*y)%66) ? 46 : 32
      )];
    }

    // EMPTY SPACE
    return [32,32,32,32,32];
  });
  

  // DISPLAY
  display.print();
  
}

fps=60;
start_animation();

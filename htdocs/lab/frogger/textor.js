console.log("textor.js");

// GLOBAL

let w,h,d,x,y,di;
let keys = {};
let key_down = false;
let mouse_x = 0;
let mouse_y = 0;

// ENCODING

const encoding = {};
encoding.decoder = new TextDecoder("utf-16");
encoding.encoder = new TextEncoder("utf-16");

function encode(str){
  var code = new Uint16Array(str.length);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    code[i] = str.charCodeAt(i);
  }
  return code.length > 1 ? code : code[0];
}

function decode(code){
  return encoding.decoder.decode(
    isNaN(Number(code)) ? code : new Uint16Array([code])
  );
}

// AREA

Area = function(w,h){
  this.width = w;
  this.height = h;
  this.length = w*h;
  this.data = new Uint16Array(this.length);
  this.pos = [0,0];
  this.origin = new Uint16Array(4);
}

Area.prototype.process = function(shader_chunk){
  w = this.width;
  h = this.height;
  d = this.data;
  for (var i = 0; i < this.data.length; i++) {
    x = i%this.width;
    y = Math.floor(i/this.width);
    di = i;
    this.data[i] = shader_chunk();
  }
}

Area.prototype.place = function(pos_x,pos_y,gap_x,gap_y){
  this.pos[0] = pos_x;
  this.pos[1] = pos_y;
  // origin x
  this.origin[0] = (pos_x*-1)%(this.width + gap_x) + this.width + gap_x;
  // origin y
  this.origin[1] = (pos_y*-1)%(this.height + gap_y) + this.height + gap_y;
  this.origin[2] = gap_x;
  this.origin[3] = gap_y;
}

Area.prototype.printAt = function(index) {
  ox = this.origin[0];
  oy = this.origin[1];
  gap_x = this.origin[2];
  gap_y = this.origin[3];
  // area x
  let ax = index%w + ox;
  // area y
  let ay = Math.floor(index/w) + oy;
  // area data x
  let adx = ax%(this.width + gap_x);
  if (adx >= this.width) return 0;
  // area data y
  let ady = ay%(this.height + gap_y)*this.width;
  // area data index
  let adi = adx+ady;
  return this.data[adi];
}

Area.prototype.render = function(options){
  if (options.type == "text"){
    return decode(this.data);
  }
}

// ASSET

function assetToArea(asset_id) {
  let asset = encode(document.getElementById(asset_id).textContent);
  let asset_w = asset.indexOf(10)+1;
  let area = new Area(asset_w,asset.length/asset_w);
  area.process(()=>asset[di]);
  console.log(area);
  return area;
}

// DISPLAY

Display = function(options){
  this.type = options.type;
  if (this.type == "text"){
    this.channel = [];
    this.output = [];
    this.dom = document.createElement("div");
    this.dom.className = "display_text";
    for (var i = 0; i < options.channels; i++) {
      this.channel.push(new Area(options.width,options.height));
      let pre = document.createElement("pre");
      pre.style.position = "absolute";
      pre.style.top = "0";
      this.output.push(pre);
      this.dom.appendChild(pre);
    }
    options.dom_destination.appendChild(this.dom);
  }
}

Display.prototype.print = function(){
  if (this.type == "text") {
    for (var i = 0; i < this.output.length; i++){
      this.output[i].textContent = this.channel[i].render({"type":"text"});
    }
  }
}

Display.prototype.set_color = function(options){
  if (this.type == "text") {
    for (var i = 0; i < options.length; i++){
      this.output[i].style.color = options[i];
    }
  }
}

Display.prototype.process = function(shader_chunk){
  w = this.channel[0].width;
  h = this.channel[0].height;
  d = [];
  for (var i = 0; i < this.channel.length; i++){
    d.push(this.channel[i].data);
  }
  for (var i = 0; i < this.channel[0].data.length; i++) {
    x = i%w;
    y = Math.floor(i/w);
    di = i;
    let data_list = shader_chunk();
    for (var j = 0; j < this.channel.length; j++){
      this.channel[j].data[i] = data_list[j];
    }
  }
}

// KEYS

document.addEventListener("keydown",function(e){
  keys[e.key] = keys[e.key] ? keys[e.key] : f;
  key_down = key_down ? key_down : f;
});
document.addEventListener("keyup",function(e){
  keys[e.key] = false;
  let all_key_down = true;
  for (var i in keys ) {
    if (keys[i]){
      all_key_down = false; 
      break;
    }
  }
  key_down = all_key_down ? false : key_down;
});

// MOUSE

//document.addEventListener("mousemove",function(e){
//  if (!font) return;
//  mouse_x = e.clientX / (font.width * scale);
//  mouse_y = e.clientY / (font.height * scale);
//});

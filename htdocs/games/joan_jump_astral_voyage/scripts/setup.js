// CHARSET

// Characters available in the game
// Need to be in an array cause glyphs are stocked as integers values between 0 and 255 in game objects "Frame" data attribute
const charset = [' ','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}','~','°','░','▒','▓','¯','§','·','▕','¡','²','▏','▕'];

// CHUNKS

// Chunk size
const chunk_w = 64;
const chunk_h = 32;

// LEVEL

// Max level size
const level_w = chunk_w * 256;
const level_h = chunk_h * 256;

// Coins
var coins = 0;
var max_coins = 0;

// SCENE

// Get scene size depending on client window size
var scene_w = Math.floor(window.innerWidth/14)//136;
var scene_h = Math.floor(window.innerHeight/28)//38;

// View size is needed for handling smooth camera movements
var view_w = scene_w;
var view_h = scene_h
while (!Number.isInteger(view_w/10)) {
  view_w++;
}
while (!Number.isInteger(view_h/10)) {
  view_h++;
}

// FRAME RELATED

var t0 = false, t1 = false, fps = 0, game_speed = 26, wanted_framerate = 38;

// Return frame per second information as string
function print_fps(){
  t1 = performance.now();
  if (t0 && t1) {
    let t3 = t1 - t0;
    fps = 1000 / t3;
  }
  t0 = performance.now();
  return String(Math.round(fps));
}

// Change game speed to obtain wanted framerate
function optimize_fps() {
  if (fps > wanted_framerate) {
    game_speed+=0.01;
  } else if (fps < wanted_framerate) {
    if (game_speed > 0) game_speed-=0.1;
  }
}

// Blink function, return true or false depending on a frame value and wanted
// blinking duration (in frames)
function blink(frame_value,phase_duration) {
  return Number.isInteger(Math.floor(frame_value/phase_duration)/2);
}

// CHUNKS

// From a raw text chunk_set return a processable array
function parse_chunks(chunk_set) {
  let parsed_chunks = [];
  let chunks = chunk_set.split('CHUNK');
  for (var i = 1; i < chunks.length; i++) {
    let x_coor = chunks[i].split('ENDAXIS')[0].split('AXIS')[0].split('-');
    for (var coor = 0; coor < x_coor.length; coor++) {
      x_coor[coor] = x_coor[coor].replaceAll(' ','');
      x_coor[coor] = x_coor[coor] == "ANY" ? "ANY" : parseInt(x_coor[coor]);
    }
    let y_coor = chunks[i].split('ENDAXIS')[0].split('AXIS')[1].split('-');
    for (var coor = 0; coor < y_coor.length; coor++) {
      y_coor[coor] = y_coor[coor].replaceAll(' ','');
      y_coor[coor] = y_coor[coor] == "ANY" ? "ANY" : parseInt(y_coor[coor]);
    }
    let decor_chunk = chunks[i].split('ENDAXIS')[1].split('DECOR')[0].slice(1);
    let ground_chunk = chunks[i].split('ENDAXIS')[1].split('DECOR')[1].split('GROUND')[0].slice(1);
    let interactive_chunk = chunks[i].split('ENDAXIS')[1].split('GROUND')[1].split('INTERACTIVE')[0].slice(1);
    parsed_chunks.push([
      x_coor,y_coor,[decor_chunk,ground_chunk,interactive_chunk]
    ]);
  }
  console.log(parsed_chunks);
  return parsed_chunks;
}

//HURT PLAYER

function hurt_playable_entity(playable_entity,from) {
  if (playable_entity.health && !playable_entity.invincible && collide_to_map(player_scene,from.get_frame(),from.x-player0_cam_x,from.y-player0_cam_y) ) {
    playable_entity.invincible = true;
    playable_entity.health-=1;
    playable_entity.vel_x = gravity_x * -2.1;
    playable_entity.vel_y = playable_entity.health > 0 ? gravity_y * -2.1 : gravity_y * -4.2;
    if (playable_entity.health < 1) {
      document.body.style.backgroundColor = "#000";
      decor_layer.style.color = "#222";
      ground_layer.style.color = "#444";
      interactive_layer.style.color = "#888";
    }
    live_display = 50;
    setTimeout(function(){
      this.invincible = false;
    }.bind(playable_entity),3000);
  }
}

// From global tiles_map declare interactives entities
// Entities art can vary depending on level (maybe useless)
function declare_chunk_entities(level,start_x,start_y,entity_x,entity_y) {
  for (var y = start_y*8; y<start_y*8+8; y++) {
    for (var x = start_x*8; x<start_x*8+8; x++) {
      let true_x = entity_x+(x-start_x*8)*tile_w;
      let true_y = entity_y+(y-start_y*8)*tile_h;

      // MUSHROOM
      if (interactive_tiles_map[x+','+y] == '^') {
        
        // Get pike tile type
        let entity = new Entity("mushroom",

          // Pike behaviour
          function(entity){

            // loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              // TRAMPOLINE
              if (!playable_entities[i].climb && collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y) && !collide_to_map(level_ground,playable_entities[i].get_frame(),playable_entities[i].x,playable_entities[i].y+1)) {
                playable_entities[i].vel_y = gravity_y*-3;
                this.animation_index = "mushroom_jump";
                this.animation_change = 10;
              }
              if (!this.animation_change) {
                this.animation_index = "mushroom";
              } else {
                this.animation_change--;
              }
            }
          },
        
        true_x,true_y,0,0,0,0);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // PIKES
      } else if (interactive_tiles_map[x+','+y] == 'X') {
        
        // Get pike tile type
        let tile_type = get_tile_type(x,y,'X',ground_tiles_map);
        
        let entity = new Entity("tiles_"+level+"_danger_"+tile_type,

          // Pike behaviour
          function(entity){

            // loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              // HURT
              hurt_playable_entity(playable_entities[i],this);
            }
          },
        
        true_x,true_y,0,0,0,0);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // PIKE_BLOCK
      } else if (interactive_tiles_map[x+','+y] == '+') {
        
        // Get pike tile type
        let tile_type = get_tile_type(x,y,'+',interactive_tiles_map);
        
        let entity = new Entity("pike_block_"+tile_type,

          // pike behaviour
          function(entity){

            // loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              hurt_playable_entity(playable_entities[i],this);
            }
          },
        
        true_x,true_y,0,0,0,0);
        
        // declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // SEMI GROUND2
      } else if (interactive_tiles_map[x+','+y] == '2') {
        
        let tile_type = get_tile_type(x,y,'2',interactive_tiles_map);
        
        let entity = new Entity("semi_ground_"+tile_type,

          // Moving block behaviour
          function(entity){

            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              if ( !playable_entities[i].climb && !playable_entities[i].action["down"] &&
                  (
                    this.animation_index.includes("top")
                    || this.animation_index == "semi_ground_centerxbridge" 
                    || this.animation_index == "semi_ground_centeralone" 
                    || this.animation_index == "semi_ground_leftalone"
                    || this.animation_index == "semi_ground_rightalone"
                  ) && collide_to_map(player_scene,animations["row"][0],this.x-player0_cam_x,this.y-player0_cam_y-1) && !collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y)) {
                if (playable_entities[i].vel_y + gravity_y > 0) {
                  playable_entities[i].on_interactive_ground = true;
                  playable_entities[i].vel_y = gravity_y*-1;
                }
              } 
            }
          },
        
        true_x,true_y,1.2,1.075,0.2,0.12);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // SEMI GROUND1
      } else if (interactive_tiles_map[x+','+y] == '1') {
        
        let tile_type = get_tile_type(x,y,'1',interactive_tiles_map);
        
        let entity = new Entity("semi_ground_"+tile_type,

          // Moving block behaviour
          function(entity){

            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              if ( !playable_entities[i].climb && !playable_entities[i].action["down"] &&
                  (
                    this.animation_index.includes("top")
                    || this.animation_index == "semi_ground_centerxbridge" 
                    || this.animation_index == "semi_ground_centeralone" 
                    || this.animation_index == "semi_ground_leftalone"
                    || this.animation_index == "semi_ground_rightalone"
                  ) && collide_to_map(player_scene,animations["row"][0],this.x-player0_cam_x,this.y-player0_cam_y-1) && !collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y)) {
                if (playable_entities[i].vel_y + gravity_y > 0) {
                  playable_entities[i].on_interactive_ground = true;
                  playable_entities[i].vel_y = gravity_y*-1;
                }
              } 
            }
          },
        
        true_x,true_y,1.2,1.075,0.2,0.12);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // SEMI GROUND
      } else if (interactive_tiles_map[x+','+y] == '0') {
        
        let tile_type = get_tile_type(x,y,'0',interactive_tiles_map);
        
        let entity = new Entity("semi_ground_"+tile_type,

          // Moving block behaviour
          function(entity){

            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              if ( !playable_entities[i].climb && !playable_entities[i].action["down"] &&
                  (
                    this.animation_index.includes("top")
                    || this.animation_index == "semi_ground_centerxbridge" 
                    || this.animation_index == "semi_ground_centeralone" 
                    || this.animation_index == "semi_ground_leftalone"
                    || this.animation_index == "semi_ground_rightalone"
                  ) && collide_to_map(player_scene,animations["row"][0],this.x-player0_cam_x,this.y-player0_cam_y-1) && !collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y)) {
                if (playable_entities[i].vel_y + gravity_y > 0) {
                  playable_entities[i].on_interactive_ground = true;
                  playable_entities[i].vel_y = gravity_y*-1;
                }
              } 
            }
          },
        
        true_x,true_y,1.2,1.075,0.2,0.12);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // LADDER
      } else if (interactive_tiles_map[x+','+y] == '=') {
        
        let tile_type = get_tile_type(x,y,'=',interactive_tiles_map);
        
        let entity = new Entity("ladder_"+tile_type,

          // Moving block behaviour
          function(entity){

            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              if (playable_entities[i].health < 1) continue;
              if (collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y)) {
                playable_entities[i].climb = true;
                if (playable_entities[i].action["up"]) {
                  playable_entities[i].vel_y = -0.5;
                } else if (!playable_entities[i].action["down"]) {
                  playable_entities[i].vel_y = 0;
                } else {
                  playable_entities[i].vel_y = 0.5;
                }
              } 
            }
          },
        
        true_x,true_y,1.2,1.075,0.2,0.12);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // MOVING BLOCKS
      } else if (interactive_tiles_map[x+','+y] == 'V' || interactive_tiles_map[x+','+y] == 'v') {
        
        let entity = new Entity("moving_block",

          // Moving block behaviour
          function(entity){

            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
              if (!entity.fall && collide_to_map(player_scene,animations["vertical_zone"][0],this.x-player0_cam_x,this.y+4-player0_cam_y)&& entity.animation_index != "moving_block_bottom" && entity.animation_index != "moving_block_top") {
                entity.fall = 20;
              }
              if (this.fall) {
                if (Number.isInteger(frame/4))entity.fall--;
                entity.vel_y = 0.5;
                entity.update_animation_index(true);
                entity.move(true);
              } else {
                entity.vel_y = -0.25;
                entity.update_animation_index(true);
                entity.move(true);
              }
              if (collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y) && entity.animation_index == "moving_block_bottom") {
                hurt_playable_entity(playable_entities[i],this);
              } else if ( !playable_entities[i].climb && !playable_entities[i].action["down"]
                && collide_to_map(player_scene,animations["row"][0],this.x-player0_cam_x,this.y-player0_cam_y-1) && !collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y)) {
                if (playable_entities[i].vel_y + gravity_y > 0) {
                  playable_entities[i].on_interactive_ground = true;
                  playable_entities[i].vel_y = gravity_y*-1+(this.animation_index == "moving_block_top" ? this.vel_y :0);
                } else if (this.animation_index == "moving_block_top") {
                  playable_entities[i].on_interactive_ground = true;
                  playable_entities[i].persistant_interactive_ground = true;
                  playable_entities[i].vel_y = this.vel_y + gravity_y*-1;
                  playable_entities[i].y = this.y-3-this.vel_y;
                }
              } 
            }
          },
        
        true_x,true_y,1.2,1.075,0.2,0.12);
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      
      // COINS - DICES
      } else if (interactive_tiles_map[x+','+y] == '$' || interactive_tiles_map[x+','+y] == '£') {
        var entity;
        if (Math.random() < 1/500) {
          entity = new Entity("dice",

            // Coin behaviour
            function(entity){
              
              // Loop in playbale entities, will need specific functions or object for multiple playbale entities
              for (var i = 0; i < playable_entities.length; i++) {
                if (playable_entities[i].health < 1) continue;
                if (collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y) && entity.animation_index != "dice_take") {
                    entity.animation_index = "dice_take";
                    if (entity.frame_index == 0) {
                      playable_entities[i].health += 6;
                    } else if (entity.frame_index == 1) {
                      playable_entities[i].health += 1;
                    } else if (entity.frame_index == 2) {
                      playable_entities[i].health += 4;
                    } else if (entity.frame_index == 3) {
                      playable_entities[i].health += 3;
                    } else if (entity.frame_index == 4) {
                      playable_entities[i].health += 5;
                    } else if (entity.frame_index == 5) {
                      playable_entities[i].health += 2;
                    }
                    entity.frame_index = 0;
                    live_display = 50;
                    setTimeout(function(){
                      this.dead = true;
                    }.bind(entity),1000);
                }
              }
            },
          
          true_x,true_y-1,0,0,0,0);
        } else {
          entity = new Entity("coin",

            // Coin behaviour
            function(entity){
              
              // Loop in playbale entities, will need specific functions or object for multiple playbale entities
              for (var i = 0; i < playable_entities.length; i++) {
                if (playable_entities[i].health < 1) continue;
                if (collide_to_map(player_scene,this.get_frame(),this.x-player0_cam_x,this.y-player0_cam_y) && entity.animation_index != "coin_taken") {
                    entity.frame_index = 0;
                    entity.animation_index = "coin_taken";
                    coins++;
                    if (coins >= 100) {
                      coins = 0;
                      playable_entities[i].health ++;
                      live_display = 50;
                    }
                    new_coin_display = 33;
                    setTimeout(function(){
                      this.dead = true;
                    }.bind(entity),1000);
                }
              }
            },
          
          true_x,true_y,0,0,0,0);
          max_coins++;
        }
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      // Banker
      } else if (interactive_tiles_map[x+','+y] == 'b') {
        let entity = new Entity("banker",

          // Coin behaviour
          function(entity){
            
            // Loop in playbale entities, will need specific functions or object for multiple playbale entities
            for (var i = 0; i < playable_entities.length; i++) {
            }
          },
        
        true_x,true_y,0,0,0,0);
        max_coins++;
        
        // Declare enitities chunk if empty
        if (!non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)]) {
          non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)] = [];
        }
        
        // Push to entity chunk
        non_playable_entities[Math.floor(true_x/chunk_w)+','+Math.floor(true_y/chunk_h)].push(entity);
      }
    }
  }
}

// From a specific point in tiles_map return a frame (art)
// For now only used for ground layer, maybe need a name more specific to this actual use
function chunk_to_frame(level,start_x,start_y) {
  for (var y = start_y*8; y<start_y*8+8; y++) {
    for (var x = start_x*8; x<start_x*8+8; x++) {
      // GROUND (will need a generic system for this)
      if (ground_tiles_map[x+','+y] == '#') {
        let tile_type = get_tile_type(x,y,'#',ground_tiles_map);
        add_frame_to_map(level_ground,animations["tiles_"+level+"_ground_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (ground_tiles_map[x+','+y] == '%') {
        let tile_type = get_tile_type(x,y,'%',ground_tiles_map);
        add_frame_to_map(level_ground,animations["tiles_0_decor1_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (ground_tiles_map[x+','+y] == 'H') {
        let tile_type = get_tile_type(x,y,'H',ground_tiles_map);
        add_frame_to_map(level_ground,animations["tiles_houses_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (ground_tiles_map[x+','+y] == '§') {
        let tile_type = get_tile_type(x,y,'§',ground_tiles_map);
        add_frame_to_map(level_ground,animations["tile_brick_"+tile_type][0],x*tile_w,y*tile_h);
      }
      // DECOR (will need a generic system for this)
      if (decor_tiles_map[x+','+y] == '#') {
        let tile_type = get_tile_type(x,y,'#',decor_tiles_map);
        add_frame_to_map(level_decor,animations["tiles_"+level+"_ground_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (decor_tiles_map[x+','+y] == '%') {
        let tile_type = get_tile_type(x,y,'%',decor_tiles_map);
        add_frame_to_map(level_decor,animations["tiles_0_decor1_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (decor_tiles_map[x+','+y] == 'H') {
        let tile_type = get_tile_type(x,y,'H',decor_tiles_map);
        add_frame_to_map(level_decor,animations["tiles_houses_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (decor_tiles_map[x+','+y] == 'T') {
        add_frame_to_map(level_decor,animations["tree0"][0],x*tile_w,y*tile_h);
      } else if (decor_tiles_map[x+','+y] == 't') {
        add_frame_to_map(level_decor,animations["tree1"][0],x*tile_w,y*tile_h);
      } else if (decor_tiles_map[x+','+y] == '§') {
        let tile_type = get_tile_type(x,y,'§',decor_tiles_map);
        add_frame_to_map(level_decor,animations["tile_brick_"+tile_type][0],x*tile_w,y*tile_h);
      }

/*
      if (tiles_map[x+','+y] == '#') {
          let tile_type = get_tile_type(x,y,'#');
          add_frame_to_map(level_ground,animations["tiles_"+level+"_ground_"+tile_type][0],x*tile_w,y*tile_h);
          if (tile_type.includes("alone")
            || tile_type.includes("bottomright")
            || tile_type.includes("bottomleft")
          ) {
            tile_type = get_tile_type(x,y,'=');
            add_frame_to_map(level_decor,animations["tiles_"+level+"_ground_"+tile_type][0],x*tile_w,y*tile_h);
          }
      } else if (tiles_map[x+','+y] == '=' || tiles_map[x+','+y] == 'x' || tiles_map[x+','+y] == '£' || tiles_map[x+','+y] == 'v') {
          let tile_type = get_tile_type(x,y,'=');
          add_frame_to_map(level_decor,animations["tiles_"+level+"_decor1_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (tiles_map[x+','+y] == 'H') {
          let tile_type = get_tile_type(x,y,'H');
          add_frame_to_map(level_ground,animations["tiles_houses_"+tile_type][0],x*tile_w,y*tile_h);
          if (
               tile_type.includes("alone")
            || tile_type.includes("bottomright")
            || tile_type.includes("bottomleft")
          ) {
            tile_type = get_tile_type(x,y,'h');
            add_frame_to_map(level_decor,animations["tiles_houses_"+tile_type][0],x*tile_w,y*tile_h);
          }
      } else if (tiles_map[x+','+y] == 'h' || tiles_map[x+','+y] == '+') {
          let tile_type = get_tile_type(x,y,'h');
          add_frame_to_map(level_decor,animations["tiles_houses_"+tile_type][0],x*tile_w,y*tile_h);
      } else if (tiles_map[x+','+y] == 'T') {
          add_frame_to_map(level_decor,animations["tree0"][0],x*tile_w,y*tile_h);
      } else if (tiles_map[x+','+y] == 't') {
          add_frame_to_map(level_decor,animations["tree1"][0],x*tile_w,y*tile_h);
      } else if (tiles_map[x+','+y] == 'B') {
          add_frame_to_map(level_decor,animations["bank"][0],x*tile_w,y*tile_h);
      }*/
    }
  }
}

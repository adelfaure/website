
// DATA

// MAPS

Map = function(w,h) {
  this.w = w;
  this.h = h;
  this.data = new Uint8Array(w*h);
}

Map.prototype.print = function() {
  let str = '';
  for (var i = 0; i < this.data.length; i++) {
    let glyph = this.data[i] == 255 ? ' ' : charset[this.data[i]];
    str += Number.isInteger((i+1)/this.w) ? glyph+'\n' : glyph;
  }
  return str;
}

function add_filter_to_map(map,filter) {
  for (var i = 0; i < map.data.length; i++) {
    if (filter[i]) map.data[i] = 0;
  }
}

function get_map_view(map,x,y,w,h) {
  x = Math.floor(x);
  y = Math.floor(y);
  w = Math.floor(w);
  h = Math.floor(h);
  let data = new Uint8Array(w*h);
  for (var i = 0; i < w * h; i++) {
    let x_index = x + i - w*Math.floor(i/w);
    if (x_index > map.w || x_index < 0) continue;
    let y_index = Math.floor(i/w) + y;
    if (y_index >= map.h || y_index < 0) continue;
    let glyph_index = map.data[x_index + y_index*map.w];
    data[i] = glyph_index;
  }
  return new Frame(10,data,w,h);
}

function add_text_to_map(map,str,x,y) {
  x = Math.floor(x);
  y = Math.floor(y);
  str = str.split('\n');
  w = str[0].length;
  h = str.length;
  str = str.join('');
  let content = new Uint8Array(w*h);
  for (var i = 0; i < str.length; i++) {
    content[i] = charset.indexOf(str[i]);
  }
  for (var i = 0; i < w * h; i++) {
    if (!content[i]) continue;
    let x_index = x + i - w*Math.floor(i/w);
    if (x_index >= map.w || x_index < 0) continue;
    let y_index = Math.floor(i/w) + y;
    if (y_index >= map.h || y_index < 0) continue;
    map.data[x_index+y_index*map.w] = content[i];
  }
}

function add_frame_to_map(map,frame,x,y) {
  x = Math.floor(x);
  y = Math.floor(y);
  for (var i = 0; i < frame.w * frame.h; i++) {
    if (!frame.data[i]) continue;
    let x_index = x + i - frame.w*Math.floor(i/frame.w);
    if (x_index >= map.w || x_index < 0) continue;
    let y_index = Math.floor(i/frame.w) + y;
    if (y_index >= map.h || y_index < 0) continue;
    map.data[x_index+y_index*map.w] = frame.data[i];
  }
}

function collide_to_map(map,frame,x,y) {
  x = Math.floor(x);
  y = Math.floor(y);
  for (var i = 0; i < frame.w * frame.h; i++) {
    if (!frame.data[i]) continue;
    let x_index = x + i - frame.w*Math.floor(i/frame.w);
    if (x_index >= map.w || x_index < 0) continue;
    let y_index = Math.floor(i/frame.w) + y;
    if (y_index >= map.h || y_index < 0) continue;
    if (map.data[x_index+y_index*map.w]) return true;
  }
  return false;
}

function fill_map(map,glyph_index,x,y,w,h) {
  x = Math.floor(x);
  y = Math.floor(y);
  for (var i = 0; i < w * h; i++) {
    if (!glyph_index) continue;
    let x_index = x + i - w*Math.floor(i/w);
    if (x_index >= map.w || x_index < 0) continue;
    let y_index = Math.floor(i/w) + y;
    if (y_index >= map.h || y_index < 0) continue;
    map.data[x_index+y_index*map.w] = glyph_index;
  }
}

// ENTITIES

let playable_entities = [];
let non_playable_entities = {};

Entity = function(name,behaviour,x,y,anti_vel_x,anti_vel_y,acc_vel_x,acc_vel_y) {
  this.name = name;
  this.animation_index = this.name;
  this.frame_index = 0;
  this.frame_time = 0;
  this.x = x;
  this.y = y;
  this.vel_x = 0;
  this.vel_y = 0;
  this.anti_vel_x = anti_vel_x;
  this.anti_vel_y = anti_vel_y;
  this.acc_vel_x = acc_vel_x;
  this.acc_vel_y = acc_vel_y;
  this.invincible = false;
  this.behaviour = behaviour;
  this.action = {};
  this.health = Infinity;
}

Entity.prototype.hurt = function() {
}

Entity.prototype.move = function(no_gravity) {
  if (this.climb) no_gravity = true;
  // COLLIDE AND MOVE
  if (!collide_to_map(level_ground,this.get_frame(true),
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
    this.x += this.vel_x + (no_gravity ? 0 : gravity_x);
  }
  if (!collide_to_map(level_ground,this.get_frame(true),
                this.x,
                this.y + this.vel_y + (no_gravity ? 0 : gravity_y))) {
    this.y += this.vel_y + (no_gravity ? 0 : gravity_y);
  }

  // ADAPT VELOCITY
  this.vel_x /= this.anti_vel_x;
  this.vel_y /= this.anti_vel_y;
  this.vel_x = this.vel_x > 0 && this.vel_x < 0.1 || this.vel_x < 0 && this.vel_x > -0.1 ? 0 : this.vel_x;
  this.vel_y = this.vel_y > 0 && this.vel_y < 0.1 || this.vel_y < 0 && this.vel_y > -0.1 ? 0 : this.vel_y;
}

Entity.prototype.get_frame = function(dont_animate) {
  if (this.frame_index > animations[this.animation_index].length-1) this.frame_index = 0;
  let frame = animations[this.animation_index][this.frame_index];
  if (dont_animate) return frame;
  let duration = animations[this.animation_index][this.frame_index].duration;
  this.frame_time = this.frame_time < duration ? this.frame_time + 1 : 0;
  this.frame_index = !this.frame_time ? this.frame_index < animations[this.animation_index].length-1 ? this.frame_index + 1 : 0 : this.frame_index;
  return frame;
}

Entity.prototype.control = function(keys,set) {
  if (this.health <= 0) return;
    this.action = {};
  if (keys[set[0]]) {
    this.action["up"] = true;
    if (collide_to_map(level_ground,this.get_frame(true),this.x,this.y + 1) || this.on_interactive_ground) {
      this.vel_y = gravity_y * -2.1;
    }
  }
  if (keys[set[1]]) {
    this.action["left"] = true;
    this.vel_x = this.vel_x > -1 ? this.vel_x - this.acc_vel_x : -1;
  }
  if (keys[set[2]]) {
    this.action["right"] = true;
    this.vel_x = this.vel_x < 1 ? this.vel_x + this.acc_vel_x : 1;
  }
  if (keys[set[3]]) {
    this.action["down"] = true;
  }
}


Entity.prototype.update_animation_index = function(no_gravity) {
  let name = this.climb ? this.name+"_climb" : this.name;
  if (this.climb) no_gravity = true;
  var vel_x = 0;
  var vel_y = 0;
  if (!collide_to_map(level_ground,this.get_frame(true),
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
    vel_x = this.vel_x + (no_gravity ? 0 : gravity_x);
  }
  if (!collide_to_map(level_ground,this.get_frame(true),
                this.x,
                this.y + this.vel_y + (no_gravity ? 0 : gravity_y))) {
    vel_y = this.vel_y + (no_gravity ? 0 : gravity_y);
  }
  if (vel_x < 0 && vel_y < 0) {
    if (!collide_to_map(level_ground, animations[name+"_top_left"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_top")+"_left";
    }
  } else if (vel_x == 0 && vel_y < 0) {
    if (!collide_to_map(level_ground, animations[name+"_top"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_top");
    }
  } else if (vel_x > 0 && vel_y < 0) {
    if (!collide_to_map(level_ground, animations[name+"_top_right"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_top")+"_right";
    }
  } else if (vel_x < 0 && vel_y == 0) {
    if (!collide_to_map(level_ground, animations[name+"_left"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+"_left";
    }
  } else if (vel_x == 0 && vel_y == 0) {
    if (!collide_to_map(level_ground, animations[name][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name;
    }
  } else if (vel_x > 0 && vel_y == 0) {
    if (!collide_to_map(level_ground, animations[name+"_right"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+"_right";
    }
  } else if (vel_x < 0 && vel_y > 0) {
    if (!collide_to_map(level_ground, animations[name+"_bottom_left"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_bottom")+"_left";
    }
  } else if (vel_x == 0 && vel_y > 0) {
    if (!collide_to_map(level_ground, animations[name+"_bottom"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_bottom");
    }
  } else if (vel_x > 0 && vel_y > 0) {
    if (!collide_to_map(level_ground, animations[name+"_bottom_right"][0],
                this.x + this.vel_x + (no_gravity ? 0 : gravity_x),
                this.y)) {
      this.animation_index = name+(this.on_interactive_ground ? '' :"_bottom")+"_right";
    }
  }
}

// TILES

const tile_w = 8;
const tile_h = 4;
var decor_tiles_map = {};
var ground_tiles_map = {};
var interactive_tiles_map = {};

function parse_tiles(tile_set) {
  let parsed_tiles = {};
  let tiles = tile_set.split('TILE');
  for (var i = 1; i < tiles.length; i++) {
    parsed_tiles[tiles[i].split('DIR')[0].replaceAll(' ','')] = tiles[i].split('DIR')[1].slice(1);
  }
  return parsed_tiles;
}

function add_chunk_to_tiles_maps(x,y,chunk) {
  for (var i = 0; i < chunk.length; i++) {
    let rows = chunk[i].split('\n');
    for (var row = 0; row < rows.length-1; row++){
      for (var col = 0; col < rows[row].length; col++) {
        if (rows[row][col] == '.') continue;
        if (i == 0 ) {
          decor_tiles_map[(x*8+col)+','+(y*8+row)] = rows[row][col];
        } else if (i == 1) {
          ground_tiles_map[(x*8+col)+','+(y*8+row)] = rows[row][col];
        } else if (i == 2) {
          interactive_tiles_map[(x*8+col)+','+(y*8+row)] = rows[row][col];
        }
      }
    }
  }
}
let tile_dirs = [
            [0,-1],           
  [-1, 0],           [1,  0],
            [0, 1]           
];
function get_tile_type(x,y,tile,map){
  let neighbors = [];
  for (var i = 0; i < tile_dirs.length; i++) {
    neighbors.push(
      // DIRT
         tile == '#' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '#'  
      || tile == '#' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '%'  
      || tile == '#' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '+'  
      || tile == '#' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '§'  
      || tile == '%' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '%'  
      || tile == '%' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '#'  
      || tile == '%' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '+'  
      || tile == '%' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '§'  
      // HOUSES
      || tile == 'H' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == 'H'  
      || tile == 'H' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '§'  
      // PIKES
      || tile == 'X' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '#'  
      || tile == 'X' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '%'  
      || tile == 'X' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == 'H'  
      // LADDER
      || tile == '=' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '='  
      // PIKE_BLOCK
      || tile == '+' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '+'  
      || tile == '+' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '#'  
      || tile == '+' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '%'  
      // SEMI GROUND
      || tile == '0' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '0'  
      || tile == '1' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '1'  
      || tile == '2' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '2'  
      // BRICK
      || tile == '§' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '§'  
      || tile == '§' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == 'H'  
      || tile == '§' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '#'  
      || tile == '§' && map[(x+tile_dirs[i][0])+','+(y+tile_dirs[i][1])] == '%'  
    );
  }
  if (neighbors[0] && neighbors[1] && neighbors[2] && !neighbors[3]) {
    return "bottom";
  } else if ( neighbors[0] && !neighbors[1] && !neighbors[2] && !neighbors[3]) {
    return "bottomalone";
  } else if ( neighbors[0] && !neighbors[1] &&  neighbors[2] && !neighbors[3]) {
    return "bottomleft";
  } else if ( neighbors[0] &&  neighbors[1] && !neighbors[2] && !neighbors[3]) {
    return "bottomright";
  } else if ( neighbors[0] &&  neighbors[1] &&  neighbors[2] &&  neighbors[3]) {
    return "center";
  } else if (!neighbors[0] && !neighbors[1] && !neighbors[2] && !neighbors[3]) {
    return "centeralone";
  } else if ( neighbors[0] && !neighbors[1] && !neighbors[2] &&  neighbors[3]) {
    return "centerybridge";
  } else if (!neighbors[0] &&  neighbors[1] &&  neighbors[2] && !neighbors[3]) {
    return "centerxbridge";
  } else if ( neighbors[0] && !neighbors[1] &&  neighbors[2] &&  neighbors[3]) {
    return "left";
  } else if (!neighbors[0] && !neighbors[1] &&  neighbors[2] && !neighbors[3]) {
    return "leftalone";
  } else if ( neighbors[0] &&  neighbors[1] && !neighbors[2] &&  neighbors[3]) {
    return "right";
  } else if (!neighbors[0] &&  neighbors[1] && !neighbors[2] && !neighbors[3]) {
    return "rightalone";
  } else if (!neighbors[0] &&  neighbors[1] &&  neighbors[2] &&  neighbors[3]) {
    return "top";
  } else if (!neighbors[0] && !neighbors[1] && !neighbors[2] &&  neighbors[3]) {
    return "topalone";
  } else if (!neighbors[0] && !neighbors[1] &&  neighbors[2] &&  neighbors[3]) {
    return "topleft";
  } else if (!neighbors[0] &&  neighbors[1] && !neighbors[2] &&  neighbors[3]) {
    return "topright";
  }
}

// ANIMATION

const animations = {};

function get_raw_assets(className) {
  let raw_animation_assets = {};
  let raw_animation_embed = document.getElementsByClassName(className);
  for (var i = 0; i < raw_animation_embed.length; i++){
    raw_animation_assets[raw_animation_embed[i].id] = raw_animation_embed[i].contentDocument.body.children[0].innerText;
  }
  while (raw_animation_embed.length) {
    raw_animation_embed[0].parentNode.removeChild(raw_animation_embed[0]);
  }
  return raw_animation_assets;
}


function parse_animation(name,raw_animation) {
  let raw_frames = raw_animation.split('FRAME');
  frames = [];
  for (var i = 1; i < raw_frames.length; i++) {
    let raw_frame = raw_frames[i].split('TIMES');
    frames.push(text_to_frame(raw_frame[1],parseInt(raw_frame[0])));
  }
  animations[name] = frames;
}

// FRAMES

Frame = function(duration,data,w,h) {
  this.duration = duration;
  this.data = data;
  this.w = w;
  this.h = h;
}

function text_to_frame(text,duration) {
  let lines = text.split('\n');
  if (!lines[0].length) lines.shift();
  if (!lines[lines.length-1].length) lines.pop();
  let w = lines[0].length;
  let h = lines.length;
  lines = lines.join('');
  let data = new Uint8Array(w*h);
  for (var i = 0; i < lines.length; i++) {
    data[i] = charset.indexOf(lines[i]);
  }
  return new Frame(duration,data,w,h);
}


// LAYERS

function create_layer(color){
  layer = document.createElement("pre");
  layer.style.color = color;
  document.body.appendChild(layer);
  return layer;
}

// CAMERA

const cameras = [];

Camera = function(x,y,w,h) {
  this.x = x;
  this.y = y;
  this.w = w;
  this.h = h;
  cameras.push(this);
}

new Camera(0,0,view_w,view_h);
new Camera(0,0,view_w,view_h);

// KEYS
let keys_pressed = {};
let keys_released = {};
document.addEventListener("keydown",function(e){
  keys_pressed[e.key] = true;
});
document.addEventListener("keyup",function(e){
  keys_released[e.key] = true;
});

// DRAW

let draw_x = 0;
let factor_x = 0;
let factor_y = 0;
let draw_y = 0;
var frame = 0; 

let view_dirs = [
/*[-2,-2],   [-1,-2],  [0,-2],  [1, -2],/* [2,-2],*/
  [-2,-1],   [-1,-1],  [0,-1],  [1, -1],   [2,-1],  
  [-2, 0],   [-1, 0],  [0, 0],  [1,  0],   [2, 0],  
  [-2, 1],   [-1, 1],  [0, 1],  [1,  1],   [2, 1],  
/*[-2, 2],   [-1, 2],  [0, 2],  [1,  2],/* [2, 2],*/
];

var player0_cam_x_offset = 0;
var player0_cam_x = 0;
var player0_cam_y_offset = 0;
var player0_cam_y = 0;
var new_coin_display = 0;
var live_display = 0;
function draw() {
  // clear
  decor_scene.data.fill(0);
  ground_scene.data.fill(0);
  interactive_scene.data.fill(0);
  player_scene.data.fill(0);
  

  // Entities


  // playable
  for (var i = 0; i < playable_entities.length; i++) {
    if (playable_entities[i].climb) {
      playable_entities[i].climb = false;
      playable_entities[i].vel_y = 
        playable_entities[i].vel_y > -gravity_y ? playable_entities[i].action["up"] ? gravity_y * -2.1 : gravity_y * -1  : 0 ;
    }
    if (playable_entities[i].on_interactive_ground) {
      if (playable_entities[i].persistant_interactive_ground) {
        playable_entities[i].persistant_interactive_ground = false;
      } else {
        playable_entities[i].on_interactive_ground = false;
      }
    }
    // camera
      if (Math.floor(playable_entities[i].x-cameras[i].x) >= Math.floor(cameras[i].w/10*6)) {
        cameras[0].x = Math.floor(playable_entities[i].x - cameras[i].w/10*6);
      }
      if (Math.floor(playable_entities[i].x-cameras[i].x) <= Math.floor(cameras[i].w/10*4)) {
        cameras[0].x = Math.floor(playable_entities[i].x - cameras[0].w/10*4);
      }
      if (Math.floor(playable_entities[i].y-cameras[i].y) >= Math.floor(cameras[i].h/10*6)) {
        cameras[0].y =  Math.floor(playable_entities[i].y -cameras[i].h/10*6);
      }
      if (Math.floor(playable_entities[i].y-cameras[i].y) <= Math.floor(cameras[i].h/10*4)) {
        cameras[0].y = Math.floor(playable_entities[i].y - cameras[i].h/10*4);
      }

      player0_cam_x_offset = Math.floor((scene_w-view_w-3)/2);
      player0_cam_x = cameras[0].x - player0_cam_x_offset;
      player0_cam_y_offset = Math.floor((scene_h-view_h-3)/2);
      player0_cam_y = cameras[0].y - player0_cam_y_offset;
    // player
    if (playable_entities[i].invincible || playable_entities[i].health < 1) {
      if (blink(frame,5)) {
        add_frame_to_map(player_scene,playable_entities[i].health < 1 ? animations["player_dead"][0] : playable_entities[i].get_frame(),playable_entities[i].x-player0_cam_x,playable_entities[i].y-player0_cam_y);
      } else {
        add_frame_to_map(player_scene,animations["dead"][0],playable_entities[i].x-player0_cam_x,playable_entities[i].y-player0_cam_y);
      }
    } else {
      add_frame_to_map(player_scene,playable_entities[i].get_frame(),playable_entities[i].x-player0_cam_x,playable_entities[i].y-player0_cam_y);
    }
      add_frame_to_map(decor_scene,get_map_view(far_bg,player0_cam_x/64,player0_cam_y/64,cameras[0].w,cameras[0].h),0,0);
      add_frame_to_map(decor_scene,get_map_view(close_bg,player0_cam_x/8,player0_cam_y/8,cameras[0].w,cameras[0].h),0,0);
      add_frame_to_map(decor_scene,get_map_view(level_decor,player0_cam_x,player0_cam_y,cameras[0].w,cameras[0].h),0,0);
    // non playable
      for (var dir = 0; dir < view_dirs.length; dir++) {
        let chunk = non_playable_entities[(Math.floor(playable_entities[i].x/chunk_w)+view_dirs[dir][0])+','+(Math.floor(playable_entities[i].y/chunk_h)+view_dirs[dir][1])];
        if (chunk) {
          for (var j = 0; j < chunk.length; j++) {
            if (chunk[j].dead) continue;
            add_frame_to_map(
                (/*chunk[j].animation_index.includes("ladder") || chunk[j].animation_index.includes("semi_ground") ? ground_scene : */interactive_scene),
                chunk[j].get_frame(),
                chunk[j].x-player0_cam_x,
                chunk[j].y-player0_cam_y
            );
            chunk[j].behaviour(chunk[j],frame);
          }
        }
      }
    if (live_display) {
      live_display--;
      add_text_to_map(interactive_scene, String(playable_entities[0].health)+'█Lives',
        playable_entities[0].x-player0_cam_x + 3,
        playable_entities[0].y-player0_cam_y - 1,
      );
    } else if (new_coin_display) {
      new_coin_display--;
      add_text_to_map(interactive_scene, String(coins)+'█Coins',
        playable_entities[0].x-player0_cam_x + 3,
        playable_entities[0].y-player0_cam_y - 1,
      );
    }
    playable_entities[i].action = false;
    playable_entities[i].behaviour(playable_entities[i],frame);
    
  
  
      add_frame_to_map(ground_scene,get_map_view(level_ground,player0_cam_x,player0_cam_y,cameras[0].w,cameras[0].h),0,0);
  }
  

  // ground layer 
  
  //add_frame_to_map(ground_scene,get_map_view(level_ground,cameras[1].x,cameras[1].y,cameras[1].w,cameras[1].h),0,scene_h/2);
  
  
  // interactive layer

  
  
  add_text_to_map(interactive_scene,"HOW█TO█PLAY█>█'J'█=█RUN█LEFT█|█'L'█=█RUN█RIGHT█|█'I'█=█JUMP█|█'K'█=█MOVE█DOWN█",0,0);
  add_text_to_map(interactive_scene,print_fps() + "█FPS█\n" + Math.floor(game_speed)+"█PERF",0,1);
  add_text_to_map(interactive_scene,"CHUNK█" + Math.floor(playable_entities[0].x/chunk_w) +','+Math.floor(playable_entities[0].y/chunk_h),0,3);
  add_text_to_map(interactive_scene,"COINS█"+coins,8,1);
  add_text_to_map(interactive_scene,"LIVES█"+ playable_entities[0].health,8,2);
  

  add_filter_to_map(decor_scene,interactive_scene.data)
  add_filter_to_map(decor_scene,player_scene.data)
  add_filter_to_map(decor_scene,ground_scene.data)
  decor_layer.textContent = decor_scene.print();
  add_filter_to_map(ground_scene,interactive_scene.data)
  add_filter_to_map(ground_scene,player_scene.data)
  ground_layer.textContent = ground_scene.print();
  add_filter_to_map(interactive_scene,player_scene.data)
  interactive_layer.textContent = interactive_scene.print();
  player_layer.textContent = player_scene.print();
  
  
  // Release keys
  for (var i in keys_released) {
    if (keys_released[i]) {
      keys_pressed[i] = false;
      keys_released[i] = false;
    }
  }
  
  optimize_fps();
  frame++;
  setTimeout(draw, game_speed);
}

// INIT

// maps
const decor_scene = new Map(scene_w,scene_h);
const ground_scene = new Map(scene_w,scene_h);
const interactive_scene = new Map(scene_w,scene_h);
const player_scene = new Map(scene_w,scene_h);

// layers
document.body.style.backgroundColor = "#"+["10","08"][Math.floor(Math.random()*2)]+["10","08"][Math.floor(Math.random()*2)]+["10","08"][Math.floor(Math.random()*2)];
const decor_layer = create_layer("#6"+["3","2"][Math.floor(Math.random()*2)]+["3","2"][Math.floor(Math.random()*2)]);
const ground_layer = create_layer("#9"+["7","6"][Math.floor(Math.random()*2)]+["7","6"][Math.floor(Math.random()*2)]);
const interactive_layer = create_layer("#"+["5","1"][Math.floor(Math.random()*2)]+"9"+["5","1"][Math.floor(Math.random()*2)]);
const player_layer = create_layer("#FF8");

// chunks
const levels_chunks = [];

// levels
var level = 0;
var gravity_x = 0;
var gravity_y = 1;
const level_ground = new Map(level_w,level_h);
const level_decor = new Map(level_w,level_h);
const close_bg = new Map(level_w,level_h);
const far_bg = new Map(level_w,level_h);

window.onload = function() {

  // animations
  let raw_animations = get_raw_assets("animation");
  for (var i in raw_animations) {
    parse_animation(i,raw_animations[i]);
  }
  
  // tile_sets
  let tile_sets = get_raw_assets("tile_set");
  for (var i in tile_sets) {
    let parsed_tile = parse_tiles(tile_sets[i]);
    for (var j in parsed_tile) {
      animations[i+'_'+j] = [text_to_frame(parsed_tile[j],1)];
    }
  }

  // player
  let player = new Entity("player",function(entity,frame){
    entity.control(keys_pressed,['i','j','l','k']);
    entity.move();
    entity.update_animation_index();
  },chunk_w*2,chunk_h*18+4*7+1,1.2,1.075,0.2,0.12);
  playable_entities.push(player);
  player.health = 3;
  
  // chunk_sets
  let chunk_sets = get_raw_assets("chunk_set");
  for (var i in chunk_sets) {
    levels_chunks.push(parse_chunks(chunk_sets[i]));
  }

  // Build level
  let end_x = 20;
  let end_y = 20;
  for (var y = 0; y < end_y; y ++) {
    for (var x = 0; x < end_x; x ++) {
      var possible_chunks = [];
      for (var i = 0; i < levels_chunks[level].length; i++) {
        let x_validity = false;
        let y_validity = false;
        let x_equal = false;
        let y_equal = false;
        let x_comparator = levels_chunks[level][i][0];
        let y_comparator = levels_chunks[level][i][1];
        if (x_comparator.length == 1 && x_comparator[0] == x) {
          x_equal = true;
        }
        if (x_comparator.length == 2 && x >= x_comparator[0] && x <= x_comparator[1] || x_comparator[0] == "ANY") {
          x_validity = true;
        }
        if (y_comparator.length == 1 && y_comparator[0] == y) {
          y_equal = true;
        }
        if (y_comparator.length == 2 && y >= y_comparator[0] && y <= y_comparator[1] || y_comparator[0] == "ANY") {
          y_validity = true;
        }
        if (x_validity && y_validity   
        ||  x_validity && y_equal        
        ||  y_validity && x_equal   ) {
          possible_chunks.push(levels_chunks[level][i][2]);
        }
        if (x_equal && y_equal) {
          var possible_chunks = [];
          possible_chunks.push(levels_chunks[level][i][2]);
          i = Infinity;
        }
      }
      let choosen_chunk = Math.floor(Math.random()*possible_chunks.length);
      if (possible_chunks[choosen_chunk]) {
        add_chunk_to_tiles_maps(x,y,possible_chunks[choosen_chunk]);
      }
    }
  }

  for (var y = 0; y < end_y; y ++) {
    for (var x = 0; x < end_x; x ++) {
      if (y == 0) {
        add_frame_to_map(far_bg,animations["bg_city_1"][0],chunk_w*x,chunk_h*y-12);
      } else if (y ==2) {
        add_frame_to_map(close_bg,animations["bg_city"][0],chunk_w*x,chunk_h*y-24);
      }
      chunk_to_frame(0,x,y);
      declare_chunk_entities(0,x,y,chunk_w*x,chunk_h*y);
    }
  }

  
  // Remove loading screen
  document.getElementById("loading_screen").textContent = '';
  
  // draw 
  draw();
}

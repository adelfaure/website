function copy_asset(asset){
  let copy = [];
  let i = assets[asset].length;
  while(i--) {
    copy[i] = assets[asset][i];
  }
  return copy;
}
function change_color(asset,from,to) {
  for (var i = 0; i < asset.length-2; i+=3) {
    if (asset[i] == from) {
      asset[i] = to;
    }
    if (asset[i+1] == from) {
      asset[i+1] = to
    }
  }
}
function change_full_color(asset,from,to) {
  for (var i = 0; i < asset.length-2; i+=3) {
    if (asset[i] == from[0] && asset[i+1] == from[1]) {
      asset[i] = to[0];
      asset[i+1] = to[1];
    }
  }
}
//function  glitch(asset,value) {
//  for (var i = 0; i < asset.length-2; i+=3) {
//      asset[i]+=Math.floor(Math.random()*value);
//      asset[i+1]+=Math.floor(Math.random()*value);
//      asset[i+2]+=Math.floor(Math.random()*value);
//  }
//}
function change_glyph(glyph,x,y) {
  let glyph_pos = get_glyph_pos(x,y);
  let glyph_data = get_glyph_data("typed",glyph_pos);
  set_glyph("typed",x,y,glyph_data[0],glyph_data[1],glyph);
}
function change_fg_color(fg_color,x,y) {
  let glyph_pos = get_glyph_pos(x,y);
  let glyph_data = get_glyph_data("typed",glyph_pos);
  set_glyph("typed",x,y,glyph_data[0],fg_color,glyph_data[2]);
}

// UI

function writeBig(str,x,y) {
  for (var i = 0; i < str.length; i++) {
    if(str[i] == ' ')continue;
    paste_zone(assets[str[i]],x+i*3,y);
  }
}
function write(str,x,y,fg,bg) {
  if (fg == undefined) fg = 0;
  if (bg == undefined) bg = 11;
  for (var i = 0; i < str.length; i++) {
    set_glyph("typed",i+x,y,20+bg,20+fg,char_set.indexOf(str[i]));
  }
}
function paragraph(str,x,y,w) {
  let start_x = x;
  let start_y = y;
  let words = str.split(' ');
  for (var i = 0; i < words.length; i++) {
    write(words[i]+' ',x,y);
    if (x + words[i].length+1 > w+start_x) {
      x = start_x;
      y++;
    } else {
      x+=words[i].length+1;
    }
  }
}

function bar(initials,value,max_value,fg,bg,x,y) {
  set_glyph("typed",x,y,11,bg,48);
  write(initials,x+1,y,fg,bg);
  write(' ',x+3,y,fg,bg);
  for (var i = 0; i < 21; i++){
    set_glyph("typed",x+4+i,y,11,bg,89);
  }
  set_glyph("typed",x+25,y,11,bg,49);
  write(String(max_value),x+25-String(max_value).length,y,11,bg);
  write(' ',x+25-String(max_value).length-1,y,11,bg);
  let bar_length = Math.ceil(value / max_value * 21);
  for (var i = 0; i < bar_length; i++){
    set_glyph("typed",x+4+i,y,fg,bg,89);
  }
  write(String(Math.floor(value)),x+(bar_length+4)-String(Math.floor(value)).length,y,bg,fg);
}

function print_cursor(x,y,unset) {
  let glyph_pos = get_glyph_pos(x,y);
  let typed_data = get_glyph_data("typed",glyph_pos);
  set_glyph("ui",x,y,unset ? 0 : typed_data[1],unset ? 0 : typed_data[0],unset ? 0 : typed_data[2]);
}

function box(x,y,w,h,fg,bg,tl,t,tr,l,r,bl,b,br) {
  set_glyph("typed",x,y,bg,fg,tl);
  for (var i = 1; i < w; i++) {
    set_glyph("typed",x+i,y,bg,fg,t);
  }
  set_glyph("typed",x+w,y,bg,fg,tr);
  for (var i = 1; i < h; i++) {
    set_glyph("typed",x,y+i,bg,fg,l);
  }
  for (var i = 1; i < h; i++) {
    set_glyph("typed",w,y+i,bg,fg,r);
  }
  set_glyph("typed",x,y+h,bg,fg,bl);
  for (var i = 1; i < w; i++) {
    set_glyph("typed",x+i,h,bg,fg,b);
  }
  set_glyph("typed",x+w,y+h,bg,fg,br);
}

function minus_plus_button(x,y,obj,key,min,max,cursor) {
  if (cursor.x >= x && cursor.x < x + 3 && cursor.y == y) {
    if (cursor.down) {
      write(" - ",x,y,11,0);
      obj[key] = obj[key] > min ? obj[key] - 1 : max;
      cursor.down = false;
    } else {
      write("[-]",x,y,10,11);
    }
  } else {
    write(" - ",x,y,1,11);
  }
  if (cursor.x >= x+3 && cursor.x < x + 6 && cursor.y == y) {
    if (cursor.down) {
      write(" + ",x+3,y,11,0);
      obj[key] = obj[key] < max ? obj[key] + 1 : min;
      cursor.down = false;
    } else {
      write("[+]",x+3,y,10,11);
    }
  } else {
    write(" + ",x+3,y,1,11);
  }
}

// PLAYER

const color_duos = [
  [0,10],
/*[1,11],*/
  [2,12],
  [12,11],
  [3,13],
  [13,11],
  [4,14],
  [5,15],
  [15,11],
  [6,16],
  [7,17],
  [17,11],
  [8,18],
  [9,19]
];
const earings_names = [
  "none",
  "right",
  "left",
  "both",
];
const mouth_names = [
  "neutral",
  "nice",
  "shy",
  "sad",
];
const ears_names = [
  "medium",
  "small",
  "mutant",
];
const eyes_names = [
  "big",
  "small",
  "mutant big",
  "mutant small",
];
const hair_names = [
  "bald",
  "punk",
  "short",
  "medium",
  "long",
  "very long"
];
const noze_names = [
  "none",
  "medium",
  "big",
  "large",
  "small"
];
const color_duos_names = [
  "white",
/*"black",*/
  "red",
  "dark red",
  "dark yellow",
  "brown",
  "yellow",
  "green",
  "dark green",
  "cyan",
  "blue",
  "dark blue",
  "purple",
  "pink",
];
const player_portrait = {
  hair : 0,
  eyes : 0,
  noze : 0,
  mouth : 0, 
  ears : 0, 
  earings : 0,
  skin : 0, 
  hair_color : 0, 
  shirt_color : 0,
};

let portrait_bg_color_index = 0; 
let skin_color = color_duos[player_portrait.skin];
let hair_color = color_duos[player_portrait.hair_color];
let shirt_color = color_duos[player_portrait.shirt_color];
let portrait_bg_color = [1,1];

function randomize_portrait(){
  player_portrait.hair = Math.floor(Math.random()*6);
  player_portrait.eyes = Math.floor(Math.random()*4);
  player_portrait.noze = Math.floor(Math.random()*5);
  player_portrait.mouth = Math.floor(Math.random()*4);
  player_portrait.ears = Math.floor(Math.random()*3);
  player_portrait.earings = Math.random() > 0.5 ? Math.floor(Math.random()*3)+1:0;
}

function randomize_colors(){
  player_portrait.skin = Math.floor(Math.random()*color_duos.length);
  player_portrait.hair_color = Math.floor(Math.random()*color_duos.length);
  player_portrait.shirt_color = Math.floor(Math.random()*color_duos.length);
  portrait_bg_color_index = Math.floor(Math.random()*color_duos.length);
  skin_color = color_duos[player_portrait.skin];
  hair_color = color_duos[player_portrait.hair_color];
  shirt_color = color_duos[player_portrait.shirt_color];
  //portrait_bg_color = color_duos[portrait_bg_color_index];
}

function print_player_portrait(
  x,
  y,
  hair_index,
  eyes_index,
  noze_index,
  mouth_index,
  ears_index,
  earings_index,
  skin_color,
  hair_color,
  shirt_color,
  portrait_bg_bolor,
){
  let player = copy_asset("player_"+player_portrait.hair);
  let player_eye = copy_asset("player_eye_"+player_portrait.eyes);
  let player_noze = copy_asset("player_noze_"+player_portrait.noze);
  let player_mouth = copy_asset("player_mouth_"+player_portrait.mouth);
  let player_left_ear = copy_asset("player_left_ear_"+player_portrait.ears);
  let player_right_ear = copy_asset("player_right_ear_"+player_portrait.ears);
  change_color(player,11,31);
  change_color(player,0,20);
  //// SKIN
  change_color(player,4,24);
  change_color(player,14,34);
  change_color(player_mouth,4,24);
  change_color(player_eye,14,34);
  change_color(player_eye,4,24);
  change_color(player_noze,14,34);
  change_color(player_noze,4,24);
  //// EAR
  change_color(player_left_ear,4,24);
  change_color(player_left_ear,14,34);
  change_color(player_left_ear,17,37);
  change_color(player_left_ear,11,31);
  change_color(player_right_ear,4,24);
  change_color(player_right_ear,14,34);
  change_color(player_right_ear,17,37);
  change_color(player_right_ear,11,31);
  //// HAIR
  change_color(player,16,36);
  change_color(player,17,37);
  //// SHIRT
  change_color(player,5,25);
  change_color(player,15,35);
  //// SKIN
  change_color(player,24,skin_color[0]);
  change_color(player,34,skin_color[1]);
  change_color(player_mouth,24,skin_color[0]);
  change_color(player_eye,24,skin_color[0]);
  change_color(player_eye,34,skin_color[1]);
  change_color(player_noze,24,skin_color[0]);
  change_color(player_noze,34,skin_color[1]);
  //// HAIR
  change_color(player,36,hair_color[0]);
  change_color(player,37,hair_color[1]);
  //// SHIRT
  change_color(player,25,shirt_color[0]);
  change_color(player,35,shirt_color[1]);
  change_full_color(player,[31,20],[portrait_bg_bolor[0],shirt_color[1]]);
  change_full_color(player,[31,hair_color[0]],[portrait_bg_bolor[0],hair_color[0]]);
  change_full_color(player,[31,skin_color[0]],[portrait_bg_bolor[0],skin_color[0]]);
  change_full_color(player,[31,shirt_color[0]],[portrait_bg_bolor[0],shirt_color[0]]);
  //// EAR
  change_color(player_left_ear,24,skin_color[0]);
  change_full_color(player_left_ear,[31,skin_color[0]],[portrait_bg_bolor[0],skin_color[0]]);
  change_full_color(player,[31,skin_color[0]],[portrait_bg_bolor[0],skin_color[0]]);
  change_full_color(player_left_ear,[31,0],[portrait_bg_bolor[0],skin_color[0]]);
  change_color(player_left_ear,34,skin_color[1]);
  change_color(player_left_ear,37,hair_color[1]);
  change_color(player_left_ear,31,11);
  change_color(player_right_ear,24,skin_color[0]);
  change_full_color(player_right_ear,[31,skin_color[0]],[portrait_bg_bolor[0],skin_color[0]]);
  change_full_color(player_right_ear,[31,0],[portrait_bg_bolor[0],skin_color[0]]);
  change_color(player_right_ear,34,skin_color[1]);
  change_color(player_right_ear,37,hair_color[1]);
  change_color(player_right_ear,31,11);
  // PORTRAIT
  paste_zone(player,x,y,[portrait_bg_bolor[0],shirt_color[1],255]);
  paste_zone(player_eye,x+7,y+5);
  paste_zone(player_eye,x+15,y+5);
  paste_zone(player_left_ear,x+3,y+5);
  paste_zone(player_right_ear,x+20,y+5);
  // EARING
  if(player_portrait.earings > 0) {
    if (player_portrait.earings == 1) {
    // LEFT
    change_glyph(136,x+5,y+7);
    change_fg_color(0,x+5,y+7);
    }
    if (player_portrait.earings == 2) {
    // RIGHT
    change_glyph(136,x+20,y+7);
    change_fg_color(0,x+20,y+7);
    }
    if (player_portrait.earings == 3) {
    // LEFT
    change_glyph(136,x+5,y+7);
    change_fg_color(0,x+5,y+7);
    // RIGHT
    change_glyph(136,x+20,y+7);
    change_fg_color(0,x+20,y+7);
    }
  }
  paste_zone(player_noze,x+11,y+6);
  paste_zone(player_mouth,x+11,y+7);
}

let vals = [0,8,16];

const palettes = {
  "black_on_white":[
    [0,0,0],
    [0,0,0],
    [0,0,0],
    [16,16,16],
  ],
};

const sprites_sheets_names = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "18",
  "19",
  "font_0",
  "font_1",
  "font_2",
  "font_3",
  "font_4",
  "font_5",
  "font_6",
  "font_7",
  "font_8",
  "font_9",
  "font_10",
  "font_11",
  "font_12",
  "font_13",
  "font_14",
  "font_15",
  "font_16",
  "font_17",
  "font_18",
  "font_19"
];

const colors = [
  "#FFF",
  "#666",
  "#F00",
  "#880",
  "#FF0",
  "#8F8",
  "#0FF",
  "#00F",
  "#80F",
  "#F8F",
  "#AAA",
  "#000",
  "#800",
  "#842",
  "#F80",
  "#084",
  "#08F",
  "#008",
  "#408",
  "#F08",
  "#FFF",
  "#666",
  "#F00",
  "#880",
  "#FF0",
  "#8F8",
  "#0FF",
  "#00F",
  "#80F",
  "#F8F",
  "#AAA",
  "#000",
  "#800",
  "#842",
  "#F80",
  "#084",
  "#08F",
  "#008",
  "#408",
  "#F08",
];

const sprites_sizes = [
  [4,8],
];

const char_set = [
  '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0',
  '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@',
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`',
  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
  'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}','~'
]

const keys = [
  '1','2','3','4','5','6','7','8','9','0','A','Z','E','R','T','Y','U','I','O','P','Q','S','D','F','G','H','J','K','L','M','W','X','C','V','B','N',',',';',':','!'
];

const key_position = {
  '&' :  0, '1' :  0,
  'é' :  1, '2' :  1,
  '"' :  2, '3' :  2,
  '\'':  3, '4' :  3,
  '(' :  4, '5' :  4,
  '-' :  5, '6' :  5,
  'è' :  6, '7' :  6,
  '_' :  7, '8' :  7,
  'ç' :  8, '9' :  8,
  'à' :  9, '0' :  9,
  'a' : 10, 'A' : 10,
  'z' : 11, 'Z' : 11,
  'e' : 12, 'E' : 12,
  'r' : 13, 'R' : 13,
  't' : 14, 'T' : 14,
  'y' : 15, 'Y' : 15,
  'u' : 16, 'U' : 16,
  'i' : 17, 'I' : 17,
  'o' : 18, 'O' : 18,
  'p' : 19, 'P' : 19,
  'q' : 20, 'Q' : 20,
  's' : 21, 'S' : 21,
  'd' : 22, 'D' : 22,
  'f' : 23, 'F' : 23,
  'g' : 24, 'G' : 24,
  'h' : 25, 'H' : 25,
  'j' : 26, 'J' : 26,
  'k' : 27, 'K' : 27,
  'l' : 28, 'L' : 28,
  'm' : 29, 'M' : 29,
  'w' : 30, 'W' : 30,
  'x' : 31, 'X' : 31,
  'c' : 32, 'C' : 32,
  'v' : 33, 'V' : 33,
  'b' : 34, 'B' : 34,
  'n' : 35, 'N' : 35,
  ',' : 36, '?' : 36,
  ';' : 37, '.' : 37,
  ':' : 38, '/' : 38,
  '!' : 39, '§' : 39
};

function import_sprites_sheets(sprites_sheets_names,sprites_sheets) {
  if (!sprites_sheets) sprites_sheets = [];
 
  //log("Import sprites sheet " + sprites_sheets_names.length + ' ' + sprites_sheets_names[0]);
  let sprite = new Image();
  sprite.src = "./assets/"+sprites_sheets_names.shift()+".png";
  return new Promise( resolve => {
    sprite.onload = function() {
      if (!sprites_sheets_names.length) {
        sprites_sheets.push(sprite);
        resolve(sprites_sheets);
      } else {
        sprites_sheets.push(sprite);
        return resolve(import_sprites_sheets(sprites_sheets_names,sprites_sheets));
      }
    }
  });
}

function get_char_position(key) {
  let key_code = char_set.indexOf(key);
  let y = Math.floor(key_code/64);
  let x = key_code - y * 64;
  return [x,y];
}

function get_glyph_position(key_code) {
  let y = Math.floor(key_code/64);
  let x = key_code - y * 64;
  return [x,y];
}

function create_past_memory(w,h) {
  let past_memory = document.createElement("canvas");
  past_memory.width = w;
  past_memory.height = h;
  return past_memory;
}
let bg_glyph_pos = get_glyph_position(79);
let size = [4,8];

/* SHEETS */

let sheet = 0;
let control_mode = false;

document.addEventListener("keydown",function(e) {
  e.preventDefault();
  e.stopPropagation();
  if(e.key == "Control") {
    control_mode = true;
  }
});

document.addEventListener("keyup",function(e) {
  if(e.key == "Control") {
    control_mode = false;
  }
});

function show_sheet(sprites,x,y) {
  for (var i = 0; i < 40; i++) {
    let glyph_pos = get_glyph_position(i+40*sheet);
    draw_glyph(
      sprites,
      enter_mode ? i == last_key_pos ? bg_color : fg_color : i == last_key_pos ? fg_color : bg_color,
      enter_mode ? i == last_key_pos ? fg_color : bg_color : i == last_key_pos ? bg_color : fg_color,
      x + i-10*Math.floor(i/10),
      y + Math.floor(i/10),
      glyph_pos
    );
  }
}

/* CURSOR*/

let cursor_blink = false;

const cursors = [];
cursors.push([Math.floor(canvas.width/2/size[0]),Math.floor(canvas.height/2/size[1]),false,false]);

/* SPEED MODE */

let speed_mode = false;

document.addEventListener("keydown",function(e) {
  e.preventDefault();
  e.stopPropagation();
  if(e.key == "Tab") {
    speed_mode = true;
  }
});

document.addEventListener("keyup",function(e) {
  if(e.key == "Tab") {
    speed_mode = false;
  }
});

/* Write MODE */

let write_mode = false;

document.addEventListener("keydown",function(e) {
  e.preventDefault();
  e.stopPropagation();
  if(e.key == "Escape") {
    write_mode = !write_mode;
  }
});

/* ENTER MODE */

let enter_mode = false;

document.addEventListener("keydown",function(e) {
  e.preventDefault();
  e.stopPropagation();
  if(e.key == "Enter") {
    enter_mode = !enter_mode;
  }
});

/* COLOR */

let fg_color = 0;
let bg_color = 11;
let alt_mode = false;

document.addEventListener("keydown",function(e) {
  e.preventDefault();
  e.stopPropagation();
  if(e.key == "Alt") {
    alt_mode = true;
  }
});

document.addEventListener("keyup",function(e) {
  if(e.key == "Alt") {
    alt_mode = false;
  }
});

function change_color(key_position) {
  if (!(key_position > -1)) return
  if (key_position < 20) {
    fg_color = key_position;
  } else {
    bg_color = key_position-20;
  }
}

/* TYPE */

last_key_pos = undefined;

function type(sprites,key) {
  console.log(key);

  last_key_pos = write_mode ? char_set.indexOf(key) : key_position[key];
  
  if (!(last_key_pos > -1)) return;
  let glyph_pos = write_mode ? get_glyph_position(last_key_pos) : get_glyph_position(last_key_pos+40*sheet);
  let memory = [];
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let glyph_bg_color = enter_mode ? write_mode ? fg_color + 20 : fg_color : write_mode ? bg_color + 20 : bg_color; 
    let glyph_fg_color = enter_mode ? write_mode ? bg_color + 20 : bg_color : write_mode ? fg_color + 20 : fg_color;
    memory.push([
      [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
    ]);
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ] = glyph_bg_color;
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = glyph_fg_color;
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2] = last_key_pos+(write_mode ? 0 : 40*sheet);
    let glyph_x  = glyph_pos[0];
    let glyph_y  = glyph_pos[1];
    draw_glyph(sprites,glyph_bg_color,glyph_fg_color,cursors[i][0],cursors[i][1],[glyph_x,glyph_y]);
    if (speed_mode) { 
      cursors[i][0] += 5;
    } else {
      cursors[i][0]++;
    }
  }
  past_memory.push(memory);
}

/* GLYPHS */

let cols = 1920/4;
let rows = 1080/8;

var glyphs = new Uint16Array(cols*rows*3);

/* MEMORY */

let past_memory = [];
let futur_memory = [];

/* INIT */

import_sprites_sheets(sprites_sheets_names).then(sprites => {

  // CURSORS
  document.addEventListener("keydown",function(e){
    e.preventDefault();
    e.stopPropagation();
    unprint_cursors(sprites);
    
    if (alt_mode) {
      change_color(key_position[e.key]);
    } else if (control_mode) {
      // UNDO
      if (e.key.toLowerCase() == 'z' && past_memory.length) {
        let undo = past_memory.pop();
        let redo = [];
        for (var i = 0; i < undo.length; i++) {
          let memory = [];
          for (var j = 0; j < undo[i].length; j++) {
            memory.push([undo[i][j][0],glyphs[undo[i][j][0]]]);
            glyphs[undo[i][j][0]] = undo[i][j][1];
          }
          redo.push(memory);
        }
        futur_memory.push(redo);
        update(sprites);
      // REDO
      } else if (e.key.toLowerCase() == 'y' && futur_memory.length){
        let redo = futur_memory.pop();
        let undo = [];
        for (var i = 0; i < redo.length; i++) {
          let memory = [];
          for (var j = 0; j < redo[i].length; j++) {
            memory.push([redo[i][j][0],glyphs[redo[i][j][0]]]);
            glyphs[redo[i][j][0]] = redo[i][j][1];
          }
          undo.push(memory);
        }
        past_memory.push(undo);
        update(sprites);
      // COPY
      } else if (e.key.toLowerCase() == 'c'){
        copy(sprites);
        update(sprites);
      // PASTE
      } else if (e.key.toLowerCase() == 'v'){
        paste(sprites);
        update(sprites);
      // COLORIZE
      } else if (e.key.toLowerCase() == 'f'){
        colorize(sprites);
        update(sprites);
      // INVERT
      } else if (e.key.toLowerCase() == 'i'){
        invert(sprites);
        update(sprites);
      // LOAD
      } else if (e.key.toLowerCase() == 'l'){
        input.click();
      // CHANGE SHEET
      } else {
        let sheet_position = key_position[e.key];
        sheet = sheet_position == undefined ? sheet : sheet_position; 
      }
      // LOAD
    } else {
      if (e.key == " ") {
        let memory = [];
        for (var i = 0; i < cursors.length; i++) {
          memory.push([
            [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
            [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
            [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
          ]);
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3] = 0;
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = 0;
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2] = 0;
          draw_glyph(sprites,11,11,cursors[i][0],cursors[i][1],[15,15]);
          if (speed_mode) { 
            cursors[i][0] += 5;
          } else {
            cursors[i][0]++;
          }
        }
        past_memory.push(memory);
      } else if (e.key == "Backspace") {
        let memory = [];
        for (var i = 0; i < cursors.length; i++) {
          if (speed_mode) { 
            cursors[i][0] -= 5;
          } else {
            cursors[i][0]--;
          }
          memory.push([
            [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
            [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
            [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
          ]);
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3] = 0;
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = 0;
          glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2] = 0;
          draw_glyph(sprites,11,11,cursors[i][0],cursors[i][1],[15,15]);
        }
        past_memory.push(memory);
      } else {
        type(sprites,e.key);
      }
    }
    

    // SELECTION
    if (alt_mode){
      if (e.key == "ArrowLeft") {
        let max_x = -Infinity;
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][0] > max_x) max_x = cursors[i][0];
        }
        let new_pos = []
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][0] == max_x && i) {
            cursors.splice(i,1);
            i--;
          }
        }
      }
      if (e.key == "ArrowUp") {
        let max_y = -Infinity;
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][1] > max_y) max_y = cursors[i][1];
        }
        let new_pos = []
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][1] == max_y && i) {
            cursors.splice(i,1);
            i--;
          }
        }
      }
      if (e.key == "ArrowRight") {
        let max_x = -Infinity;
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][0] > max_x) max_x = cursors[i][0];
        }
        let new_pos = []
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][0] == max_x) {
            new_pos.push([cursors[i][0]+1,cursors[i][1]]);
          }
        }
        for (var i = 0; i < new_pos.length; i++) {
          cursors.push([new_pos[i][0],new_pos[i][1],cursors[0][2],false]);
        }
      }
      if (e.key == "ArrowDown") {
        let max_y = -Infinity;
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][1] > max_y) max_y = cursors[i][1];
        }
        let new_pos = []
        for (var i = 0; i < cursors.length; i++) {
          if (cursors[i][1] == max_y) {
            new_pos.push([cursors[i][0],cursors[i][1]+1]);
          }
        }
        for (var i = 0; i < new_pos.length; i++) {
          cursors.push([new_pos[i][0],new_pos[i][1],cursors[0][2],false]);
        }
      }

    // MOVE
    } else {
      for (var i = 0; i < cursors.length; i++) {
        if (e.key == "ArrowLeft") {
          cursors[i][0] = speed_mode ? cursors[i][0] - 5 : cursors[i][0] - 1;
        }
        if (e.key == "ArrowRight") {
          cursors[i][0] = speed_mode ? cursors[i][0] + 5 : cursors[i][0] + 1;
        }
        if (e.key == "ArrowUp") {
          cursors[i][1] = speed_mode ? cursors[i][1] - 5 : cursors[i][1] - 1;
        }
        if (e.key == "ArrowDown") {
          cursors[i][1] = speed_mode ? cursors[i][1] + 5 : cursors[i][1] + 1;
        }
      }
    }
    print_cursors(sprites);
    show_sheet(sprites,0,0);
  });
  
  context.fillStyle = "#000";
  context.fillRect(0,0,canvas.width,canvas.height);
  print_cursors(sprites);
  input.sprites = sprites;

  /*
  // RENDER
  window.setInterval(function(){
    for (var i in glyphs) {
      let pos = i.split(',');
      pos[0] = Number(pos[0]);
      pos[1] = Number(pos[1]);
      draw_glyph(sprites,glyphs[i][0],glyphs[i][1],pos[0],pos[1],glyphs[i][2]);
    }
  },17);
  */

});

// PRINT CURSORS

function unprint_cursors(sprites) {
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let bg_color = glyphs[(cols * cursors[i][1] + cursors[i][0])*3];
    let fg_color = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1];
    let glyph_index  = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2];
    if (!bg_color && !fg_color && !glyph_index) {
      draw_glyph(sprites,11,11,cursors[i][0],cursors[i][1],[15,15]);
    } else {
      draw_glyph(sprites,bg_color,fg_color,cursors[i][0],cursors[i][1],get_glyph_position(glyph_index));
    }
  }
}

function print_cursors(sprites) {
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let bg_color = glyphs[(cols * cursors[i][1] + cursors[i][0])*3];
    let fg_color = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1];
    let glyph_index  = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2];
    if (!bg_color && !fg_color && !glyph_index) {
      draw_glyph(sprites,0,0,cursors[i][0],cursors[i][1],[15,15]);
    } else {
      draw_glyph(sprites,fg_color,bg_color,cursors[i][0],cursors[i][1],get_glyph_position(glyph_index));
    }
  }
}

function copy() {
}

// DRAW GLYPH

function draw_glyph(sprites,bg,fg,x,y,glyph_pos){
  context.fillStyle = colors[bg];
  context.fillRect(x*size[0],y*size[1],size[0],size[1]);
  context.drawImage(sprites[fg], size[0]*glyph_pos[0], size[1]*glyph_pos[1], size[0], size[1], x*size[0], y*size[1], size[0], size[1]);
}

// SAVE
function arrayBufferToBase64(Arraybuffer, Filetype, fileName) {
  let binary = '';
  const bytes = new Uint8Array(Arraybuffer);
  const len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  const file = window.btoa(binary);
  const mimType = Filetype === 'pdf' ? 'application/pdf' : Filetype === 'xlsx' ? 'application/xlsx' :
    Filetype === 'pptx' ? 'application/pptx' : Filetype === 'csv' ? 'application/csv' : Filetype === 'docx' ? 'application/docx' :
      Filetype === 'jpg' ? 'application/jpg' : Filetype === 'png' ? 'application/png' : '';
  const url = `data:${mimType};base64,` + file;

  // download the file
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
}
// LOAD
let input = document.createElement("input");
input.type = "file";
//input.accept = ".png";
input.addEventListener("change",function(e){
  let file = input.files[0];
  let fr = new FileReader();
  fr.addEventListener("load", function(){
    glyphs = new Uint8Array(_base64ToArrayBuffer(String(fr.result).slice("data:application/octet-stream;base64,".length)));
    console.log(this);
    update(this);
    /*
    img = new Image();
    img.src = fr.result;
    img.addEventListener("load",function(){
      clearCanvas();
      ctx.drawImage(this, 0, 0);
      let new_ImageData = ctx.getImageData(0,0,width,height);
      for (var i = 0; i < new_ImageData.data.length; i++) {
        Scene.data[i] = new_ImageData.data[i];
      }
      new_memory(Scene);
    },false);
      */
  }.bind(this.sprites), false);
  fr.readAsDataURL(file); 
});

// B64 to STR
function _base64ToArrayBuffer(base64) {
    console.log(base64);
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

// UPDATE

function update(sprites) {
  context.fillStyle = "#000";
  context.fillRect(0,0,canvas.width,canvas.height);
  for (var y = 0; y < canvas.height/size[1]; y++) {
    for (var x = 0; x < canvas.width/size[0]; x++) {
      let bg_color = glyphs[(cols * y + x)*3];
      let fg_color = glyphs[(cols * y + x)*3 + 1];
      let glyph_index  = glyphs[(cols * y + x)*3 + 2];
      if (!bg_color && !fg_color && !glyph_index) continue;
      draw_glyph(sprites,bg_color,fg_color,x,y,get_glyph_position(glyph_index));
    }
  }
}

// PASTE

function paste(sprites) {
  let memory = [];
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0 || !cursors[i][3]) continue;
    let bg_color = cursors[i][3][0];
    let fg_color = cursors[i][3][1];
    let glyph_index  = cursors[i][3][2];
    memory.push([
      [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
    ]);
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3] = bg_color
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = fg_color
    glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2] = glyph_index;
  }
  past_memory.push(memory);
}

// COPY

function copy(sprites) {
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let bg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3];
    let fg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1];
    let glyph_index  = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2];
    if (!bg_color && !fg_color && !glyph_index) {
      cursors[i][3] = false;
    } else {
      cursors[i][3] = [
        bg_color,
        fg_color,
        glyph_index
      ];
    }
  }
}

// INVERT

function invert(sprites) {
  let memory = [];
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let glyph_bg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3];
    let glyph_fg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1];
    let glyph_index  = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2];
    memory.push([
      [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
    ]);
    if (!(!glyph_bg_color && !glyph_fg_color && !glyph_index)) {
      glyphs[(cols * cursors[i][1] + cursors[i][0])*3] = glyph_fg_color;
      glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = glyph_bg_color;
    }
  }
  past_memory.push(memory);
}

// COLORIZE

function colorize(sprites) {
  let memory = [];
  for (var i = 0; i < cursors.length; i++) {
    if (cursors[i][0] >= cols || cursors[i][1] >= rows
     || cursors[i][0] < 0     || !cursors[i][1] < 0) continue;
    let glyph_bg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3];
    let glyph_fg_color     = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1];
    let glyph_index  = glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2];
    memory.push([
      [(cols * cursors[i][1] + cursors[i][0])*3    ,glyphs[(cols * cursors[i][1] + cursors[i][0])*3    ]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 1,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1]],
      [(cols * cursors[i][1] + cursors[i][0])*3 + 2,glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 2]]
    ]);
    if (!(!glyph_bg_color && !glyph_fg_color && !glyph_index)) {
      glyphs[(cols * cursors[i][1] + cursors[i][0])*3] = enter_mode ? fg_color : bg_color;
      glyphs[(cols * cursors[i][1] + cursors[i][0])*3 + 1] = enter_mode ? bg_color : fg_color;
    }
  }
  past_memory.push(memory);
}

/*

TODO

MULTI-CURSOR / SELECTION AREA
CANCEL / REDO
COPY / PASTE
WRITE MODE
COLORIZE
CAMERA (+ ZOOM?)
SAVE / LOAD

???

REFRESH EVERYFRAME ONLY UNDER CURSOR
*/

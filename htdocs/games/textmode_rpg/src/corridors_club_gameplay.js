let player = {
  "name": "player",
  "attack": 1,
  "defense": 1,
  "speed": 1,
  "vitality": 1,
  "invention": 1,
  "skills": []
};

function get_total_stats(entity) {
  let total = 0;
  total += entity.attack;
  total += entity.defense;
  total += entity.speed;
  total += entity.vitality;
  total += entity.invention;
  return total;
}

// DUNGEON

const dungeon_map = [];
const dungeon_known_map = [];
const dungeon_enemy_map = [];

let dungeon_position = [0,0];

let dungeon_color = Math.floor(Math.random()*dungeon_colors.length);

let dungeon_variety = Math.floor(Math.random()*12);

let dungeon_length = Math.floor(Math.random()*28)+2;

let dungeon_entrance = 2;

function build_dungeon(entrance,w,h,rooms) {
  // DUNGEON MAP
  let dungeon_path = get_path_by_length(entrance,w,h,0.5,rooms);
  let new_dungeon_map = path_to_map(dungeon_path);
  new_dungeon_map = define_map(entrance,new_dungeon_map);
  while(dungeon_map.length)dungeon_map.pop();
  for (var i = 0; i < new_dungeon_map.length; i++) dungeon_map.push(new_dungeon_map[i]);
  dungeon_position[1] = entrance;
  // KNOWN MAP
  while(dungeon_known_map.length) dungeon_known_map.pop();
  for (var y = 0; y < h; y++) {
    dungeon_known_map.push([]);
    for (var x = 0; x < w; x++) {
      dungeon_known_map[dungeon_known_map.length-1].push(0);
    }
  }
  // ENEMY MAP
  while(dungeon_enemy_map.length) dungeon_enemy_map.pop();
  for (var y = 0; y < h; y++) {
    dungeon_enemy_map.push([]);
    for (var x = 0; x < w; x++) {
      dungeon_enemy_map[dungeon_enemy_map.length-1].push(
        !(x == 0 && y == dungeon_entrance) && dungeon_map[y] && dungeon_map[y][x] ? Math.random() > 0.5 ? 1 : 0 : 0
      );
    }
  }
}


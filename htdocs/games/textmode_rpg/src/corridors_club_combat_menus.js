/**************
 * FIGHT MENU *
 **************/

function fight_menu(transition_speed) {
  init_menu(transition_speed);
  // ANIMATED FRAME 
  box(0,0,cols,rows,12,11,146,92,146,106,107,136,91,136);
  events.push(function(frame){
    // PATTERN
    pattern(1,0,62,1,[
      [[11,11,92],[11,11,92],[11,11,92],[11,11,92],[11,12,92],[11,12,92]],
    ],
    /*static ? 0 : */cycle(Math.floor(frame/5),6),
    0,
    [true],false);
    pattern(1,31,62,1,[
      [[11,11,91],[11,11,91],[11,11,91],[11,11,91],[11,12,91],[11,12,91]],
    ],
    /*static ? 0 : */cycle(Math.floor(frame/5),6),
    0,
    [false],false);
    pattern(0,1,1,30,[
      [[11,11,106]],
      [[11,11,106]],
      [[11,12,106]],
    ],
    0,
    /*static ? 0 : */cycle(Math.floor(frame/10),3),
    [false],false);
    pattern(63,1,1,30,[
      [[11,11,116]],
      [[11,11,116]],
      [[11,12,116]],
    ],
    0,
    /*static ? 0 : */cycle(Math.floor(frame/10),3),
    [false],true);
  });
  // PLAYER
  print_player_portrait(
    3,
    16,
    player_portrait.hair,
    player_portrait.eyes,
    player_portrait.nose,
    player_portrait.mouth,
    player_portrait.ears,
    player_portrait.earings,
    color_duos[player_portrait.skin],
    color_duos[player_portrait.hair_color],
    color_duos[player_portrait.shirt_color]
  );
  write(player.name,3,27);
  bar("HP",player.vitality*6,player.vitality*6,2,12,3,28);
  bar("MP",player.invention*6,player.invention*6,7,17,3,29);

  // MONSTER
  paste_zone(assets["slime"],3,2,[11,0,255]);
  write("Slime",3,13);
  bar("HP",player.vitality*6,player.vitality*6,2,12,3,14);

  // LOG
  write("A horrible Slime appears !",32,2);
  
  // UI
  events.push(function(frame){
    button(47,27,14,"CONTINUE",function(){
      transition_menu(dungeon_menu,function(){
          // print player during transition
          print_player_portrait(
            3,
            16,
            player_portrait.hair,
            player_portrait.eyes,
            player_portrait.nose,
            player_portrait.mouth,
            player_portrait.ears,
            player_portrait.earings,
            color_duos[player_portrait.skin],
            color_duos[player_portrait.hair_color],
            color_duos[player_portrait.shirt_color]
          );
          write(player.name,3,27);
          bar("HP",player.vitality*6,player.vitality*6,2,12,3,28);
          bar("MP",player.invention*6,player.invention*6,7,17,3,29);
      },15);
    },mouse);
  });
  /* MOUSE */
 
  ui.push(function(frame,unset){
    /* cursor */
      print_cursor(mouse.x,mouse.y,unset);
  });

  document.onmousemove = function(e){
    //if (transition != Infinity || crawl_transition_w || crawl_transition_h) return;
    mouse.x = Math.floor(e.clientX / size[0] / scale - left_offset / size[0] / scale  );
    mouse.y = Math.floor(e.clientY / size[1] / scale - top_offset / size[1] / scale );
  };
  document.onmousedown = function(e){
    if (transition != Infinity) return;
    mouse.down = true;
  };
  document.onmouseup = function(e){
    if (transition != Infinity) return;
    mouse.down = false;
  };
};

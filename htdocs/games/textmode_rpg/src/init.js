/* COLORS 

0 white
1 darkgrey
2 red
3 darkyellow
4 yellow
5 green
6 cyan
7 deepblue
8 purple
9 pink
10 grey
11 black
12 darkred
13 brown
14 orange
15 darkgreen
16 blue
17 darkblue
18 darkpurple
19 candy

*/

// MOUSE

let mouse_x = 0;
let mouse_y = 0;
let mouse_down = false;
const mouse = {
  'x':0,
  'y':0,
  'down':false
};

// INIT

let transition = Infinity;
let render_speed = 17;


function init() {
  build_dungeon(dungeon_entrance,6,6,dungeon_length);
  character_creation_menu(30);
  //fight_menu(30);
};

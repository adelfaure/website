var menu_choices = {};

function init_menu(transition_speed) {
  crawl_transition_w = 32;  
  crawl_transition_h = 15;
  total_transition = false;  
  mouse.down = false;
  fill_glyphs("typed",11,0,-1);
  menu_choices = {};
  while(ui.length) ui.pop();
  while(events.length) events.pop();
  document.onmousedown = false;
  document.onmouseup = false;
  document.onmousemove = false;
  document.onkeydown = false;
  transition = transition_speed;
  ui.push(function(frame,unset){
    if (unset && !draw_n) {
      transition = Infinity;
    }
  });
}

function transition_menu(menu,transition_function,transition_speed) {
  init_menu(transition_speed);
  transition_function();
  events.push(function(frame){
    if (transition == Infinity) {
      menu(transition_speed);
    }
  });
}

/************
 * MAP MENU *
 ************/

function map_menu(transition_speed){
  init_menu(transition_speed);
  box(0,0,cols,rows,17,11,146,92,146,106,107,136,91,136);
  map_case( 3+1, 2);
  map_case(12+1, 2);
  map_case(21+1, 2);
  map_case(30+1, 2);
  map_case(39+1, 2);
  map_case(48+1, 2);
  map_case( 3+1, 6);
  map_case(12+1, 6);
  map_case(21+1, 6);
  map_case(30+1, 6);
  map_case(39+1, 6);
  map_case(48+1, 6);
  map_case( 3+1,10);
  map_case(12+1,10);
  map_case(21+1,10);
  map_case(30+1,10);
  map_case(39+1,10);
  map_case(48+1,10);
  map_case( 3+1,14);
  map_case(12+1,14);
  map_case(21+1,14);
  map_case(30+1,14);
  map_case(39+1,14);
  map_case(48+1,14);
  map_case( 3+1,18);
  map_case(12+1,18);
  map_case(21+1,18);
  map_case(30+1,18);
  map_case(39+1,18);
  map_case(48+1,18);
  map_case( 3+1,22);
  map_case(12+1,22);
  map_case(21+1,22);
  map_case(30+1,22);
  map_case(39+1,22);
  map_case(48+1,22);
  for (var y = 0; y < dungeon_map.length; y++) {
    for (var x = 0; x < dungeon_map[y].length; x++) {
      if (dungeon_map[y][x] && dungeon_known_map[y][x] == 1) {
        var asset_left, asset_center, asset_right;
      
        asset_left = copy_asset(
          dungeon_map[y][x][1][0] == 1 ? "mini_left_door" :
          dungeon_map[y][x][1][0] == 2 ? "mini_left_door" :
          "mini_left_wall"
        );
        
        asset_center = copy_asset(
          dungeon_map[y][x][1][1] == 1 ? "mini_center_door" :
          dungeon_map[y][x][1][1] == 2 ? "mini_center_door" :
          dungeon_map[y][x][1][1] == 3 ? "mini_center_door" :
          "mini_center_wall"
        );
        
        asset_right = copy_asset(
          dungeon_map[y][x][1][2] == 1 ? "mini_right_door":
          "mini_right_wall"
        );
        
        var asset = concat_zones(asset_left,asset_center);      
        asset = concat_zones(asset,asset_right);
        
        asset = colorize_dungeon_map_asset(asset,x,y,dungeon_map[y][x][0]);

        paste_zone(asset,6+9*x,3+4*y);
        if (dungeon_map[y][x][1][0] == 1 || dungeon_map[y][x][1][0] == 2){
          paste_zone(assets["h_corridor"],4+9*x,3+4*y);
        }
        if (dungeon_map[y][x][1][2]){
          paste_zone(assets["h_corridor"],5+8+9*x,3+4*y);
        }
        if (dungeon_map[y][x][1][1] == 1 || dungeon_map[y][x][1][1] == 3 ){
          paste_zone(assets["v_corridor"],6+9*x,2+4*y);
        }
        if (dungeon_map[y][x][1][1] == 2 || dungeon_map[y][x][1][1] == 3 ){
          paste_zone(assets["v_corridor"],6+9*x,3+3+4*y);
        }
      } else if (dungeon_known_map[y][x] == 2){
        var asset = copy_asset("unexplored_room");
        //asset = colorize_dungeon_map_asset(asset,x,y,dungeon_known_map[y][x][0]);
        paste_zone(asset,6+9*x,3+4*y);
      }
    }
  }

  events.push(function(frame){
    button(47,27,14,"CONTINUE",function(){
      dungeon_menu(50);
    },mouse);
  });
  
  /* MOUSE */
 
  ui.push(function(frame,unset){
    /* cursor */
      print_cursor(mouse.x,mouse.y,unset);
    //ui = [];
  });

  document.onmousemove = function(e){
    //if (transition != Infinity || crawl_transition_w || crawl_transition_h) return;
    mouse.x = Math.floor(e.clientX / size[0] / scale - left_offset / size[0] / scale  );
    mouse.y = Math.floor(e.clientY / size[1] / scale - top_offset / size[1] / scale );
  };
  document.onmousedown = function(e){
    if (transition != Infinity) return;
    mouse.down = true;
  };
  document.onmouseup = function(e){
    if (transition != Infinity) return;
    mouse.down = false;
  };

}

/****************
 * DUNGEON MENU *
 ****************/

function dungeon_menu(transition_speed,previous_pos){

  // KNOWN MAP UPDATE
  dungeon_known_map[dungeon_position[1]][dungeon_position[0]] = 1;
  if (dungeon_position[0] && dungeon_map[dungeon_position[1]][dungeon_position[0]][1][0] && !dungeon_known_map[dungeon_position[1]][dungeon_position[0]-1]) {
    dungeon_known_map[dungeon_position[1]][dungeon_position[0]-1] = 2;
  }
  if (dungeon_position[1] && (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 1 || dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 3)  && !dungeon_known_map[dungeon_position[1]-1][dungeon_position[0]]) {
    dungeon_known_map[dungeon_position[1]-1][dungeon_position[0]] = 2;
  }
  if (dungeon_position[0] < 5 && dungeon_map[dungeon_position[1]][dungeon_position[0]][1][2] && !dungeon_known_map[dungeon_position[1]][dungeon_position[0]+1]) {
    dungeon_known_map[dungeon_position[1]][dungeon_position[0]+1] = 2;
  }
  if (dungeon_position[1] < 5 && (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 2 || dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 3)  && !dungeon_known_map[dungeon_position[1]+1][dungeon_position[0]]) {
    dungeon_known_map[dungeon_position[1]+1][dungeon_position[0]] = 2;
  }

  // INIT DUNGEON MENU
  init_menu(transition_speed);
  
  box(0,0,cols,rows,17,11,146,92,146,106,107,136,91,136);


  /* room */
  var room_asset = get_room_asset(dungeon_position[0],dungeon_position[1]);
  display_room_asset = room_asset;
  
  if (previous_pos) {
    let previous_room_asset = get_room_asset(previous_pos[0],previous_pos[1]);
    if (dungeon_position[1] == previous_pos[1]) {
      let room_trans_asset = crop_zone(copy_asset("room_transition"),20,0,0,0);
      if (dungeon_position[0] < previous_pos[0]) {
        display_room_asset = concat_zones(room_asset,room_trans_asset);
        display_room_asset = concat_zones(display_room_asset,previous_room_asset);
      } else {
        display_room_asset = concat_zones(previous_room_asset,room_trans_asset);
        display_room_asset = concat_zones(display_room_asset,room_asset);
      }
    } else {
      let room_trans_asset = crop_zone(copy_asset("room_transition"),0,0,9,0);
      if (dungeon_position[1] < previous_pos[1]) {
        display_room_asset = v_concat_zones(room_asset,room_trans_asset);
        display_room_asset = v_concat_zones(display_room_asset,previous_room_asset);
      } else {
        display_room_asset = v_concat_zones(previous_room_asset,room_trans_asset);
        display_room_asset = v_concat_zones(display_room_asset,room_asset);
      }
    }
  } else {
    crawl_transition_w = 0;
    crawl_transition_h = 0;
  }
  //write("Dungeon name",3,14);
  if (previous_pos) {
    let wall = colorize_dungeon_map_asset(copy_asset("wall"),previous_pos[0],previous_pos[1],dungeon_map[previous_pos[1]][previous_pos[0]][0],true);
    paste_zone(wall,3,16);
    if (dungeon_position[1] == previous_pos[1]) {
      if (previous_pos[0] < dungeon_position[0]) {
        let cropped_display_room_asset = crop_zone(display_room_asset,32-crawl_transition_w,32 - (32-crawl_transition_w),0,0);
        paste_zone(cropped_display_room_asset,3,2,[],"typed");
      } else {
        let cropped_display_room_asset = crop_zone(display_room_asset,32 - (32-crawl_transition_w),32-crawl_transition_w,0,0);
        paste_zone(cropped_display_room_asset,3,2,[],"typed");
      }
    } else {
      if (previous_pos[1] < dungeon_position[1]) {
        let cropped_display_room_asset = crop_zone(display_room_asset,0,0,15-crawl_transition_h,15 - (15-crawl_transition_h));
        paste_zone(cropped_display_room_asset,3,2,[],"typed");
      } else {
        let cropped_display_room_asset = crop_zone(display_room_asset,0,0,15 - (15-crawl_transition_h),15-crawl_transition_h);
        paste_zone(cropped_display_room_asset,3,2,[],"typed");
      }
    }
  } else {
    let wall = colorize_dungeon_map_asset(copy_asset("wall"),dungeon_position[0],dungeon_position[1],dungeon_map[dungeon_position[1]][dungeon_position[0]][0],true);
    paste_zone(wall,3,16);
    paste_zone(display_room_asset,3,2,[],"typed");
  }
  
  print_player_portrait(
    3,
    16,
    player_portrait.hair,
    player_portrait.eyes,
    player_portrait.nose,
    player_portrait.mouth,
    player_portrait.ears,
    player_portrait.earings,
    color_duos[player_portrait.skin],
    color_duos[player_portrait.hair_color],
    color_duos[player_portrait.shirt_color]
  );
  write(player.name,3,27);
  bar("HP",player.vitality*6,player.vitality*6,2,12,3,28);
  bar("MP",player.invention*6,player.invention*6,7,17,3,29);

  /* BUTTONS */

  button(47,27,14,"CONTINUE",function(){
  },mouse);
  button(31,27,14,"MAP",function(){
  },mouse);
  
  events.push(function(frame){
    if (transition != Infinity) return;
    if (crawl_transition_w == 0 && !total_transition) {
      if (dungeon_map[dungeon_position[1]]){
        let wall = colorize_dungeon_map_asset(copy_asset("wall"),dungeon_position[0],dungeon_position[1],dungeon_map[dungeon_position[1]][dungeon_position[0]][0],true);
        paste_zone(wall,3,16);
      }
      print_player_portrait(
        3,
        16,
        player_portrait.hair,
        player_portrait.eyes,
        player_portrait.nose,
        player_portrait.mouth,
        player_portrait.ears,
        player_portrait.earings,
        color_duos[player_portrait.skin],
        color_duos[player_portrait.hair_color],
        color_duos[player_portrait.shirt_color]
      );
      write(player.name,3,27);
      bar("HP",player.vitality*6,player.vitality*6,2,12,3,28);
      bar("MP",player.invention*6,player.invention*6,7,17,3,29);
      write("Which way to go ?",32,2);
      transition = 25;
      total_transition = true;
// CHECK MONSTER
/*
      if (dungeon_enemy_map[dungeon_position[1]][dungeon_position[0]]) {
        dungeon_enemy_map[dungeon_position[1]][dungeon_position[0]] = 0;
        return transition_menu(fight_menu,function(){
          // print player during transition
          print_player_portrait(
            3,
            16,
            player_portrait.hair,
            player_portrait.eyes,
            player_portrait.nose,
            player_portrait.mouth,
            player_portrait.ears,
            player_portrait.earings,
            color_duos[player_portrait.skin],
            color_duos[player_portrait.hair_color],
            color_duos[player_portrait.shirt_color]
          );
          write(player.name,3,27);
          bar("HP",player.vitality*6,player.vitality*6,2,12,3,28);
          bar("MP",player.invention*6,player.invention*6,7,17,3,29);
        },15);
      }
*/
    }
    if (previous_pos && patterns["every"](frame,[2])) {
      crawl_transition_w = crawl_transition_w > 0 ? crawl_transition_w - 2 : 0;
    }
    if (previous_pos && patterns["every"](frame,[2])) {
      crawl_transition_h = crawl_transition_h > 0 ? crawl_transition_h - 1 : 0;
    }
    if (previous_pos) {
      if (dungeon_position[1] == previous_pos[1]) {
        if (previous_pos[0] < dungeon_position[0]) {
          let cropped_display_room_asset = crop_zone(display_room_asset,32-crawl_transition_w,32 - (32-crawl_transition_w),0,0);
          paste_zone(cropped_display_room_asset,3,2,[],"typed");
        } else {
          let cropped_display_room_asset = crop_zone(display_room_asset,32 - (32-crawl_transition_w),32-crawl_transition_w,0,0);
          paste_zone(cropped_display_room_asset,3,2,[],"typed");
        }
      } else {
        if (previous_pos[1] < dungeon_position[1]) {
          let cropped_display_room_asset = crop_zone(display_room_asset,0,0,15-crawl_transition_h,15 - (15-crawl_transition_h));
          paste_zone(cropped_display_room_asset,3,2,[],"typed");
        } else {
          let cropped_display_room_asset = crop_zone(display_room_asset,0,0,15 - (15-crawl_transition_h),15-crawl_transition_h);
          paste_zone(cropped_display_room_asset,3,2,[],"typed");
        }
      }
    } else {
      paste_zone(display_room_asset,3,2,[],"typed");
    }

    /* BUTTONS */

    button(47,27,14,"CONTINUE",function(){
      //transition_menu(dungeon_menu);
      dungeon_color = Math.floor(Math.random()*dungeon_colors.length);
      dungeon_variety = Math.floor(Math.random()*12);
      dungeon_length = Math.floor(Math.random()*28)+2;
      dungeon_position[0] = 0;
      dungeon_color = Math.floor(Math.random()*dungeon_colors.length);
      
      dungeon_variety = Math.floor(Math.random()*12);
      
      dungeon_length = Math.floor(Math.random()*28)+2;
      
      build_dungeon(dungeon_entrance,6,6,dungeon_length);
      dungeon_menu(50);
    },mouse);
    button(31,27,14,"MAP",function(){
      map_menu(50);
    },mouse);
    if (total_transition) {
      let arrow_x = 32;
      if (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][0] == 1) {
        arrow_button("left",arrow_x,4,function(){
          dungeon_position[0]--;
          dungeon_menu(25,[dungeon_position[0]+1,dungeon_position[1]]);
        },mouse);
      } else {
        paste_zone(assets["no_arrow"],arrow_x,4);
      }
        arrow_x+= 6;
      if (!total_transition) return;
      if (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 1   
       || dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 3) {
        arrow_button("up",arrow_x,4,function(){
          dungeon_position[1]--;
          dungeon_menu(25,[dungeon_position[0],dungeon_position[1]+1])
        },mouse);
      } else {
        paste_zone(assets["no_arrow"],arrow_x,4);
      }
        arrow_x+= 6;
      if (!total_transition) return;
      if (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 2   
       || dungeon_map[dungeon_position[1]][dungeon_position[0]][1][1] == 3) {
        arrow_button("bottom",arrow_x,4,function(){
          dungeon_position[1]++;
          dungeon_menu(25,[dungeon_position[0],dungeon_position[1]-1])
        },mouse);
      } else {
        paste_zone(assets["no_arrow"],arrow_x,4);
      }
        arrow_x+= 6;
      if (!total_transition) return;
      if (dungeon_map[dungeon_position[1]][dungeon_position[0]][1][2] == 1) {
        arrow_button("right",arrow_x,4,function(){
          dungeon_position[0]++;
          dungeon_menu(25,[dungeon_position[0]-1,dungeon_position[1]])
        },mouse);
      } else {
        paste_zone(assets["no_arrow"],arrow_x,4);
      }
        arrow_x+= 6;
    }
  });
 
  /* MOUSE */
 
  ui.push(function(frame,unset){
    /* cursor */
      print_cursor(mouse.x,mouse.y,unset);
  });

  document.onmousemove = function(e){
    //if (transition != Infinity || crawl_transition_w || crawl_transition_h) return;
    mouse.x = Math.floor(e.clientX / size[0] / scale - left_offset / size[0] / scale  );
    mouse.y = Math.floor(e.clientY / size[1] / scale - top_offset / size[1] / scale );
  };
  document.onmousedown = function(e){
    if (transition != Infinity || crawl_transition_w || crawl_transition_h) return;
    mouse.down = true;
  };
  document.onmouseup = function(e){
    if (transition != Infinity || crawl_transition_w || crawl_transition_h) return;
    mouse.down = false;
  };
}

let biomes = ["roof","temple","tower","cellar","jail","cave"];

function get_room_asset(x,y) {
  var asset_left, asset_center, asset_right;
  
  asset_left = copy_asset(
    dungeon_map[y][x][1][0] == 1 ? biomes[y]+"_left_door" :
    dungeon_map[y][x][1][0] == 2 ? "tower_entrance" :
    biomes[y]+"_left_wall"
  );
  
  asset_center = copy_asset(
    dungeon_map[y][x][1][1] == 1 ? biomes[y]+"_center_up_stair" :
    dungeon_map[y][x][1][1] == 2 ? biomes[y]+"_center_down_stair" :
    dungeon_map[y][x][1][1] == 3 ? biomes[y]+"_center_up_down_stair" :
    biomes[y]+"_center_wall"
  );
  
  asset_right = copy_asset(
    dungeon_map[y][x][1][2] == 1 ? biomes[y]+"_right_door" :
    biomes[y]+"_right_wall"
  );
 
  var asset = concat_zones(asset_left,asset_center);      
  asset = concat_zones(asset,asset_right);
  
  return colorize_dungeon_map_asset(asset,x,y,dungeon_map[y][x][0],true);
}

/***************************
 * CHARACTER CREATION MENU *
 ***************************/

function character_creation_menu(transition_speed){
  init_menu(transition_speed);

  /* SETUP */
  randomize_portrait();
  randomize_colors();
  box(0,0,cols,rows,17,11,146,92,146,106,107,136,91,136);
  writeBig("character creation",5,2);
  write("Face................change",3,18,17,11);
  write("Hair",3,19);
  write("Eyes",3,20);
  write("Nose",3,21);
  write("Ears",3,22);
  write("Mouth",3,23);
  write("Earings",3,24);
  write("Colors..............change",3,26,17,11);
  write("Skin",3,27);
  write("Hair",3,28);
  write("Shirt",3,29);
  write("Stats..................change",32,8,17,11);
  write("Skills.................choose",32,16,17,11);
  player_name_prompt = new Prompt(28);

  /* UI */
  ui.push(function(frame,unset){
    /* portrait */
    print(12,19,0,11,hair_names[player_portrait.hair],unset);
    print(12,20,0,11,eyes_names[player_portrait.eyes],unset);
    print(12,21,0,11,nose_names[player_portrait.nose],unset);
    print(12,22,0,11,ears_names[player_portrait.ears],unset);
    print(12,23,0,11,mouth_names[player_portrait.mouth],unset);
    print(12,24,0,11,earings_names[player_portrait.earings],unset);
    print(12,27,color_duos[player_portrait.skin][0],11,color_duos_names[player_portrait.skin],unset);
    print(12,28,color_duos[player_portrait.hair_color][0],11,color_duos_names[player_portrait.hair_color],unset);
    print(12,29,color_duos[player_portrait.shirt_color][0],11,color_duos_names[player_portrait.shirt_color],unset);

    /* name */
    text_input(32,6,28,"Player name",mouse,player_name_prompt,unset);

    /* stats */
    print(   48, 9,14,11,player.attack,unset);
    print(   48,10, 5,11,player.defense,unset);
    print(   48,11, 4,11,player.speed  ,unset);
    print(   48,12, 2,11,player.vitality,unset);
    print(   48,13, 7,11,player.invention,unset);
    print(   32,14, 1,11,"Points left "+(8-get_total_stats(player)),unset);

    /* cursor*/
    if (!player_name_prompt.active){
      print_cursor(mouse.x,mouse.y,unset);
    }
  });

  /* EVENTS */
  events.push(function(frame){
    /* portrait */
    minus_plus_button(23,19,player_portrait,"hair",0,5,mouse);
    minus_plus_button(23,20,player_portrait,"eyes",0,3,mouse);
    minus_plus_button(23,21,player_portrait,"nose",0,4,mouse);
    minus_plus_button(23,22,player_portrait,"ears",0,2,mouse);
    minus_plus_button(23,23,player_portrait,"mouth",0,3,mouse);
    minus_plus_button(23,24,player_portrait,"earings",0,3,mouse);
    minus_plus_button(23,27,player_portrait,"skin",0,color_duos.length-1,mouse);
    minus_plus_button(23,28,player_portrait,"hair_color",0,color_duos.length-1,mouse);
    minus_plus_button(23,29,player_portrait,"shirt_color",0,color_duos.length-1,mouse); 
    rect(3,5,26,12,0,11);
    box(3,5,26,13,17,11,255,93,255,107,106,255,90,255);
    print_player_portrait(
      3,
      5,
      player_portrait.hair,
      player_portrait.eyes,
      player_portrait.nose,
      player_portrait.mouth,
      player_portrait.ears,
      player_portrait.earings,
      color_duos[player_portrait.skin],
      color_duos[player_portrait.hair_color],
      color_duos[player_portrait.shirt_color]
    );
    /* stats */
    stat_bar(32,9 ,14,13,"ATT",player.attack);
    stat_bar(32,10,5 ,15,"DEF",player.defense);
    stat_bar(32,11,4 ,3,"SPE",player.speed);
    stat_bar(32,12,2 ,12,"VIT",player.vitality);
    stat_bar(32,13,7 ,17,"INT",player.invention);
    minus_plus_button(55,9 ,player,"attack",1,10,mouse,true,get_total_stats(player) == 8); 
    minus_plus_button(55,10,player,"defense",1,10,mouse,true,get_total_stats(player) == 8); 
    minus_plus_button(55,11,player,"speed",1,10,mouse,true,get_total_stats(player) == 8); 
    minus_plus_button(55,12,player,"vitality",1,10,mouse,true,get_total_stats(player) == 8); 
    minus_plus_button(55,13,player,"invention",1,10,mouse,true,get_total_stats(player) == 8); 

    /* skills */
    skill_ui(32,17,"haste",mouse);   
    skill_ui(32,20,"resistance",mouse);   
    skill_ui(32,23,"force",mouse);   
 
    /* buttons */
    button(47,27,14,"CONTINUE",function(){
      //transition_menu(dungeon_menu);
      player.name = player_name_prompt.print(false) ? player_name_prompt.print(false) : "Noname";
      transition_menu(dungeon_menu,function(){},30);
    },mouse);
  });

  /* INPUT */
  document.onmousedown = function(e){
    if (transition != Infinity) return;
    mouse.down = true;
  };
  document.onmouseup = function(e){
    if (transition != Infinity) return;
    mouse.down = false;
  };
  document.onmousemove = function(e){
    if (!player_name_prompt.active) {
      mouse.x = Math.floor(e.clientX / size[0] / scale - left_offset / size[0] / scale  );
      mouse.y = Math.floor(e.clientY / size[1] / scale - top_offset / size[1] / scale );
    }
  };
  document.onkeydown = function(e){
    if (transition != Infinity) return;
    if (e.key == "Enter") {
      test = !test;
      transition = 50;
      player_name_prompt.active = false;
    }
    if (player_name_prompt.active) {
      player_name_prompt.send(e.key);
    }
  };
}
let test = 0;

// EDITOR
/* Info */

function info(frame,unset) {
  let info_col = Math.floor(display_cols/6);
  print(0,0,1,11,"Mode ",unset);
  for (var x = 1; x < 7; x++) {
    print(x+4,0,mode+1 == x ? 11 : 1,mode+1 == x ? 1 : 11,x,unset);
  }
  print(11,0,1,11,' : ',unset);
  print(14,0,fg_color,bg_color,mode_names[mode],unset);
  print(14,0,fg_color,bg_color,mode_names[mode],unset);
  
  print(0,1,1,11,"step = "+step+"; cursor = "+cursors[0][0]+','+cursors[0][1]+"; cam = "+cam[0]+','+cam[1]+"; sel = "+cursors_sel_w+','+cursors_sel_h+"; scale = "+scale+';',unset);
  if (mode == 0) {
    for (var y = 0; y < 4; y++) {
      for (var x = 0; x < 10; x++) {
        print(x,y*2+3,last_key_position == (y*10+x) ? 11 : 1,last_key_position == (y*10+x) ? 1 : 11,keys[(y*10+x)],unset);
        set_glyph("ui",cam[0]+x,y*2+4+cam[1],unset ? 0 : last_key_position == (y*10+x) ? fg_color : bg_color,unset ? 0 : last_key_position == (y*10+x) ? bg_color : fg_color,unset ? 0 : (y*10+x)+40*sheet); 
      }
    }
  }
  if (mode == 3) {
    for (var y = 0; y < 4; y++) {
      for (var x = 0; x < 10; x++) {
        let fg = unset ? 0 : (y*10+x) < 20 ? (y*10+x) : (y*10+x)-20 == 11 ? 1 : 11;
        let bg = unset ? 0 : (y*10+x) < 20 ? (y*10+x) == 11 ? 1 : 11 : (y*10+x)-20;
        print(x,y*2+2,last_key_position == (y*10+x) ? 11 : 1,last_key_position == (y*10+x) ? 1 : 11,keys[(y*10+x)],unset);
        print(x,y*2+3,last_key_position == (y*10+x) ? bg : fg,last_key_position == (y*10+x) ? fg : bg,'A',unset);
        set_glyph("ui",cam[0]+x,y+11+cam[1],unset ? 0 : last_key_position == (y*10+x) ? fg_color : bg_color,unset ? 0 : last_key_position == (y*10+x) ? bg_color : fg_color,unset ? 0 : (y*10+x)+40*sheet); 
      }
    }
  }
  if (mode == 4) {
    print(0,2,1,11,"Sheet ",unset);
    for (var x = 1; x < 6; x++) {
      print(x+5,2,sheet+1 == x ? 11 : 1,sheet+1 == x ? 1 : 11,x,unset);
    }
    for (var y = 0; y < 4; y++) {
      for (var x = 0; x < 10; x++) {
        set_glyph("ui",cam[0]+x,y+3+cam[1],unset ? 0 : last_key_position == (y*10+x) ? fg_color : bg_color,unset ? 0 : last_key_position == (y*10+x) ? bg_color : fg_color,unset ? 0 : (y*10+x)+40*sheet); 
      }
    }
  }
  if (mode == 0 || mode == 1) {
    set_cursors(unset);
  }
}

// KEYS

function editor_keydown(e) {
  e.preventDefault();
  e.stopPropagation();
  if (e.key == "Control") {
    control_down = true;
  }
  if (control_down) {
    let position = key_position[e.key];
    if (position < mode_names.length) mode = position;
    if (e.key.toLowerCase() == 'a'){
        glyphs.typed = new Uint8Array(cols*rows*3);
        fill_glyphs("typed",11,0,-1);
    }
    if (e.key.toLowerCase() == 'l'){
      input.click();
      control_down = false;
    }
    if (e.key.toLowerCase() == 's'){
      arrayBufferToBase64(glyphs["typed"], 'raw', 'liblab')
      control_down = false;
    }
    if (e.key.toLowerCase() == 'm'){
      zone_input.click();
      control_down = false;
    }
    if (e.key.toLowerCase() == 'e'){
      glyphs["export"] = new Uint8Array(cursors_sel_w*cursors_sel_h*3+2);
      glyphs["export"][glyphs["export"].length-2] = cursors_sel_w;
      glyphs["export"][glyphs["export"].length-1] = cursors_sel_h;
      save_zone();
      arrayBufferToBase64(glyphs["export"], 'raw', 'liblab')
    }
    if ( mode == 0 || mode == 1 ) {
      resize_selection(e);
      if (e.key.toLowerCase() == "c") {
        copy();
      }
      if (e.key.toLowerCase() == "v") {
        put_in_memory();
        paste();
      }
      if (e.key.toLowerCase() == "p") {
        let data = get_glyph_data("typed",get_glyph_pos(cursors[0][0]+cam[0],cursors[0][1]+cam[1]));
        bg_color = data[0];
        fg_color = data[1];
      }
      if (e.key.toLowerCase() == "f") {
        put_in_memory();
        fill();
      }
      if (e.key.toLowerCase() == 'z' && past_memory.length) {
        let undo = past_memory.pop();
        let redo = [];
        for (var i = 0; i < undo.length; i++) {
          let memory = [];
          for (var j = 0; j < undo[i].length; j++) {
            memory.push([undo[i][j][0],glyphs["typed"][undo[i][j][0]]]);
            glyphs["typed"][undo[i][j][0]] = undo[i][j][1];
          }
          redo.push(memory);
        }
        futur_memory.push(redo);
      } 
      if (e.key.toLowerCase() == 'y' && futur_memory.length){
        let redo = futur_memory.pop();
        let undo = [];
        for (var i = 0; i < redo.length; i++) {
          let memory = [];
          for (var j = 0; j < redo[i].length; j++) {
            memory.push([redo[i][j][0],glyphs["typed"][redo[i][j][0]]]);
            glyphs["typed"][redo[i][j][0]] = redo[i][j][1];
          }
          undo.push(memory);
        }
        past_memory.push(undo);
      }
    }
    if ( mode == 0 || mode == 1 || mode == 3 || mode == 4 ) {
      if (e.key.toLowerCase() == "i") {
        let new_fg = bg_color;
        let new_bg = fg_color;
        bg_color = new_bg;
        fg_color = new_fg;
      }
    }
  } else {
    if (mode == 0 || mode == 1) {
      if (e.key.length == 1) {
        put_in_memory();
        last_key_position = key_position[e.key];
        type(e.key);
      } else {
        if (e.key == "ArrowLeft") {
          move_cursors(step*-1,0);
        }
        if (e.key == "ArrowRight") {
          move_cursors(step,0);
        }
        if (e.key == "ArrowUp") {
          move_cursors(0,step*-1);
        }
        if (e.key == "ArrowDown") {
          move_cursors(0,step);
        }
        if (e.key == "Backspace") {
          move_cursors(step*-1,0);
          type(' ');
          move_cursors(step*-1,0);
        }
      }
    } else if (mode == 2) {
      if (e.key == "ArrowLeft") {
        cam[0] = cam[0] + step*-1 >= 0 ? cam[0] + step*-1 : cam[0];
      }
      if (e.key == "ArrowRight") {
        cam[0] = cam[0] + step < cols - display_cols ? cam[0] + step : cam[0];
      }
      if (e.key == "ArrowUp") {
        cam[1] = cam[1] + step*-1 >= 0 ? cam[1] + step*-1 : cam[1];
      }
      if (e.key == "ArrowDown") {
        cam[1] = cam[1] + step < rows - display_rows ? cam[1] + step : cam[1];
      }
      if (e.key.toLowerCase() == 'o'){
        if (scale == 1) return;
        scale--;
        fill_glyphs("visible",11,0,0);
        build_canvas();
        cam = [cam[0],cam[1],display_cols,display_rows];
      }
      if (e.key.toLowerCase() == 'i'){
        scale++;
        fill_glyphs("visible",11,0,0);
        build_canvas();
        cam = [cam[0],cam[1],display_cols,display_rows];
      }
    } else if (mode == 3) {
      if (e.key.length == 1) {
        last_key_position = key_position[e.key];
        if (last_key_position < 20) {
          fg_color = last_key_position;
        } else {
          bg_color = last_key_position-20;
        }
      }
    } else if (mode == 4) {
      if (e.key.length == 1) {
        last_key_position = key_position[e.key];
        if (last_key_position < 5) {
          sheet = last_key_position;
        }
      }
    } else if (mode == 5) {
      if (e.key.length == 1) {
        last_key_position = key_position[e.key];
        if (last_key_position > -1) {
          step = last_key_position+1;
        }
      }
    }
  }
}

function create_path(entrance,noise_map) {
  let noise_map_h_offset = Math.floor(Math.random()*noise_map.length);
  if (entrance + noise_map_h_offset > 6) noise_map_h_offset = 1;
  while(entrance > noise_map.length-noise_map_h_offset) {
    noise_map.unshift([]);
  }
  let path = [];
  let queue = [[0,entrance,0]];
  let dirs = [[1,0],[-1,0],[0,1],[0,-1]];
  while (queue.length) {
    let pos = queue.shift();
    if (!noise_map[pos[1]] || !noise_map[pos[1]][pos[0]] || pos_in_path(pos,path)) continue;
    for (var i = 0; i < dirs.length; i++) {
      queue.push([pos[0]+dirs[i][0],pos[1]+dirs[i][1],pos[2]+1]);
    }
    path.push(pos);
  }
  return path;
}

function find_way_out(x,y,map_in_definition) {
  let path = [];
  let queue = [[x,y,0]];
  let dirs = [[1,0],[-1,0],[0,1],[0,-1]];
  let iter = 10;
  while (queue.length) {
    let pos = queue.shift();
    if (!map_in_definition[pos[1]] || !map_in_definition[pos[1]][pos[0]] || pos_in_path(pos,path)) continue;
    for (var i = 0; i < dirs.length; i++) {
      if(dirs[i][1] == -1 && map_in_definition[pos[1]][pos[0]][1][1] == 0) continue;
      if(dirs[i][1] == 1 && map_in_definition[pos[1]][pos[0]][1][1] == 0) continue;
      queue.push([pos[0]+dirs[i][0],pos[1]+dirs[i][1],pos[2]+1]);
    }
    path.push(pos);
  }
  return path;
}

function dead_room_in_map(entrance,map) {
  for (var y = 0; y < map.length; y++) {
    for (var x = 0; x < map[y].length; x++) {
      if(map[y][x]) {
        let path = find_way_out(x,y,map);
        if (!pos_in_path([0,entrance],path)) {
          return true;
        }
      }
    }
  }
  return false;
}

function define_map(entrance,map) {
  let max_difficulty = 0;
  for (var y = 0; y < map.length; y++) {
    for (var x = 0; x < map[y].length; x++) {
      if(map[y][x][0] > max_difficulty) max_difficulty = map[y][x][0];
    } 
  }
  for (var y = 0; y < map.length; y++) {
    for (var x = 0; x < map[y].length; x++) {
      if(map[y][x]) {
        let left =
          !x && y == entrance ? 2 :
          map[y][x-1] ? 1 :
        0;
        let center = 0;
        let right =
          map[y][x+1] ? 1 :
        0;
        map[y][x].push([left,center,right]);
      }
    } 
  }
  while (dead_room_in_map(entrance,map)){
    let y_dir = Math.random() > 0.5;
    for (
      var y = y_dir ? 0 : map.length-1;
      y_dir ? (y < map.length) : (y > -1); 
      y_dir ? (y++) : (y--)
    ) {
      let x_dir = Math.random() > 0.5;
      for (
        var x = x_dir ? 0 : map[y].length-1;
        x_dir ? (x < map[y].length) : (x > -1); 
        x_dir ? (x++) : (x--)
      ) {
        if(map[y][x]) {
          let path = find_way_out(x,y,map);
          if (!pos_in_path([0,entrance],path)) {
            if (map[y-1] && map[y-1][x]) {
              let sub_path = find_way_out(x,y-1,map);
              if (pos_in_path([0,entrance],sub_path)) {
                map[y][x][1][1] = map[y][x][1][1] == 1 || map[y][x][1][1] == 2 ? 3 : 1;
                map[y-1][x][1][1] = map[y-1][x][1][1] == 1 || map[y-1][x][1][1] == 2 ? 3 : 2;
              }
            }
            if (map[y+1] && map[y+1][x]) {
              let sub_path = find_way_out(x,y+1,map);
              if (pos_in_path([0,entrance],sub_path)) {
                map[y][x][1][1] = map[y][x][1][1] == 1 || map[y][x][1][1] == 2 ? 3 : 2;
                map[y+1][x][1][1] = map[y+1][x][1][1] == 1 || map[y+1][x][1][1] == 2 ? 3 : 1;
              }
            }
          }  
        }
      }
    }
  }
  for (var y = 0; y < map.length; y++) {
    for (var x = 0; x < map[y].length; x++) {
      if(map[y][x]) {
        if(map[y][x][1][1] == 1 && map[y+1] && map[y+1][x] && map[y+1][x][1][1] == 2 && Math.random() > 0.5){
          map[y][x][1][1] = 3;
          map[y+1][x][1][1] = 3;
            console.log("cOUCOU",x,y);
        }
        /*
        if((map[y][x][1][1] == 1 && map[y-1] && map[y-1][x]
        || map[y][x][1][1] == 2 && map[y+1][x] && map[y+1][x]) && Math.random() > 0.75) {
            map[y][x][1][1] == 3;
            console.log("cOUCOU",x,y);
          if(map[y-1] && map[y-1][x]) {
            map[y-1][x][1][1] == 3;
          } 
          if(map[y+1] && map[y+1][x]) {
            map[y+1][x][1][1] == 3;
          } 
        }
        if((map[y][x][1][1] == 1 && map[y-1][x][1][1] == 2
        || map[y][x][1][1] == 2 && map[y+1][x][1][1] == 1 ) && Math.random() > 0.5) {
            map[y][x][1][1] == 3;
            console.log("cOUCOU",x,y);
          if(map[y-1] && map[y-1][x] && map[y-1][x][1][1] == 2) {
            map[y-1][x][1][1] == 3;
          } 
          if(map[y+1] && map[y+1][x] && map[y+1][x][1][1] == 1) {
            map[y+1][x][1][1] == 3;
          } 
        }
        */
      } 
    }
  }
  return map;
}

function double_map(map) {
  let new_map = []
  for (var y = 0; y < map.length; y++) {
    new_map.push([]);
    for (var x = 0; x < map[y].length; x++) {
      new_map[y].push(map[y][x] ? [map[y][x][0]] : false);
      new_map[y].push(map[y][x] ? [map[y][x][0]] : false);
    }
  }
  return new_map;
}

function path_to_map(path) {
  let w = 0;
  let h = 0;
  for (var i = 0; i < path.length; i++) {
    if (path[i][0] > w) {
      w = path[i][0];
    }
    if (path[i][1] > h) {
      h = path[i][1];
    }
  }
  let map = [];
  for (var y = 0; y < h+1; y++) {
    map.push([]);
    for (var x = 0; x < w+1; x++) {
      map[y].push(pos_in_path([x,y],path) ? pos_in_path([x,y],path) : false);
    }
  }
  return map;
}

function get_path_by_length(entrance,w,h,noise_ratio,length) {
  var path = create_path(entrance,create_noise_map(w,h,noise_ratio));
  let it = 0;
  while (path.length != length){
    path = create_path(entrance,create_noise_map(w,h,noise_ratio));
    it ++;
    if (it >= 1000) {
      length = length > 1 ? length - 1 : length;
      noise_ratio = noise_ratio > 0 ? noise_ratio - 0.01 : noise_ratio;
    }
    if (it >= 2000) {
      break;
    }
  }
  console.log(path,length,noise_ratio,entrance);
  return path
}
function get_min_length_path(w,h,noise_ratio,min_length) {
  var path = create_path(create_noise_map(w,h,noise_ratio));
  while (path.length < min_length){
    path = create_path(create_noise_map(w,h,noise_ratio));
  }
  return path
}

function print_map(map){
  let str = "";
  for (var y = 0; y < map.length; y++) {
    for (var x = 0; x < map[y].length; x++) {
      str += map[y][x] ? map[y][x][1].join('')+' ' : '    ';
    }
    str+='\n';
  }
  return str;
}

function create_noise_map(w,h,noise_ratio) {
  let noise_map = [];
  for (var y = 0; y < h; y++) {
    noise_map.push([]);
    for (var x = 0; x < w; x++) {
      noise_map[y].push(Math.random() > noise_ratio ? 1 : 0);
    }
  }
  return noise_map;
}
/*
function entrance_in_path(path) {
  for (var i = 0; i < path.length; i++) {
    if (!path[i][0] && !path[i][1]) {
      return true; 
    }
  }
  return false;
}*/
function pos_in_path(pos,path) {
  for (var i = 0; i < path.length; i++) {
    if (path[i][0] == pos[0] && path[i][1] == pos[1]) {
      return [path[i][2]]; 
    }
  }
  return false;
}

/*
V#     V#  
^#V# V#X#  
  ^#V^#X#  
    ^# X#  
       ^#

>=V         
  X=V       
>=X=^=V=V<  
  X   X=X<  
>=^   ^=^<  
            
==V<         
  X=X<       
>=X=X=X=X<   
  X<  X=X<   
>=X<  X=X<   
        
        
=V      
 X<     
>X=X<   
 X X<   
>^ ^<   
        
#<           
X=V=V=V<     
^=X=^=X==<   
  ^<  X<     
      ^<     

#=V<  V<    
^=^===X<    
      X<    
      ^<    
        
#<  V<     
X<  X<     
^===^<     

V==<      
^<        

        
 1    2    3    4    5    6       
E.>  <.>  <V>  <.>  <V>  <..
           7    8    9    10     
          .X>  <.>  <X>  <..
           11   12   13   14        
          .^>  <.>  <^>  <..        


 1    2    3    4
EV>  <.>  <.>  <..
 5    6            
.X>  <..          
 7    8    9    10 
.^>  <.>  <.>  <..        
                
 1    2    3    4                
EV>  <.>  <V>  <..                    
 5    6    7    8    9    10   11   12
.X>  <.>  <^>  <.>  <V>  <.>  <V>  <..
 13   14             15   16   17   18
.X>  <..            .^>  <.>  <^>  <..
 19   209
.^>  <..                                      
                       
             
            
*/ 

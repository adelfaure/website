<!DOCTYPE html>
<?php
  function ascii_assets() {
    $path = "./art/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<pre class=\"ascii\" id=\"$path$file\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</pre>\n";
      }
    }
  }
  function js_assets() {
    $path = "./libs/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<script src=\"$path$file\"></script>\n";
      }
    }
  }
?>
<!-- Tilde Rider -->
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
    <title></title>
    <style>
      @font-face{
        font-family:"jgs";
        src:url("src/jgs_Font.ttf");
      }
      * {
        font-family:"jgs";
        font-size:2.9vw;
        line-height:2.9vw;
        font-weight:bold;
        margin:0;
        padding:0;
        cursor:none;
      }
      @media (min-width:1600px) {
        * {
          font-size:1.45vw;
          line-height:1.45vw;
        }
      }
      body {
        height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;
        transition: color 1s, background-color 1s, transform 1s;
      }
      .ascii{
        display: none;
      }
    </style>
  </head>
  <body>
<?php ascii_assets(); ?>
    <pre id="scene">Loading</pre>
  </body>
<?php js_assets(); ?>
  <script>
  window.onload = init;
  </script>
</html>

console.log("art.js");
function load_art(){
  let sprites = document.getElementsByClassName("ascii");
  for (var i = 0; i < sprites.length; i++) {
    let name = sprites[i].id.split('/').pop().split('.')[0];
    art[name] = text_to_screen(sprites[i].innerText);
  }
  art["horizon_line"] = screen(screen_w,1,'.');
  art["horizon"] = screen(screen_w,screen_h+10,':');
}

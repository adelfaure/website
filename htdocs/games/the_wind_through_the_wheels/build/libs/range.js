function range(
  range_elements,
  min,current,max,
  interactive_screen,
  screen,
  x,
  y,
  w,
  interaction
) {
  let previous_range_elements = [];
  for (var i = 0; i < range_elements.length; i++) {
    previous_range_elements.push(range_elements[i]);
  }
  while (range_elements.length) {
    range_elements.pop();
  }
  for (var i = 0; i < w; i++) {
    range_element(
      min,current,max,
      interactive_screen,
      screen,
      range_elements,
      x+i,
      y,
      interaction
    )
  }
  if (min) {
    let current_index = uF((current-min) * ((range_elements.length-3)/(max-min)))+1;
    range_elements[current_index] = 2;
  } else {
    let current_index = uF(current / (max / (range_elements.length-3)));
    range_elements[current_index+1] = 2;
  }
  for (var i = 0; i < previous_range_elements.length; i++) {
    range_elements[i] = previous_range_elements[i] == 2 ? 2 : 0;
  }
}

function range_element(
  min,current,max,
  interactive_screen,
  screen,
  range_elements,
  x,
  y,
  interaction
){
  let id = range_elements.length;
  let first = text_to_screen(range_elements_sprite(5));
  let last = text_to_screen(range_elements_sprite(7));
  let out = text_to_screen(range_elements_sprite(0));
  let out_before_down = text_to_screen(range_elements_sprite(3));
  let out_after_down = text_to_screen(range_elements_sprite(4));
  let down = text_to_screen(range_elements_sprite(2));
  range_elements.push(0);
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        out,
        x,
        y,
        function() {
          if (id == 0 || id == range_elements.length-1) return false;
          range_elements[id] = range_elements[id] == 2 ? 2 : 1;
          if (mouse_down) {
            range_elements[id] = 2;
            for (var i = 0; i < range_elements.length; i++) {
              if (i==id) continue;
              range_elements[i] = 0;
            }
            interaction(id,range_elements);
          }
          return true;
        }
      );
    }
  );
  display_elements.push(
    function () {
      if (id == 0 || id == range_elements.length-1) return;
      add_to_screen(
        screen,
        Number.isInteger((id-1)/5) ?  text_to_screen('l') : text_to_screen('\''),
        x,
        y+2
      );
      if (!Number.isInteger((id-1)/5)) return;
      let value = min+(id-1) * ((max-min) / (range_elements.length-3));
      let number_mark = String(value < 10 && max < 15 ? value.toFixed(1) : uF(value));
      let offset_mark = number_mark.length > 1 ? 1 : 0;
      add_to_screen(
        screen,
        text_to_screen(number_mark),
        x-offset_mark,
        y+3
      );
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        range_elements[id] == 0 ? 
          range_elements[id + 1] && range_elements[id + 1] == 2 ? 
            out_before_down :
          range_elements[id - 1] && range_elements[id - 1] == 2 ? 
            out_after_down :
          id == 0 ? first : id == range_elements.length-1 ? last : 
            out :
        range_elements[id] == 1 ? out : down,
        x,
        y
      );
    }
  );
}

function range_elements_sprite(state) {
  return state == 0 ? " \n=\n " :
         state == 3 ? " \n(\n " :
         state == 4 ? " \n)\n " :
         state == 5 ? " \n[\n " :
         state == 7 ? " \n]\n " :
         ' \n⚌\n ';
}

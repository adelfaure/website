console.log("init.js");
function init() {

  altitude = -(screen_h/2-9);

  // LOAD ART

  load_art();

  // DISPLAY
  
  init_display();

  // ANIMATE
  
  start_animation(fps);

  //new_checkpoint(1);

  let main_process = start_animation_event(1, function(current_frame) {
    main(current_frame);
  });

  // KEYBOARD
  
  init_controls();
}

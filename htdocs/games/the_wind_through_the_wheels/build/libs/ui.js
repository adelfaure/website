console.log("ui.js");
var cursor = "cursor";
var interactive_elements = [];
var display_elements = [];
var main_menu = [];
var tools_menu = [];

function elements_declaration(interactive_screen,screen) {
  interactive_elements = [];
  display_elements = [];
  
  if (tools_menu[0] == 2) {
    power_tool(
      interactive_screen,
      screen
    );
  }
  if (tools_menu[1] == 2) {
    time_tool(
      interactive_screen,
      screen
    );
  }
  if (tools_menu[2] == 2) {
    wind_tool(
      interactive_screen,
      screen
    );
  }
  if (tools_menu[3] == 2) {
    gradient_tool(
      interactive_screen,
      screen
    );
  }
 
  if (main_menu[0] == 2) {
    menu(
      tools_menu,
      interactive_screen,
      screen,
      0,
      3,
      16,
      [
        ["Power",0],
        ["Time",0],
        ["Wind",0],
        ["Gradient",0],
        //["Weather",0]
      ],
      false,
      function () {}
    );
  }

  menu(
    main_menu,
    interactive_screen,
    screen,
    0,
    0,
    16,
    [
      ["Tools",0],
      [''/*"Stats"*/,main_menu[0] == 2 ? 5 : 0],
    ],
    true,
    function (id,menu_elements) {
      tools_menu = []; // close tool menu
    }
  );


  //// TOOLBOX
  //toolbox(interactive_screen,screen);
  //
  //// SPEED
  //if (main_menu[0]) {
  //  speed_ui(interactive_screen,screen,screen_w-15,0);
  //}
  let text = uF(convert_speed('m/s','km/h',speed))+"km/h";
  while (text.length < 7) {
    text = ' '+text;
  }
  digital_text(screen,text+' ',screen_w-(text.length*3+6),0);

  ////// CLOCK
  //if (main_menu[1]) {
  //  clock_ui(interactive_screen,screen,screen_w-art["clock"][0].length-2,0);
  //}

  //// WIND
  ////wind_ui(interactive_screen,screen,screen_w-42,0);

}

function ui(screen,frame) {
  
  // INTERACTIVE UI
  
  // CURSOR
  add_to_screen(
    screen,
    art[cursor],
    mouse_x-1,
    mouse_y-1
  );
  

  // BUTTONS

  elements_declaration(interactive_screen,screen);

  
  // INTERACTIVE ELEMENTS

  for (var i = 0; i < interactive_elements.length; i++) {
    interactive_elements[i]();
  }
  
  

  // CURSOR CHANGE FOR INTERACTION
  
  if (interactive_screen[mouse_y][mouse_x]) {
    let interaction = interactive_screen[mouse_y][mouse_x]();
    if (interaction) {
      cursor = "cursor_active";
    } else {
      cursor = "cursor";
      mouse_over = false;
    }
  } else {
    cursor = "cursor";
    mouse_over = false;
  }
 
  // DISPLAY UI

  
  // DISPLAY ELEMENTS

  for (var i = 0; i < display_elements.length; i++) {
    display_elements[i]();
  }

  add_to_screen(
    screen,
    text_to_screen(String(readable_fps)+" FPS"/* +"fps , render : "+ in_render+'/'+total_in_render+" elements, "+render_char+" characters"*/),
    0,
    screen_h-1
  );
  /*
  let distance_log = Math.floor(get_distance('km',distance-offset_distance))+'km '+(Math.floor(get_distance('m',distance-offset_distance))-Math.floor(get_distance('km',distance-offset_distance))*1000)+'m';


  let msg = text_to_screen(message(
    "traveled ... "+distance_log + '\n'+
    "power ...... "+wanted_power.toFixed(0)+'/'+max_power.toFixed(0)+"W\n"+
    "altitude ... "+((screen_h+altitude)*10).toFixed(0)+"\n"+
    "stress ..... "+cyclist_tss.toFixed(0)+'\n'
  ));
  add_to_screen(
    screen,
    msg,
    0,
    0
  );
    */

  



  // CHECKPOINT
  /*
  if (checkpoint_info) {
    let msg = text_to_screen(message(
      "Checkpoint !\n"+
      "[y] : continue\n"
    ));
    add_to_screen(
      screen,
      msg,
      Math.floor(screen_w/2 - msg[0].length/2),
      Math.floor(screen_h/2 - msg.length/2)
    );
  }
  if (next_checkpoint_command_confirm) {
    let next_time = get_next_checkpoint_time();
    let msg = text_to_screen(message(
      "Skip to next checkpoint ?\n"+
      next_time[0]+'\n'+
      next_time[1]+'\n'+
      get_distance('km',(checkpoint_position - true_distance())).toFixed(0) + "km at " + convert_speed('m/s','km/h',speed).toFixed(0)+"km/h\n"+
      "[y/n] : confirm/cancel\n"
    ));
    add_to_screen(
      screen,
      msg,
      Math.floor(screen_w/2 - msg[0].length/2),
      Math.floor(screen_h/2 - msg.length/2)
    );
  }
  if (!next_checkpoint_command_confirm   
  &&  !next_checkpoint_command        
  &&  !checkpoint_info
  &&  speed > 0 ) {
    let next_time = get_next_checkpoint_time();
    let msg = text_to_screen(message(
      "Next checkpoint in "+next_time[0]+'\n'+ 
      uF(get_distance('km',(checkpoint_position - true_distance()))) + "km "+ 
      uF(get_distance('m',(checkpoint_position - true_distance())) - uF(get_distance('km',(checkpoint_position - true_distance())))*1000) + "m left\n"+ 
      "[Enter] : skip\n"
    ));
    add_to_screen(
      screen,
      msg,
      Math.floor(screen_w/2 - msg[0].length/2),
      0
    );
  }
  */
  in_render = 0;
  total_in_render = 0;
  render_char = 0;

  return screen;
}

function get_next_checkpoint_time() {
  let speed_time = (checkpoint_position - true_distance()) / speed;
  let next_game_time = game_time + speed_time * 1000;
  let next_time = new Date(game_date.getTime() + next_game_time);
  return [
    uF(speed_time/60/60) +"h "+ uF(speed_time/60 - uF(speed_time/60/60)*60)+"m "+ uF(speed_time - uF(speed_time/60) * 60)+"s",
    next_time.toLocaleTimeString(),
  ];
}

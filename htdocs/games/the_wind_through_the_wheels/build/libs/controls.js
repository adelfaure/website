console.log("controls.js");

function init_controls() {
//document.addEventListener("

  // MOUSE

  document.addEventListener("mousemove",function(e){
    mouse_x = uF((e.clientX/(window.innerWidth/screen_w)));
    mouse_y = uF((e.clientY/(window.innerHeight/screen_h)));
  });
  document.addEventListener("mousedown",function(e){
    mouse_down = true;
  });
  document.addEventListener("mouseup",function(e){
    mouse_down = false;
    mouse_active = false;
  });

  // KEYBOARD

  document.addEventListener("keydown",function(e){
    if (e.key.toLowerCase() == "g") {
      if (!tools_menu[3]) {
        tools_menu = []; // close tool menu
        main_menu = []; // close main menu
      }
      main_menu[0] = main_menu[0] ? 0 : 2; // open tools menu
      tools_menu[3] = tools_menu[3] ? 0 : 2; // open power tool
    }
    if (e.key.toLowerCase() == "w") {
      if (!tools_menu[2]) {
        tools_menu = []; // close tool menu
        main_menu = []; // close main menu
      }
      main_menu[0] = main_menu[0] ? 0 : 2; // open tools menu
      tools_menu[2] = tools_menu[2] ? 0 : 2; // open power tool
    }
    if (e.key.toLowerCase() == "o") {
      if (!tools_menu[0]) {
        tools_menu = []; // close tool menu
        main_menu = []; // close main menu
      }
      main_menu[0] = main_menu[0] ? 0 : 2; // open tools menu
      tools_menu[0] = tools_menu[0] ? 0 : 2; // open power tool
    }
    if (e.key.toLowerCase() == "t") {
      if (!tools_menu[1]) {
        tools_menu = []; // close tool menu
        main_menu = []; // close main menu
      }
      main_menu[0] = main_menu[0] ? 0 : 2; // open tools menu
      tools_menu[1] = tools_menu[1] ? 0 : 2; // open power tool
    }
    if (e.key.toLowerCase() == "p") {
      game_pause = !game_pause;
    }
    //if (e.key == "y" /*&& game_speed > 1*/) {
    //  if (next_checkpoint_command_confirm) {
    //    next_checkpoint_command_confirm = false;
    //    next_checkpoint_command = true;
    //  }
    //  if (checkpoint_info) {
    //    checkpoint_info = false;
    //    new_checkpoint();
    //    game_speed = 1;
    //  }
    //}
    //if (e.key == "n" /*&& game_speed > 1*/) {
    //  if (next_checkpoint_command_confirm) {
    //    next_checkpoint_command_confirm = false;
    //  }
    //}
    //if (
    //  next_checkpoint_command || 
    //  checkpoint_info         
    //) return;
    //if (!ctx) ctx = new(window.AudioContext || window.webkitAudioContext)();
    //if (e.key == "ArrowUp" && wanted_power < max_power) {
    //  wanted_power+=1;
    //  //speed += convert_speed("km/h","m/s",1);
    //  //speed = uF(speed*1000)/1000;
    ////speed_record = speed > speed_record ? speed : speed_record;
    //}
    //if (e.key == "ArrowDown" && wanted_power > 0) {
    //  wanted_power-=1;
    //  if (wanted_power == 0) speed = 0;
    //  //speed -= convert_speed("km/h","m/s",1);
    //  //if (next_checkpoint_command_confirm) {
    //  //  speed = speed < convert_speed("km/h","m/s",1) ? convert_speed("km/h","m/s",1) : speed;
    //  //}
    //  //speed = uF(speed*1000)/1000;
    //  //speed = speed < 1 ? 0 : speed;
    //}
    //if (e.key == "PageUp" /*&& game_speed * 1.1 < 1000*/) {
    //  altitude += 1/10;
    //}
    //if (e.key == "PageDown" /*&& game_speed > 1*/) {
    //  altitude -= 1/10;
    //}
    ////if (e.key == "ArrowRight" /*&& game_speed * 1.1 < 1000*/) {
    ////  game_speed = !game_speed ? 1 : game_speed * 2;
    ////}
    ////if (e.key == "ArrowLeft" /*&& game_speed > 1*/) {
    ////  game_speed = !game_speed ? 1 : game_speed / 2;
    ////}
    //if (e.key == "Enter" /*&& game_speed > 1*/) {
    //  if (speed > 0) {
    //    next_checkpoint_command_confirm = true;
    //  }
    //}
    //if (e.key == " " /*&& game_speed > 1*/) {
    //  game_time += 1000*60*2.5;
    //}
    //if (e.key == "r" /*&& game_speed > 1*/) {
    //  rain_density = rain_density > 0 ? rain_density - 1 : 0;
    //}
    //if (e.key == "R" /*&& game_speed > 1*/) {
    //  rain_density = rain_density < 6 ? rain_density + 1 : rain_density;
    //}
    //if (e.key == "w" /*&& game_speed > 1*/) {
    //  real_wind_speed -= 1;
    //}
    //if (e.key == "W" /*&& game_speed > 1*/) {
    //  real_wind_speed += 1;
    //}
    //if (e.key == "g" /*&& game_speed > 1*/) {
    //  slope_gradient -= 1;
    //}
    //if (e.key == "G" /*&& game_speed > 1*/) {
    //  slope_gradient += 1;
    //}
  });
}

// CHECKPOINT

let next_checkpoint_command_confirm = false;
let next_checkpoint_command = false;
let checkpoint_info = false;
let checkpoint_position = 0;
let checkpoint_interval = 100;

function next_checkpoint(time_elapsed,new_step) {
  if (!speed|| !next_checkpoint_command) return new_step;
  if (distance + new_step <= checkpoint_position + offset_distance) {
    game_speed = game_speed * 1.1;
    return new_step;
  } else {
    //distance = checkpoint_position + offset_distance;
    //distance_played = true_distance();
    next_checkpoint_command = false;
    let skip_time = (checkpoint_position - true_distance()) / speed
    game_time += skip_time * 1000 - time_elapsed*game_speed;
    console.log(time.toLocaleTimeString());
    time = new Date(game_date.getTime() + game_time);
    game_speed = 1;
    return checkpoint_position - true_distance();
  }
}

function checkpoint_stop(new_step) {
  if (next_checkpoint_command) return;
  if (distance + new_step > checkpoint_position + offset_distance) {
    //game_speed = 0;
    //checkpoint_info = true;
    //new_checkpoint();
  }
}

function new_checkpoint() {
  rain_density = uFR(0,6);
  //wind = Math.random() * 10 - Math.random()*20;
  //real_wind_speed = wind*-1;
  next_altitude = uF(altitude + (Math.random()*10) *( Math.random() > 0.5 ? 1 : -1));
  while ((screen_h+next_altitude)*10 <= 0) {
    next_altitude = uF(altitude + (Math.random()*10) *( Math.random() > 0.5 ? 1 : -1));
  }
  //slope = 0//((next_altitude - altitude) / checkpoint_interval)*2;
  //slope_gradient = slope*50;
  //document.body.style.transform = "rotate("+(360*slope/4)*-1+"deg)";
  checkpoint_position += checkpoint_interval;
  place_element(1,
  "checkpoint", 
  'm',checkpoint_position+offset_distance);
}

console.log("utils.js");
function get_distance(unit,root_distance) {
  return unit == "m" ? root_distance : 
         unit == "km"? root_distance /1000 :
        "need valid unit";
}

function convert_speed(from,to,speed) {
  return from == "m/s"  ?
           to == "km/h" ? speed * 3600 / 1000 :
           "need valid to" :
         from == "km/h" ?
           to == "m/s" ? speed * 1000 / 3600 :
           "need valid to" :
         "need valid from";
}

function invert(value) {
  return 1/value;
}

function ratio(value,ratio) {
  return Math.floor(value * ratio);
}

function cycle(i,a) {
  return i > a.length-1 ? i - Math.floor(i/a.length) * a.length : i;
}

function every(n,feed) {
  return Number.isInteger(feed/n);
}

function blink(n,feed) {
  return Number.isInteger(Math.floor(feed/n)/2);
}

function uF(value) {
  return Math.floor(value);
}

function uR(a,b) {
  return a + Math.random()*(b-a);
}
function uFR(a,b) {
  return uF(a + Math.random()*(b-a));
}

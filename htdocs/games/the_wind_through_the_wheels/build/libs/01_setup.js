console.log("setup.js");
// SETUP

//let screen_w = Math.floor(window.innerHeight/((window.innerWidth/100*1.45)/2))-8;
let mouse_x = 0;
let mouse_y = 0;
let mouse_down = false;
let mouse_active = false;
let mouse_over = false;


let char_per_m = 3;
let filler = '░';

let fps = 120;

let game_speed = 1;
let max_game_speed = 5;
let game_pause = false;
let game_time = 0;

let altitude = 0;
let next_altitude = 0;
let speed = 0;
let max_power = 200;
let wanted_power = max_power/2;
//let max_speed = convert_speed("km/h","m/s",24);

let offset_distance = 0;
let trail_length = 400000+offset_distance;
let distance = 0;
function true_distance(){ 
  return distance-offset_distance;
};

let art = {};
let layers = [];
let sky_layers = [];

let game_date = new Date('July 1, 1996, 08:00:00');
let time = game_date;

let init_time = Date.now();
let last_time = init_time;

let cloud_chance = 
      Math.random() > 0.5 ? 
        Math.random() > 0.5 ? Math.random()   : 
                              Math.random()/2 : 
        Math.random() > 0.5 ? Math.random()/4 : 
                              Math.random()/8 ;
let rain_density = 0;
let min_wind = -50;
let max_wind = 50;

let min_gradient = -15;
let max_gradient = 15;
let slope = -0.05;

var screen_h = uF(window.innerHeight/(window.innerWidth/100*
  (window.innerWidth > 1600 ? 1.45 : 2.9)
))-2;
var screen_w = uF(window.innerWidth/((window.innerWidth/100*
  (window.innerWidth > 1600 ? 1.45 : 2.9)
)/2))-8;
//var screen_w = screen_h*2;
let chunk_size = Math.floor(screen_w/char_per_m);

// TOOLBOX
//let toolbox_open = false;

//let time_tool = true;
//let time_tool_open = false;
//
//let wind_tool = true;
//let wind_tool_open = false;

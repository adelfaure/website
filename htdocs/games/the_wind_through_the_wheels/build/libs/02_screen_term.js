#!/usr/bin/env node

console.log("text_screen.js");
function screen(w,h,fill) {
  let screen = [];
  while (h--) {
    let x = w;
    screen.push([]);
    while (x--) {
      screen[screen.length-1].push(fill);
    }
  }
  return screen;
}

let render_char = 0;

function text_to_screen(text) {
  let screen = [];
  text = text.split('\n');
  for (var i = 0; i < text.length; i++) {
    screen.push(text[i].split(''));
  }
  return screen;
}

function add_to_screen(screen,content,x,y) {
  if (!content) return;
  for (var row = 0; row < content.length; row++) {
    if (row + y > screen.length-1
    ||  row + y < 0 ) continue;
    for (var col = 0; col < content[row].length; col++) {
      if (col + x > screen[row + y].length-1
      ||  col + x < 0 
      ||  content[row    ][col    ] == '░'
      ||  screen[ row + y][col + x] != filler ) continue;
      screen[     row + y][col + x] = content[row][col];
      render_char++;
    }
  }
}

function screen_to_text(screen) {
  let text = [];
  for (var i = 0; i < screen.length; i++) {
    text.push(screen[i].join(''));
  }
  return text.join('\n');
}

// SCRIPT

let filler = '#';
var base_screen = screen(80,20,filler);
let bonhomme = 
"░O░\n"+
"/|\\\n"+
"/░\\";

let bonhomme_x = 0;
let bonhomme_y = 0;

function process() {
  bonhomme_x+=0.1;
  bonhomme_y+=0.1;
  base_screen = screen(80,40,filler);
  let final_screen = screen(80,40,' ');

  add_to_screen(base_screen,text_to_screen(bonhomme),Math.floor(bonhomme_x),Math.floor(bonhomme_y));
  add_to_screen(base_screen,text_to_screen(bonhomme),Math.floor(bonhomme_x*2),Math.floor(bonhomme_y*2));
  add_to_screen(base_screen,text_to_screen(bonhomme),Math.floor(bonhomme_x*3),Math.floor(bonhomme_y*3));
  add_to_screen(base_screen,final_screen,0,0);
  console.log(screen_to_text(base_screen));
};

setInterval(process,17);

console.log("ui_elements.js");

function add_interaction_to_screen(screen,content,x,y,interaction) {
  if (!content) return;
  for (var row = 0; row < content.length; row++) {
    if (row + y > screen.length-1
    ||  row + y < 0 ) continue;
    for (var col = 0; col < content[row].length; col++) {
      if (col + x > screen[row + y].length-1
      ||  col + x < 0 
      ||  content[row    ][col    ] == '░'
      ||  screen[ row + y][col + x] ) continue;
      screen[     row + y][col + x] = interaction;
    }
  }
}
function ascii_window(name,w,h) {
  let line_0 = ".-";
  while (line_0.length < w+2) line_0 += '-';
  line_0 += "-.\n";
  name = ' '+name+' ';
  while (name.length < w) name = Number.isInteger(name.length/2) ? name + ':' : ':' + name;
  name = '| '+name+" |\n";
  let line_2 = "|-";
  while (line_2.length < w+2) line_2 += '-';
  line_2 += "-|\n";
  let body = "";
  while (h--) {
    let line = "| ";
    while (line.length < w+2) line+=' ';
    line+=" |\n";
    body += line;
  }
  let last_line = "'-";
  while (last_line.length < w+2) last_line += '-';
  last_line += "-'\n";
  return line_0+name+line_2+body+last_line;
}

function digital_window(w) {
  let line_0 = '';
  while (line_0.length < w*3+1) {
    line_0 += Number.isInteger(line_0.length/2) ? ' ' : '_';
  }
  line_0+='\n';
  let body = '';
  for (var i = 0; i < 4; i++) {
    let line = 'l';
    while (line.length < w*3+1) {
      line += ' ';
    }
    line += 'l\n';
    body += line;
  }
  let last_line = '';
  while (last_line.length < w*3+1) {
    last_line += Number.isInteger(last_line.length/2) ? ' ' : '¯';
  }
  last_line+='\n';
  return line_0+body+last_line;
}

function digital_text(screen,txt,x,y) {
  let letters = txt.split('');
  for (var i = 0; i < letters.length; i++) {
    let letter = 
      letters[i] == '.' ? 'dot' : 
      letters[i] == '%' ? 'percent' : 
      letters[i] == '/' ? 'slash' : 
      letters[i];
    let x_offset = i*3;
    display_elements.push(
      function () {
        add_to_screen(
          screen,
          art[letter],
          x+x_offset+1,
          y+1,
        );
      }
    );
  }
  let text_window = text_to_screen(digital_window(letters.length));
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        text_window,
        x,
        y,
      );
    }
  );
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        text_window,
        x,y,
        function(){ 
          info_bulle(screen,txt+'\n');
        }
      );
    }
  );
}


//function ascii_window(name,w,h) {
//  let text_content = '';
//  for (var y = 0; y < h; y++) {
//    if (name && y == 1) {
//      text_content += "| "+name;
//      for (var x = name.length+2; x < w-1; x ++) {
//        text_content += ' ';
//      }
//      text_content += '|';
//    } else if (name && y == 2){
//      text_content += "| ";
//      for (var x = 0; x < name.length; x ++) {
//        text_content += '-';
//      }
//      for (var x = name.length+2; x < w-1; x ++) {
//        text_content += ' ';
//      }
//      text_content += '|';
//      
//    } else {
//      for (var x = 0; x < w; x++) {
//        text_content += 
//          !y ?
//            !x || x == w-1 ? '.'  : '-':
//          y == h-1 ?
//            !x || x == w-1 ? '\'' : '-':
//          !x || x == w-1 ? '|' : ' ';
//          
//      }
//    }
//    text_content+='\n';
//  }
//  return text_content;
//}

function message(txt) {
  let lines = txt.split('\n');
  let max_length = 0;
  for (var i = 0; i < lines.length; i++) {
    if (lines[i].length > max_length) max_length = lines[i].length;
  }
  for (var i = 0; i < lines.length; i++) {
    while (lines[i].length < max_length) lines[i] = lines[i] + ` `;
  }
  let sign_up = ":'";
  for (var i = 0; i < lines[0].length; i++) {
    sign_up+='\'';
  }
  sign_up+="':";
  let sign_mid = '';
  for (var i = 0; i < lines.length-1; i++) {
    sign_mid += ": "+lines[i]+" :\n";
  }
  let sign_down = ":.";
  for (var i = 0; i < lines[0].length; i++) {
    sign_down+='.';
  }
  sign_down+=".:";
  return sign_up+'\n'+sign_mid+sign_down;
}


function info_bulle(screen,txt) {
  let msg = text_to_screen(message(
    txt
  ));
  add_to_screen(
    screen,
    msg,
    mouse_x+1,// mouse_x < screen_w/2 ? mouse_x+1 : mouse_x-msg[0].length,
    mouse_y+1 // mouse_y < screen_h/2 ? mouse_y+1 : mouse_y-msg.length
  );
}


      //let secondes = Math.floor(game_time/1000);
      //let minutes = Math.floor(secondes/60);
      //let hours = Math.floor(minutes/60);
        //hours+'h '+(minutes-hours*60)+'m '+(minutes ? secondes -minutes * 60 : secondes)+"s ("+game_speed.toFixed(2)+')'+'\n'


// function speed_ui(interactive_screen,screen,x,y) {
//   
//   // SPEED INFO BULLE
//   interactive_elements.push(function(){
//     add_interaction_to_screen(
//       interactive_screen,
//       art["speed_screen"],
//       x,
//       y,
//       function(){
//         info_bulle(screen,
//           "Cyclist speed : "+convert_speed('m/s','km/h',speed).toFixed(0)+"km/h\n"
//         );
//       }
//     )
//   });
//  
//   // SPEED SPRITE
//   display_elements.push(function(){ 
//     let speed_txt = String(convert_speed('m/s','km/h',speed).toFixed(0));
//     speed_txt = speed_txt.length == 1 ? '00' + speed_txt :
//                 speed_txt.length == 2 ? '0'  + speed_txt :
//                 speed_txt;
//     for (var i = 0; i < speed_txt.length; i++) {
//       add_to_screen(
//         screen,
//         art[speed_txt.charAt(i)],
//         x+2+i*3,
//         y+1
//       );
//     }
//     add_to_screen(
//       screen,
//       art["speed_screen"],
//       x,
//       y
//     )
//   });
// }

function clock_ui(interactive_screen,screen,x,y) {

  // CLOCK INFO BULLE
  interactive_elements.push(function(){
    add_interaction_to_screen(
      interactive_screen,
      art["clock"],
      x,
      y,
      function(){
        info_bulle(screen,
          time.toDateString()+'\n'+
          time.toLocaleTimeString()+'\n'
        );
      }
    )
  });
  

  // CLOCK SPRITE
  display_elements.push(function(){
    let secondes = Math.floor(game_time/1000);
    let minutes = Math.floor(secondes/60);
    let hours = Math.floor(minutes/60);
    let clock_minutes = Math.floor(time.getMinutes()*0.2);
    if (clock_minutes > 20) clock_minutes -=12;
    if (clock_minutes < 9) clock_minutes +=12;
    let hour = time.getHours();
    if (hour > 20) hour -=12;
    if (hour < 9) hour +=12;
    add_to_screen(
      screen,
      art["hour_"+clock_minutes],
      x,
      y
    );
    add_to_screen(
      screen,
      art["hour_short_"+hour],
      x,
      y
    );
    add_to_screen(
      screen,
      art["clock"],
      x,
      y
    );
  }); 
}

function time_ui(interactive_screen,screen,x,y) {
  
  // CLOCK
  clock_ui(interactive_screen,screen,x+2,y+3);
  
  // SPEED TIME BUTTON
  button(
    interactive_screen,
    screen,
    x+3,                      // x position
    y+12,                     // y position
    '[>',                   // button text
    function(){             // interaction with mouse
      info_bulle(screen,
        "Time x 2\n"
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      game_speed = game_speed == 2 ? 1 : 2;
      mouse_active = true;
    },
    function() {            // down sprite condition
      return game_speed == 2;
    }
  );
  
  // SUPER SPEED TIME BUTTON
  let super_speed = 1024*4;
  button(
    interactive_screen,
    screen,
    x+8,                      // x position
    y+12,                     // y position
    '>>',                   // button text
    function(){             // interaction with mouse
      info_bulle(screen,
        "Time x "+super_speed+'\n'
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      game_speed = game_speed == super_speed ? 1 : super_speed;
      mouse_active = true;
    },
    function() {            // down sprite condition
      return game_speed == super_speed;
    }
  );
  
  // STOP TIME BUTTON
  button(
    interactive_screen,                            
    screen,
    x+3,                // x position
    y+10,               // y position
    'll',             // button text
    function(){       // interaction with mouse
      info_bulle(screen,
        "Pause\n"
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      game_speed = game_speed ? 0 : 1;
      mouse_active = true;
    },
    function() {      // down sprite condition
      return !game_speed;
    }
  );
  
  // SLOW TIME BUTTON
  button(
    interactive_screen,
    screen,
    x+8,                      // x position
    y+10,                     // y position
    ":>",                   // button text
    function(){             // interaction with mouse
      info_bulle(screen,
        "Time / 2\n"
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      game_speed = game_speed == 0.5 ? 1 : 0.5;
      mouse_active = true;
    },
    function() {            // down sprite condition
      return game_speed == 0.5;
    }
  );
  display_elements.push(function(){
    add_to_screen(
      screen,
      text_to_screen(ascii_window("Time",17,18)),
      x,
      y,
    );
  });
}

function wind_control_ui(interactive_screen,screen,x,y) {
 
  wind_ui(interactive_screen,screen,x+1,y+3);
 
  // LEFT WIND BUTTON
  button(
    interactive_screen,                            
    screen,
    x+6,                // x position
    y+10,               // y position
    '<-',             // button text
    function(){       // interaction with mouse
      info_bulle(screen,
        "Orient wind to left\n"
      );
      if (!mouse_down) return;
      real_wind_speed -= 0.2;
      mouse_over = "left_wind_button";
    },
    function() {      // down sprite condition
      return mouse_down && mouse_over == "left_wind_button";
    }
  );
  
  // RIGHT WIND BUTTON
  button(
    interactive_screen,                            
    screen,
    x+12,                // x position
    y+10,               // y position
    '->',             // button text
    function(){       // interaction with mouse
      info_bulle(screen,
        "Orient wind to right\n"
      );
      if (!mouse_down) return;
      real_wind_speed += 0.2;
      mouse_over = "right_wind_button";
    },
    function() {      // down sprite condition
      return mouse_down && mouse_over == "right_wind_button";
    }
  );
  
  display_elements.push(function(){
    add_to_screen(
      screen,
      text_to_screen(ascii_window("Wind",23,16)),
      x,
      y,
    );
  });
}

function wind_ui(interactive_screen,screen,x,y) {
  let wind_speed = convert_speed('m/s','km/h',real_wind_speed).toFixed(0);

  // WIND SPRITE 
  let wind_sprite = wind_speed >    0 && wind_speed <= 15 ? "slight_tailwind":
                    wind_speed >   15 && wind_speed <= 30 ? "light_tailwind" :
                    wind_speed >   30 && wind_speed <= 45 ? "medium_tailwind":
                    wind_speed >   45                     ? "heavy_tailwind" :
                    wind_speed <=   0 && wind_speed > -15 ? "slight_headwind":
                    wind_speed <= -15 && wind_speed > -30 ? "light_headwind" :
                    wind_speed <= -30 && wind_speed > -45 ? "medium_headwind":
                    "heavy_headwind";

  // WIND
  display_elements.push(function(){
    add_to_screen(
      screen,
      art[
        wind_sprite
      ],
      x,
      y,
    );
  });

  //// WIND INFO BULLE
  //interactive_elements.push(function(){
  //  add_interaction_to_screen(
  //    interactive_screen,
  //    art[wind_sprite],
  //    x,
  //    y,
  //    function(){
  //      info_bulle(screen,
  //        (real_wind_speed > 0 ?
  //            "tailwind ... "+wind_speed+"km/h\n" :
  //            "headwind ... "+wind_speed+"km/h\n")
  //      );
  //    }
  //  )
  //});
  
}


function gradient_ui(interactive_screen,screen,x,y) {
  // GRADIENT SPRITE 
  let gradient_sprite = slope_gradient == 0                          ? "no_gradient"        :
                        slope_gradient >  0  && slope_gradient <= 2  ? "positive_gradient_0":
                        slope_gradient >  2  && slope_gradient <= 4  ? "positive_gradient_1":
                        slope_gradient >  4  && slope_gradient <= 6  ? "positive_gradient_2":
                        slope_gradient >  6  && slope_gradient <= 8  ? "positive_gradient_3":
                        slope_gradient >  8  && slope_gradient <= 10 ? "positive_gradient_4":
                        slope_gradient >  10 && slope_gradient <= 12 ? "positive_gradient_5":
                        slope_gradient >  12                         ? "positive_gradient_6":
                        slope_gradient <  0  && slope_gradient >=-2  ? "negative_gradient_0":
                        slope_gradient < -2  && slope_gradient >=-4  ? "negative_gradient_1":
                        slope_gradient < -4  && slope_gradient >=-6  ? "negative_gradient_2":
                        slope_gradient < -6  && slope_gradient >=-8  ? "negative_gradient_3":
                        slope_gradient < -8  && slope_gradient >=-10 ? "negative_gradient_4":
                        slope_gradient < -10 && slope_gradient >=-12 ? "negative_gradient_5":
                        "negative_gradient_6";

  // GRADIENT
  display_elements.push(function(){
    add_to_screen(
      screen,
      art[
        gradient_sprite
      ],
      x,
      y,
    );
  });

  //// GRADIENT INFO BULLE
  //interactive_elements.push(function(){
  //  add_interaction_to_screen(
  //    interactive_screen,
  //    art[gradient_sprite],
  //    x,
  //    y,
  //    function(){
  //      info_bulle(screen,
  //        (real_gradient_speed > 0 ?
  //            "tailgradient ... "+gradient_speed+"km/h\n" :
  //            "headgradient ... "+gradient_speed+"km/h\n")
  //      );
  //    }
  //  )
  //});
  
}


  // // GRADIENT UI 
  // msg = text_to_screen(message(
  //   "gradient "+slope_gradient.toFixed(2)+"%\n"
  // ));
  // 
  // slope_art = slope_gradient == 0                          ? "no_gradient"        :
  //             slope_gradient >  0  && slope_gradient <= 2  ? "positive_gradient_0":
  //             slope_gradient >  2  && slope_gradient <= 4  ? "positive_gradient_1":
  //             slope_gradient >  4  && slope_gradient <= 6  ? "positive_gradient_2":
  //             slope_gradient >  6  && slope_gradient <= 8  ? "positive_gradient_3":
  //             slope_gradient >  8  && slope_gradient <= 10 ? "positive_gradient_4":
  //             slope_gradient >  10 && slope_gradient <= 12 ? "positive_gradient_5":
  //             slope_gradient >  12                         ? "positive_gradient_6":
  //             slope_gradient <  0  && slope_gradient >=-2  ? "negative_gradient_0":
  //             slope_gradient < -2  && slope_gradient >=-4  ? "negative_gradient_1":
  //             slope_gradient < -4  && slope_gradient >=-6  ? "negative_gradient_2":
  //             slope_gradient < -6  && slope_gradient >=-8  ? "negative_gradient_3":
  //             slope_gradient < -8  && slope_gradient >=-10 ? "negative_gradient_4":
  //             slope_gradient < -10 && slope_gradient >=-12 ? "negative_gradient_5":
  //             "negative_gradient_6";
  // //add_to_screen(
  // //  screen,
  // //  msg,
  // //  screen_w - msg[0].length,
  // //  screen_h - art[slope_art].length - 2 - 6,
  // //);
  // add_to_screen(
  //   screen,
  //   art[
  //     slope_art
  //   ],
  //   uF(screen_w/15)*7,
  //   0,
  // );

console.log("training.js");
var athlete_position = [
  {
    "label": {
      "fr": "Mains au cintre, bras légèrement fléchis, buste droit",
      "en": "Mains au cintre, bras légèrement fléchis, buste droit"
    },
    "value": 0.43,
  },
  {
    "label": {
      "fr": "Mains aux cocottes, bras fléchis, buste penché vers l'avant",
      "en": "Mains aux cocottes, bras fléchis, buste penché vers l'avant"
    },
    "value": 0.40,
  },
  {
    "label": {
      "fr": "Mains aux cocottes, bras fléchis, buste très penché",
      "en": "Mains aux cocottes, bras fléchis, buste très penché"
    },
    "value": 0.39,
  },
  {
    "label": {
      "fr": "Mains aux creux du cintre, bras très fléchis, buste pratiquement à l'horizontale",
      "en": "Mains aux creux du cintre, bras très fléchis, buste pratiquement à l'horizontale"
    },
    "value": 0.36,
  }
];

var bike_condition = [
  {
    "label": {
      "fr": "Vélo non entretenu",
      "en": "Vélo non entretenu"
    },
    "value": 0.012,
  },
  {
    "label": {
      "fr": "Vélo bien entretenu",
      "en": "Vélo bien entretenu"
    },
    "value": 0.01,
  },
  {
    "label": {
      "fr": "Vélo très bien réglé",
      "en": "Vélo très bien réglé"
    },
    "value": 0.008,
  },
];
var road_condition = [
  {
    "label": {
      "fr": "Ciment lisse",
      "en": "Ciment lisse"
    },
    "value": 0.002,
  },
  {
    "label": {
      "fr": "Billard asphalte",
      "en": "Billard asphalte"
    },
    "value": 0.003,
  },
  {
    "label": {
      "fr": "Route normale",
      "en": "Route normale"
    },
    "value": 0.004,
  },      {
    "label": {
      "fr": "Route dégradée",
      "en": "Route dégradée"
    },
    "value": 0.005,
  }
];
// variables for air power
var air_pressure = 101325; // Pa
var air_temperature = 293.15; // K
var air_molar_mass = 0.028965; // kg/mol
var gas_constant = 8.3144621; // J·K-1·mol-1
var air_density = (air_pressure * air_molar_mass) / (gas_constant * air_temperature); // kg/m3
var air_drag_coef = athlete_position[1].value; // float
var real_wind_speed = 0; // m/s
// variables for friction power and slope power
var road_efficiency_coef = road_condition[2].value; // float
var bike_efficiency_coef = bike_condition[1].value; // float
var g_force = 9.81; // m/s2
var bike_weight = 10; // kg
var athlete_weight = 70; // kg
var cyclist_weight = athlete_weight + bike_weight; // kg
var slope_gradient = 0; // %

// variables for cyclist power
var cyclist_power = 0; // W
var cyclist_ftp = 50;  // W, Functional Threshold Power => puissance max tenue sur 1h
// variables for cyclist training
var cyclist_tss = 0;   // Training Stress Score
var cyclist_ctl = 0;   // Chronic training load => niveau de fitness/progression/entrainement/experience
var cyclist_atl = 0;   // Acute training load => fatigue actuelle
var cyclist_tsb = 0;   // Training Stress Balance  => niveau de forme/fraicheur

function calc_power() {
  // POWER

  // sens du vent, si vent favorable wind_assistance = -1
  var wind_assistance = (real_wind_speed >= 0) ? -1 : 1;
  var air_power = 0.5 * air_density * air_drag_coef * Math.pow(speed + (wind_assistance * Math.abs(real_wind_speed)),2) * speed;
  var road_friction_power = road_efficiency_coef * cyclist_weight * g_force * speed;
  var bike_friction_power = bike_efficiency_coef * cyclist_weight * g_force * speed;
  var friction_power = bike_friction_power + road_friction_power;
  var slope_power = speed * cyclist_weight * g_force * slope_gradient / 100;
  cyclist_power = air_power + friction_power + slope_power;
  // power à 0 si on est dans une descente par ex.
  cyclist_power = (cyclist_power < 0) ? cyclist_power = 0 : cyclist_power;
}

function training() {

  // TRAINING
  cyclist_tss = Math.pow((cyclist_power/cyclist_ftp),2) * (game_time/3600000) * 100;
  // console.log(cyclist_tss);
}

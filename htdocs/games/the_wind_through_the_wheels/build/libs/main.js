console.log("main.js");
function main(current_frame){

  let time_elapsed = process_time();

  if (!time_elapsed) return;
  
    if (cyclist_power.toFixed(0) > wanted_power   
    ||  cyclist_power.toFixed(0) > max_power) {
      while (cyclist_power.toFixed(0) > wanted_power || cyclist_power > max_power) {
        speed -= 1/100;
        calc_power();
      }
    } else {
      while (cyclist_power.toFixed(0) < wanted_power) { 
        speed += 1/100;
        calc_power();
      }
    }
 
  let new_step = step(time_elapsed);

  new_step = next_checkpoint(time_elapsed,new_step);
  distance += new_step;
  altitude += new_step * (slope_gradient/100);
  checkpoint_stop(new_step);
 
  sky_step(time_elapsed);

  calc_power();
  training();

  let game_screen = screen(screen_w,screen_h,filler);
  interactive_screen = screen(screen_w,screen_h,false);
  
  display(
    game_screen,
    current_frame
  );

  // LOG 
  scene.textContent = screen_to_text(game_screen);

  soundtrack(current_frame);
}

function step(time_elapsed) {
  let step = speed*(time_elapsed/1000)*(game_pause ? 0 : game_speed);
  //if (distance + step > trail_length - 1000) {
  //  step = (trail_length - 1000) - distance;
  //  game_speed = 0;
  //  speed = 0;
  //}
  return step;
}

function process_time() {
  let current_time = Date.now() - init_time;
  let time_elapsed = current_time - last_time;
  last_time = current_time;
  
  if (time_elapsed < 0) return false;

  game_time += time_elapsed *(game_pause ? 0 : game_speed);
  time = new Date(game_date.getTime() + game_time);
  
  return time_elapsed;
}


function menu(
  menu_elements,
  interactive_screen,
  screen,
  x,
  y,
  w,
  txt_list,
  solid_style,
  interaction
) {
  let previous_menu_elements = [];
  for (var i = 0; i < menu_elements.length; i++) {
    previous_menu_elements.push(menu_elements[i]);
  }
  while (menu_elements.length) {
    menu_elements.pop();
  }
  for (var i = 0; i < txt_list.length; i++) {
    menu_element(
      interactive_screen,
      screen,
      menu_elements,
      x,
      y+i*(solid_style ? 2 : 1)+txt_list[i][1],
      w,
      txt_list[i][0],
      solid_style ? 
        txt_list.length == 1 ? 1 : i == 0 ? 2 : i == txt_list.length-1 ? 4 : 3
      : 0,
      interaction
    )
  }
  for (var i = 0; i < previous_menu_elements.length; i++) {
    menu_elements[i] = previous_menu_elements[i] == 2 ? 2 : 0;
  }
}

function menu_element(
  interactive_screen,
  screen,
  menu_elements,
  x,
  y,
  w,
  txt,
  style,
  interaction
){
  let up = text_to_screen(menu_elements_sprite(txt,0,w,style));
  let over = text_to_screen(menu_elements_sprite(txt,1,w,style));
  let down = text_to_screen(menu_elements_sprite(txt,2,w,style));
  let id = menu_elements.length;
  menu_elements.push(0);
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        menu_elements[id] == 0 ? up   : 
        menu_elements[id] == 1 ? over : 
        down,
        x,
        y,
        function() {
          menu_elements[id] = menu_elements[id] == 2 ? 2 : 1;
          if (menu_elements[id] == 2) {
            for (var i = 0; i < menu_elements.length; i++) {
              if (i==id) continue;
              menu_elements[i] = 0;
            }
          }
          if (mouse_down && !mouse_active) {
            menu_elements[id] = menu_elements[id] == 2 ? 1 : 2;
            mouse_active = true;
            interaction(id,menu_elements);
          }
          return true;
        }
      );
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        menu_elements[id] == 0 ? up   : 
        menu_elements[id] == 1 ? over : 
        down,
        x,
        y
      );
    }
  );
}

function menu_elements_sprite(txt,state,w,style) {
  let content = state == 0 ? (txt.length ? '- ' : ' ') + txt :
                state == 1 ? (txt.length ? '> ' : ' ') + txt :
                (txt.length ? 'X ' : ' ') + txt ;
  if (!style) {
    content = '| '+content
      while(content.length < w) {
        content += ' ';
      }
    return content+' |';
  } else {
    let line_0 = get_style_corner_char(0,style) + "-";
    while(line_0.length < w) {
      line_0 += '-';
    }
    line_0 += '-'+get_style_corner_char(0,style)+'\n';
    let line_1 = "| "+content+' ';
    while(line_1.length < w) {
      line_1 += ':';
    }
    line_1 += " |\n";
    let line_2 = get_style_corner_char(1,style) + "-";
    while(line_2.length < w) {
      line_2 += '-';
    }
    line_2 += '-'+get_style_corner_char(1,style)+'\n';
    return line_0+line_1+line_2;
  }
}

function get_style_corner_char(corner,style) {
  return corner == 0 ? 
           style == 1 ? '.'  :
           style == 2 ? '.'  :
           style == 3 ? '|'  :
           '|'
         : 
           style == 1 ? '\'' :
           style == 2 ? '|'  :
           style == 3 ? '|'  :
           '\'';
}

function close_tools_but(exept) {
  time_tool_open = exept != "time" ? false : time_tool_open;
  wind_tool_open = exept != "wind" ? false : wind_tool_open;
}
  
function toolbox(interactive_screen,screen){
  button(
    interactive_screen,
    screen,
    0,                      // x position
    0,                     // y position
    'Toolbox',                   // button text
    function(){             // interaction with mouse
      info_bulle(screen,
        toolbox_open ? "Close Toolbox\n" : "Open Toolbox\n"
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      close_tools_but(false);
      toolbox_open = !toolbox_open;
      mouse_active = true;
    },
    function() {            // down sprite condition
      return toolbox_open;
    }
  );

  if (toolbox_open) {

    // WIND TOOL
    if (wind_tool) {
      button(
        interactive_screen,
        screen,
        2,                      // x position
        6,                     // y position
        'Wind',                   // button text
        function(){             // interaction with mouse
          info_bulle(screen,
            wind_tool_open ? "Close wind window\n" : "Open wind window\n"
          );
          if (!mouse_down) return;
          if (mouse_active) return;
          close_tools_but("wind");
          wind_tool_open = !wind_tool_open;
          mouse_active = true;
        },
        function() {            // down sprite condition
          return wind_tool_open;
        }
      );
    }
    if (wind_tool_open) {
      wind_control_ui(interactive_screen,screen,uF(screen_w/2)-12,uF(screen_h/2)-8);
    }

    // TIME TOOL
    if (time_tool) {
      // BUTTONS
      button(
        interactive_screen,
        screen,
        2,                      // x position
        4,                     // y position
        'Time',                   // button text
        function(){             // interaction with mouse
          info_bulle(screen,
            time_tool_open ? "Close time window\n" : "Open time window\n"
          );
          if (!mouse_down) return;
          if (mouse_active) return;
          close_tools_but("time");
          time_tool_open = !time_tool_open;
          mouse_active = true;
        },
        function() {            // down sprite condition
          return time_tool_open;
        }
      );
    }
    display_elements.push(function(){
      add_to_screen(
        screen,
        text_to_screen(ascii_window(false,20,20)),
        0,
        3,
      );
    });
    if (time_tool_open) {
      time_ui(interactive_screen,screen,uF(screen_w/2)-9,uF(screen_h/2)-9);
    }
  }
}

// POWER TOOL

var power_tool_range = [];

function power_tool(
  interactive_screen,
  screen
) {
  let power_window = text_to_screen(ascii_window("Power",26,9));
  let x = uF(screen_w/2 - power_window[0].length/2);
  let y = uF(screen_h/2 - power_window.length/2);
  let text = wanted_power+" W";
  while (text.length < 7) {
    text = ' '+text;
  }
  digital_text(screen,text+' ',x+2,y+3);
  range(
    power_tool_range,
    0,wanted_power,max_power,
    interactive_screen,
    screen,
    x+3,
    y+8,
    23,
    function (id,range_elements) {
      wanted_power = uF((id-1) * (max_power / (range_elements.length-3)));
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        power_window,
        x,y
      );
    }
  );
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        power_window,
        x,y,
        function(){ return false ;}
      );
    }
  );
}

// TIME TOOL

var time_tool_range = [];

function time_tool(
  interactive_screen,
  screen
) {
  let time_window = text_to_screen(ascii_window("Time",26,16));
  let x = uF(screen_w/2 - time_window[0].length/2);
  let y = uF(screen_h/2 - time_window.length/2);
  let text = 'X ' + (game_speed < 10 ? game_speed.toFixed(1) : uF(game_speed));
  while (text.length < 7) {
    text = ' '+text;
  }
  clock_ui(interactive_screen,screen,x+2,y+3);
  button(
    interactive_screen,
    screen,
    x+16,                      // x position
    y+5,                     // y position
    "Pause ll",                   // button text
    function(){             // interaction with mouse
      info_bulle(screen,
        "(P)\n"
      );
      if (!mouse_down) return;
      if (mouse_active) return;
      mouse_active = true;
      game_pause = !game_pause;
    },
    function() {            // down sprite condition
      return (game_pause);
    }
  );
  digital_text(screen,text+' ',x+2,y+10);
  range(
    time_tool_range,
    1,game_speed,max_game_speed,
    interactive_screen,
    screen,
    x+3,
    y+15,
    23,
    function (id,range_elements) {
      game_speed = 1+(id-1) * ((max_game_speed-1) / (range_elements.length-3));
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        time_window,
        x,y
      );
    }
  );
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        time_window,
        x,y,
        function(){ return false ;}
      );
    }
  );
}

// WIND TOOL

var wind_tool_range = [];

function wind_tool(
  interactive_screen,
  screen
) {
  let wind_window = text_to_screen(ascii_window("Wind",26,16));
  let x = uF(screen_w/2 - wind_window[0].length/2);
  let y = uF(screen_h/2 - wind_window.length/2);
  wind_ui(interactive_screen,screen,x+4,y+3);
  let wind_speed = uF(convert_speed('m/s','km/h',real_wind_speed));
  let text = wind_speed > 0 ? '+' + wind_speed : String(wind_speed);
  text += "km/h";
  while (text.length < 7) {
    text = ' '+text;
  }
  digital_text(screen,text+' ',x+2,y+10);
  range(
    wind_tool_range,
    min_wind,real_wind_speed,max_wind,
    interactive_screen,
    screen,
    x+3,
    y+15,
    23,
    function (id,range_elements) {
      real_wind_speed = convert_speed('km/h','m/s',
        ((id-1) / ((range_elements.length-3)/(max_wind-min_wind)))+min_wind
      );
      //game_speed = 1+(id-1) * ((max_game_speed-1) / (range_elements.length-3));
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        wind_window,
        x,y
      );
    }
  );
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        wind_window,
        x,y,
        function(){ return false ;}
      );
    }
  );
}

// GRADIENT TOOL

var gradient_tool_range = [];

function gradient_tool(
  interactive_screen,
  screen
) {
  let gradient_window = text_to_screen(ascii_window("Gradient",26,16));
  let x = uF(screen_w/2 - gradient_window[0].length/2);
  let y = uF(screen_h/2 - gradient_window.length/2);
  gradient_ui(interactive_screen,screen,x+8,y+3);
  let text = (slope_gradient > 0 ? '+' + slope_gradient : String(slope_gradient))+'%';
  while (text.length < 7) {
    text = ' '+text;
  }
  digital_text(screen,text+' ',x+2,y+10);
  range(
    gradient_tool_range,
    min_gradient,slope_gradient,max_gradient,
    interactive_screen,
    screen,
    x+3,
    y+15,
    23,
    function (id,range_elements) {
      slope_gradient = ((id-1) / ((range_elements.length-3)/(max_gradient-min_gradient)))+min_gradient;
      //game_speed = 1+(id-1) * ((max_game_speed-1) / (range_elements.length-3));
    }
  );
  display_elements.push(
    function () {
      add_to_screen(
        screen,
        gradient_window,
        x,y
      );
    }
  );
  interactive_elements.push(
    function () {
      add_interaction_to_screen(
        interactive_screen,
        gradient_window,
        x,y,
        function(){ return false ;}
      );
    }
  );
}

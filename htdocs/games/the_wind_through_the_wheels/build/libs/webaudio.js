console.log("webaudio.js");
// Audio Context
var ctx;

let C  = [16.35, 32.7, 65.41, 130.81, 261.63, 523.25, 1046.5, 2093.0, 4186.01];
let Db = [17.32, 34.65, 69.3, 138.59, 277.18, 554.37, 1108.73, 2217.46, 4434.92];
let D  = [18.35, 36.71, 73.42, 146.83, 293.66, 587.33, 1174.66, 2349.32, 4698.64];
let Eb = [19.45, 38.89, 77.78, 155.56, 311.13, 622.25, 1244.51, 2489.02, 4978.03];
let E  = [20.6, 41.2, 82.41, 164.81, 329.63, 659.26, 1318.51, 2637.02];
let F  = [21.83, 43.65, 87.31, 174.61, 349.23, 698.46, 1396.91, 2793.83];
let Gb = [23.12, 46.25, 92.5, 185.0, 369.99, 739.99, 1479.98, 2959.96];
let G  = [24.5, 49.0, 98.0, 196.0, 392.0, 783.99, 1567.98, 3135.96];
let Ab = [25.96, 51.91, 103.83, 207.65, 415.3, 830.61, 1661.22, 3322.44];
let A  = [27.5, 55.0, 110.0, 220.0, 440.0, 880.0, 1760.0, 3520.0];
let Bb = [29.14, 58.27, 116.54, 233.08, 466.16, 932.33, 1864.66, 3729.31];
let B  = [30.87, 61.74, 123.47, 246.94, 493.88, 987.77, 1975.53, 3951.07];

function play(note,gain,in_dur,mid_dur,out_dur,res,noise,flat,dist) {
  if (!ctx) ctx = new(window.AudioContext || window.webkitAudioContext)();
  if(!gain  || !note || (!in_dur && !mid_dur && !out_dur)) return;
  if(!res   || res   < 0) res = 10000;
  if(!noise || noise < 0) noise = 0;
  if(!flat  || flat  < 0) flat = 1;
  if(!dist  || dist  < 0) dist = 1;

  /* WARNING NON GENERIC */
  
  var tuner = game_speed < 4 ? game_speed : 4;
  tuner = tuner < 1 ? 1 : tuner;
 
  gain = tuner > 1 ? gain / tuner : gain;
  note = note * tuner;
  in_dur = in_dur / tuner ;
  mid_dur = mid_dur / tuner;
  out_dur = out_dur / tuner;

  /* END WARNING */

  // Gain
  const gainNode = ctx.createGain();
  gainNode.gain.setValueAtTime(in_dur ? 0 : gain, ctx.currentTime);
  if (in_dur) gainNode.gain.linearRampToValueAtTime(gain, ctx.currentTime + in_dur);
  if (mid_dur) gainNode.gain.linearRampToValueAtTime(gain, ctx.currentTime + in_dur + mid_dur);
  if (out_dur) gainNode.gain.linearRampToValueAtTime(0, ctx.currentTime + in_dur + mid_dur + out_dur);

  // Oscillator
  const oscNode = noise_oscillator(ctx,note,in_dur+mid_dur+out_dur,res,noise,flat,dist);

  // Play
  oscNode.connect(gainNode);
  gainNode.connect(ctx.destination);
  oscNode.start(ctx.currentTime);
  oscNode.stop(ctx.currentTime+in_dur+mid_dur+out_dur);
}

let buffers = {};

function noise_oscillator(ctx, note, dur, res, noise, flat, dist){
  let pattern = [];
  let i = 0;
  let buffer_key = 
    String(Math.floor(note *1   ))+'.'+
    String(Math.floor(dur  *100 ))+'.'+
    String(Math.floor(res  *1   ))+'.'+
    String(Math.floor(noise*100 ))+'.'+
    String(Math.floor(flat *1   ))+'.'+
    String(Math.floor(dist *10  ));
  var buffer;
  if (buffers[buffer_key]) {
    buffer = buffers[buffer_key];
  } else {
    while (i < ctx.sampleRate/note) {
      let rdm_val = Math.random() - Math.random();
      let val = (Math.sin(2*Math.PI*note * (i/ctx.sampleRate)) + rdm_val * noise) * dist;
      val = Math.floor(val * res)/res;
      for (var r = 0; r < flat; r++) {
        pattern.push(val > 1 ? 1 : val < -1 ? -1 : val);
        i++;
      }
    }
    buffer = ctx.createBuffer(1, ctx.sampleRate*dur, ctx.sampleRate);
    for (let ch=0; ch<buffer.numberOfChannels; ch++) {
      let samples = buffer.getChannelData(ch);
      for (let s=0; s<buffer.length; s++) {
        samples[s] = pattern[s > pattern.length-1 ? s - Math.floor(s/pattern.length) * pattern.length : s ];
      }
    }
    buffers[buffer_key] = buffer;
  }
  return new AudioBufferSourceNode(ctx, {buffer:buffer})
}

// SOUNDTRACK


let distance_played = 0;

function soundtrack(current_frame) {
  let d = Math.floor(true_distance());
  
  // RAIN
  if (Number.isInteger(current_frame/Math.floor(Math.random()*128))) {
    play(1,0.015,1,0,rain_density/12,0,rain_density/6,Math.random()*rain_density*10,1);
  }
  
  // MUSIC
  track_A(d,current_frame);
  distance_played = d > distance_played ? d : distance_played;
}

//function notes(gamme,g_index,step) {
//  return [
//    gamme[cycle(g_index,gamme)][3],
//    gamme[cycle(g_index+step,gamme)][3],
//    gamme[cycle(g_index+step*2,gamme)][3]
//  ];
//}

function tab(gamme,length,step,start) {
  let tab = [];
  for (var i = 0; i < length; i++) {
    tab.push(gamme[cycle(i*step+start,gamme)]);
  }
  return tab;
}


function track_A(d,current_frame){
  if (d <= distance_played) return;
  var solo = blink(3*2*5*8,distance_played);
  var solo_b = blink(3*2*5*4,distance_played);
  var solo_c = blink(3*2*5*12,distance_played);
  var long_note = ( every(3*2*5*12,uF(distance_played))
                 || every(3*2*5*8 ,uF(distance_played)) 
                 || every(3*2*5*4 ,uF(distance_played)) 
                 ||(0  == uF(distance_played)));
  var gamme = 
    blink(3*2*5*4,distance_played) ? 
      [
        blink(3*2*5*16,distance_played) ? C : D,
        blink(3*2*5*8,distance_played) ? E : F,
        G,
        blink(3*2*5*8,distance_played) ? B : A
      ] : 
      [
        blink(3*2*5*8,distance_played) ? E : F,
        G,
        blink(3*2*5*8,distance_played) ? B : A,
        blink(3*2*5*16,distance_played) ? D : C,
      ];
  var step = 2;
  octave = 2;
  if (every(step,distance_played)) {
    let track_tab = tab(gamme,3,1,0);
    let track_tab_b = tab([1,1.5,2],2,1,0);
    let track_tab_c = tab([1,1.5,2,3,4],5,1,0);
    var n = track_tab[cycle(uF(d/step),track_tab)][octave]*track_tab_b[cycle(uF(d/step),track_tab_b)]*track_tab_c[cycle(uF(d/step),track_tab_c)];
    if (long_note) {
      play( 
        /* note  */  n*4,
        /* gain  */  0.03/12,
        /* in    */  2,
        /* mid   */  2,
        /* end   */  5,
        /* res   */  0,
        /* noise */  0.02,
        /* flat  */  1,
        /* dist  */  3,
      );
      play( 
        /* note  */  n*3,
        /* gain  */  0.03/10,
        /* in    */  4,
        /* mid   */  4,
        /* end   */  10,
        /* res   */  0,
        /* noise */  0.01,
        /* flat  */  2,
        /* dist  */  3,
      );
      play( 
        /* note  */  n*2,
        /* gain  */  0.03/8,
        /* in    */  6,
        /* mid   */  6,
        /* end   */  15,
        /* res   */  0,
        /* noise */  0.03,
        /* flat  */  3,
        /* dist  */  3,
      );
    }
    if (solo_c) {
      play( 
        /* note  */  n,
        /* gain  */  (0.04/3/(speed/20)) > 0.04/4 ? 0.04/4 : (0.04/3/(speed/20)) ,
        /* in    */  0.02/(speed/4),
        /* mid   */  0,
        /* end   */  0.4/(speed/4),
        /* res   */  0,
        /* noise */  0,
        /* flat  */  0,
        /* dist  */  3,
      );
    }
    if (!solo) {
      n = track_tab[cycle(uF(d/step),track_tab)][octave]*track_tab_b[cycle(uF(d/step),track_tab_b)];
      play( 
        /* note  */  n*2,
        /* gain  */  0.03/3,
        /* in    */  0.01,
        /* mid   */  0,
        /* end   */  0.5*(speed/20),
        /* res   */  20,
        /* noise */  0,
        /* flat  */  0,
        /* dist  */  2,
      );
    }
    if (!solo_b || (!solo_c && solo)) {
      n = track_tab[cycle(uF(d/step),track_tab)][octave];
      play( 
        /* note  */  n,
        /* gain  */  0.03/4,
        /* in    */  0.03,
        /* mid   */  0.1/(speed/10),
        /* end   */  0.1/(speed/10),
        /* res   */  30,
        /* noise */  0,
        /* flat  */  3,
        /* dist  */  4,
      );
      play( 
        /* note  */  n/2,
        /* gain  */  0.03/3,
        /* in    */  0.04,
        /* mid   */  0.1/(speed/10),
        /* end   */  0.1/(speed/10),
        /* res   */  30,
        /* noise */  0,
        /* flat  */  3,
        /* dist  */  4,
      );
    }
  }
}

//function track_0(d,local_frame_index){
//  let gamme = [C,D,E,G,A];
//  let note_steps = [1,2,3,4]
//  if (!(d > last_distance && game_speed < 3 || game_speed > 3 && Number.isInteger(local_frame_index/4))) return;
//  let n2 = notes(gamme,Math.floor(d/128),note_steps[cycle(Math.floor(d/512),note_steps)]);
//  let n = notes(gamme,Math.floor(d/64),note_steps[cycle(Math.floor(d/256),note_steps)]);
//  let n1 = notes(gamme,Math.floor(d/32),note_steps[cycle(Math.floor(d/128),note_steps)]);
//  let ni = cycle(d,n);
//  play(n2[ni]   ,   0.025,      0.05/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3),  0.35/(game_speed < 3 ? game_speed : 3), 32, 0, 0, 1);
//  play(n[ni]/2  ,    0.05,      0.02/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3), 0.175/(game_speed < 3 ? game_speed : 3),  8, 0, 2, 1.2);
//  play(n1[ni]*2 ,  0.005,       0.04/(game_speed < 3 ? game_speed : 3), 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3),  2, 0, 1, 1.3);
//}
//
//function track_0b(d,local_frame_index){
//  let gamme = [C,D,E,G,A];
//  let note_steps = [1,3,5]
//  if (!(d > last_distance && game_speed < 3 || game_speed > 3 && Number.isInteger(local_frame_index/4))) return;
//  let n2 = notes(gamme,Math.floor(d/128),note_steps[cycle(Math.floor(d/512),note_steps)]);
//  let n = notes(gamme,Math.floor(d/64),note_steps[cycle(Math.floor(d/256),note_steps)]);
//  let n1 = notes(gamme,Math.floor(d/32),note_steps[cycle(Math.floor(d/128),note_steps)]);
//  let ni = cycle(d,n);
//  play(n2[ni]   ,   0.025,      0.05/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3),  0.35/(game_speed < 3 ? game_speed : 3), 32, 0, 0, 1);
//  play(n[ni]/2  ,    0.05,      0.02/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3), 0.175/(game_speed < 3 ? game_speed : 3),  8, 0, 2, 1.2);
//  play(n1[ni]*2 ,  0.005,       0.04/(game_speed < 3 ? game_speed : 3), 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3),  2, 0, 1, 1.3);
//}
//
//function track_1(d,local_frame_index){
//  let gamme = [C,E,G,B,D];
//  let note_steps = [1,2,3,4];
//  if (!(d > last_distance && game_speed < 3 || game_speed > 3 && Number.isInteger(local_frame_index/4))) return;
//  let n2 = notes(gamme,Math.floor(d/128),note_steps[cycle(Math.floor(d/512),note_steps)]);
//  let n = notes(gamme,Math.floor(d/64),note_steps[cycle(Math.floor(d/256),note_steps)]);
//  let n1 = notes(gamme,Math.floor(d/32),note_steps[cycle(Math.floor(d/128),note_steps)]);
//  let ni = cycle(d,n);
//  play(n2[ni]   ,   0.025,      0.05/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3),  0.35/(game_speed < 3 ? game_speed : 3), 32, 0, 0, 1);
//  play(n[ni]/2  ,    0.05,      0.02/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3), 0.175/(game_speed < 3 ? game_speed : 3),  8, 0, 2, 1.2);
//  play(n1[ni]*2 ,  0.005,       0.04/(game_speed < 3 ? game_speed : 3), 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3),  2, 0, 1, 1.3);
//}
//
//function track_2(d,local_frame_index) {
//  let gamme = [D,F,A,C,E];
//  let note_steps = [1,2,3,4];
//  if (!(d > last_distance && game_speed < 3 || game_speed > 3 && Number.isInteger(local_frame_index/4))) return;
//  let n2 = notes(gamme,Math.floor(d/128),note_steps[cycle(Math.floor(d/512),note_steps)]);
//  let n = notes(gamme,Math.floor(d/64),note_steps[cycle(Math.floor(d/256),note_steps)]);
//  let n1 = notes(gamme,Math.floor(d/32),note_steps[cycle(Math.floor(d/128),note_steps)]);
//  let ni = cycle(d,n);
//  play(n2[ni]*2 ,   0.03,      0.05/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3)*2,  0.35/(game_speed < 3 ? game_speed : 3), 32, 0, 0, 1);
//  play(n[ni]    ,    0.06,      0.02/(game_speed < 3 ? game_speed : 3),    0/(game_speed < 3 ? game_speed : 3)*2, 0.175/(game_speed < 3 ? game_speed : 3),  8, 0, 2, 1.2);
//  play(n1[ni]/3 ,  0.0008,       0.04/(game_speed < 3 ? game_speed : 3), 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3)*2,  64, 4, 20, 1);
//  play(n1[ni]/4 ,  0.0015,       0.04/(game_speed < 3 ? game_speed : 3), 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3)*2,  64, 4, 20, 1);
//  play(n1[ni]/1.5 ,  0.027,       0.04/(game_speed < 3 ? game_speed : 3)*2, 0.25/(game_speed < 3 ? game_speed : 3),  0.05/(game_speed < 3 ? game_speed : 3),  0, 0, 0, 2);
//}

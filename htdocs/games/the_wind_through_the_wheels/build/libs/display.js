console.log("display.js");
function sign(txt) {
  let sign_up = ".-";
  for (var i = 0; i < txt.length; i++) {
    sign_up+='-';
  }
  sign_up+="-.";
  let sign_mid = "| "+txt+" |";
  let sign_down = "`n";
  for (var i = 0; i < txt.length; i++) {
    sign_down+='-';
  }
  sign_down+="n'";
  let sign_feet = "█║";
  for (var i = 0; i < txt.length; i++) {
    sign_feet+='█';
  }
  sign_feet += "║█";
  return sign_up+'\n'+sign_mid+'\n'+sign_down+'\n'+sign_feet;
}


// SKY

function sky_step(time_elapsed){
  let sky_step = (real_wind_speed*-1)*(time_elapsed/1000)*(game_pause ? 0 : game_speed);
  if (sky_distance + sky_step > trail_length - 1000
  || sky_distance + sky_step < offset_distance) {
    sky_distance = trail_length/2;
  } 
  sky_distance += sky_step;
}


// Circle

function draw_circle(centerX,centerY,radius,steps) {
  let points = [];
  for (var i = 0; i < steps; i++) {
    points.push([
      centerX + radius * Math.cos(2 * Math.PI * i / steps),
      centerY + radius * Math.sin(2 * Math.PI * i / steps)
    ]);
  }
  return points;
}


function x_el(layer,sprite,unit,value){
  let center = Math.floor(screen_w/2-art[sprite][0].length/2);
  return unit == 'm'  ? center+value*      char_per_m*2/(layer+1) :
         unit == 'km' ? center+value*1000*char_per_m*2/(layer+1)  :
         "invalid unit";
}

function get_chunk(layer,value) {
  return chunk = Math.floor(value/chunk_size/(layer+1));
}

function y_el(layer,sprite) {
  let sky = sprite.includes("sky");
  return sky ? 
          ((art[sprite].length/2) * layer)-art[sprite].length/2 :
          screen_h-(layer == 0 ? 0 : 6)-(sprite.includes("ground") ? (layer == 0 ? 1 : 1) : art[sprite].length);
}

function place_sky_element(layer,sprite,x_unit,x_value) {
  while (sky_layers.length-1 < layer) {
    sky_layers.push([]);
  }
  let chunk = get_chunk(layer,x_value);
  while (sky_layers[layer].length-1 < chunk) {
    sky_layers[layer].push([]);
  }
  let pos = Math.floor(x_el(layer,sprite,x_unit ,x_value));
  sky_layers[layer][chunk].push([
    sprite,
    pos,
    y_el(layer,sprite),
    art[sprite][0].length,
    art[sprite].length,
  ]);
}
function place_element(layer,sprite,x_unit,x_value) {
  if (sprite.includes("sky")) {
    place_sky_element(layer,sprite,x_unit,x_value);
    return;
  }
  while (layers.length-1 < layer) {
    layers.push([]);
  }
  let chunk = get_chunk(layer,x_value);
  while (layers[layer].length-1 < chunk) {
    layers[layer].push([]);
  }
  let pos = Math.floor(x_el(layer,sprite,x_unit ,x_value));
  layers[layer][chunk].push([
    sprite,
    pos,
    y_el(layer,sprite),
    art[sprite][0].length,
    art[sprite].length,
  ]);
}

function get_hour_color(color,hour) {
  let r = color[0]/(12*60)*(hour>12*60 ? 12*60-(hour-12*60) : hour)*(hour > 6*60 && hour < 19*60 ? 1.75 : hour > 21*60 || hour < 6*60 ? 0.25 : 1);
  let v = color[1]/(12*60)*(hour>12*60 ? 12*60-(hour-12*60) : hour)*(hour > 6*60 && hour < 19*60 ? 1.75 : hour > 21*60 || hour < 6*60 ? 0.25 : 1);
  let b = color[2]/(12*60)*(hour>12*60 ? 12*60-(hour-12*60) : hour)*(hour > 6*60 && hour < 19*60 ? 1.75 : hour > 21*60 || hour < 6*60 ? 0.25 : 1);
  return [
    [r,v,b],
    [
      hour > 6*60 && hour < 19*60 ? r/4 : hour < 6*60 || hour > 21*60 ? 255 - r*8 : 255 - r/2,
      hour > 6*60 && hour < 19*60 ? v/4 : hour < 6*60 || hour > 21*60 ? 255 - v*8 : 255 - v/2,
      hour > 6*60 && hour < 19*60 ? b/4 : hour < 6*60 || hour > 21*60 ? 255 - b*8 : 255 - b/2
    ]
  ];
}

let sky_distance = 0;

function place_sky(){
  sky_layers = [];
  for (var i = 0; i < trail_length; i++) {
    for (var j = 1; j < screen_h/2; j++) {
      if (Math.random() > 0.5) {
        if (Math.random() > 0.5) {
          if (Number.isInteger(i/(j)) && Math.random() < cloud_chance) {
          place_element(j-1,
          "sky_cloud_0",
          'm',distance+i);
          }
        } else {
          if (Number.isInteger(i/(j*2)) && Math.random() < cloud_chance) {
          place_element(j-1,
          "sky_cloud_1",
          'm',distance+i);
          }
        }
      } else {
        if (Math.random() > 0.5) {
          if (Number.isInteger(i/(j*4)) && Math.random() < cloud_chance) {
          place_element(j-1,
          "sky_cloud_2",
          'm',distance+i);
          }
        } else {
          if (Number.isInteger(i/(j*8)) && Math.random() < cloud_chance) {
          place_element(j-1,
          "sky_cloud_3",
          'm',distance+i);
          }
        }
      }
    }
  }
}

function init_display() {
  place_sky();
  for (var i = 0; i < trail_length; i++) {
    if (Number.isInteger(i/10)) {
      place_element(1,"planks_road_ground",'m',distance+i);
    }
    if (Number.isInteger(i/10)) {
        place_element(0,
        "ground",
        'm',(distance+i)/1.5);
        //place_element(2,
        //"planks_gro//und",
        //'m',(distan//ce+i)*(2/1.5));
    }                //
    if (Number.isInteger(i/2)) {
        //place_element(0,
        //"ground",
        //'m',(distance+i)/1.5);
      for (var j = 2; j < 10; j ++) {
        place_element(j,
        "ground",
        'm',(distance+i)*(j/1.5));
      }
    }
    //for (var j = 0; j < 6; j ++) {
    //  place_element(j,
    //  "rain_0",
    //  'm',distance+i);
    //}
    

    if (every(uFR(8,64),i)) {
      place_element(0,
      "fg_tree_"+(uR(0,1) > 0.5 ? '1' : '0'),
      'm',distance+i);
    }
    //if (every(26,i+3)) {
    //  place_element(2,
    //  "pdx_med_col",
    //  'm',distance+i);
    //}
    //if (every(13,i)) {
    //  place_element(2,
    //  "wall",
    //  'm',distance+i);
    //}
    //if (every(64,i)) {
    //  place_element(1,
    //  "pdx_diplo_skel",
    //  'm',distance+i);
    //}
    //if (every(64,i+32)) {
    //  place_element(1,
    //  "pdx_tricer_skel",
    //  'm',distance+i);
    //}
    if (every(uFR(8,16),i)) {
      place_element(2,
      "g_tree_"+(uR(0,1) > 0.5 ? '1' : '0'),
      'm',distance+i);
    }
    if (every(uFR(8,8),i)) {
      place_element(3,
      "bg_tree_"+(uR(0,1) > 0.5 ? '1' : '0'),
      'm',distance+i);
    }
    if (every(uFR(8,4),i)) {
      place_element(4,
      "rg_tree_"+(uR(0,1) > 0.5 ? '1' : '0'),
      'm',distance+i);
    }
    if (every(uFR(8,2),i)) {
      place_element(5,
      "frg_tree_"+(uR(0,1) > 0.5 ? '1' : '0'),
      'm',distance+i);
    }
    if (every(8*16,i)) {
      place_element(16,
      "frg_hill_2",
      'm',distance+i);
    }
    if (every(8*64,i)) {
      place_element(32,
      "frg_hill_1",
      'm',distance+i);
    }
    if (every(8*250,i)) {
      place_element(64,
      "frg_hill_0",
      'm',distance+i);
    }
    if (every(8*500,i)) {
      place_element(128,
      "frg_mountain_0",
      'm',distance+i);
    }
  }
  distance = offset_distance;
  sky_distance = offset_distance;


}

let in_render = 0;
let total_in_render = 0;
let readable_fps = 1;
let scene = document.getElementById("scene");
let info = document.getElementById("info");
let game_hour = -1;
var clean_screen = screen(screen_w,screen_h,' ');
var astral_circle = draw_circle(screen_w/2,screen_h/2,screen_h/2,24*60);
var interactive_screen = screen(screen_w,screen_h,false);

function display(game_screen,frame) {

  if (Number.isInteger(frame/readable_fps)) readable_fps = animation_fps;

  //add_to_screen(
  //  game_screen,
  //  art["title"],
  //  uF(screen_w/2-art["title"][0].length/2),
  //  uF(screen_h/2-art["title"].length/2)
  //);
  
  // UI
  ui(game_screen,frame);

  // FOREGROUND
  let fg_chunk = get_chunk(0,distance);
  print_fg_elements(game_screen,0,fg_chunk-1,frame);
  print_fg_elements(game_screen,0,fg_chunk,frame);
  print_fg_elements(game_screen,0,fg_chunk+1,frame);
  
  // CYCLIST
  add_to_screen(
    game_screen,
    art[cyclist(frame)],
    Math.floor(screen_w/2-1),
    screen_h-5,
  );
  add_to_screen(
    game_screen,
    art[bike()],
    Math.floor(screen_w/2-4),
    screen_h-4,
  );
  
  // BACKGROUD LAYERS
  for (var i = 1; i < layers.length; i++) {
    chunk = get_chunk(i,distance);
    print_layer_elements(game_screen,i,chunk-1,frame);
    print_layer_elements(game_screen,i,chunk,frame);
    print_layer_elements(game_screen,i,chunk+1,frame);
  }
  
  // HORIZON
  let horizon_y = uF(screen_h-9+(altitude/2));
  add_to_screen(
    game_screen,
    art["horizon_line"],
    0,
    horizon_y
  );
  add_to_screen(
    game_screen,
    art["horizon"],
    0,
    horizon_y
  );
  
  // SKY LAYERS
  for (var i = 1; i < sky_layers.length; i++) {
    sky_chunk = get_chunk(i,sky_distance);
    print_sky_layer_elements(game_screen,i,sky_chunk-1,frame);
    print_sky_layer_elements(game_screen,i,sky_chunk,frame);
    print_sky_layer_elements(game_screen,i,sky_chunk+1,frame);
  }
  
 
  // SUN
  let hour = time.getHours()*60+time.getMinutes();
  if (hour != game_hour) {
    game_hour = hour;
    let color = get_hour_color([8*19,8*18,8*17],hour);
    document.body.style.backgroundColor = "rgb("+color[0][0]+','+color[0][1]+','+color[0][2]+')';
    document.body.style.color = "rgb("+color[1][0]+','+color[1][1]+','+color[1][2]+')';
  }
  add_to_screen(
    game_screen,
    art["sun"],
    Math.floor(astral_circle[cycle(hour+6*60,astral_circle)][0]*2-art["sun"][0].length/2)-Math.floor(screen_w/2),
    Math.floor(astral_circle[cycle(hour+6*60,astral_circle)][1]-art["sun"].length/4),
  );
  add_to_screen(
    game_screen,
    art["moon"],
    Math.floor(astral_circle[cycle(hour+18*60,astral_circle)][0]*2-art["moon"][0].length/2)-Math.floor(screen_w/2),
    Math.floor(astral_circle[cycle(hour+18*60,astral_circle)][1]-art["moon"].length/4),
  );

  // CLEAN
  add_to_screen(
    game_screen,
    clean_screen,
    0,
    0
  );
}

function print_fg_elements(screen,layer,chunk,frame) {
  if (!layers[layer] || !layers[layer][chunk]) return screen;
  for (var j = 0; j < layers[layer][chunk].length; j++) {
    total_in_render++; 
    let sprite_name = layers[layer][chunk][j][0];
    let y = layers[layer][chunk][j][2];
    let x = layers[layer][chunk][j][1]-ratio(distance,char_per_m*2);
    if (x > screen_w) continue;
    if (x + layers[layer][chunk][j][3] < 0) continue;
    in_render ++;
    if (sprite_name.includes("rain")) {
      if (layer >= rain_density) continue;
      sprite_name = "rain_b_"+cycle(Math.floor(frame/game_speed/4+x/5),[0,1,2,3]);
    }
    add_to_screen(
      screen,
      art[sprite_name],
      x,
      Math.floor(y)
    );
  }
}

function print_sky_layer_elements(screen,sky_layer,chunk,frame) {
  if (!sky_layers[sky_layer] || !sky_layers[sky_layer][chunk]) return screen;
  for (var j = 0; j < sky_layers[sky_layer][chunk].length; j++) {
    total_in_render++; 
    let sprite_name = sky_layers[sky_layer][chunk][j][0];
    let y = sky_layers[sky_layer][chunk][j][2];
    let x = sky_layers[sky_layer][chunk][j][1]-ratio(sky_distance,char_per_m*2/(sky_layer+1));
    if (x > screen_w) continue;
    if (x + sky_layers[sky_layer][chunk][j][3] < 0) continue;
    in_render ++;
    add_to_screen(
      screen,
      art[sprite_name],
      x,
      Math.floor(y)
    );
  }
}
function print_layer_elements(screen,layer,chunk,frame) {
  if (!layers[layer] || !layers[layer][chunk]) return screen;
  for (var j = 0; j < layers[layer][chunk].length; j++) {
    total_in_render++; 
    let sprite_name = layers[layer][chunk][j][0];
    let y = layers[layer][chunk][j][2];
    if (!sprite_name.includes("sky")) {
      for (var k = layer; k > 1; k--) {
          y += altitude/(layer*2);//(altitude*k )/Math.exp(k/2);
      }
    }

    let x = layers[layer][chunk][j][1]-ratio(distance,char_per_m*2/(layer+1));
    if (x > screen_w) continue;
    if (x + layers[layer][chunk][j][3] < 0) continue;
    if (sprite_name.includes("water")) {
      sprite_name = "water_ground_"+cycle(Math.floor(frame/(layer*5/game_speed)),[0,1,2,3,4,5,6,7,8,9]);
    }
    if (sprite_name.includes("rain")) {
      if (layer >= rain_density) continue;
      sprite_name = (Number.isInteger(layer/2) ? "rain_b_" : "rain_")+cycle(Math.floor(frame/(layer*5/game_speed)+x/5),[0,1,2,3]);
    }
    in_render ++;
    add_to_screen(
      screen,
      art[sprite_name],
      x,
      Math.floor(y)
    );
  }
}

function bike() {
  let sprite = "bike_"+cycle(uF(distance*3),[0,1,2,3,4,5,6,7]);
  return sprite; 
}
function cyclist(frame) {
  let ratio = max_power*2 / wanted_power > 0 ? max_power*2 / wanted_power : 1;
  let sprite = "cyclist_"+(blink(ratio,distance) ? '0' : '1');
  return sprite; 
}

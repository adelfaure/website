//  Joan Jump is a ASCII art plateform game.
//  Copyright (C) 2021 adelfaure
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// @Commons

function isInt(n) {
   return n % 1 === 0;
}

// @Consts

const layers = {
  'decor': {}, 
  'ground': {}, 
  'entities': {},
  'transitions': {},
}
const outputs = {
  'decor': document.getElementById('decor'), 
  'ground': document.getElementById('ground'), 
  'entities': document.getElementById('entities'),
  'transitions': document.getElementById('transitions')
}
const frame_length = 20, cam = [0, 0, 0, 0], gravity = [0,0], unit = 16;

// @Vars

var player, current_level = 0; 

// @ELEMENTS

const elements = [];

Element = function(options) {
  if (!options) options = {}

  // MOVEMENT
  this.pos = options.pos? options.pos: [0,0];
  this.velocity = options.velocity? options.velocity: [0,0];
  this.min_speed = options.min_speed? options.min_speed: unit * 0.06;
  this.speed = options.speed? options.speed: unit * 0.06;
  this.max_speed = options.max_speed? options.max_speed: unit * 0.12;
  this.speed_acceleration = options.speed_acceleration? options.speed_acceleration: 1.5;
  this.jumping = false;
  this.jump_speed = 0;
  this.max_jump_speed = options.max_jump_speed? options.max_jump_speed: unit * 0.5;
  this.max_jumps = options.max_jumps? options.max_jumps: 1;
  this.jumps_left = options.max_jumps? options.max_jumps: 1;
  this.jump_speed_decceleration = options.jump_speed_decceleration? options.jump_speed_decceleration: 1.2;
  this.ground_rub = options.ground_rub? options.ground_rub: 1.2;
  this.air_rub = options.air_rub? options.air_rub: 1.2;
  
  // DISPLAY
  this.layer = options.layer? options.layer: 'ground';
  this.sprite = options.sprite? options.sprite.split('\n'): ['#'];
  
  // BEHAVIOUR
  this.remove = false;
  this.ghost = options.ghost? options.ghost: false;
  this.obstacle = options.obstacle? options.obstacle: true;
  this.behaviour = options.behaviour? options.behaviour: false;
  this.collide_behaviour = options.collide_behaviour? options.collide_behaviour: false;
  this.movable = options.movable? options.movable: false;

  // COLLECTIBLES
  this.score = options.score? options.score: 0;

  elements.push(this);
}

// @FUNCTIONS

// # @DISPLAY

function update_display() {
  // setup layers
  layers['decor'] = {}
  layers['ground'] = {}
  layers['entities'] = {}
  layers['transitions'] = {}
  // clean elements
  for (var i = 0; i < elements.length; i++){
    if (elements[i].remove) {
      elements.splice(i,1);
      i-=1;
    }
  }
  // print
  elements.forEach( element => {
    for (y = 0; y < element.sprite.length; y++){
      for (x = 0; x < element.sprite[y].length; x++){
        let glyph = element.sprite[y][x] 
        if (glyph == ' ') continue;
        let x1 = Math.round(element.pos[0]/unit)+x;
        let y1 = Math.round(element.pos[1]/unit)+y;
        layers[element.layer][x1+','+y1] = [glyph,element];
      }
    } 
  });
}

//@BOOTS
//@BOOT_LEVEL
function boot_level(level){
  gravity[0] = level['gravity'][0], gravity[1] = level['gravity'][1];
  let mood = level['mood'], content = level['content'];
  while (elements.length) elements.shift();
  for (var i in layers) {
    if (i == 'decor') {
      if (mood[i]) outputs[i].style.color = mood[i];
      continue;
    }
    layers[i] = {}
    if (mood[i]) outputs[i].style.color = '#0000';
  }
  setTimeout(function(){
    for (var i in layers) {
      layers[i] = {}
      if (mood[i]) outputs[i].style.color = mood[i];
    }
  },1000);
  outputs['decor'].style.backgroundColor = mood['bg'];
  update_display();
  content();
}

//@BOOT_END
function boot_end(pos, next, number, options){
  let defaults = {
    'layer': 'entities',
    'sprite': '◘',
    'pos': [pos[0]*unit,pos[1]*unit],
    'behaviour': function(element) {
      // move(element, element.velocity);
      // move(element, gravity);
    },
    'collide_behaviour': function(from, to) {
      current_level ++;
      player.remove = true;
      init_camera();
      for (var i = 0; i < unit; i ++){
        new Element({
          'layer': 'decor',
          'sprite': '*',
          'pos': [player.pos[0]+Math.random()*(unit*3)-Math.random()*(unit*3),player.pos[1]+Math.random()*(unit*2)-Math.random()*(unit*2)],
          'behaviour': function(element) {
            if (isInt(frame/parseInt(Math.random()*20))){
              if (this.sprite == '*') { this.sprite = '☺'; } else
              if (this.sprite == '☺') { this.sprite = '☻'; } else
              if (this.sprite == '☻') { this.sprite = '☼'; } else
              if (this.sprite == '☼') { this.sprite = '·'; } else
              if (this.sprite == '·') { this.sprite = ' '; } else
              if (this.sprite == ' ') { this.remove = true; }
            }
          }
        });
      }
      setTimeout(function(){
        boot_level(levels[current_level]);
      },1000);
    }
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  new Element(defaults);
}

// @BOOT_PLAYER

function boot_player(pos, options){
  player = new Element({
    'sprite': ' ',
    'pos': [pos[0]*unit,pos[1]*unit],
  });
  for (var i = 0; i < unit; i ++){
    new Element({
      'layer': 'decor',
      'sprite': '*',
      'pos': [player.pos[0]+Math.random()*(unit*3)-Math.random()*(unit*3),player.pos[1]+Math.random()*(unit*2)-Math.random()*(unit*2)],
      'behaviour': function(element) {
        if (isInt(frame/parseInt(Math.random()*20))){
          if (this.sprite == '*') { this.sprite = '☺'; } else
          if (this.sprite == '☺') { this.sprite = '☻'; } else
          if (this.sprite == '☻') { this.sprite = '☼'; } else
          if (this.sprite == '☼') { this.sprite = '·'; } else
          if (this.sprite == '·') { this.sprite = ' '; } else
          if (this.sprite == ' ') { this.remove = true; }
        }
      }
    });
  }
  setTimeout(function(){
    let defaults = {
      'layer': 'entities',
      'pos': [pos[0]*unit,pos[1]*unit],
      'behaviour': function(element) {
        element.velocity[0] /= element.ground_rub;
        element.velocity[1] /= element.air_rub;
        if (commands.jump && !element.jumping && element.jumps_left){
           element.jumping = true;
           element.jumps_left--;
        }
        if (element.jumping) jump(element); 
        if (commands.run && element.speed < element.max_speed) {
          element.speed *= element.speed_acceleration;
        }
        if (element.speed > element.min_speed) element.speed /= element.ground_rub;
        if (commands.right) element.velocity[0] += element.speed;
        if (commands.left) element.velocity[0] -= element.speed;
        if (commands.run) {
          new Element({
            'layer': 'decor',
            'sprite': '+',
            'pos': [element.pos[0],element.pos[1]],
            'behaviour': function(element) {
              if (isInt(frame/4)){
                if (this.sprite == '+') { this.sprite = '*'; } else
                if (this.sprite == '*') { this.sprite = '·'; } else
                if (this.sprite == '·') { this.sprite = ' '; } else
                if (this.sprite == ' ') { this.remove = true; }
              }
            }
          });
        } else {
          new Element({
            'layer': 'decor',
            'sprite': '*',
            'pos': [element.pos[0],element.pos[1]],
            'behaviour': function(element) {
              if (isInt(frame/4)){
                if (this.sprite == '*') { this.sprite = '·'; } else
                if (this.sprite == '·') { this.sprite = ' '; }
                if (this.sprite == ' ') { this.remove = true; }
              }
            }
          });
        }
        move(element, gravity);
        move(element, element.velocity);
      }
    }
    for (var i in options) {
      if (options[i]) defaults[i] = options[i];
    }
    player = new Element(defaults);
  },1000);
  entrance = new Element({
    'layer': 'decor',
    'sprite': '◘',
    'pos': [pos[0]*unit,pos[1]*unit]
  });
  init_camera();
}

// # @NONBOOTS

function collide(element, pos){
  for (y = 0; y < element.sprite.length; y++){
    for (x = 0; x < element.sprite[y].length; x++){
      let bloc = [Math.round(pos[0]/unit+x),Math.round(pos[1]/unit+y)];
      let ground_target = layers['ground'][bloc[0]+','+bloc[1]];
      let entities_target = layers['entities'][bloc[0]+','+bloc[1]];
      if (ground_target && ground_target[1] != element ) return [bloc, ground_target];
      if (entities_target && entities_target[1] != element ) return [bloc, entities_target];
    }
  }
  return false;
}

//@MOVE
function move(element, dir) {
  let pos = [element.pos[0] + dir[0], element.pos[1] + dir[1]];
  if (element.ghost) {
      element.pos[0] += dir[0];
      element.pos[1] += dir[1];
      return;
  }
  let collision_x = collide(element,[pos[0],element.pos[1]]);
  let collision_y = collide(element,[element.pos[0],pos[1]]);
  if ( collision_x[1] ){
    if (element.collide_behaviour) {
      element.collide_behaviour(element, collision_x[1][1]);
    } else if (collision_x[1][1].collide_behaviour) {
      collision_x[1][1].collide_behaviour(collision_x[1][1], element);
    }
    if (!element.jumping && collision_x[1][1].obstacle) {
      // CLIMBER
      // element.jumps_left = element.max_jumps;
    }
    if (collision_x[1][1].obstacle == 'none'){
      element.pos[0] += dir[0];
    }
  } else {
    element.pos[0] += dir[0];
    let collision = collide(element,element.pos)
    if (collision){
      element.pos[0] -= dir[0];
    }
  } 
  
  if ( collision_y[1] ){
    if (element.collide_behaviour) {
      element.collide_behaviour(element, collision_y[1][1]);
    } else if (collision_y[1][1].collide_behaviour) {
      collision_y[1][1].collide_behaviour(collision_y[1][1], element);
    }
    if (!element.jumping && collision_y[1][1].obstacle) {
      let collision = collide(element,[element.pos[0],element.pos[1]+(gravity[1] > 0? unit: -unit)])
      if (collision){
        element.jumps_left = element.max_jumps; 
      }
    }
    if (collision_y[1][1].obstacle == 'none'){
      element.pos[1] += dir[1];
    }
  } else {
    element.pos[1] += dir[1];
    let collision = collide(element,element.pos)
    if (collision){
      element.pos[1] -= dir[1];
    }
  } 
}

//@PUT_N
function put_n(x, y, n, options){
  let defaults = {
    'layer': 'entities',
    'obstacle': 'none',
    'sprite': String(n),
    'pos': [x*unit,y*unit],
    'collide_behaviour': function(from, to) {
      if (to.score != n-1) return;
      to.score = n;
      to.sprite = ['#'+to.score];
      from.remove = true;
    }
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  new Element(defaults);
}

//@DRAW_TRAP
function draw_trap(x, y, w, h, glyph, options){
  let defaults = {
    'layer': 'entities',
    'collide_behaviour': function(from, to) {
      player.remove = true;
      fire(unit, [player.pos[0], player.pos[1]], [3,2], 20, ['#','%','?','!','$','&']);
      setTimeout( function(){ boot_level(levels[current_level]); }, 1000);
    }
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  return draw(x, y, w, h, glyph, defaults);
}

//@RAIN
function rain(glyphs, chances, base_dir, options){
  let defaults = {
    "ghost" : true,
    "behaviour": function(element) {
      move(element, [base_dir[0]/Math.round(Math.random()*20+1),base_dir[1]/Math.round(Math.random()*20+1)]);
      if (Math.random() > 0.99) element.remove = true;
    }
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  particules(cam[0], cam[1], cam[2], cam[3], glyphs[parseInt(Math.random()*glyphs.length)], chances, defaults);
}

//@FIRE
function fire(n, pos, amp, pers, chars) {
  for (var i = 0; i < n; i ++){
    new Element({
      'layer': 'decor',
      'sprite': chars[0],
      'pos': [pos[0]+Math.random()*amp[0]*unit-Math.random()*amp[0]*unit,player.pos[1]+Math.random()*amp[1]*unit-Math.random()*amp[1]*unit],
      'behaviour': function(element) {
        if (isInt(frame/parseInt(Math.random()*pers))){
          for (var i = 0; i < chars.length; i++){
            if (element.sprite[0] == chars[i] && i == chars.length-1) {
              element.remove = true;
              return;
            }
            if (element.sprite[0] == chars[i]) {
              element.sprite = [chars[i+1]];
              return;
            }
          }
        }
      }
    });
  }
}

//@PARTICULES
function particules(x, y, w, h, glyph, chances, options){
  for (var y1 = 0; y1 < h; y1++) {
    for (var x1 = 0; x1 < w; x1++) {
      if (Math.random() > chances) {
        let defaults = {
          'pos': [(x + x1) * unit,(y + y1) * unit],
          'sprite': glyph,
          'layer': 'decor'
        }
        for (var i in options) {
          if (options[i]) defaults[i] = options[i];
        }
        new Element(defaults);
      }
    }
  }
}

//@DRAW
function draw(x, y, w, h, glyph, options){
  let box = '';
  for (var y1 = 0; y1 < h; y1++) {
    for (var x1 = 0; x1 < w; x1++) {
      if (!y1 || !x1 || y1 == h-1 || x1 == w-1){
        box += glyph[0];
      } else {
        box += glyph[1];
      }
    }
    box += '\n';
  }
  let defaults = {
    'pos': [x*unit,y*unit],
    'sprite': box,
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  return new Element(defaults);
}

//@DRAW_COND
function draw_cond(x, y, w, h, cond, options){
  let box = '';
  for (var y1 = 0; y1 < h; y1++) {
    for (var x1 = 0; x1 < w; x1++) {
      if (!y1 || !x1 || y1 == h-1 || x1 == w-1){
        box += cond;
      } else {
        box += cond;
      }
    }
    box += '\n';
  }
  let defaults = {
    'pos': [x*unit,y*unit],
    'sprite': box,
    'collide_behaviour': function(from, to) {
      if (to.score == cond) {
        from.remove = true;
        to.score = 0;
        to.sprite = ['#'];
        fire(unit, [to.pos[0], to.pos[1]], [3,3], 80, [String(cond),'·']);
      }
    }
  }
  for (var i in options) {
    if (options[i]) defaults[i] = options[i];
  }
  return new Element(defaults);
}


//@JUMP
function jump(element) {
  if (Math.round(element.jump_speed) > 0) {
    element.jump_speed /= element.jump_speed_decceleration;
  } else {
    element.jump_speed = element.max_jump_speed;
    element.jumping = false;
    return false;
  }
  if (gravity[1] > 0) {
    element.velocity[1] -= element.jump_speed;
  } else {
    element.velocity[1] += element.jump_speed;
  }
}

// @Inputs


//@COMMANDS
const commands = {
  'pause': true,
  'start': false,
  'gravity': false,
  'camLeft': false,
  'camRight': false,
  'left': false,
  'right': false,
  'run': false,
  'jump': false
}

const key_map = {
  'enter': 'start',
  'p': 'pause',
  'g': 'gravity',
  'q': 'camLeft',
  'd': 'camRight',
  'arrowleft': 'left',
  'arrowright': 'right',
  'arrowup': 'jump',
  'shift': 'run'
}

document.addEventListener('keydown', function(e){
  let key = e.key.toLowerCase();
  if (key_map[key]) commands[key_map[key]] = true;
});

document.addEventListener('keyup', function(e){
  let key = e.key.toLowerCase();
  if (key_map[key]) commands[key_map[key]] = false;
});

// @Cameras

function init_camera(){
  let font_size = (window.innerWidth > 80 * 5 * 4)? 10 * 5: 
                  (window.innerWidth > 80 * 5 * 3)? 10 * 4: 
                  (window.innerWidth > 80 * 5 * 2)? 10 * 3: 
                  (window.innerWidth > 80 * 5)? 10 * 2: 
                  5;
  document.documentElement.style.fontSize = font_size + 'px';
  let w = /*12*/window.innerWidth / (font_size / 2), h = /*6*/window.innerHeight / font_size;
  cam[0] = 0, cam[1] = 0, cam[2] = w, cam[3] = h;
  var diff = (Math.round(player.pos[0] / unit) - cam[0]) - (cam[2] - cam[2] / 2);
  cam[0] = Math.round(diff);
  var diff = (Math.round(player.pos[1] / unit) - cam[1]) - (cam[3] - cam[3] / 2);
  cam[1] = Math.round(diff);
}

function camera(){
  if (Math.round(player.pos[0] / unit) - cam[0] > Math.round(cam[2] - cam[2] / 2.5)) {
    let diff = (Math.round(player.pos[0] / unit) - cam[0]) - Math.round(cam[2] - cam[2] / 2.5);
    cam[0] += Math.round(diff);
  } else if (Math.round(player.pos[0] / unit) - cam[0] < Math.round(cam[2] - cam[2] / 2.5 * 1.6)) {
    let diff = (Math.round(player.pos[0] / unit) - cam[0]) - Math.round(cam[2] - cam[2] / 2.5 * 1.6);
    cam[0] += Math.round(diff);
  } 
  if (Math.round(player.pos[1] / unit) - cam[1] > Math.round(cam[3] - cam[3] / 3)) {
    let diff = (Math.round(player.pos[1] / unit) - cam[1]) - Math.round(cam[3] - cam[3] / 3);
    cam[1] += Math.round(diff);
  } else if (Math.round(player.pos[1] / unit) - cam[1] < Math.round(cam[3] - cam[3] / 3 * 2)) {
    let diff = (Math.round(player.pos[1] / unit) - cam[1]) - Math.round(cam[3] - cam[3] / 3 * 2);
    cam[1] += Math.round(diff);
  } else if (!collide(player, player.pos)) {
    if (frame - last_moving_frame > 20){
      if (isInt(frame/2)) {
        if (player.pos[0] / unit - cam[0] > cam[2] - cam[2] / 2) {
          let diff = (player.pos[0] / unit - cam[0]) - (cam[2] - cam[2] / 2);
          if (diff > 1) cam[0] += 1;
        } else if (player.pos[0] / unit - cam[0] < cam[2] - cam[2] / 2) {
          let diff = (cam[2] - cam[2] / 2) - (player.pos[0] / unit - cam[0]);
          if (diff > 1) cam[0] -= 1;
        }
      }
    }
    if (frame - last_moving_frame > 40){
      if (isInt(frame/4)) {
        if (player.pos[1] / unit - cam[1] > cam[3] - cam[3] / 2) {
          let diff = (player.pos[1] / unit - cam[1]) - (cam[3] - cam[3] / 2);
          if (diff > 1) cam[1] += 1;
        } else if (player.pos[1] / unit - cam[1] < cam[3] - cam[3] / 2) {
          let diff = (cam[3] - cam[3] / 2) - (player.pos[1] / unit - cam[1]);
          if (diff > 1) cam[1] -= 1;
        }
      }
    }
  }
}


//@TIME
var pause = false, frame = 0, last_moving_frame = 0;
function time(){
  // # Not pause related
  if (commands.pause && pause || commands.start && pause) {
    commands.pause = false;
    pause = false;
    if (document.getElementById('music').src.indexOf('sound/'+'main'+'.ogg') == -1) {
      document.getElementById('music').pause();
      document.getElementById('music').src = 'src/'+'main'+'.ogg';
      document.getElementById('music').play();
    }
  } else if (commands.pause && !pause) {
    commands.pause = false;
    pause = true;
  }
  if (commands.gravity) {
    commands.gravity = false;
    gravity[1] *= -1;
  }
  if (commands.camLeft) cam[0]++;
  if (commands.camRight) cam[0]--;
  if (!pause) {
    // # Pause related
    // Camera
    let move_commands = ['left','right','jump'];
    let is_moving = false;
    for (var i = 0; i < move_commands.length; i++) {
      if (commands[move_commands[i]]) is_moving = true;
    }
    if (is_moving){
      last_moving_frame = frame;
    }
    update_display();
    // behaviour
    elements.forEach( element => {
      if (element.behaviour) element.behaviour(element);
    });
    output('ground');
    output('entities');
    output('decor',['entities','ground']);
    output('transitions');
    camera();
    // pause = true;
  }
  frame ++;
  setTimeout(time, frame_length);
}

function output(layer,masks,tomask){
  let text_content = '';
  for (var y = cam[1]; y < cam[3]+cam[1]; y++) {
    for (var x = cam[0]; x < cam[2]+cam[0]; x++) {
      if (masks){
        for (var i = 0; i <  masks.length; i++) {
          if (layers[masks[i]][x+','+y]) layers[layer][x+','+y] = false; 
        }
      }
      if (layers[layer][x+','+y]) {
        text_content += layers[layer][x+','+y][0]; 
      } else {
        text_content += ' '; 
      }
    }
    text_content += '\n';
  }
  if (outputs[layer].textContent != text_content) {
    outputs[layer].textContent = text_content;
  }
}

// @Setups

// # @LEVELS

const levels = [
  // @LEVEL0
  {
    'gravity': [0,unit/2], 
    'mood': {'bg': '#000', 'decor': '#AAA', 'ground': '#666', 'entities': '#FFF'}, 
    'content': function() {
      // # Entities
      boot_player([1, 8]);
      boot_end([16, 8], 1);
      // # Ground
      // Main
      draw(0, 0, 18, 10, ['░', ' ']);
      // Decor
      // # particules
      rain(['-','>','·','~'], 0.99995, [4,0]);

      // # Message
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': '#0\n← → = move\n\n-->',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['-','>','·','~'], 0.99995, [4,0]);
        }
      });
    }
  },
  // @LEVEL1
  {
    'gravity': [0,unit/2], 
    'mood': {'bg': '#000', 'decor': '#AAA', 'ground': '#666', 'entities': '#FFF'}, 
    'content': function() {
      // # Entities
      boot_player([34, 8]);
      boot_end([1, 8], 1);
      // # Ground
      // Main
      draw(0, 0, 36, 10, ['░', ' ']);
      // Decor
      // # particules
      rain(['-','<','·','~'], 0.99995, [-4,0]);

      // # Message
      new Element({
        'pos': [23*unit,2*unit],
        'sprite': '#1\n← → = move\n\n<--',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['-','<','·','~'], 0.99995, [-4,0]);
        }
      });
    }
  },
  // @LEVEL2
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#100', 'decor': '#F00', 'ground': '#A00', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([1, 8]);
      boot_end([46, 8], 2);
      //draw_trap(20, 18, 8, 1, '▲');
      // # Ground
      // Main
      draw(0, 0, 48, 20, ['░',' ']);
      // Walls
      draw(0, 9, 20, 11, ['░','▒'])
      draw(19, 17, 4, 3, ['░','▒'])
      draw(25, 14, 4, 1, ['░','▒'])
      draw(19, 11, 4, 1, ['░','▒'])
      draw(28, 9, 20, 11, ['░','▒'])
      // Decor
      // # particules
      rain(['\'','/',',','·','%'], 0.99995, [4,-4]);

      // # Message
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': '#3\n← → + ↑ = jump\n\n-->',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['\'','/',',','·','%'], 0.99995, [4,-4]);
        }
      });
    }
  },
  // @LEVEL3
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#100', 'decor': '#F00', 'ground': '#A00', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([94, 8]);
      boot_end([1, 8], 2);
      //draw_trap(20, 18, 8, 1, '▲');
      // # Ground
      // Main
      draw(0, 0, 96, 23, ['░',' ']);
      // Walls
      draw(48, 9, 20, 11, ['░','▒'])
      draw(67, 17, 4, 1, ['░','▒'])
      draw(73, 14, 4, 1, ['░','▒'])
      draw(73, 20, 4, 3, ['░','▒'])
      draw(67, 11, 4, 1, ['░','▒'])
      draw(76, 9, 20, 14, ['░','▒'])
      draw(40, 12, 9, 8, ['░','▒'])
      draw(10, 12, 8, 11, ['░','▒'])
      draw(24, 12, 10, 8, ['░','▒'])
      draw(1, 9, 10, 14, ['░','▒'])
     // draw(36, 9, 20, 11, ['░','▒'])
      // Decor
      // # particules
      rain(['`','\\','·'], 0.99995, [-4,-4]);

      // # Message
      new Element({
        'pos': [79*unit,2*unit],
        'sprite': '#3\n← → + ↑ = jump\n<--',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['`','\\','·'], 0.99995, [-4,-4]);
        }
      });
    }
  },
  // @LEVEL4
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#010', 'decor': '#0F0', 'ground': '#0A0', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([1, 8]);
      boot_end([56, 8], 2);
      //draw_trap(20, 18, 8, 1, '▲');
      // # Ground
      // Main
      draw(0, 0, 58, 20, ['░',' ']);
      // Walls
      draw(0, 9, 20, 11, ['░','▒'])
      draw(19, 17, 7, 3, ['░','▒'])
      draw(19, 11, 7, 1, ['░','▒'])
      // right
      draw(32, 14, 6, 1, ['░','▒'])
      draw(38, 9, 20, 11, ['░','▒'])
      // Decor
      // # particules
      rain(['\'','/',',','·','>','-'], 0.99995, [8,-4]);

      // # Message
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': '#4\nSHIFT + ← → = run\nrun + jump = long jump\n\n-->',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['\'','/',',','·','>','-'], 0.99995, [8,-4]);
        }
      });
    }
  },
  // @LEVEL4
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#010', 'decor': '#0F0', 'ground': '#0A0', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([126, 12]);
      boot_end([1, 15], 2);
      // # Ground
      // Main
      draw(0, 0, 128, 24, ['░',' ']);
      // Walls (from left to right)
      draw(112, 13, 16, 11, ['░','▒']);
      draw(106, 17, 6, 1, ['░','▒']);
      draw(96, 20, 6, 1, ['░','▒']);
      draw(80, 13, 16, 8, ['░','▒']);
      draw(52, 10, 16, 11, ['░','▒']);
      draw(0, 16, 8, 8, ['░','▒']);
      draw(18, 16, 8, 5, ['░','▒']);
      // Decor
      // # particules
      rain(['`','\\',',','·','<','-'], 0.99995, [-8,-4]);

      // # Message
      new Element({
        'pos': [103*unit,5*unit],
        'sprite': '#5\nSHIFT + ← → = run\nrun + jump = long jump\n\n<--',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['`','\\',',','·','<','-'], 0.99995, [-8,-4]);
        }
      });
    }
  },
  // @LEVEL6
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#001', 'decor': '#00F', 'ground': '#00A', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([1, 5]);
      boot_end([66, 5], 2);
      // # Ground
      // Main
      draw(0, 0, 68, 7, ['░',' ']);
      // Aside (from left to right)
      draw_trap(12, 5, 5, 1, '▲');
      draw_trap(28, 5, 6, 1, '▲');
      draw_trap(38, 1, 7, 1, '▼');
      draw_trap(48, 5, 8, 1, '▲');
      draw_trap(58, 1, 9, 1, '▼');
      // Decor
      // # particules
      rain(['v','V','^','<','>'], 0.99995, [0,0]);

      // # Message
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': '#6\n▲ ▼ ▶ ◀ = death\n\n-->',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['v','V','^','<','>'], 0.99995, [0,0]);
        }
      });
    }
  },
  // @LEVEL7
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#001', 'decor': '#00F', 'ground': '#00A', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([66, 8]);
      boot_end([1, 8], 2);
      // # Ground
      // Main
      draw(0, 0, 68, 20, ['░',' ']);
      // WALLS
      // left
      draw(0, 9, 29, 11, ['░','▒'])
      draw_trap(8, 8, 6, 1, '▲');
      draw(29, 15, 6, 1, ['░','▒'])
      draw_trap(29, 11, 1, 4, '▶')
      // middle
      draw(29, 0, 18, 6, ['░','▒']);
      draw_trap(36, 6, 10, 1, '▼');
      draw_trap(29, 18, 18, 1, '▲');
      // right
      draw(41, 12, 7, 1, ['░','▒'])
      draw(47, 9, 20, 11, ['░','▒'])
      draw_trap(56, 8, 6, 1, '▲');
      // Decor
      // # particules
      rain(['v','V','^','<','>'], 0.99995, [0,0]);

      // # Message
      new Element({
        'pos': [50*unit,2*unit],
        'sprite': '#7\n▲ ▼ ▶ ◀ = death\n\n<--',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['v','V','^','<','>'], 0.99995, [0,0]);
        }
      });
    }
  },
  // @LEVEL4
  { 
    'gravity': [0,unit/2], 
    'mood': {'bg': '#110', 'decor': '#FF0', 'ground': '#990', 'entities': '#FFF'}, 
    'content': function() {
      // Entities
      boot_player([1, 8]);
      boot_end([66, 8], 2);
      // collectible
      put_n(18, 5, 1);
      put_n(4, 5, 2);
      put_n(44, 2, 3);
      put_n(31, 2, 4);
      put_n(31, 15, 5);

      // # Ground
      // Main
      draw(0, 0, 68, 20, ['░',' ']);
      // WALLS
      // left
      draw(0, 9, 29, 11, ['░','▒'])
      draw(28, 12, 7, 1, ['░','▒'])
      draw_trap(10, 8, 3, 1, '▲');
      // middle
      draw(29, 5, 18, 1, ['░','▒']);
      draw_trap(30, 6, 10, 1, '▼');
      draw(36, 0, 4, 6, ['░','▒']);
      draw_trap(37, 18, 10, 1, '▲');
      draw(29, 17, 8, 3, ['░','▒']);
      // right
      draw(41, 15, 6, 1, ['░','▒'])
      draw_trap(46, 11, 1, 4, '◀')
      draw(47, 9, 20, 11, ['░','▒'])
      draw_cond(57, 1, 1, 8, 5);
      // Decor
      // # particules
      rain(['£','$','§','¥'], 0.99995, [0,0]);

      // # Message
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': '#4\nNumbers are collectible\nin a certain order\n\n-->',
        'layer': 'decor',
        'behaviour': function(element) {
          rain(['£','$','§','¥'], 0.99995, [0,0]);
        }
      });
    }
  },
  // @LEVELEND
  {
    'gravity': [0,0], 
    'mood': {'bg': '#0000', 'decor': '#AAA', 'ground': '#666', 'entities': '#FFF'}, 
    'content': function() {
      new Element({
        'pos': [4*unit,2*unit],
        'sprite': ' ',
        'layer': 'decor',
        'behaviour': function(element) {
          if (commands.start) {
            current_level = 0;
            boot_level(levels[current_level]);
          }
        }
      });
    }
  }
];

// # Global

current_level = 0;
boot_level(levels[current_level]);

time();

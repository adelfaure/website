// COMMON
String.prototype.replaceAt = function(index, replacement) {
    return this.substring(0, index) + replacement + this.substring(index + replacement.length);
}
function shuffle(array) {
  var counter = array.length;
  while (counter) {
    let pick = Math.floor(Math.random() * counter);
    counter--;
    let save = array[counter];
    array[counter] = array[pick];
    array[pick] = save;
  }
  return array;
}

// LEVEL

let level_length = 0;
let level_number = 0;
let level_decay = 0;
let level_base_decay = -17;
let decaying_speed = 1500;
let level_ground = 0, level_sky = 1, level_underground = 2;
let gate = 4, ground = 1, wall = 2, pit = 0, apple = 3, alien = 6, worm = 7, life = 8, snail = 9, cherry = 10, diamond = 11;
let level = [
  [],
  [],
  []
//[4,1,1,2,0,1,1,2,3,3,4], // ground
//[0,0,0,0,0,0,0,0,0,0,0], // sky
//[0,0,0,0,0,0,0,0,0,0,0], // underground
];
let print = [
  [],
  [],
  []
//['','','','','','','','','',''],
//['','','','','','','','','',''],
//['','','','','','','','','',''],
];

// level generation

function create_pattern(initial_element, initial_element_length, elements, length, sub_pattern_variety, sub_pattern_length) {
  let pattern = [];
  let sub_patterns = [];
  let chosen_variety = Math.floor(Math.random()*sub_pattern_variety)+1;
  for (var i = 0; i < chosen_variety; i ++) {
    let chosen_length = Math.floor(Math.random()*sub_pattern_length)+1;
    let sub_pattern = [];
    for (var j = 0; j < initial_element_length; j++) { 
      sub_pattern.push(initial_element);
    }
    for (var j = 0; j < chosen_length; j++) {
      sub_pattern.push( elements[Math.floor(Math.random()*elements.length)] );
    }
    sub_patterns.push(sub_pattern);
  }
  let sub_pattern_index = Math.floor(Math.random()*sub_patterns.length);
  let sub_pattern_progress = 0;
  for (var i = 0; i < length; i++) {
    pattern.push(sub_patterns[sub_pattern_index][sub_pattern_progress]);
    sub_pattern_progress = 
      sub_pattern_progress >= sub_patterns[sub_pattern_index].length-1 ? 0 : sub_pattern_progress + 1;
    sub_pattern_index = 
      !sub_pattern_progress ? Math.floor(Math.random()*sub_patterns.length) : sub_pattern_index;
  }
  return pattern;
}

var ground_elements = [];
var sky_elements = [];
var underground_elements = [];

function setup_decay() {
  level_base_decay = level_base_decay < -2 ? level_base_decay + 1 : -2;
  decaying_speed = decaying_speed > 600 ? decaying_speed - 50 : 600;
  level_decay = level_base_decay;
}

function bonus_level() {
  died = false;
  level_type = "stage";
  level_length = 11 + level_number;
  pattern_0 = Math.ceil(Math.random()*level_length/2);
  ground_elements = [];
  sky_elements = [];
  underground_elements = [];
  ground_elements.push(apple);
  setup_decay();
  console.log(level_decay);
  let level_ground_pattern = create_pattern(score < 10 ? Math.random() > 0.5 ? apple : ground : ground,1,ground_elements,level_length-2,Math.floor(level_length / 3),player_max_jump);
  let level_sky_pattern = create_pattern(pit,Math.floor(8*(Math.random()*2+1)),sky_elements,level_length,Math.floor(level_length / 3),Math.floor(2*(Math.random()*1+0.5)));
  let level_underground_pattern = create_pattern(pit,Math.floor(8*(Math.random()*2+1)),underground_elements,level_length,Math.floor(level_length / 3),Math.floor(2*(Math.random()*1+0.5)));
  level_ground_pattern.unshift(gate);
  level_ground_pattern.push(gate);
  for (var i = 0; i < level_ground_pattern.length; i ++) {
    if (level_ground_pattern[i] == apple) {
      if (Math.random() < 1/2) {
        level_ground_pattern[i] = cherry;
      }
      if (Math.random() < 1/4) {
        level_ground_pattern[i] = diamond;
      }
      if (Math.random() < 1/20) {
        level_ground_pattern[i] = life;
      }
    }
  }
  level[level_ground] = level_ground_pattern;
  level[level_sky] = level_sky_pattern;
  level[level_underground] = level_underground_pattern;
  player_max_jump += 1;
}
function new_level() {
  died = false;
  level_type = "stage";
  level_length = 11 + level_number;
  pattern_0 = Math.ceil(Math.random()*level_length/2);
  ground_elements = [];
  sky_elements = [];
  underground_elements = [];
  if (level_number > -1) {
    ground_elements.push(ground);
  }
  if (level_number > 0) {
    ground_elements.push(apple);
  }
  if (level_number > 1) {
    ground_elements.push(wall);
  }
  if (level_number > 3) {
    ground_elements.push(pit);
  }
  if (level_number > 7) {
    sky_elements.push(alien);
  }
  if (level_number > 15) {
    underground_elements.push(worm);
  }
  setup_decay();
  let level_ground_pattern = create_pattern(score < 10 ? Math.random() > 0.5 ? apple : ground : ground,1,ground_elements,level_length-2,Math.floor(level_length / 3),player_max_jump);
  let level_sky_pattern = create_pattern(pit,Math.floor(8*(Math.random()*2+1)),sky_elements,level_length,Math.floor(level_length / 3),Math.floor(2*(Math.random()*1+0.5)));
  let level_underground_pattern = create_pattern(pit,Math.floor(8*(Math.random()*2+1)),underground_elements,level_length,Math.floor(level_length / 3),Math.floor(2*(Math.random()*1+0.5)));
  level_ground_pattern.unshift(gate);
  level_ground_pattern.push(gate);
  for (var i = 0; i < level_ground_pattern.length; i ++) {
    if (level_ground_pattern[i] == apple) {
      if (Math.random() < 1/10) {
        level_ground_pattern[i] = cherry;
      }
      if (Math.random() < 1/20) {
        level_ground_pattern[i] = diamond;
      }
      if (Math.random() < 1/40) {
        level_ground_pattern[i] = life;
      }
    }
  }
  level[level_ground] = level_ground_pattern;
  level[level_sky] = level_sky_pattern;
  level[level_underground] = level_underground_pattern;
}

// PLAYER

let player_pos = 1;
let score = 0;
let lifes = 3;
let player_jump = 0;
let player_max_jump = 2;
let player_jumping = false;
let player_fall = false;
let game_over = false;
let stage_complete = false;
let died = false;
let cant_move = false;
let dying_animation_pos = 0;
let dying_animation_progress = 0;

// GAME

function game_status() {

  // hit wall
  while (!player_jumping && level[level_ground][player_pos] == wall) {
    player_pos--;
  }
  
  // fall in pit
  player_fall = !player_jumping && level[level_ground][player_pos] == pit ? true : false;
  
  // grab apple
  if (!player_jumping && level[level_ground][player_pos] == apple) {
    score+=1;
    level[level_ground][player_pos] = ground;
  }
  
  // grab cherry
  if (!player_jumping && level[level_ground][player_pos] == cherry) {
    score+=5;
    level[level_ground][player_pos] = ground;
  }
  
  // grab diamond
  if (!player_jumping && level[level_ground][player_pos] == diamond) {
    score+=10;
    level[level_ground][player_pos] = ground;
  }
  
  // grab life
  if (!player_jumping && level[level_ground][player_pos] == life) {
    lifes+=1;
    level[level_ground][player_pos] = ground;
  }
  if (score > 99) {
    score = 0;
    lifes += 1;
  }
  
  // grab snail
  if (!player_jumping && level[level_ground][player_pos] == snail) {
    level_base_decay--;
    level_decay = level_base_decay;
    decaying_speed += 50;
    level[level_ground][player_pos] = ground;
  }
  
  // is game_over ?
  game_over = 
       player_fall // fall in pit
    || player_pos <= level_decay // level decay
    || level[level_underground][player_pos] && !player_jumping && player_pos > 1// worm beneath
    || level[level_sky][player_pos] && player_jumping // hit alien
    ? true : false;
  if (game_over && lifes > 1 &&  !(player_pos >= level_length-1)) {
    setup_decay();
    dying_animation_pos = player_pos;
    player_pos = 1;
    lifes--;
    died = true;
    score = score < Math.ceil(level_length / player_max_jump) * 2 ? Math.ceil(level_length / player_max_jump) * 2 : score;
    game_over = false;
    cant_move = true;
    setTimeout(function(){
      cant_move = false;
    },2000);
  }
  stage_complete = player_pos >= level_length-1 && !game_over ? true : false;
}

jump_event = false;

function player_turn(root) {
  if (game_over || stage_complete || !player_jumping || root && jump_event) {
    jump_event = false;
    return;
  }
  jump_event = false;
  player_jumping = player_jump > player_max_jump ? false : player_jumping;
  if (player_jumping) {
    player_jump++;
    player_pos++;
  }
  jump_event = true;
  setTimeout(player_turn, 170);
}

player_turn(true);

function alien_turn() {
  level[level_sky].push(level[level_sky].shift());
}

function worm_turn() {
  level[level_underground].push(level[level_underground].shift());
}

function level_decaying() {
  if (player_pos > 1) {
    level_decay ++;
  }
}

// KEYS

audio_play = false;

document.addEventListener("keydown",function(e){
  if (!audio_play) {
    //document.getElementById("audio").play();
    audio_play = true;
  }
  if (game_over || stage_complete || cant_move) {
    return;
  }
  if (e.key == '*') return;
  if (player_jump > player_max_jump) return;
  if (!player_jumping && player_jump == 0) {
    player_jumping = true;
    if (player_jump == 0) {
      player_turn(true);
    }
  }
});

document.addEventListener("touchstart",function(e){
  if (game_over || stage_complete || cant_move) {
    return;
  }
  if (player_jump > player_max_jump) return;
  if (!player_jumping && player_jump == 0) {
    player_jumping = true;
    if (player_jump == 0) {
      player_turn(true);
    }
  }
});

document.addEventListener("keyup",function(e){
  player_jump = 0;
  player_jumping = false;
});

document.addEventListener("touchend",function(e){
  player_jump = 0;
  player_jumping = false;
});

// DISPLAY

// font

function config_font() {
  let size = Math.floor(window.innerWidth / (level_length) / 8)*8;
  size = size > 80 ? 80 : size;
  let padding_left = Math.floor((window.innerWidth - size * level_length)/2);
  let padding_top = Math.floor((window.innerHeight - size * 6)/2);
  document.body.style.paddingLeft = padding_left + 'px';
  document.body.style.paddingTop = padding_top + 'px';
  document.body.style.fontSize = size + 'px';
  document.body.style.lineHeight = size + 'px';
  //let pixel_size = size / 8;
  //document.getElementById("pixel_overlay").style.backgroundSize = pixel_size+'px '+pixel_size+'px';
  //document.getElementById("pixel_overlay").style.backgroundPosition = (padding_left-1)+'px '+(padding_top-1)+'px';
}

// color

//let colors = ['000','800','080','880','008','808','088','CCC','888','F00','0F0','FF0','00F','F0F'];
let dark = [3,3,3];
let light = [3,3,3];
let bright = ['8','9','a','b','c','d','e','f']

function pick_theme() {
  let mode = false;
  let fg = '#'+dark[0]+dark[1]+dark[2]; 
  let bg = '#'+bright[light[0]]+bright[light[1]]+bright[light[2]];
  dark[0] = Math.random() > 0.5 ? dark[0] + 1 : dark[0] - 1;
  dark[1] = Math.random() > 0.5 ? dark[1] + 1 : dark[1] - 1;
  dark[2] = Math.random() > 0.5 ? dark[2] + 1 : dark[2] - 1;
  dark[0] = dark[0] > 7 ? 7 : dark[0] < 0 ? 0 : dark[0];
  dark[1] = dark[1] > 7 ? 7 : dark[1] < 0 ? 0 : dark[1];
  dark[2] = dark[2] > 7 ? 7 : dark[2] < 0 ? 0 : dark[2];
  light[0] = Math.random() > 0.5 ? light[0] + 1 : light[0] - 1;
  light[1] = Math.random() > 0.5 ? light[1] + 1 : light[1] - 1;
  light[2] = Math.random() > 0.5 ? light[2] + 1 : light[2] - 1;
  light[0] = light[0] > 7 ? 7 : light[0] < 0 ? 0 : light[0];
  light[1] = light[1] > 7 ? 7 : light[1] < 0 ? 0 : light[1];
  light[2] = light[2] > 7 ? 7 : light[2] < 0 ? 0 : light[2];
  document.body.style.backgroundColor = bg; 
  //log.style.backgroundColor = bg;
  //document.getElementById("pixel_overlay").style.backgroundImage = "linear-gradient("+bg+" 1px, transparent 1px), linear-gradient(90deg, "+bg+" 1px, transparent 1px)";
  document.body.style.color = fg;
}

// print

let log = document.getElementById('log');
let pattern_0 = 0;
let pattern_1 = 0;
function print_level(){
  let to_print = "";
  // level
  for (var i = 0; i < level_length; i++) {
    let blank = Number.isInteger(Math.floor(frame/1000/(i/pattern_0))/10) ?'(' : ' ';
    let blank_2 = Number.isInteger(Math.floor(frame/1001/(i/pattern_0))/11) ?'(' : ' ';
    let blank_3 = Number.isInteger(Math.floor(frame/1003/(i/pattern_0))/13) ?'(' : ' ';
    if (died && cant_move && i == dying_animation_pos) {
        print[level_sky][i] = Number.isInteger(Math.floor(frame/20)/2) ? ':' : '(' ;  
        print[level_ground][i] = Number.isInteger(Math.floor(frame/20)/2) ? ':' : '€' ;  
        print[level_underground][i] = Number.isInteger(Math.floor(frame/20)/2) ? ':' : '(' ;  
        continue;
    }
    if (i < level_decay-1) {
        print[level_sky][i] = '"'; 
        print[level_ground][i] = ';'; 
        print[level_underground][i] = '_'; 
        continue;
    }
    if (i == level_decay-1) {
        print[level_sky][i] = '°'; 
        print[level_ground][i] = ';'; 
        print[level_underground][i] = '_'; 
        continue;
    }
    if (i == level_decay) {
        print[level_sky][i] = '~'; 
        print[level_ground][i] = '<'; 
        print[level_underground][i] = '!'; 
        continue;
    }
    switch (level[level_ground][i]) {
      case gate:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '>'; 
        print[level_underground][i] = '#'; 
        break;
      case pit:
        print[level_sky][i] = blank; 
        print[level_ground][i] = blank_2; 
        print[level_underground][i] = blank_3; 
        break;
      case ground:
        print[level_sky][i] = blank; 
        print[level_ground][i] = Number.isInteger(Math.floor(i/pattern_0)/2) ? blank_2 : ','; 
        print[level_underground][i] = '#'; 
        break;
      case wall:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '%'; 
        print[level_underground][i] = '#'; 
        break;
      case apple:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '$'; 
        print[level_underground][i] = '#'; 
        break;
      case cherry:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '?'; 
        print[level_underground][i] = '#'; 
        break;
      case diamond:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '&'; 
        print[level_underground][i] = '#'; 
        break;
      case life:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '€'; 
        print[level_underground][i] = '#'; 
        break;
      case snail:
        print[level_sky][i] = blank; 
        print[level_ground][i] = '@'; 
        print[level_underground][i] = '#'; 
        break;
    }
    if (level[level_sky][i]) {
      print[level_sky][i] = Number.isInteger(i/2) ? '§' : '*'; 
    }
    if (level[level_underground][i] && i > 1) {
      print[level_underground][i] = Number.isInteger(i/2) ? "'" : '^'; 
    }
  }

  // player print 
  print[
    player_jumping ? level_sky : player_fall ? level_underground : level_ground
  ][player_pos] = game_over || stage_complete ? '=' : player_jumping ? '/' : '|';
  if (Number.isInteger(Math.floor(frame/20)/2) && cant_move) {
    print[
      player_jumping ? level_sky : player_fall ? level_underground : level_ground
    ][player_pos] = ' ';
  }
  // end

  let jump_counter = "";
  for (var i = 0; i < player_max_jump - player_jump; i++) {
    jump_counter += '.'; 
  }

  to_print += 
    game_over ? 
      'STAGE ' + level_number + ' FAILED\n'
    :
      ( level_number > 0 && Number.isInteger(level_number/10) && Number.isInteger(Math.floor(frame/40)/2) ? 'BONUS ' : 'STAGE '  + level_number) + '\n'
    ;
  to_print += 
    stage_complete ? 
      'COMPLETE' + '\n'
    :
    game_over && level[level_sky][player_pos] && player_jumping ?
      'EATEN' + '\n'
    :
    level[level_underground][player_pos] && player_pos > 1 && !player_jumping ?
      'EATEN' + '\n'
    :
    level_decay >= player_pos ?
      'EATEN' + '\n'
    :
    player_fall ?
      'FELL' + '\n'
    :
      ( score == 0 &&  Number.isInteger(Math.floor(frame/20)/2)  ? '   ' : '$' + (score < 10 ? '0' : '') + score ) + (cant_move && Number.isInteger(Math.floor(frame/20)/2) && died ? ' ' : ' €' + lifes )+ '\n';
  to_print += jump_counter + '\n';
  to_print += '\n';
  to_print += print[level_sky].join('') + '\n';
  to_print += print[level_ground].join('') + '\n';
  to_print += print[level_underground].join('') + '\n';
  return to_print;
}

let frame = 12345;
let fade_index = 0;
let fade_logs = document.getElementsByClassName('fade_log');
let sell = "";
let sell_amount = 0;
let buy = "";
let buy_amount = 0;
let level_type = "";

function draw() {
  if (stage_complete) {
    stage_complete = false;
    player_pos = 1;
    level_number++;
    if (Number.isInteger(level_number/10)) {
      bonus_level();
    } else {
      new_level();
    }
    player_jumping = false;
    player_jump = Infinity;
    config_font();
    pick_theme();
    setTimeout(draw);
    return;
  }
  if (Number.isInteger(frame/3)) {
    fade_logs[fade_index].innerText = print_level();
    fade_index = fade_index < fade_logs.length-1 ? fade_index + 1 : 0;
  }
  if (Number.isInteger(frame/20)) {
    alien_turn();
  }
  if (Number.isInteger(frame/40)) {
    worm_turn();
  }
  if (Number.isInteger(frame/Math.floor(decaying_speed/20))) {
    level_decaying();
  }
  game_status();
  log.innerText = print_level();
  frame++;
  setTimeout(draw,17);
}
new_level();
config_font();
pick_theme();
draw();

   // // TRANSITION
   // if (Math.random() > 0.8 && player_jumping) {
   //   let index = Math.floor(Math.random() * log.innerText.length);
   //   let trying = 0;
   //   while (log.innerText[index] == '\n' || log.innerText[index] == ' ' ){
   //     index = Math.floor(Math.random() * log.innerText.length);
   //     trying ++;
   //     if (trying > 1000) {
   //       console.log("end");
   //       return;
   //     }
   //   }
   //   log.innerText = log.innerText.replaceAt(index, ' ');
   // }
   // setTimeout(draw);

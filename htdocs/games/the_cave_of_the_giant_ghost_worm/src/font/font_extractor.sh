#!/bin/bash
font_glyphs="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

for (( i=0; i<${#font_glyphs}; i++ )); do
  convert font.xcf[$i] ${font_glyphs:$i:1}.png
done

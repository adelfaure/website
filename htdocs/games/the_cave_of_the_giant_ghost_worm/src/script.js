/* Script */

function change_level_pattern_index(){
  return Math.floor(Math.random()*level_patterns.length);
}

function build_display(){
  while(level_display.children.length) level_display.removeChild(level_display.children[0]);
  while(level_display_parts.length) level_display_parts.pop(); 
  for (var i = 0; i < level_length+4; i ++) {
    let part = document.createElement('ul');
    let log = document.createElement('li');
    let log_2 = document.createElement('li');
    let line_break = document.createElement('li');
    let sky = document.createElement('li');
    let ground = document.createElement('li');
    let underground = document.createElement('li');
    log.style.width = "calc(100vw / "+(level_length+4)+")";
    log.style.height = "calc(100vw / "+(level_length+4)+")";
    log_2.style.width = "calc(100vw / "+(level_length+4)+")";
    log_2.style.height = "calc(100vw / "+(level_length+4)+")";
    line_break.style.width = "calc(100vw / "+(level_length+4)+")";
    line_break.style.height = "calc(100vw / "+(level_length+4)+")";
    sky.style.width = "calc(100vw / "+(level_length+4)+")";
    sky.style.height = "calc(100vw / "+(level_length+4)+")";
    ground.style.width = "calc(100vw / "+(level_length+4)+")";
    ground.style.height = "calc(100vw / "+(level_length+4)+")";
    underground.style.width = "calc(100vw / "+(level_length+4)+")";
    underground.style.height = "calc(100vw / "+(level_length+4)+")";
    part.appendChild(log_2); 
    part.appendChild(log); 
    part.appendChild(line_break); 
    part.appendChild(sky); 
    part.appendChild(ground); 
    part.appendChild(underground);
    level_display_parts.push([underground,ground,sky,log_2,log]);
    level_display.appendChild(part);
  }
  level_display.style.marginTop = "calc(50vh - calc(100vw /"+(level_length+4)+") * 3)";
  let color = '#'+(Math.random() > 0.5 ? 'F' : '0') + (Math.random() > 0.5 ? 'F' : '0') + (Math.random() > 0.5 ? 'F' : '0') + '2';
  document.getElementById('color').style.backgroundColor = color;
}

/*
const gate              =  0;
const ground            =  1;
const wall              =  2;
const pit               =  3;
const coin              =  4;
const life              =  5;
const falling_ground    =  7;
const player_on_ground  =  8;
const jump_on_ground    =  9;
const jump_on_wall      = 10;
const jump_on_pit       = 11;
*/

function log(str,line){
  for (var i = 0; i < level_display_parts.length; i++) {
    level_display_parts[i][3 + line].style.backgroundImage = "none"
  }
  for (var i = 0; i < str.length; i++) {
    if (str[i] == ' ') continue;
    if (str[i] == '@') {
      level_display_parts[i][3 + line].style.backgroundImage = "url('src/jump.png')"
      continue;
    }
    level_display_parts[i][3 + line].style.backgroundImage = "url('src/font/"+str[i]+".png')"
  }
}

function print_display(){
  for (var i = 0; i < level.length; i++) {
    level_display_parts[i][0].style.backgroundImage = "url('src/"+(
      on_ground.includes(level[i]) ? "ground" : 
      level[i] == player_on_pit ? "player_dead" : 
      "pit")+".png')";
    level_display_parts[i][1].style.backgroundImage = "url('src/"+(
      on_wall.includes(level[i]) ? "wall"  :
      on_gate.includes(level[i]) ? "gate"  :
      on_bonus.includes(level[i]) ? "coin"  :
      on_player.includes(level[i]) ? "still" :
      level[i] == player_no_jumps ? "player_dead" : 
        "pit")+".png')";
    level_display_parts[i][2].style.backgroundImage = "url('src/"+(
      on_alien.includes(level[i]) ? "alien" : 
      on_jump.includes(level[i])  ? "jump" : 
      level[i] == player_win ? "player_dead" : 
        "pit")+".png')";
  }
}

function create_patterns() {
  while (level_patterns.length) level_patterns.pop();
  let variety = Math.ceil(Math.random() * level_variety);
  for ( var i = 0; i < variety; i++ ) {
    let pattern_length = Math.ceil(Math.random()*(max_jump-1));
    let pattern = [jumps <= 10 ? (Math.random() > 0.5 ? bonus : ground) : ground];
    for (var j = 0; j < pattern_length; j++){
      pattern.push(pattern_allowed[Math.floor(Math.random()*pattern_allowed.length)]);
    }
    level_patterns.push(pattern);
  }
}

function create_level() {
  build_display();
  player_pos = 1;
  while (level.length) level.pop();
  level.push(gate);
  level.push(player_on_ground);
  create_patterns(); 
  level_pattern_index = change_level_pattern_index();
  level_pattern_progress = 0;
  count = level_length;
  while (count--){
    level.push(
      level_patterns[level_pattern_index][level_pattern_progress]
    );
    level_pattern_progress = level_pattern_progress < level_patterns[level_pattern_index].length-1 ? level_pattern_progress + 1 : 0;
    level_pattern_index = !level_pattern_progress ? change_level_pattern_index() : level_pattern_index;
  }
  
  level.push(ground);
  level.push(gate);
  jumps += 2;
}

function move_enemies() {
  if (on_alien.includes(level[0])){
    level[0] = gate;
  }
  for (var i = 0; i < level.length; i++){
    if (on_alien.includes(level[i+1])){
      level[i+1] = on_bonus.includes(level[i+1])  ? bonus  :
      level[i+1] = on_player.includes(level[i+1])  ? player_on_ground  :
      level[i+1] = on_gate.includes(level[i+1])  ? gate  :
      level[i+1] = on_pit.includes(level[i+1])  ? pit  :
      level[i+1] = on_wall.includes(level[i+1])  ? wall  :
      level[i+1] = on_ground.includes(level[i+1]) ? ground :
                 level[i+1];
      level[i] = on_bonus.includes(level[i])  ? alien_on_bonus  :
      level[i] = on_player.includes(level[i])  ? alien_on_player  :
      level[i] = on_gate.includes(level[i])  ? alien_on_gate  :
      level[i] = on_pit.includes(level[i])  ? alien_on_pit  :
      level[i] = on_wall.includes(level[i])  ? alien_on_wall  :
      level[i] = on_ground.includes(level[i]) ? alien_on_ground :
                 level[i];
    }
  }
}

function update_level(){
  for (var i = 0; i < level.length; i++){
    if (player_pos == i && !jump && level[i] == jump_on_bonus){
      jumps += 2;
    }
    level[i] =
      // end jump
      player_pos == i && !jump ? 
        on_pit.includes(level[i])  ? player_on_pit  : 
        on_gate.includes(level[i]) ? player_win     : 
        on_alien.includes(level[i]) ? alien_on_player     : 
        player_on_ground  :
      // start jump
      level[i] == player_on_ground && player_pos + Math.floor(jump) != i ? ground :
      // de jump
      level[i] == jump_on_ground && player_pos + Math.floor(jump) != i ? ground :
      level[i] == jump_on_wall   && player_pos + Math.floor(jump) != i ? wall   :
      level[i] == jump_on_pit    && player_pos + Math.floor(jump) != i ? pit    :
      level[i] == jump_on_gate   && player_pos + Math.floor(jump) != i ? gate   :
      level[i] == jump_on_bonus  && player_pos + Math.floor(jump) != i ? bonus  :
      // to jump
      level[i] == ground && player_pos + Math.floor(jump) == i && Math.floor(jump) ? jump_on_ground :
      level[i] == wall   && player_pos + Math.floor(jump) == i && Math.floor(jump) ? jump_on_wall   :
      level[i] == pit    && player_pos + Math.floor(jump) == i && Math.floor(jump) ? jump_on_pit    :
      level[i] == gate   && player_pos + Math.floor(jump) == i && Math.floor(jump) ? jump_on_gate   :
      level[i] == bonus  && player_pos + Math.floor(jump) == i && Math.floor(jump) ? jump_on_bonus  :
        level[i];
  }
  print_display();
  if (level[player_pos] == player_on_pit || level[player_pos] == player_no_jumps) {
    log("GAME OVER",0);
    game_over = true;
    fall = true;
  }
  if (level[player_pos] == player_win) {
    log("STAGE COMPLETE",0);
    next_level = true;
    fall = true;
  }
}

const gate              =  0;
const ground            =  1;
const wall              =  2;
const pit               =  3;
const bonus             =  4;
const player_on_ground  =  5;
const player_no_jumps   =  6;
const player_on_pit     =  7;
const player_on_gate    =  8;
const player_win        =  9;
const player_meet_alien_on_ground    = 10;
const player_meet_alien_on_wall      = 11;
const player_meet_alien_on_pit       = 12;
const player_meet_alien_on_gate      = 13;
const player_meet_alien_on_bonus     = 14;
const jump_on_ground    = 15;
const jump_on_wall      = 16;
const jump_on_pit       = 17;
const jump_on_gate      = 18;
const jump_on_bonus     = 19;
const alien_on_ground   = 20;
const alien_on_wall     = 21;
const alien_on_pit      = 22;
const alien_on_gate     = 23;
const alien_on_bonus    = 24;
const alien_on_player   = 25;

let on_ground = [gate,ground,wall,player_on_ground,player_win,player_no_jumps,bonus,jump_on_ground,jump_on_wall,jump_on_gate,jump_on_bonus,alien_on_ground,alien_on_wall,alien_on_gate,alien_on_bonus,alien_on_player];
let on_jump = [jump_on_ground,jump_on_wall,jump_on_pit,jump_on_gate,jump_on_bonus];
let on_alien = [alien_on_ground,alien_on_wall,alien_on_pit,alien_on_gate,alien_on_bonus,alien_on_player];
let on_wall = [wall,jump_on_wall,alien_on_wall];
let on_pit = [pit,jump_on_pit,alien_on_pit];
let on_player = [alien_on_player,player_on_ground];
let on_bonus = [bonus,jump_on_bonus,alien_on_bonus];
let on_gate = [gate,jump_on_gate,player_win,alien_on_gate];
let stop_update = [player_on_pit,player_win,player_no_jumps];

let level_display = document.getElementById("level_display");
var level = [];
let level_pattern_progress = 0
let level_display_parts = [];
let level_variety = 1;
let pattern_allowed = [1];
let level_patterns = [];
let level_pattern_index = change_level_pattern_index();
let level_length = 11;

// game
let player_pos = 1;
let max_jump = 4;
let jump_key = false;
let jump = 0;
let jumps = Math.ceil(level_length/max_jump);
let alien_pop = 1;
let alien_pop_speed = 0.01;
let alien_move = 1;
let alien_move_speed = 0.1;
let fall = false;
let jump_speed = 0.11;
let game_over = false;
let next_level = false;

function time() {
  jump_key = jump > max_jump ? false : jump_key;
  fall = jump > max_jump ? true : fall;
  player_pos = !jump_key ? player_pos + Math.floor(jump) : player_pos;
  /*if (on_alien.includes(level[player_pos+Math.floor(jump)]) && jump) {
    level[player_pos] = player_no_jumps;
    log("GAME OVER",0);
    game_over = true;
    fall = true;
    jump = 0;
    print_display();
  }*/
  while (on_wall.includes(level[player_pos])) player_pos--;
  jump = jump_key ? jump < max_jump && player_pos + jump + jump_speed < level.length ? jump + jump_speed : jump : 0;
  if (player_pos + Math.floor(jump) == level.length-1) jump_key = false;
  if (!stop_update.includes(level[player_pos])) {
    update_level();
  }/*
  alien_pop -= alien_pop_speed;
  if (alien_pop <= 0) {
    level[level.length-1] = alien_on_gate;
    alien_pop = 1;
  }
  alien_move -= alien_move_speed;
  if (alien_move <= 0) {
    move_enemies();
    alien_move = 1;
  }*/
  log("@ "+jumps,1);
  setTimeout(time,17);
};

// keys

document.addEventListener("keydown",function(e){
  if (game_over && !fall) {
    jumps = Math.ceil(level_length/max_jump);
    level_length = 11;
    level_variety = 1;
    pattern_allowed = [1];
    create_level();
    log("ONE WAY",0);
    document.getElementById('color').style.backgroundColor = "transparent";
    game_over = false;
    fall = true;
  } else if (next_level && !fall) {
    level_length++;
    level_variety++;
    alien_pop_speed += 0.0001;
    pattern_allowed.push(Math.floor(Math.random()*4)+1);
    create_level();
    log("STAGE "+(level_length-10),0);
    next_level = false;
    fall = true;
  } else {
    jump = !fall && !jump_key? 1 :jump;
    if (jump == 1) {
      jumps--;
      if (jumps < 0) {
        level[player_pos] = player_no_jumps;
        log("GAME OVER",0);
        game_over = true;
        fall = true;
        jump = 0;
        print_display();
      }
    }
    jump_key = fall ? false : true;
  }
});

document.addEventListener("keyup",function(e){
  jump_key = false;
  fall = false;
});

for (var i = 0; i < 1; i ++){
  //level_length++;
  //level_variety++;
  //pattern_allowed.push(Math.floor(Math.random()*4)+1);
  create_level();
  //log("STAGE "+(level_length-10),0);
  //next_level = false;
  //fall = true;
}

time();
log("NO TURNING BACK",0);
document.getElementById('color').style.backgroundColor = "transparent";

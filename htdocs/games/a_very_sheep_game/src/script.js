/*
  A Very Sheep Game copyright (C) 2021-2022 Adel Faure
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// PRINT

function add_to_print(from,to,x,y) {
  from = from.split('');
  to = to.split('');
  let width = 0;
  let width_set = false;
  for (var char_index = 0; char_index < from.length; char_index++) {
    if (from[char_index] == '\n') {
      x+=80-width;
      width_set = true;
      continue;
    } else if (!width_set){
      width++;
    }
    to[char_index+x+(y*81)] = from[char_index] == ' ' ? to[char_index+x+(y*81)] : from[char_index] == '┼' ? ' ' : from[char_index];
  }
  return to.join('');
}

// SLEEP MINI GAME

let fence_distance = 0;
let sheep_distance = 0;
let sheep_walk_sequence = 0;
let sheep_jump_sequence = 0;
let jumps = 0;
let record = 0;
let previous = 0;
var supporter_pos = [];
var supporter_line = [];
var supporter_assets = [];
var supporter_ratio = 5;
function print_sleep_mini_game(key){
  let sleep_mini_game = art_assets["sleep_bg"];
  for (var i = 0; i < supporter_pos.length; i++) {
    sleep_mini_game = add_to_print(art_assets["supporter_"+supporter_assets[i]],sleep_mini_game,supporter_pos[i],15-supporter_line[i]);
  }
  document.body.style.color = "rgb("+Math.floor(game_speed*1.7*8)+','+Math.floor(game_speed*1.7)+','+Math.floor(game_speed*1.7)+')';
  fence_distance = !fence_distance ? Math.ceil(Math.random()*35) : fence_distance;
  if (!sheep_walk_sequence) {
    document.getElementById("audio").play();
      sleep_mini_game = art_assets["empty"];
    current_game_speed = 750;
    document.body.style.color = '#FFF';
    sleep_mini_game = add_to_print("PRESS┼SPACE┼TO┼JUMP",sleep_mini_game,fence_distance+15,15);
    key = 'a';
  }
  if (key != ' ' && sheep_jump_sequence == 0 || sheep_jump_sequence > 2){
    if (sheep_jump_sequence > 0 || key == ' ' && sheep_jump_sequence == 0 ) {
      sleep_mini_game = add_to_print(art_assets["fence"],sleep_mini_game,20+fence_distance,17);
    }
    sleep_mini_game = add_to_print(art_assets[
      (!sheep_walk_sequence ? "sheep_0" : Number.isInteger(sheep_walk_sequence/2)? "sheep_1" : "sheep_2")
    ],sleep_mini_game,sheep_distance,21);
    sheep_distance = sheep_distance + 2;//Math.ceil(Math.random() * 4);
    sheep_walk_sequence++;
    sleep_mini_game = add_to_print("Escape to stop",sleep_mini_game,0,0);
    sleep_mini_game = add_to_print("Sheeps : "+jumps,sleep_mini_game,0,1);
    sleep_mini_game = add_to_print("Best : "+record,sleep_mini_game,0,2);
    sleep_mini_game = add_to_print("Previous : "+previous,sleep_mini_game,0,3);
  } else {
    if (!sheep_jump_sequence) {
      document.getElementById("jump").currentTime = 0;
      document.getElementById("jump").play();
    }
    if (sheep_jump_sequence == 2 ) {
      sleep_mini_game = art_assets["empty"];
      for (var i = 0; i < supporter_pos.length; i++) {
        sleep_mini_game = add_to_print(art_assets["supporter_2"],sleep_mini_game,supporter_pos[i],15-supporter_line[i]);
      }
      document.body.style.color = '#FFF';
    }
    if (sheep_jump_sequence > 0 || key == ' ' && sheep_jump_sequence == 0 ) {
      sleep_mini_game = add_to_print(art_assets["fence"],sleep_mini_game,20+fence_distance,17);
    }
    sheep_distance = !sheep_jump_sequence ? sheep_distance + 4 : sheep_distance;
    sleep_mini_game = add_to_print(art_assets[
      (!sheep_jump_sequence ? "sheep_3" : sheep_jump_sequence ==1 ? "sheep_4" : "sheep_6" )
    ],sleep_mini_game,sheep_distance,sheep_jump_sequence != 2 ? 17 : 21);
    sheep_distance = !sheep_jump_sequence ? sheep_distance + 8 : sheep_jump_sequence == 1 ? sheep_distance+ 6 : sheep_distance + 2;
    sheep_jump_sequence = !sheep_jump_sequence ? 1 : sheep_jump_sequence == 1 ? 2 : 3;
    current_game_speed = sheep_jump_sequence == 3 ? 500 : 200;
  }
  if (sheep_jump_sequence < 1) {
    sleep_mini_game = add_to_print(art_assets["fence"],sleep_mini_game,20+fence_distance,17);
  }
  if (sheep_jump_sequence == 0 && fence_distance-sheep_distance < -12
   || sheep_jump_sequence == 1 && fence_distance-sheep_distance < -22
   || sheep_jump_sequence == 3 && fence_distance-sheep_distance > -26) {
    document.body.style.color = '#888';
    document.getElementById("fail").currentTime = 0;
    document.getElementById("fail").play();
    sleep_mini_game = art_assets["empty"];
    for (var i = 0; i < supporter_pos.length; i++) {
      sleep_mini_game = add_to_print(art_assets["supporter_6"],sleep_mini_game,supporter_pos[i],15-supporter_line[i]);
    }
    sleep_mini_game = add_to_print(art_assets["fail"],sleep_mini_game,0,5);
    sleep_mini_game = add_to_print(art_assets["fence"],sleep_mini_game,20+fence_distance,17);
    sleep_mini_game = add_to_print(art_assets["sheep_5"],sleep_mini_game,sheep_distance-5,21);
    fence_distance = 0;
    sheep_distance = 0;
    sheep_walk_sequence = 0;
    sheep_jump_sequence = 0;
    current_game_speed = 1500;
    game_speed = 150;
    supporter_ratio = 5;
    supporter_pos = [];
    supporter_assets = [];
    supporter_line = [];
    for (var i = 0; i < Math.floor(record/supporter_ratio); i++) {
      supporter_pos.push(Math.floor(Math.random()*77));
      supporter_line.push(Math.random()>0.5 ? 0 : 0);
      supporter_assets.push(Math.floor(Math.random()*6));
    }
    previous = jumps;
    jumps = 0;
    document.getElementById("audio").pause();
    document.getElementById("audio").currentTime = 0;
  }
  if (sheep_distance == 64 || sheep_jump_sequence == 3) {
    document.getElementById("sucess").currentTime = 0;
    document.getElementById("sucess").play();
    jumps++;
    if (jumps>record) {
      sleep_mini_game = add_to_print(jumps+" new record ! ",sleep_mini_game,sheep_distance,15);
      record = jumps;
    } else if (jumps-1 == previous ) {
      sleep_mini_game = add_to_print(jumps+" better than last time",sleep_mini_game,sheep_distance-6,15);
    } else {
      sleep_mini_game = add_to_print(String(jumps),sleep_mini_game,sheep_distance+5,15);
    }
    fence_distance = 0;
    sheep_distance = 0;
    sheep_walk_sequence = 0;
    sheep_jump_sequence = 0;
    current_game_speed = 750;
    game_speed = game_speed > 17 ? Math.floor(game_speed * 0.95) : 17;
    supporter_ratio = supporter_ratio * 0.95;
    supporter_pos = [];
    supporter_assets = [];
    supporter_line = [];
    for (var i = 0; i < Math.floor(record/supporter_ratio); i++) {
      supporter_pos.push(Math.floor(Math.random()*77));
      supporter_line.push(Math.random()>0.5 ? 0 : 0);
      supporter_assets.push(Math.random()>0.5 ? 0 : Math.floor(Math.random()*6));
    }
  }
  return sleep_mini_game;
}

// MENUS

let keys = {};
let keys_up = {};

document.addEventListener("keydown",function(e) {
  keys[e.key] = true;
});
document.addEventListener("keyup",function(e) {
  keys_up[e.key] = true;
});

document.addEventListener("touchstart",function(e){
  keys[' '] = true;
});

document.addEventListener("touchend",function(e){
  keys[' '] = false;
});


let log = document.getElementById("log");

let menu = "title";

let menus = {
  "title" : function(){
    game_speed = 17;
    let title = art_assets["title"];
    if (Number.isInteger(Math.floor(frame/40)/2)) {
      title = add_to_print("                              PRESS SPACE TO START",title,0,12);
    }
    log.innerText = title;
    if (keys[' ']) {
      document.getElementById("sucess").volume = 0.1;
      document.getElementById("audio").volume = 0.5;
      document.getElementById("jump").volume = 0.25;
      document.getElementById("fail").volume = 0.25;
      document.getElementById("audio").play();
      game_speed = 150;
      menu = "main";
      for (var i in keys){
        keys[i] = false;
      }
    }
  },
  "main" : function(){
    log.innerText = print_sleep_mini_game(keys[' '] ? ' ' : 'a');
    if (keys['Escape']) {
      document.getElementById("audio").pause();
      document.getElementById("audio").currentTime = 0;
      menu = "title";
      fence_distance = 0;
      sheep_distance = 0;
      sheep_walk_sequence = 0;
      sheep_jump_sequence = 0;
      jumps = 0;
      previous = 0;
    }
    for (var i in keys_up){
      if (keys_up[i]) {
        keys[i] = false;
        keys_up[i] = false;
      }
    }
  }
}

// DRAW

let game_speed = 150;
let current_game_speed = 100;

let frame = 0;

function draw(){
  menus[menu]();
  setTimeout(draw,current_game_speed);
  if (current_game_speed != game_speed) current_game_speed = game_speed;
  frame++;
}

// INIT

const art_assets = {};
window.addEventListener('load',function(){
  
  let art_embed = document.getElementsByClassName("art");
  
  for (var i = 0; i < art_embed.length; i++){
    art_assets[art_embed[i].id] = art_embed[i].contentDocument.body.children[0].innerText;
  }
  while (art_embed.length) {
    art_embed[0].parentNode.removeChild(art_embed[0]);
  }
  
  draw();
});



/*
    Everlasting Household love, copyright (C) 2021 Adel Faure,
    contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// MENUS

var commands = {};

// Header

function menu_header(){
  commands = {};
  log.textContent = '';
}

function intro_menu(i){
  menu_header();
  print_asset('intro_'+i);
  if (i > 2) { 
    commands['c'] = function(){
      day_menu();
    };
  } else {
    commands['c'] = function(){
      intro_menu(i+1);
    };
  }
}

function end_menu(){
    menu_header();
    audio.pause();
    audio.src = 'src/renard_low.ogg';
    audio.currentTime = 0;
    audio.play();
    var win = true;
    for (var i = 0; i < filth.length; i++){
      if (filth[i]) {
        win = false;
        break;
      }
    }
    if (win) {
      print_asset('master');
      add_to_print('Your cleaning was enough to summon you master.\n"Well done fool ! Now I will be able to enslave all humans."\nYou win but wonder if it was such a good idea...   [ R ] To restart\n');
      commands['r'] = function(){
        init();
        day_menu();
      };
    } else {
      print_asset('doomed');
      add_to_print("Due to the dirtiness you left on the manor your master didn't come. You loose and are now condemned to haunt the manor for eternity...   [ R ] To restart\n");
      commands['r'] = function(){
        init();
        day_menu();
      };
    }
}

function day_menu(position) {
  menu_header();

  if (day > 3){
    mode = 5;
    audio.pause();
    audio.src = 'src/bell_low.ogg';
    audio.currentTime = 0;
    audio.play();
    audio.loop = false;
    print_asset('clock_0');
    add_to_print('It is time. Will your master rise again ?   [ C ] To continue\n');
    commands['c'] = function(){
      audio.loop = true;
      end_menu();
    };
  } else {
    audio.pause();
    audio.src = 'src/day_low.ogg';
    audio.currentTime = 0;
    audio.play();
    audio.loop = false;
    print_asset('day_'+day);
    add_to_print("[ C ] To continue\n");
    commands['c'] = function(){
      audio.loop = true;
      audio.pause();
      audio.src = 'src/souvenirs_de_chez_soi_low.ogg';
      audio.currentTime = 0;
      audio.play();
      if (position!=undefined){
        room_menu(position);
      } else {
        map_menu();
      }
    };
  }
}

function map_menu() {
  menu_header();
  
  print_map();

  // MENU
  add_to_print('[ 0-8 ] Move   [ L ] Look   [ W ] Wait\n');
  for (var i = 0; i < rooms.length; i++){
    let room = i;
    commands[i] = function(){
      positions[0] = room;
      if (is_spotted()) return;
      map_menu(); 
    } 
  }
  commands['l'] = function(){
    room_menu(positions[0]); 
  } 
  commands['w'] = function(){
    wait();
    if (is_spotted()) return;
    if (time == 0) {
      day_menu();
    } else {
      map_menu();
    }
  } 
}

function room_menu(position) {
  menu_header();
  
  print_room(position);

  // MENU
  add_to_print('You are in the '+rooms[position]+'.   '+(filth[position]?'[ C ] Clean   ':'')+'[ M ] Manor map\n');
  commands['m'] = function(){ 
    map_menu(); 
  };
  if (!filth[position]) return;
  commands['c'] = function(){
    wait();
    if (is_spotted()) return;
    if (filth[position]) filth[position]--;
    if (time == 0) {
      day_menu(position);
    } else {
      room_menu(position); 
    }
  };
}

function encounter_menu(encounter_id){
  menu_header();

  print_asset(encounter_id+'_0');
  
  if (encounter_id == 'E'){
    audio.pause();
    audio.src = 'src/cymbal_low.ogg';
    audio.currentTime = 0;
    audio.play();
    audio.loop = false;
    add_to_print('"VADE RETRO SATANA!" You have been exorcised!   [ R ] To restart\n');
    commands['r'] = function(){
      audio.loop = true;
      init();
      day_menu();
    };
  } else {
    add_to_print("You have been spotted!   [ C ] To continue\n");
    commands['c'] = function(){
      ghost_menu(encounter_id);
    };
  }
}

function calling_menu(encounter_id){
  menu_header();

  print_asset(encounter_id+'_1');
  add_to_print('"I call the exorcist."   [ C ] To continue\n');
  commands['c'] = function(){
    exorcism = true;
    map_menu();
  };
}

function ghost_menu(encounter_id){
  menu_header();

  print_asset('@');
  add_to_print('"Gee, a ghost in the house!"   [ C ] To continue\n');
  commands['c'] = function(){
    calling_menu(encounter_id);
  };
}

/*
function commands_menu() {
  commands = {};
  log.textContent = '';
  
  print_asset('bedroom_0');

  // MENU
  add_to_print('[ '+commands.length+' ] List commands\n');
  commands.push(function(){ 
    intro_menu(); 
  });
}*/

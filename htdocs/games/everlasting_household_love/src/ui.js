/*
    Everlasting Household love, copyright (C) 2021 Adel Faure,
    contact@adelfaure.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// UI

// colors

let bg_colors = ['#000','#004','#044','#404','#800','#000'];
let fg_colors = ['#888','#88F','#8FF','#F8F','#F00','#0f0'];

var mode = 0;

// dom
const log = document.createElement('pre');
document.body.appendChild(log);
var font_width = 0, font = 'jgs5', font_size = 0;

function get_font_size(){
  if (font == 'ubuntu' || font == 'topaz') {
    font_width = 8;
  } else if (font == 'jgs5') {
    font_width = 5;
  } else if (font == 'jgs9') {
    font_width = 9;
  }
  return (font_width * 2) * Math.floor(window.innerWidth / 80 / font_width);
}

function update_dom(){
  font_size = get_font_size();
  let margin_left = Math.floor(( window.innerWidth - font_size / 2 * 80 ) / 2);
  let margin_top = Math.floor(( window.innerHeight - font_size * 22 ) / 2);
  bg = bg_colors[mode];
  fg = fg_colors[mode];
  log.style.fontSize = font_size + 'px';
  log.style.lineHeight = font_size + 'px'; 
  log.style.width = font_size / 2 * 80 + 'px';
  log.style.height = font_size * 22 + 'px';
  log.style.marginLeft = margin_left + 'px';
  log.style.marginTop = margin_top + 'px';
  log.style.fontFamily = font;
  log.style.backgroundColor = bg; 
  log.style.color = fg;
  document.body.style.backgroundColor = bg; 
}

// assets

function merge_asset(a,b) {
  let lines = [];
  let lines_to_merge = [];
  let new_asset = [];
  for (var i = 0; i < a.length; i++){
    lines.push(a[i].split(''));
    lines_to_merge.push(b[i].split(''));
  }
  for (var i = 0; i < lines.length; i++){
    for (var j = 0; j < lines[i].length; j++){
      if (lines_to_merge[i][j] != ' ') {
        lines[i][j] = lines_to_merge[i][j];
      }
    }
  }
  for (var i = 0; i < lines.length; i++){
    new_asset.push(lines[i].join(''));
  }
  return new_asset;
};

// print

var print_speed = 0, caret_speed = Math.floor(100/Math.ceil((print_speed+1)/6)), print_number = 0, pause = false;
const to_print = [];

function add_to_print(str) {
  let lines = log.textContent.split('\n');
  let words = str.split(' ');
  for (var i = 0; i < words.length; i++){
    if (to_print.join('').split('\n').pop().length + words[i].length > 79) {
      to_print.push('\n');
    }
    for (var j = 0; j < words[i].length; j++){
      to_print.push(words[i][j]);
    }
    if (i < words.length-1) to_print.push(' ');
  }
}

function print(caret) {
  if (!pause){
    update_dom();
    let char_to_print = to_print.shift();
    if (char_to_print) {
      log.textContent += char_to_print;
    } else {
      if (Number.isInteger(print_number/caret_speed)) {
        if (caret) {
          log.textContent += '█';
          caret = false;
        } else {
          if (log.textContent[log.textContent.length-1] == '█') {
            log.textContent = log.textContent.slice(0,-1);
          }
          caret = true;
        }
      }
    }
    print_number++;
  }
  setTimeout(function(){
    print(caret);
  },print_speed);
}

// audio

let audio = document.getElementById('audio');
var audio_triggered = false;

// user input

document.addEventListener('keydown', function(e){
  if (!audio_triggered && e.key.length == 1) {
    audio.play();
    audio.volume = 0.25;
    audio_triggered = true;
  }
  if (pause) return;
  if (to_print.length) return;
  if (log.textContent[log.textContent.length-1] == '█') {
    log.textContent = log.textContent.slice(0, -1);
  }
  let lines = log.textContent.split('\n');
  let command = lines.pop();
  if (e.key.length == 1) {
    if (command == 'command not found') {
      log.textContent = lines.join('\n') + '\n';
    }   
    add_to_print(e.key);
  } else if (e.key == 'Backspace' && log.textContent[log.textContent.length-1] != '\n') {
    log.textContent = log.textContent.slice(0, -1);
  } else if (e.key == 'Enter'){
    command = command.toLowerCase();
    log.textContent = lines.join('\n') + '\n';
    if (commands[command]) {
      commands[command](command);
    } else {
      add_to_print('command not found');
    }
  }
});

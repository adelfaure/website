<!DOCTYPE html><!-- 

## Create index

`create_index.php` is meant to create the index.html that run the game.

Use command line:

`php create_index.php > index.html`

It echo all files content from the `art/` folder into `<pre>` `textContent` and all create a sourced `<script>` element for each file within `javascript/` folder.
This is useful for avoiding using javascript 'fetch' to load ascii art files content in the game.

All `<pre>` `textContent` are then processed into text sprites in `javascript/sprites.js`

-->
<?php

  // Loop through the art folder at game root and echo text files content as `<pre>` `textContent`
  function ascii_assets() {
    $path = "./art/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        $id=substr($file,0,-4);
        echo "<pre class=\"art_source\" id=\"$id\">".htmlspecialchars(file_get_contents("$path$file",TRUE), ENT_QUOTES)."</pre>\n";
      }
    }
  }
  
  // Loop through the art folder at game root and create a sourced `<script>` element for each file
  function js_assets() {
    $path = "./javascript/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "<script defer src=\"$path$file\"></script>\n";
      }
    }
  }
?>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
    <title>A Very Bold Character</title>
    <?php include "tom/tom_include.html" ?>
  </head>
  <body>
    <!-- ASCII assets are stored here -->
    <?php ascii_assets(); ?>
  </body>
  <!-- Link script files here -->
  <?php js_assets(); ?>
</html>

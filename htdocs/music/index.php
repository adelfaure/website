<!DOCTYPE html>
<!--
<?php include '../html/license.txt'; ?>
-->
<?php
  # FUNCTIONS

  function human_filesize($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
  }

  function add($src){
    echo htmlspecialchars(file_get_contents($src,TRUE), ENT_QUOTES);
  }
  
  function link_year($year,$up) {
    echo ($up ? "/\\" : "\\/")." <a id=\"index$year\" href=\"#$year\">$year</a>\n";
  }

  function index_year($year) {
    $path = "./$year/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      if (filetype("$path$file") == "file") {
        echo "\/ <a href=\"#$file\">$file</a>\n";
      }
    }
  }

  function year($year) {
    $path = "./$year/";
    $dir = scandir($path);
    foreach ($dir as $file) {
      $filename = $path.$file;
      if (filetype("$path$file") == "file") {
        echo "<img id=\"$file\" src=\"./art$year/$file.png\">\n\n";
        echo "/\ <a href=\"#$year\">$year</a> > $file ";
        echo "<a href=\"$path$file\" download>Download ( ".human_filesize(filesize($filename))." )</a>\n";
        echo "<audio download controls src=\"$path$file\">";
        echo  "</audio>\n";
        echo "\n\n";
      }
    }
  }
?>
<html>
  <head>
    <title>Adel Faure ‒ Music</title>
    <!-- search engines -->
    <meta charset="utf-8">
    <meta name="description" content="Adel Faure ASCII art collection."/>
    <meta name="keywords" content="Text mode, ASCII art">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- twitter card -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@adelfaure" />
    <meta name="twitter:title" content="Adel Faure ‒ ASCII art" />
    <meta name="twitter:description" content="Adel Faure ASCII art collection" />
    <meta name="twitter:image" content="./card_image_small.png" />
    <link href="../src/style.css" rel="stylesheet">
  </head>
  <body class="music" style="display:none" id="top">
<pre>
<?php include '../html/header.html'; ?>
================================================================================
< <a href="../">Back</a>                   __ __ __ __ _____ _____ _____ 
                        |  V  |  |  |   __|     |     |
                        |     |  |  |__   ||   ||   --|
                        |__V__|_____|_____|_____|_____|

Music of various quality and loudness, live coded, guaranteed 100% non-mastered 

================================================================================

<?php
  link_year(2023,false);
  link_year(2022,false);
  link_year(2021,false);
?>

<span id="2023">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |__   |
                           |   __|  |  |   __|__   |
                           |_____|_____|_____|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2022,false);
  echo "\n";
  index_year(2023);
  echo "\n";
  year(2023); 
?>
<span id="2022">================================================================================
                            _____ _____ _____ _____
                           |__   |     |__   |__   |
                           |   __|  |  |   __|   __|
                           |_____|_____|_____|_____|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2023,true);
  link_year(2021,false);
  echo "\n";
  index_year(2022);
  echo "\n";
  year(2022); 
?>
<span id="2021">================================================================================
                            _____ _____ _____  ____
                           |__   |     |__   |/_   |
                           |   __|  |  |   __| |   |
                           |_____|_____|_____| |___|

================================================================================
/\ <a href="#top">Top</a></span>
<?php
  link_year(2022,true);
  echo "\n";
  index_year(2021);
  echo "\n";
  year(2021); 
?>
/\ <a href="#top">Top</a></span>
<?php include '../html/footer.html' ?>
</pre>
  </body>
  <script src="../src/script.js"></script>
</html>

## Rendering ASCII smooth lines using character subdivision

I worked on rendering smooth lines in ASCII art using character subdivision.

![](./media/spider.mp4)

Each character is subdivided into rounding positions to determine the entry and exit points of a line.

![](./media/char_nodes.png)

I've added 65 special characters to the Jgs font corresponding to these junction points.

![](./media/sub_chars.png)

I used line equations like `y = slope * (x - aX) + aY` and sort functions to find these in and out between two given points to render these smooth lines. And because I am very bad at maths this step took me almost an entire week...

![](./media/smooth_line.mp4)

This feature should be very useful for road rendering in The Wind Through the Wheels, in particular finally being able to render road crossings in the player's path.


## Making and rendering 3d roads and landscapes in ASCII art

![](./media/render_relief_roads_wind.mp4)

With Vincent Maillard, we worked on how to build and render the road and its
background landscapes in The Wind Though the Wheels. Using the "ascii smooth
lines" system I introduced last month, we focused on approaching the landscape
as layers of relief profiles projected into space.

![](./media/relief_profiles.png)

We created some sort of shader that renders ASCII relief volumes using arrays of 2D
coordinates projected into space. The first two relief arrays in the data are
used for the roadside.

![](./media/bitmap.png)

Once the 3D environment of the scene is in place, it's a matter of finding the
right XYZ scale, balancing geographical realism, the length of the race, the
difficulty and the relevance of the viewpoint.  Perhaps these factors will be
something that the player can modify during the game as special powers or
abilities?

![](./media/scaling_relief.mp4)

Beyond the rendering of roads and landscapes, there is the issue of their
design. It's quite difficult to create consistent landscape data by hand, so we
started using data from Open Elevation. As a test, we used it to model the
relief around Retournac, a town near where we live.

![](./media/open_elevation_relief_retournac.png)

Using existing rides (from Open Runner data) as a starting point for
designing stages is a neat aspect of the method.

![](./media/open_runner_0.png)

However, our way of visualising cycling from a side view means that we flatten
out the road from all its bends, so we can't build a coherent stage from
unmodified geographical data. We will design stages by modifying and
interpolating cycle route reliefs and selected or generated landscapes that are
not geographically coherent at first.

![](./media/interpolation.png)

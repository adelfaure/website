console.log("animation.js");

let frame_index = 0;
let animation = [];
let paused_animation = [];

function start_animation(frame_duration) {
  setInterval(function() {
    let loop = animation.length;
    while(loop--) {
      animation[loop]();
    }
    frame_index = frame_index + (animation.length ? 1 : 0);
  }, frame_duration);
}

function anim(fn) {
  animation.push(fn);
}

function pause_animation() {
  while(animation.length) paused_animation.push(animation.shift());
}

function resume_animation() {
  while(paused_animation.length) animation.push(paused_animation.shift());
}

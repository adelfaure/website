function main() {
  area.fitCanvas(scale,canvas,font);
  
  let density = [];
  let colors = [
    "Black","Red","Green","Blue","White",
    //"Bright Black","Bright Red","Bright Green","Bright Blue","Bright White"
  ];
  let wait = 60*mc(mr()*5);
  for (var fg = 0; fg < colors.length; fg++){
    for (var bg = 0; bg < colors.length; bg++){
      density = density.concat(
        font.encoding.strToData('▓▒░▄▌│─┼',{
          "FG":colors[fg],
          "BG":colors[bg]
        })
      )
    }
  }
  density = density.concat([0]);
  console.log(density);
  fps = 60;
  // flood_fill options
  let floodfill_a = new Floodfill(
    [mf(mr()*(area.width*area.height))],
    density,
    density[mf(mr()*density.length)],
    [
      mf(mr()*area.width*3),
      mf(mr()*area.width*3),
      mf(mr()*area.width*-3),
      mf(mr()*area.width*-3)
    ],
    1,
    function(floodfill){
      if (f%wait) return;
      floodfill.pace = mf(mr()*5)
      wait = 60*mc(mr()*5);
      floodfill.queue = [mf(mr()*(area.width*area.height))];
      floodfill.dirs = [
        mf(mr()*area.width*3),
        mf(mr()*area.width*3),
        mf(mr()*area.width*-3),
        mf(mr()*area.width*-3)
      ];
      floodfill.target = density;
      floodfill.replacement = density[mf(mr()*density.length)];
    }
  );

  animation = function(){
    area.render(
      render_options,
      function(){
        
        // flood_fill process
        let fill_a = floodfill_a.process();

        return (
          fill_a > -1 ? fill_a : 
          dn[i]
        );
      }
    );
  }
}

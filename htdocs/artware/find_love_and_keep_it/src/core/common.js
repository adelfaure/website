// Render related
let canvas, context, font, scale, area, render_options;

// Micro variables names for render shader function use
// 
// Area data now, area data index, area data before, x-axis area data, x-axis
// area data, area width, area height
let dn,i,db,x,y,w,h;

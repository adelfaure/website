// BRIGHT 255
// BITTER 192
// SWEET 128
// SOFT 64
// DARK 0



// TEXTOR BITMAP FONT
// ------------------

BitmapFont = function(width,height,encoding,ImageBitmap,loadCharacterData) {
  this.width = width;
  this.height = height;
  this.encoding = encoding;
  this.ImageBitmap = ImageBitmap;
  this.ImageBitmapCanvas = document.createElement("canvas");
  this.ImageBitmapContext = this.ImageBitmapCanvas.getContext("2d");
  this.ImageBitmapCanvas.width = this.ImageBitmap.width;
  this.ImageBitmapCanvas.height = this.ImageBitmap.height;
  this.ImageBitmapContext.drawImage(this.ImageBitmap,0,0);
  this.length = this.encoding.rules.body.name.length;
  this.mapWidth = this.ImageBitmap.width / width;
  this.mapHeight = this.ImageBitmap.height / height;
  this.mapLength = this.mapWidth * this.mapHeight;
  this.characterData = [];
  this.loadCharacterData = loadCharacterData;
}

// TODO REDIRECTION INCOMPATIBLE

BitmapFont.prototype.loadAllCharacterData = function(){
  while (this.characterData.length < this.mapLength) {
    this.getCharacterData(this.characterData.length);
  }
}

BitmapFont.prototype.getCharacterData = function(
  cd,  // Character decimal
){
  //// REDIRECTION
  //if (this.redirections) {
  //  cd = this.redirections[cd*2+1];
  //}

  if (this.characterData[cd]) return this.characterData[cd];

  // Character top-left x-axis coordinate inside image font source
  let cx = (
    (cd % this.mapWidth) * this.width
  );
  // Character top-left y-axis coordinate inside image font source
  let cy = (
    Math.floor(cd / this.mapWidth) * this.height
  )
  // Character width
  let cw = this.width
  // Character height
  let ch = this.height

  let characterData = this.ImageBitmapContext.getImageData(cx,cy,cw,ch);
  characterData.density = 0;
  for (var i = 0; i < characterData.data.length; i++){
    characterData.density += characterData.data[i];
  }

  this.characterData[cd] = characterData;

  return characterData;
}

BitmapFont.prototype.createDotData = function() {
  if (this.characterData.length < this.mapLength) this.loadAllCharacterData();
  this.dotData = [];
  for (var i = 0; i < this.characterData.length; i++){
    this.dotData.push([]);
    for (var j = 0; j < this.characterData[i].data.length; j+=4){
      this.dotData[i].push(
        (
          !this.characterData[i].data[j]
          && !this.characterData[i].data[j+1]
          && !this.characterData[i].data[j+2]
        ) ? 0 : 1
      );
    }
  }
}

BitmapFont.prototype.printDotData = function(i,filler) {
  let str = "";
  let fill_0 = '';
  let fill_1 = '';
  for (var j = 0; j < filler[2]; j++) {
    fill_0 += filler[0];
    fill_1 += filler[1];
  }
  for (var j = 0; j < this.dotData[i].length; j++){
    str += (
      (this.dotData[i][j] ? fill_1 : fill_0 )
      + (
        !((j+1)%this.width) && j < this.dotData[i].length-1 ? '\n' : 
        ''
      )
    );
  }
  return str;
}

BitmapFont.prototype.colorDotData = function(i,color_0,color_1) {
  let coloredDotData = [];
  for (var j = 0; j < this.dotData[i].length; j++){
    coloredDotData = coloredDotData.concat(this.dotData[i][j] ? color_1 : color_0);
  }
  return coloredDotData;
}

BitmapFont.prototype.colorAllDotData = function(colors_number) {
  let new_dotData = [];
  for (var fi = 0; fi < colors_number; fi++) {
    for (var bi = 0; bi < colors_number; bi++) {
      let ci = 0;
      while (ci < this.mapLength) {
        let colored_data = this.colorDotData(
          ci,
          fi,
          bi
        ); 
        ci++;
        new_dotData.push(colored_data);
      }
    }
  }
  this.dotData = new_dotData;
}

BitmapFont.prototype.compressDotData = function(){
  let new_dotData = [];
  let new_dotData_str = [];
  this.redirections = [];
  for (var i = 0; i < this.dotData.length; i++){
    let dotData_str = this.dotData[i].join('');
    let dotData_index = new_dotData_str.indexOf(dotData_str);
    if (dotData_index == -1) {
      this.redirections.push(i);
      this.redirections.push(new_dotData.length);
      new_dotData.push(this.dotData[i]);
      new_dotData_str.push(dotData_str);
    } else {
      this.redirections.push(i);
      this.redirections.push(dotData_index);
    }
  }
  this.dotData = new_dotData;
}

BitmapFont.prototype.drawDotData = function(i,colors) {
  let color_data = new Uint8ClampedArray(this.width*this.height*4);
  for (var j = 0; j < this.dotData[i].length; j++){
    color_data[j*4]   = colors[this.dotData[i][j]][0];
    color_data[j*4+1] = colors[this.dotData[i][j]][1];
    color_data[j*4+2] = colors[this.dotData[i][j]][2];
    color_data[j*4+3] = colors[this.dotData[i][j]][3];
  }
  return new ImageData(color_data,this.width,this.height);
}

BitmapFont.prototype.redirectTo = function(ci) {
  return font.redirections[ci*2+1];
}
    

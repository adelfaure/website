// Save 
function save(data, filename, type) {
  var file = new Blob(data, {type: type});
  var a = document.createElement("a"), url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);  
  }, 0); 
}

// Micro notation
function ma(val){
  return Math.abs(val);
}
function mf(val){
  return Math.floor(val);
}
function mc(val){
  return Math.ceil(val);
}
function ms(val){
  return Math.sin(val);
}
function mn(val){
  return Math.round(val);
}
function mi(val){
  return Math.sign(val);
}
function mo(val){
  return Math.cos(val);
}
function mr(){
  return Math.random();
}

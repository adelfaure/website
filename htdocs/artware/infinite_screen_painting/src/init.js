function init() {
  // Render output (Canvas)
  canvas = document.getElementById("canvas");
  context = canvas.getContext("2d");
  
  // Font
  let bitmap_font = document.getElementById("color_ibm_vga8x16_compressed.png");
  let encoding = new Encoding(color_cp437_rules);
  font = new BitmapFont(8,16,encoding,bitmap_font,true);
  font.redirections = JSON.parse('['+document.getElementById("color_ibm_vga8x16_redirections.txt").textContent+']'); 
  
  // Scene
  x_scale = mc(window.innerWidth/font.width/80);
  y_scale = mc(window.innerHeight/font.height/40);
  scale = x_scale < y_scale ? x_scale : y_scale;
  let sh = mf(window.innerHeight/font.height/scale);
  let sw = mf(window.innerWidth/font.width/scale);
  
  // Area 
  area = new Area(sw,sh);
  let state_area_before = new Area(sw,sh);
  state_before_area = new Area(sw,sh);
  render_options = [
    "bitmapCharacterRender",// Character rendering type
    context,                // Web Canvas API CanvasRenderingContext2D
    font,                   // TextorBitmapFont
    state_before_area
  ];
}

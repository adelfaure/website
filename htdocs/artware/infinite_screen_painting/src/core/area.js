
// TEXTOR AREA
// -----------

// Area represents data of an area

Area = function(
  width,  // area width
  height, // area height
){
  this.width = width;
  this.height = height;
  this.data = new Uint16Array(width*height); // character area data
}

Area.prototype.place = function(area,i,pos_x,pos_y,gap_x,gap_y,transparency) {
  // origin x
  let ox = (pos_x*-1)%(this.width + gap_x) + this.width + gap_x;
  // origin y
  let oy = (pos_y*-1)%(this.height + gap_y) + this.height + gap_y;
  // area x
  let ax = i%area.width + ox;
  // area y
  let ay = Math.floor(i/area.width) + oy;
  // data x
  let dx = ax%(this.width + gap_x);
  // data y
  let dy = ay%(this.height + gap_y)*this.width;
  // data index
  let di = dx+dy;
  return (
    dx >= this.width ? 0 :
    this.data[di] == transparency ? 0 :
    this.data[di]
  )
}

Area.prototype.fitCanvas = function(
  scale,
  canvas,
  font
) {
  canvas.width = this.width * font.width;
  canvas.height = this.height * font.height;

  canvas.style.transformOrigin = "top left";
  canvas.style.transform = "scale("+scale+")";
}

Area.prototype.render = function(
  options, // Render options
  shader,  // A shader function to apply
) {

  // Iterate through data area
  for (i = 0; i < this.data.length; i++) {

    // Area x-axis coordinate
    x = i % this.width
    // Area y-axis coordinate
    y = Math.floor(i / this.width)

    let before_state = options[3] ? options.pop() : false;

    // Apply shader to data
    if (shader) {
      dn = this.data;
      db = before_state ? before_state.data : 0;
      w = this.width;
      h = this.height;
      this.data[i] = shader();
    }

    // Look at before data, if same continue because there is no change to render
    if (before_state && this.data[i] == before_state.data[i]) continue;
    
    // Apply changes to before area for next render optimization
    if (before_state) before_state.data[i] = this.data[i];
    
    // TODO REPHRASE
    // Positionned data will be interpreted as character data. This data
    // will correspond to source sub-image character shape
    let characterData = this.data[i];

    this[options[0]](...[i,x,y,characterData].concat(options.slice(1)));
  }
}

// TODO REPHRASE
// Area.bitmapRender method paints data as  font characters onto
// given Web Canvas

Area.prototype.bitmapCharacterRender = function(
  i,              // Data index
  x,              // Area x-axis
  y,              // Area y-axis
  characterData,
  context,        // Web Canvas API CanvasRenderingContext2D
  font,           // BitmapFont
){

  // REDIRECTION
  if (font.redirections) {
    characterData = font.redirections[characterData*2+1];
  }

  // Character sub-image source (font ImageBitmap)
  // x-axis coordinate
  let sx = (
    (characterData % font.mapWidth) * font.width
  );
  // y-axis coordinate
  let sy = (
    Math.floor(characterData / font.mapWidth) * font.height
  )
  // sub-rectangle width
  let sw = font.width
  // sub-rectangle height
  let sh = font.height

  // Character sub-image destination (context Canvas). Character bitmap
  // render position will correspond to data area position scaled by font
  // size and ratio
  // x-axis coordinate
  let dx = x * font.width
  // y-axis coordinate
  let dy = y * font.height
  // sub-rectangle width
  let dw = font.width
  // sub-rectangle height
  let dh = font.height

  // Render character sub-image into context
  if (font.loadCharacterData) {
    context.putImageData(font.getCharacterData(characterData),dx,dy);
  } else {
    context.drawImage(font.ImageBitmap,sx,sy,sw,sh,dx,dy,dw,dh);
  }
}

Area.prototype.txtExport = function(font,file_name){
  // export as lone surrogates chars between 0 and 65535
  let txt = String.fromCharCode(this.width,this.height);
  let last_charCode = false;
  let charCode_repeat = 0;
  for (var i = 0; i < this.data.length; i++){
    let charCode = font.redirectTo(this.data[i]);
    txt += String.fromCharCode(charCode);
    if (charCode != last_charCode) {
      console.log(charCode_repeat);
    }
    charCode_repeat = charCode == last_charCode ? charCode_repeat + 1 : 0;
    last_charCode = charCode;
  }
  console.log(txt);
  //save([txt],file_name,"text/plain");
}

console.log("keys.js");

let keys = {};
let keys_functions = {};

function keys_process(frame){
  for (var i in keys) {
    if (keys[i] && keys_functions[i]) {
      keys_functions[i]();
    }
  }
}

document.addEventListener("keydown",function(e){
  keys[e.key] = true;
});

document.addEventListener("keyup",function(e){
  keys[e.key] = false;
});


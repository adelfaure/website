console.log("textmode data");
// glyph, foreground, background, mode : semigraphic, horizontal flip, vertical flip, rotate
// 0-199, 0-40, 0-40, 0-15
// S0H0V0R0 = 0
// S1H0V0R0 = 1
// S1H1V0R0 = 2
// S1H1V1R0 = 3
// S1H0V1R0 = 4
// S1H1V1R1 = 5
// S1H0V1R1 = 6
// S1H1V0R1 = 7
// S1H0V0R1 = 8
// S0H1V0R0 = 9
// S0H1V1R0 = 10
// S0H1V1R1 = 11
// S0H1V0R1 = 12
// S0H0V1R0 = 13
// S0H0V1R1 = 14
// S0H0V0R1 = 15


TextmodeData = function(data,width,height) {
  this.width = width;
  this.height = height;
  this.data = data;
  this.data_printed = new Uint8ClampedArray(data);
}

TextmodeData.prototype.read = function(x,y) {
  return [
    this.data[(x+y*this.width)*4],
    this.data[(x+y*this.width)*4+1],
    this.data[(x+y*this.width)*4+2],
    this.data[(x+y*this.width)*4+3],
  ]
}

TextmodeData.prototype.write = function(x,y,glyph,foreground,background,mode) {
  this.data[(x+y*this.width)*4] = glyph;
  this.data[(x+y*this.width)*4+1] = foreground;
  this.data[(x+y*this.width)*4+2] = background;
  this.data[(x+y*this.width)*4+3] = mode;
}

TextmodeData.prototype.write_printed = function(x,y,glyph,foreground,background,mode) {
  this.data_printed[(x+y*this.width)*4] = glyph;
  this.data_printed[(x+y*this.width)*4+1] = foreground;
  this.data_printed[(x+y*this.width)*4+2] = background;
  this.data_printed[(x+y*this.width)*4+3] = mode;
}

TextmodeData.prototype.read_printed = function(x,y) {
  return [
    this.data_printed[(x+y*this.width)*4],
    this.data_printed[(x+y*this.width)*4+1],
    this.data_printed[(x+y*this.width)*4+2],
    this.data_printed[(x+y*this.width)*4+3],
  ]
}

TextmodeData.prototype.print = function(
  image_data,
  text_patterns,
  semigraphic_patterns,
  pixel_values,
  start_x,
  start_y,
  at_x,
  at_y,
  width,
  height
) {
  let pattern_data = this.read(at_x,at_y);
  if (
    !pattern_data[0]&&!pattern_data[1]&&!pattern_data[2]&&!pattern_data[3]
  ) return;
  let printed_pattern_data = this.read_printed(at_x,at_y);
  if (
    pattern_data[0] == printed_pattern_data[0] &&
    pattern_data[1] == printed_pattern_data[1] &&
    pattern_data[2] == printed_pattern_data[2] &&
    pattern_data[3] == printed_pattern_data[3]
  ) return;
  this.write_printed(at_x,at_y,pattern_data[0],pattern_data[1],pattern_data[2],pattern_data[3]);
  print_pattern(
    image_data,
    text_patterns,
    semigraphic_patterns,
    pixel_values,
    start_x + at_x * width,
    start_y + at_y * height,
    width,
    height,
    pattern_data
  );
};
TextmodeData.prototype.print_zone = function(
  image_data,
  text_patterns,
  semigraphic_patterns,
  pixel_values,
  start_x,
  start_y,
  start_at_x,
  start_at_y,
  end_at_x,
  end_at_y,
  width,
  height,
) {
  let at_y = start_at_y;
  while (at_y++ < end_at_y-1) {
    let at_x = start_at_x;
    if (at_y < 0 || at_y > this.height-1) continue;
    while (at_x++ < end_at_x-1) {
      if (at_x < 0 || at_x > this.width-1) continue;
      this.print(
        image_data,
        text_patterns,
        semigraphic_patterns,
        pixel_values,
        start_x,
        start_y,
        at_x,
        at_y,
        width,
        height
      );
    }
  }
}

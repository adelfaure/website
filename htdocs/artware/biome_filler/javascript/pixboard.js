console.log("pixboard.js");

Pixboard = function(width,height,scale) {
  this.canvas = document.createElement("canvas");
  this.canvas.style.transformOrigin = "top left";
  this.canvas.style.imageRendering = "pixelated";
  this.canvas.style.transform = "scale("+scale+")";
  this.canvas.width = Math.floor(width/scale);
  this.canvas.height = Math.floor(height/scale);
  this.context = this.canvas.getContext('2d');
  this.imageData = this.context.getImageData(0,0,this.canvas.width,this.canvas.height);
  document.body.appendChild(this.canvas);
}

Pixboard.prototype.print = function() {
  this.context.putImageData(this.imageData,0,0);
}  

Pixboard.prototype.setpixel = function (x,y,pixel_values) {
  this.imageData.data[(x+y*this.imageData.width)*4] = pixel_values[0];
  this.imageData.data[(x+y*this.imageData.width)*4+1] = pixel_values[1];
  this.imageData.data[(x+y*this.imageData.width)*4+2] = pixel_values[2];
  this.imageData.data[(x+y*this.imageData.width)*4+3] = pixel_values[3];
}
// PATTERNS
Pixboard.prototype.print_pattern = function(
  pattern,
  fg_color,
  bg_color,
  start_x,
  start_y
) {
  
  let yi = -1;
  while (yi++ < pattern.length-1) {
    let xi = -1;
    while (xi++ < pattern[yi].length-1) {
      this.setpixel(
        start_x+xi,
        start_y+yi,
        [
          pattern[yi][xi] ? fg_color[0] : bg_color[0],
          pattern[yi][xi] ? fg_color[1] : bg_color[1],
          pattern[yi][xi] ? fg_color[2] : bg_color[2],
          pattern[yi][xi] ? fg_color[3] : bg_color[3],
        ]
      );
    }
  }
}

console.log("math.js");

// Random
function MR() {
  return Math.random();
}

// Random between
function MRB(min,max) {
  return MR() * (max - min) + min;
}

//Random index from array
function MRIA(array) {
  return MF(MR()*array.length);
}

// Random from array
function MRA(array) {
  return array[MRIA(array)];
}

// Pick from array (or delete)
function MPA(array,index) {
  let pick = array.splice(index, 1);
  return pick[0];
}

// Random pick from array
function MRPA(array) {
  return MPA(array,MRIA(array));
}

// Luck
function ML(difficulty) {
  difficulty = difficulty == undefined ? 0.5 : difficulty;
  return MR() > difficulty;
}

// Shuffle array
function MS(array) {
  let new_array = [];
  while (array.length) {
    new_array.push(MRPA(array));
  }
  return new_array;
}

// Move array
function MM(array,dir) {
  if (dir) {
    array.push(array.shift());
  } else {
    array.unshift(array.pop());
  }
}

// Floor
function MF(val) {
  return Math.floor(val);
}

// Ceil
function MC(val) {
  return Math.ceil(val);
}


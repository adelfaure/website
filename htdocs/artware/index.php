<!DOCTYPE html>
<!--
<?php include '../html/license.txt'; ?>
-->
<html>
  <head>
    <title>Adel Faure ‒ Artware</title>
    <!-- search engines -->
    <meta charset="utf-8">
    <meta name="description" content="ASCII art games, fonts, artworks and livecoded lofi techno. Love for textmode, computer art and net art <3. GNU GPL, SIL open font, creative commons."/>
    <meta name="keywords" content="Text mode, ASCII art, Fonts, Computer art, Lofi">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- twitter card -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@adelfaure" />
    <meta name="twitter:title" content="Adel Faure ‒ Games, music, tools, fonts, ASCII art" />
    <meta name="twitter:description" content="ASCII art games, fonts, artworks and livecoded lofi techno. Love for textmode, computer art and net art <3. GNU GPL, SIL open font, creative commons." />
    <meta name="twitter:image" content="../card_image_small.png" />
    <link href="../src/style.css" rel="stylesheet">
  </head>
  <body style="display:none;">
    <main>
<pre>
<?php include '../html/header.html'; ?>
================================================================================
< <a href="../">Back</a>             _____ _____ _____ _____ _____ _____ _____ 
                  |  _  |  _  |     |  |  |  _  |  _  |   __|
                  |     |    -|_   _| / \ |     |    -|   __|
                  |__|__|__\__||___||_____|__|__|__\__|_____|

================================================================================
<a href="./face_study" >                                                                                
                F             A              C              E                   
                                                                                
       .---.         .---.          .---.          .---.           .---.        
      ( _ _ )       /_ _  )        /_    \        /     \         (     )       
      [ °|° ]       )°)°  ]        )°   ]/        )  ]  /         [     ]       
       \ - /        \ - /|         \- ,´|         \ ;  !           L : J        
       |`-´|         `┬´  \         `¬   L         `)   \          /   \        
   _.-´_   _`-._    .-'  _.`._      .┘ _..\      _.´._   `.    _.-´_   _`-._    
                                                                                
         S             T              U              D               Y          
                                                                                </a>
================================================================================
<a href="./infinite_screen_painting" >                                                                                
                I N F I N I T E   S C R E E N   P A I N T I N G                 
  ----------------------------------------------------------------------------  
  ▏-----------------:    \       ████████████████████ ██ \█  █ ██████████████▕  
  ▏-----------------             ██████████████████ ██ \█  \ \█ █████████████▕  
  ▏----------------:             ████████████████ ██ \█  \ \\  █ ████████████▕  
  ▏----------------              ██████████████ ██ \█  \ \\\ \ \█ ███████████▕  
  ▏---------------:               ███████████ ██ \█  \ \\\ \\\\  █ ██████████▕  
  ▏---------------                █████████ ██ \█  \ \\\ \\\\\ \ \█ █████████▕  
  ▏--------------:                ███████ ██ \█  \ \\\ \\\\\\\\\\  █ ████████▕  
  ▏--------------                  ████ ██ \█  \ \\\ \\\\\\\\\\\ \ \█ ███████▕  
  ▏-------------:               █████ ██ \█  \ \\\ \\\\\\\\\\\\\\\\  █ ██████▕  
  ▏-------------                ███ ██ \█  \ \\\ \\\\\\\\\\\\\\\\\ \ \█ █████▕  
  ▏------------:               ██ ███\█  \ \\\ \\\\\\\\\\\\\\\\\\\\\\  █ ████▕  
  ▏------------                 ███\█  \ \\\ \\\\\\\\\\\\\\\\\\\\\\\ \ \█ ███▕  
  ▏-----------:               ███\█  \█\\\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\  █ ██▕  
  ▏-----------                █\█  \█\\\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \ \█ █▕  
  ▏----------:               ██  \█\\\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  █ ▕  
  ▏----------                █ ██\\\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \ \█▕  
  ----------------------------------------------------------------------------  
                                                                                </a>
================================================================================
<a href="./find_love_and_keep_it" >                                                                                
    ♥♥♥♥♥♥♥  ♥♥  ♥♥    ♥♥  ♥♥♥♥♥        ♥♥        ♥♥♥♥    ♥♥     ♥♥  ♥♥♥♥♥♥     
    ♥♥       ♥♥  ♥♥♥♥  ♥♥  ♥♥   ♥♥      ♥♥      ♥♥    ♥♥  ♥♥     ♥♥  ♥♥         
    ♥♥♥♥♥    ♥♥  ♥♥  ♥♥♥♥  ♥♥   ♥♥      ♥♥      ♥♥    ♥♥    ♥♥  ♥♥   ♥♥♥♥       
    ♥♥       ♥♥  ♥♥    ♥♥  ♥♥   ♥♥      ♥♥      ♥♥    ♥♥    ♥♥  ♥♥   ♥♥         
    ♥♥       ♥♥  ♥♥    ♥♥  ♥♥♥♥♥        ♥♥♥♥♥♥    ♥♥♥♥        ♥♥     ♥♥♥♥♥♥     
                                                                                
         ♥♥♥    ♥♥    ♥♥  ♥♥♥♥♥        ♥♥    ♥♥  ♥♥♥♥♥♥  ♥♥♥♥♥♥  ♥♥♥♥♥          
       ♥♥   ♥♥  ♥♥♥♥  ♥♥  ♥♥   ♥♥      ♥♥  ♥♥    ♥♥      ♥♥      ♥♥   ♥♥        
       ♥♥♥♥♥♥♥  ♥♥  ♥♥♥♥  ♥♥   ♥♥      ♥♥♥♥      ♥♥♥♥    ♥♥♥♥    ♥♥♥♥♥          
       ♥♥   ♥♥  ♥♥    ♥♥  ♥♥   ♥♥      ♥♥  ♥♥    ♥♥      ♥♥      ♥♥             
       ♥♥   ♥♥  ♥♥    ♥♥  ♥♥♥♥♥        ♥♥    ♥♥  ♥♥♥♥♥♥  ♥♥♥♥♥♥  ♥♥             
                                                                                
                                   ♥♥  ♥♥♥♥♥♥♥♥                                 
                                   ♥♥     ♥♥                                    
                                   ♥♥     ♥♥                                    
                                   ♥♥     ♥♥                                    
                                   ♥♥     ♥♥                                    
                                                                                </a>
================================================================================
<a href="./smooth_web" >     J)        (L                                              J)        (L     
    //          \\                                            //          \\    
   ll     __     ll                                          ll     __     ll   
   ll   .´  `.   ll                                          ll   .´  `.   ll   
   L\_ /  ::  \ _/J             _  _  _  _ ___               L\_ /  ::  \ _/J   
J)  `-l  :  :  l-´  (L         |_ |||| || | | |_|         J)  `-l  :  :  l-´  (L
\\__  l: :  : :l  __//          _|| ||_||_| | | |         \\__  l: :  : :l  __//
 '·-===`._::_.´===--'                  _  _                '·-===`._::_.´===--' 
     .´/: "" :\`.                  | ||_ |_|                   .´/: "" :\`.     
   .:·´L .··. J`·:.                ||||_ |_|                 .:·´L .··. J`·:.   
  //  //(o88o)\\  \\                ¯                       //  //(o88o)\\  \\  
 l/   ll LJLJ ll   \l                                      l/   ll LJLJ ll   \l 
 ll   ll      ll   ll                                      ll   ll      ll   ll 
 J)   ll      ll   (L                                      J)   ll      ll   (L 
       \\    //                                                  \\    //       
        J)  (L                                                    J)  (L        </a>
================================================================================
<a href="./biome_filler" >/\  /\  /\  /\  /\  /\  /\  /\  /\            /\  /\  /\  /\  /\  /\  /\  /\  /\
  \/  \/  \/  \/  \/  \/  \/  \/  \          /  \/  \/  \/  \/  \/  \/  \/  \/  
  \/  \/  \/  \/  \/  \/  \/  \/  \  Biome   /  \/  \/  \/  \/  \/  \/  \/  \/  
  \/  \/  \/  \/  \/  \/  \/  \/  \          /  \/  \/  \/  \/  \/  \/  \/  \/  
__\/__\/__\/__\/__\/__\/__\/__\/__\  Filler  /__\/__\/__\/__\/__\/__\/__\/__\/__
||  ||  ||  ||  ||  ||  ||  ||  ||            ||  ||  ||  ||  ||  ||  ||  ||  ||
||  ||  ||  ||  ||  ||  ||  ||  ||            ||  ||  ||  ||  ||  ||  ||  ||  ||</a>
<?php include '../html/footer.html'; ?>
</pre>
  </body>
  <script src="../src/script.js"></script>
</html>

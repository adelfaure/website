// INIT

let char_width = 14.5;
let char_height = 28;

let keys = {};

document.addEventListener("keydown",function(e){
	render_spider = render_spider < 2 ? render_spider + 1 : 0;
  keys[e.key] = true;
});
document.addEventListener("keyup",function(e){
  keys[e.key] = false;
});

let keys_process = start_animation_event(1,function(frame){
  //view_x+=0.5;
  if (keys["ArrowRight"]) view_x+=1;
  if (keys["ArrowLeft"]) view_x-=1;
  if (keys["ArrowDown"]) view_y+=0.5;
  if (keys["ArrowUp"]) view_y-=0.5;
});

let mouse_x = 0;
let mouse_y = 0;

document.addEventListener("mousemove",function(e){
	mouse_x = e.clientX;
	mouse_y = e.clientY;
});

// LINE

// function get_angle(a,b) {
//   var dy = b[1] - a[1];
//   var dx = b[0] - a[0];
//   var theta = Math.atan2(dy, dx); // range (-PI, PI]
//   theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
//   //if (theta < 0) theta = 360 + theta; // range [0, 360)
//   return theta;
// }
// 
// // Degrees to direction
// function angle_to_dir(degrees){
//   return [Math.sin(degrees*(Math.PI/180)),Math.cos(degrees*(Math.PI/180))];
// }

let sub_chars = ['⠀', '⠁', '⠂', '⠃', '⠄', '⠅', '⠆', '⠇', '⠈', '⠉', '⠊', '⠋', '⠌', '⠍', '⠎', '⠏', '⠐', '⠑', '⠒', '⠓', '⠔', '⠕', '⠖', '⠗', '⠘', '⠙', '⠚', '⠛', '⠜', '⠝', '⠞', '⠟', '⠠', '⠡', '⠢', '⠣', '⠤', '⠥', '⠦', '⠧', '⠨', '⠩', '⠪', '⠫', '⠬', '⠭', '⠮', '⠯', '⠰', '⠱', '⠲', '⠳', '⠴', '⠵', '⠶', '⠷', '⠸', '⠹', '⠺', '⠻', '⠼', '⠽', '⠾', '⠿', '⡀', '⡁' ];

function sub_char(from,to) {
	if (to[0] == 0   && to[1] == 0    && from[0] == 0.5  && from[1] == 0    || from[0] == 0   && from[1] == 0    && to[0] == 0.5  && to[1] == 0    ) return sub_chars[ 0];
	if (to[0] == 0   && to[1] == 0    && from[0] == 1    && from[1] == 0    || from[0] == 0   && from[1] == 0    && to[0] == 1    && to[1] == 0    ) return sub_chars[ 1];
	if (to[0] == 0   && to[1] == 0    && from[0] == 1    && from[1] == 0.25 || from[0] == 0   && from[1] == 0    && to[0] == 1    && to[1] == 0.25 ) return sub_chars[ 2];
	if (to[0] == 0   && to[1] == 0    && from[0] == 1    && from[1] == 0.5  || from[0] == 0   && from[1] == 0    && to[0] == 1    && to[1] == 0.5  ) return sub_chars[ 3];
	if (to[0] == 0   && to[1] == 0    && from[0] == 1    && from[1] == 0.75 || from[0] == 0   && from[1] == 0    && to[0] == 1    && to[1] == 0.75 ) return sub_chars[ 4];
	if (to[0] == 0   && to[1] == 0    && from[0] == 1    && from[1] == 1    || from[0] == 0   && from[1] == 0    && to[0] == 1    && to[1] == 1    ) return sub_chars[ 5];
	if (to[0] == 0   && to[1] == 0    && from[0] == 0.5  && from[1] == 1    || from[0] == 0   && from[1] == 0    && to[0] == 0.5  && to[1] == 1    ) return sub_chars[ 6];
	if (to[0] == 0   && to[1] == 0    && from[0] == 0    && from[1] == 1    || from[0] == 0   && from[1] == 0    && to[0] == 0    && to[1] == 1    ) return sub_chars[ 7];
	if (to[0] == 0   && to[1] == 0    && from[0] == 0    && from[1] == 0.75 || from[0] == 0   && from[1] == 0    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[ 8];
	if (to[0] == 0   && to[1] == 0    && from[0] == 0    && from[1] == 0.5  || from[0] == 0   && from[1] == 0    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[ 9];
	if (to[0] == 0   && to[1] == 0    && from[0] == 0    && from[1] == 0.25 || from[0] == 0   && from[1] == 0    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[10];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 1    && from[1] == 0    || from[0] == 0.5 && from[1] == 0    && to[0] == 1    && to[1] == 0    ) return sub_chars[11];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 1    && from[1] == 0.25 || from[0] == 0.5 && from[1] == 0    && to[0] == 1    && to[1] == 0.25 ) return sub_chars[12];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 1    && from[1] == 0.5  || from[0] == 0.5 && from[1] == 0    && to[0] == 1    && to[1] == 0.5  ) return sub_chars[13];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 1    && from[1] == 0.75 || from[0] == 0.5 && from[1] == 0    && to[0] == 1    && to[1] == 0.75 ) return sub_chars[14];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 1    && from[1] == 1    || from[0] == 0.5 && from[1] == 0    && to[0] == 1    && to[1] == 1    ) return sub_chars[15];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 0.5  && from[1] == 1    || from[0] == 0.5 && from[1] == 0    && to[0] == 0.5  && to[1] == 1    ) return sub_chars[16];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 0    && from[1] == 1    || from[0] == 0.5 && from[1] == 0    && to[0] == 0    && to[1] == 1    ) return sub_chars[17];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 0    && from[1] == 0.75 || from[0] == 0.5 && from[1] == 0    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[18];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 0    && from[1] == 0.5  || from[0] == 0.5 && from[1] == 0    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[19];
	if (to[0] == 0.5 && to[1] == 0    && from[0] == 0    && from[1] == 0.25 || from[0] == 0.5 && from[1] == 0    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[20];
	if (to[0] == 1   && to[1] == 0    && from[0] == 1    && from[1] == 0.25 || from[0] == 1   && from[1] == 0    && to[0] == 1    && to[1] == 0.25 ) return sub_chars[21];
	if (to[0] == 1   && to[1] == 0    && from[0] == 1    && from[1] == 0.5  || from[0] == 1   && from[1] == 0    && to[0] == 1    && to[1] == 0.5  ) return sub_chars[22];
	if (to[0] == 1   && to[1] == 0    && from[0] == 1    && from[1] == 0.75 || from[0] == 1   && from[1] == 0    && to[0] == 1    && to[1] == 0.75 ) return sub_chars[23];
	if (to[0] == 1   && to[1] == 0    && from[0] == 1    && from[1] == 1    || from[0] == 1   && from[1] == 0    && to[0] == 1    && to[1] == 1    ) return sub_chars[24];
	if (to[0] == 1   && to[1] == 0    && from[0] == 0.5  && from[1] == 1    || from[0] == 1   && from[1] == 0    && to[0] == 0.5  && to[1] == 1    ) return sub_chars[25];
	if (to[0] == 1   && to[1] == 0    && from[0] == 0    && from[1] == 1    || from[0] == 1   && from[1] == 0    && to[0] == 0    && to[1] == 1    ) return sub_chars[26];
	if (to[0] == 1   && to[1] == 0    && from[0] == 0    && from[1] == 0.75 || from[0] == 1   && from[1] == 0    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[27];
	if (to[0] == 1   && to[1] == 0    && from[0] == 0    && from[1] == 0.5  || from[0] == 1   && from[1] == 0    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[28];
	if (to[0] == 1   && to[1] == 0    && from[0] == 0    && from[1] == 0.25 || from[0] == 1   && from[1] == 0    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[29];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 1    && from[1] == 0.5  || from[0] == 1   && from[1] == 0.25 && to[0] == 1    && to[1] == 0.5  ) return sub_chars[30];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 1    && from[1] == 0.75 || from[0] == 1   && from[1] == 0.25 && to[0] == 1    && to[1] == 0.75 ) return sub_chars[31];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 1    && from[1] == 1    || from[0] == 1   && from[1] == 0.25 && to[0] == 1    && to[1] == 1    ) return sub_chars[32];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 0.5  && from[1] == 1    || from[0] == 1   && from[1] == 0.25 && to[0] == 0.5  && to[1] == 1    ) return sub_chars[33];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 0    && from[1] == 1    || from[0] == 1   && from[1] == 0.25 && to[0] == 0    && to[1] == 1    ) return sub_chars[34];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 0    && from[1] == 0.75 || from[0] == 1   && from[1] == 0.25 && to[0] == 0    && to[1] == 0.75 ) return sub_chars[35];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 0    && from[1] == 0.5  || from[0] == 1   && from[1] == 0.25 && to[0] == 0    && to[1] == 0.5  ) return sub_chars[36];
	if (to[0] == 1   && to[1] == 0.25 && from[0] == 0    && from[1] == 0.25 || from[0] == 1   && from[1] == 0.25 && to[0] == 0    && to[1] == 0.25 ) return sub_chars[37];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 1    && from[1] == 0.75 || from[0] == 1   && from[1] == 0.5  && to[0] == 1    && to[1] == 0.75 ) return sub_chars[38];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 1    && from[1] == 1    || from[0] == 1   && from[1] == 0.5  && to[0] == 1    && to[1] == 1    ) return sub_chars[39];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 0.5  && from[1] == 1    || from[0] == 1   && from[1] == 0.5  && to[0] == 0.5  && to[1] == 1    ) return sub_chars[40];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 0    && from[1] == 1    || from[0] == 1   && from[1] == 0.5  && to[0] == 0    && to[1] == 1    ) return sub_chars[41];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 0    && from[1] == 0.75 || from[0] == 1   && from[1] == 0.5  && to[0] == 0    && to[1] == 0.75 ) return sub_chars[42];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 0    && from[1] == 0.5  || from[0] == 1   && from[1] == 0.5  && to[0] == 0    && to[1] == 0.5  ) return sub_chars[43];
	if (to[0] == 1   && to[1] == 0.5  && from[0] == 0    && from[1] == 0.25 || from[0] == 1   && from[1] == 0.5  && to[0] == 0    && to[1] == 0.25 ) return sub_chars[44];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 1    && from[1] == 1    || from[0] == 1   && from[1] == 0.75 && to[0] == 1    && to[1] == 1    ) return sub_chars[45];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 0.5  && from[1] == 1    || from[0] == 1   && from[1] == 0.75 && to[0] == 0.5  && to[1] == 1    ) return sub_chars[46];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 0    && from[1] == 1    || from[0] == 1   && from[1] == 0.75 && to[0] == 0    && to[1] == 1    ) return sub_chars[47];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 0    && from[1] == 0.75 || from[0] == 1   && from[1] == 0.75 && to[0] == 0    && to[1] == 0.75 ) return sub_chars[48];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 0    && from[1] == 0.5  || from[0] == 1   && from[1] == 0.75 && to[0] == 0    && to[1] == 0.5  ) return sub_chars[49];
	if (to[0] == 1   && to[1] == 0.75 && from[0] == 0    && from[1] == 0.25 || from[0] == 1   && from[1] == 0.75 && to[0] == 0    && to[1] == 0.25 ) return sub_chars[50];
	if (to[0] == 1   && to[1] == 1    && from[0] == 0.5  && from[1] == 1    || from[0] == 1   && from[1] == 1    && to[0] == 0.5  && to[1] == 1    ) return sub_chars[51];
	if (to[0] == 1   && to[1] == 1    && from[0] == 0    && from[1] == 1    || from[0] == 1   && from[1] == 1    && to[0] == 0    && to[1] == 1    ) return sub_chars[52];
	if (to[0] == 1   && to[1] == 1    && from[0] == 0    && from[1] == 0.75 || from[0] == 1   && from[1] == 1    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[53];
	if (to[0] == 1   && to[1] == 1    && from[0] == 0    && from[1] == 0.5  || from[0] == 1   && from[1] == 1    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[54];
	if (to[0] == 1   && to[1] == 1    && from[0] == 0    && from[1] == 0.25 || from[0] == 1   && from[1] == 1    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[55];
	if (to[0] == 0.5 && to[1] == 1    && from[0] == 0    && from[1] == 1    || from[0] == 0.5 && from[1] == 1    && to[0] == 0    && to[1] == 1    ) return sub_chars[56];
	if (to[0] == 0.5 && to[1] == 1    && from[0] == 0    && from[1] == 0.75 || from[0] == 0.5 && from[1] == 1    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[57];
	if (to[0] == 0.5 && to[1] == 1    && from[0] == 0    && from[1] == 0.5  || from[0] == 0.5 && from[1] == 1    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[58];
	if (to[0] == 0.5 && to[1] == 1    && from[0] == 0    && from[1] == 0.25 || from[0] == 0.5 && from[1] == 1    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[59];
	if (to[0] == 0   && to[1] == 1    && from[0] == 0    && from[1] == 0.75 || from[0] == 0   && from[1] == 1    && to[0] == 0    && to[1] == 0.75 ) return sub_chars[60];
	if (to[0] == 0   && to[1] == 1    && from[0] == 0    && from[1] == 0.5  || from[0] == 0   && from[1] == 1    && to[0] == 0    && to[1] == 0.5  ) return sub_chars[61];
	if (to[0] == 0   && to[1] == 1    && from[0] == 0    && from[1] == 0.25 || from[0] == 0   && from[1] == 1    && to[0] == 0    && to[1] == 0.25 ) return sub_chars[62];
	if (to[0] == 0   && to[1] == 0.75 && from[0] == 0    && from[1] == 0.5  || from[0] == 0   && from[1] == 0.75 && to[0] == 0    && to[1] == 0.5  ) return sub_chars[63];
	if (to[0] == 0   && to[1] == 0.75 && from[0] == 0    && from[1] == 0.25 || from[0] == 0   && from[1] == 0.75 && to[0] == 0    && to[1] == 0.25 ) return sub_chars[64];
	if (to[0] == 0   && to[1] == 0.5  && from[0] == 0    && from[1] == 0.25 || from[0] == 0   && from[1] == 0.5  && to[0] == 0    && to[1] == 0.25 ) return sub_chars[65];
	return '#';
}

function h_node(val) {
	return (Math.round(val * 2)/2);
}
function v_node(val) {
	return (Math.round(val * 4)/4);
}

function calc_slope(a,b) {
	return (a[1] - b[1]) / (a[0] - b[0]);
}

function y_SAX(slope,a,x) {
	return slope * (x - a[0]) + a[1];
}

// y = slope * (x - aX) + aY
// slope * (x - aX) + aY = y
// (slope * x) - (slope * aX) + aY = y
// (slope * x) - (slope * aX) = y - aY
// (slope * x) = y - aY + (slope * aX)
// x = (y - aY + (slope * aX)) / slope

function x_SAY(slope,a,y) {
	return (slope * a[0] + y - a[1])/slope;
}

function draw_line(a,b){
	let dx;
	let dy;
	let breaks = [];
	let slope = calc_slope(a,b);	

	dx = b[0] - a[0];
	if (dx > 0) {
		let start_x = Math.ceil(a[0]);
		for (var i = start_x; i < dx+start_x; i++) {
			let y = y_SAX(slope,a,i);
			let node = [h_node(i),v_node(y)];
			if (node[0] > b[0]) continue;
			breaks.push(node);
		}
	} else {
		let start_x = Math.floor(a[0]);
		for (var i = start_x; i > dx+start_x; i--) {
			let y = y_SAX(slope,a,i);
			let node = [h_node(i),v_node(y)];
			if (node[0] < b[0]) continue;
			breaks.push(node);
		}
	}

	dy = b[1] - a[1];
	if (dy > 0) {
		let start_y = Math.ceil(a[1]);
		for (var i = start_y; i < dy+start_y; i++) {
			let x = x_SAY(slope,a,i);
			let node = [h_node(x),v_node(i)];
			if (node[1] > b[1]) continue;
			breaks.push(node);
		}
	} else {
		let start_y = Math.floor(a[1]);
		for (var i = start_y; i > dy+start_y; i--) {
			let x = x_SAY(slope,a,i);
			let node = [h_node(x),v_node(i)];
			if (node[1] < b[1]) continue;
			breaks.push(node);
		}
	}
	breaks.sort(function(a,b) {
		return a[0] == b[0] ? dy > 0 ? (a[1] - b[1]) : (b[1] - a[1]) : dx > 0  ? (a[0] - b[0]) : (b[0] - a[0]);
	});

	for (var i = 0; i < breaks.length-1; i++) {
		let break_x = breaks[i][0];
		let break_y = breaks[i][1];
		let break_x1 = breaks[i+1][0];
		let break_y1 = breaks[i+1][1];
		if (break_x == break_x1 && break_y == break_y1) continue;
		let pos = [
			dx < 0 ? Math.floor(break_x1) : Math.floor(break_x),
			dy < 0 ? Math.floor(break_y1) : Math.floor(break_y)
		];
		let from = [break_x-pos[0],break_y-pos[1]];
		let to = [break_x1-pos[0],break_y1-pos[1]];
		if (pos[0] < render_width-1 && pos[1] < render_height &&
			render[
	  		get_render_position(
	  			pos[0],
	  			pos[1],
	  			render_width
	  		)
			] ==  free_render_character) {
			render[
	  		get_render_position(
	  			pos[0],
	  			pos[1],
	  			render_width
	  		)
	  	] = sub_char(from,to);
		}
	}
}

// INIT
let spider = new_sprite_render("spider",0,0);
let circle = new_sprite_render("circle",0,0);
let mountain = new_sprite_render("frg_mountain_0",0,0);
let hill = new_sprite_render("frg_hill_0",0,0);
let render_spider = 0;

let display_process_event = start_animation_event(1,function(frame){
	//view_process();
	clear_render_process();

  render_sprite(animation_fps+'FPS',0,0);
	if (Number.isInteger(Math.floor(frame/16)/2)) {
		if (Number.isInteger(Math.floor(frame/8)/2)) {
			spider[0] = sprites_list["spider_2"];
		} else {
			spider[0] = sprites_list["spider_4"];
		}
	} else {
		if (Number.isInteger(Math.floor(frame/8)/2)) {
			spider[0] = sprites_list["spider"];
		} else {
			spider[0] = sprites_list["spider_3"];
		}
	}
	spider[1] = mouse_x/char_width-spider[3]/2;
	spider[2] = mouse_y/char_height-spider[4]/2;
	circle[1] = mouse_x/char_width-circle[3]/2;
	circle[2] = mouse_y/char_height-circle[4]/2;
	mountain[1] = 0-mountain[3]/2;
	mountain[2] = render_height-mountain[4];
	hill[1] = render_width-hill[3]/2;
	hill[2] = render_height-hill[4];
	if (render_spider == 0) {
		render_sprite(spider[0],spider[1],spider[2]);
	} else if (render_spider == 2) {
		render_sprite(hill[0],hill[1],hill[2]);
		render_sprite(mountain[0],mountain[1],mountain[2]);
		render_sprite(circle[0],circle[1],circle[2]);
	}
	draw_line([0,0],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width,0],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/2,0],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/4,0],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/4*3,0],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/2,render_height],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/4,render_height],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width/4*3,render_height],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width,render_height],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width,render_height/4],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width,render_height/4*3],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([0,render_height],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([0,render_height/2],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([0,render_height/4],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([0,render_height/4*3],[mouse_x/char_width,mouse_y/char_height]);
	draw_line([render_width,render_height/2],[mouse_x/char_width,mouse_y/char_height]);

	render_process();
	//pause_animation();
});
start_animation(60);

/*

## Sprites

In the game, sprites are text art content in `string` form that can be placed
on a specific X/Y position.

They are stored in the `sprites_list` object with their corresponding file name
without extension as key.

*/


// sprites

let sprites_list = {};
let sprites_width_list = {};
let sprites_height_list = {};

// init sprites

// loop through `<pre>` classed as `sprite_source` during the `index.html`
// creation (see `create_index.php`) and store `textContent` as `sprites_list`
// entries with `<pre>` `id` as key (corresponding to the initial filename
// without extension)

let sprites_sources = document.getElementsByClassName("sprite_source");
for (var i = 0; i < sprites_sources.length; i++){
  if (!sprites_sources[i].id.length) continue
  let sprite_content = sprites_sources[i].textContent;
  let sprite_lines = sprite_content.split('\n');
  let sprite_width = sprite_lines[0].length-1;
  let sprite_height = sprite_lines.length-2;
  sprites_list[sprites_sources[i].id] = sprite_content;
  sprites_width_list[sprites_sources[i].id] = sprite_width;
  sprites_height_list[sprites_sources[i].id] = sprite_height;
}

// Init list of sprites to render

let sprites_renders_list = [];

// ### Defining a new sprite render
//
// Using `new_sprite_render` function will add a new sprite to render on
// screen.
//
// For example :
// 
// `let sprite_render = new_sprite_render("square",5,10);`
//
// Will start rendering "square.txt" content at fifth col, tenth line on
// screen.  Its x/col and y/line position can be modified later accessing
// second and third index of `sprite_render`
//
// ``` sprite_render[2] = 20; sprite_render[3] = 15; ```
//
// Will move the ASCII art square to col 20, line 15
//
// Changing first index of `sprite_render` will change sprite content depending
// on the corresponding file in `art/` existing or not.
//
// `sprite_render[0] = "diamond";`
//
// Will change the square to a diamond drawn in `art/diamond.txt`
//
// Fourth and fifth indexes correspond to width and height of the sprite,
// defined automatically in the `new_sprite_render`
//
// ``` let sprite_render_width = sprite_render[4]; let sprite_render_height =
// sprite_render[5]; ```
//
// Sprites can be render without being stored in a variable but it allow to
// modify them later and remove them specifically from `render_list` see
// `remove_sprite_render`.

let sprites_renders = 0;

// TODO update sprite_render_width and sprite_render_height auto or with function

function new_sprite_render(sprite_name,x,y) {
  let sprite = sprites_list[sprite_name];
  let sprite_render = [sprite,x,y,sprites_width_list[sprite_name],sprites_height_list[sprite_name]];
  sprites_renders++;
  return sprite_render;
}

// ## Defining a new sprite render
//
// Using `new_sprite_render` function will add a new sprite to render on
// screen.

function remove_sprite_from_render(sprite_render) {
  let index = sprites_renders_list.indexOf(sprite_render);
  sprites_renders_list.splice(index,1);
}

// add sprite to render

function render_sprite(sprite_content,x,y) {
  let sprite_width = 0;
  for (var i = 0; i < sprite_content.length; i++) {
    if (sprite_content[i] == transparent_character) continue;
    if (sprite_content[i] == '\n') {
      y++;
      sprite_width = !sprite_width ? i : sprite_width;
      x-= (sprite_width+1);
      continue;
    }
    let render_position = get_render_position(x+i,y,render_width);
    if (
         x+i > render_width-1 
      //|| y > Math.ceil(render_height/2 - view_y)
      || x+i < 0
      || y < 0
      || render[render_position] == '\n'
      || render[render_position] != free_render_character
    ) continue;
    render[render_position] = sprite_content[i];
  }
}

let road_chars = ['⠍','⠌','⠋','⠊','⠉','⠈','⠇','⠆','⠅','⠄','⠃','⠂','⠁','⠀'];
let road_chars_2 = ['\'','·','.','¸'];

function render_sprites(){
  for (var i = 0; i < sprites_renders_list.length; i++) {
    let sprite = sprites_renders_list[i];
    if (sprite[1] > render_width || sprite[1] + sprite[3] < 0) continue;
    render_sprite(sprite[0],Math.trunc(sprite[1]),Math.trunc(sprite[2]));
  }
}

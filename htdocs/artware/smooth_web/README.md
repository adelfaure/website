## Create index

`create_index.php` is meant to create the index.html that run the game.

Use command line:

`php create_index.php > index.html`

It echo all files content from the `art/` folder into `<pre>` `textContent` and
all create a sourced `<script>` element for each file within `javascript/`
folder. This is useful for avoiding using javascript 'fetch' to load ascii art
files content in the game.

All `<pre>` `textContent` are then processed into text sprites in
`javascript/sprites.js`

## Sprites

In the game, sprites are text art content in `string` form that can be placed
on a specific x (render col), y (render line) position.

They are stored in the `sprites_list` object with their corresponding file name
without extension as key.

### Defining a new sprite render

Using `new_sprite_render` function will add a new sprite to render on
screen.

For example :

`let sprite_render = new_sprite_render("square",5,10);`

Will start rendering "square.txt" content at fifth col, tenth line on screen.
Its x/col and y/line position can be modified later accessing second and third
index of `sprite_render`

``` 
sprite_render[2] = 20; 
sprite_render[3] = 15;
```

Will move the ASCII art square to col 20, line 15

Changing first index of `sprite_render` will change sprite content depending
on the corresponding file in `art/` existing or not.

`sprite_render[0] = "diamond";`

Will change the square to a diamond drawn in `art/diamond.txt`

Fourth and fifth indexes correspond to width and height of the sprite,
defined automatically in the `new_function_render`

```
let sprite_render_width = sprite_render[4]; 
let sprite_render_height = sprite_render[5]; 
```

Sprites can be render without being stored in a variable but it allow to
modify them later and remove them specifically from `render_list` see
`remove_sprite_render`.

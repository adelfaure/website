# Jeu vidéo X textmode

## Présentation

## Qu'est ce que l'ASCII art ?

- Citation jgs
- Pour chaque système une scène ASCII
- Scènes ASCII
  + ASCII art
  + ANSI art
  + PETSCII
  + Teletext
  + SHIFT JIS

## Les arts en mode texte

- Unicode et elargissement de la notion d'ASCII art
- Textmode friends
- Improvisation
- the antique shop
- Bob Neil
- Printer grammar / Beirut press 

## Un idéal plutôt qu'une nostalgie

- Citation Heikki Lotvonen
- Absence de standard et Culture hacker

## Animer en mode texte

- Chaque artiste possède sa propre méthode voir créer ses propres outils
- Rexpaint
- lvlvlvl
- Polyducks
- 
- Jarfish
- Jellica Jake
- HA
- Joniscii
- lbs
- 60thcomedian
- IndyJoenz & Durdraw
- Standard Combo & stonescript 
- Orca
- Andreas Gysin
- Julian Hespenied

## Jeux en mode texte

- Stone Story
- Rainy Day
- Warsim
- Sanctuary
- ASCIIdenz
- Uncultured games
- Rexpaint creator games (cogmind, ???, Moon patrol)

## Mes jeux et outils

- Joan Jump, Leonidas want a spear, Mx Numeral etc.
- Textor

## The Wind Through the Wheels

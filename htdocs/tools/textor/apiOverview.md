<!--
---

## Class {#Class}
### Index {#ClassIndex}

- [Example](#ClassExample)
- [Constructor](#ClassConstructor)
  + [Syntax](#ClassConstructorSyntax)
  + [Parameters](#ClassConstructorParameters)
- [Properties](#ClassProperties)
- [Methods](#ClassMethods)

### Example {#ClassExample}
### Constructor {#ClassConstructor}
#### Syntax {#ClassConstructorSyntax}
#### Parameters {#ClassConstructorParameters}
### Properties {#ClassProperties}
### Methods {#ClassMethods}
-->

# Textor reference

__Note about undocumented methods and properties__

Not all methods and properties from source code are documented. While they
probably can be useful I decided to only document the most meaningful routines.

## Index {#index}

- [Animation](#Animation)
- [BitmapOutput](#BitmapOutput)
- [Sprite](#Sprite)
- [TileGrid](#TileGrid)
- [TileSet](#TileSet)

---

[↑ Main index](#index)

## Animation {#Animation}

The `Animation` class processes a frame program over time.

### Index {#AnimationIndex}

- [Example](#AnimationExample)
- [Constructor](#AnimationConstructor)
  + [Syntax](#AnimationConstructorSyntax)
  + [Parameters](#AnimationConstructorParameters)
    * [targetFrameRate](#AnimationConstructorParametersTargetFrameRate)
    * [frameProgram](#AnimationConstructorParametersFrameProgram)
    * [outputContext](#AnimationConstructorParametersBitmapOutputContext)
- [Properties](#AnimationProperties)
  + [Animation.frame](#AnimationPropertiesFrame)
  + [Animation.targetFrameRate](#AnimationPropertiesTargetFrameRate)
  + [Animation.frameRate](#AnimationPropertiesFrameRate)
  + [Animation.frameProgram](#AnimationPropertiesFrameProgram)
  + [Animation.outputContext](#AnimationPropertiesBitmapOutputContext)
- [Methods](#AnimationMethods)
  + [Animation.play](#AnimationMethodsPlay)
    * [Syntax](#AnimationMethodsPlaySyntax)
  + [Animation.pause](#AnimationMethodsPause)
    * [Syntax](#AnimationMethodsPauseSyntax)

### Example {#AnimationExample}

This example will create an `Animation` instance aiming sixty frames per second
rate which will log its current frames per second rate along its elapsed frames
number during five seconds before stopping.

```mjs
import * as Textor from "./modules/textor.mjs";
const animationInstance = new Textor.Animation(
  60,
  async function(input) {
    const {fps,f} = input;
    console.log(fps,f);
    return !fps || f < fps*5;
  }
);
animationInstance.play();
```

### Constructor {#AnimationConstructor}

`Animation()` return a newly instantiated `Animation` object from given target
frame rate, frame program and optional output context.

#### Syntax {#AnimationConstructorSyntax}

```js
new Animation(targetFrameRate,frameProgram);
new Animation(targetFrameRate,frameProgram,outputContext);
```

#### Parameters {#AnimationConstructorParameters}

##### `targetFrameRate` {#AnimationConstructorParametersTargetFrameRate}

An unsigned integer specifying the frames per second rate that will be targeted during animation process.

##### `frameProgram` {#AnimationConstructorParametersFrameProgram}

An asynchronous anonymous function that will be processed each frame,
determining if animation continue or break.

Must return upcoming running status, `1` for continue running, `0` for break
running.

At frame process, `Animation` instance will send to `frameProgram` parameter an
object with the following properties:

- `f`: `Animation` instance elapsed frames number.
- `out`: `Animation` instance output context (or 0 value if it was not
  specified).
- `fps`: `Animation` instance current frames per second rate
- `changeFps`: Anonymous function using a unsigned integer to change
  `Animation` instance target frame rate.

Note that `frameProgram` will be stored as `Animation.frameProgram` instance
property and that it can be changed anytime by overriding this property with a
new `frameProgram`.

##### `outputContext` {#AnimationConstructorParametersBitmapOutputContext}

Depending on the kind of desired ouput `outputContext` can either be an
[`BitmapOutput.context`](#BitmapOutputPropertiesContext) or an
[`TextBitmapOutput.context`](#TextBitmapOutputPropertiesContext) property.

### Properties {#AnimationProperties}

#### `Animation.frame` {#AnimationPropertiesFrame}

An unsigned integer representing animation elapsed frames.

#### `Animation.targetFrameRate` {#AnimationPropertiesTargetFrameRate}

An unsigned integer specifying the frames per second rate that will be targeted during animation process.

#### `Animation.frameRate` {#AnimationPropertiesFrameRate}

An unsigned integer representing the current animation frame per second rate.
  
#### `Animation.frameProgram` {#AnimationPropertiesFrameProgram}

An asynchronous anonymous function that will be processed each frame,
determining if animation continue or break. See animation constructor
parameters [`frameProgram`](#AnimationConstructorParametersFrameProgram).

#### `Animation.outputContext` {#AnimationPropertiesBitmapOutputContext}

As pointed in Animation constructor argument
[`outputContext`](#AnimationConstructorParametersBitmapOutputContext) see
[`BitmapOutput.context`](#BitmapOutputPropertiesContext) and
[`TextBitmapOutput.context`](#textBitmapOutputPropertiesContext).

Can be changed anytime by overriding this property with another context.

### Methods {#AnimationMethods}

#### `Animation.play` {#AnimationMethodsPlay}

`Animation` method to play animation, starting or resuming animation process.

##### Syntax {#AnimationMethodsPlaySyntax}

```js
play()
```

#### `Animation.pause` {#AnimationMethodsPause}

`Animation` method to pause animation.

##### Syntax {#AnimationMethodsPauseSyntax}

```js
pause()
```

---

## BitmapOutput {#BitmapOutput}

The `BitmapOutput` class handle bitmap display in HTML context. 

It is meant to be the graphical end point of a program using Textor library.

It is a layer on top on HTML canvas 2d API making shortcuts for upscaled
pixelated scenes and giving access to its 2d context. It is needed for
[`Sprite.draw`](#SpriteMethodsDraw) method and can be used for
[`TileSet.drawTile`](#TileSetMethodsDrawTile) for drawing tiles outside of a
[`Sprite.updateBitmap`](#SpriteMethodsUpdateBitmap) method context.

### Index {#BitmapOutputIndex}

- [Example](#BitmapOutputExample)
- [Constructor](#BitmapOutputConstructor)
  + [Syntax](#BitmapOutputConstructorSyntax)
  + [Parameters](#BitmapOutputConstructorParameters)
    * [destination](#BitmapOutputConstructorParametersDestination)
    * [displayWidth](#BitmapOutputConstructorParametersDisplayWidth)
    * [displayHeight](#BitmapOutputConstructorParametersDisplayHeight)
    * [pixelSize](BitmapOutputConstructorParametersDisplayHeight)
- [Properties](#BitmapOutputProperties)
  + [canvas](#BitmapOutputPropertiesCanvas)
  + [context](#BitmapOutputPropertiesContext)
  + [width](#BitmapOutputPropertiesWidth)
  + [height](#BitmapOutputPropertiesHeight)
- [Methods](#BitmapOutputMethods)
  + [scale](#BitmapOutputMethodsScale)
    * [Syntax](#BitmapOutputMethodsScaleSyntax)

### Example {#BitmapOutputExample}

This example will create a full window Textor bitmap output with a pixel size
mutliplied by four

```mjs
import * as Textor from "./modules/textor.mjs";

const bitmapOutputInstance= new Textor.BitmapOutput(
  document.body,
  window.innerWidth,
  window.innerHeight,
  4
);
```

### Constructor {#BitmapOutputConstructor}

`BitmapOutput()` return a newly instantiated `BitmapOutput` object from given
HTML element display width, height and pixel size.

#### Syntax {#BitmapOutputConstructorSyntax}

```js
new BitmapOutput(destination,displayWidth,displayHeight);
```

#### Parameters {#BitmapOutputConstructorParameters}

##### `destination` {#BitmapOutputConstructorParametersDestination}
    
HTML element destination for BitmapOutput display. BitmapOutput will create an
HTML canvas element for displaying bitmap data and append it to destination as
HTML child element.

##### `displayWidth` {#BitmapOutputConstructorParametersDisplayWidth}

An unsigned integer defining BitmapOutput instance display pixel width that
will be applied to display canvas width.

##### `displayHeight` {#BitmapOutputConstructorParametersDisplayHeight}

An unsigned integer defining BitmapOutput instance display pixel height
that will be applied to display canvas height.

##### `pixelSize` {#BitmapOutputConstructorParametersDisplayHeight}

An unsigned integer defining BitmapOutput instance display pixel size.

### Properties {#BitmapOutputProperties}

#### `canvas` {#BitmapOutputPropertiesCanvas}
  
HTMLElement canvas used to display output bitmap

#### `context` {#BitmapOutputPropertiesContext}

HTMLElement canvas 2d context used to manipulate BitmapOutput bitmap data

#### `width` {#BitmapOutputPropertiesWidth}

BitmapOutput bitmap width in pixels

#### `height` {#BitmapOutputPropertiesHeight}

BitmapOutput bitmap height in pixels

### Methods {#BitmapOutputMethods}

#### `scale` {#BitmapOutputMethodsScale}

Scale output bitmap data and display according to destination width, height and
pixel size.

##### Syntax {#BitmapOutputMethodsScaleSyntax}

```js
scale(displayWidth,displayHeight,pixelSize)
```

---

## Sprite {#Sprite}

Sprite class is used to link a tileSet, a tileGrid and a palette together for
rendering a graphical object that can be put freely on a bitmap output

### Index {#SpriteIndex}

- [Example](#SpriteExample)
- [Constructor](#SpriteConstructor)
  + [Syntax](#SpriteConstructorSyntax)
  + [Parameters](#SpriteConstructorParameters)
- [Properties](#SpriteProperties)
- [Methods](#SpriteMethods)

### Example {#SpriteExample}

This example will create a fullscreen Sprite filled with random colored tiles.

```mjs
// Import Textor classes and functions
import * as Textor from "./modules/textor.mjs";
import * as TileSets from "./modules/tileSets.mjs";
import * as Palettes from "./modules/palettes.mjs";

const width = window.innerWidth;
const height = window.innerHeight;
const pixelSize = 3;

const bitmapOutput = new Textor.BitmapOutput(
  width,
  height,
  pixelSize,
  document.body
);

const tileSet = TileSets.semigraphBold8x8;
const palette = Palettes.base4;
const tileMatrix = new Textor.Matrix(
  Math.ceil(bitmapOutput.width/tileSet.width),
  Math.ceil(bitmapOutput.height/tileSet.height),
);

const sprite = new Textor.Sprite(
  tileSet,
  palette,
  tileMatrix
);

function frame(f,fps){
  sprite.tileMatrix.process(()=>{
    const fg = sprite.fg(Math.floor(Math.random()*palette.length));
    let bg = fg;
    while (bg == fg) {
      bg = sprite.bg(Math.floor(Math.random()*palette.length));
    }
    return (
      Math.floor(Math.random()*tileSet.length)
      + fg
      + bg
    )
  });
  sprite.tileMatrix.write(
    "FPS "+fps+' '+f,
    + sprite.fg(15)
    + sprite.bg(0),
    0,
    0
  );
  sprite.updateBitmap();
  sprite.draw(bitmapOutput.context);
  return f < fps*5;
}

const animation = new Textor.Animation(
  60,
  frame
);

animation.play();
```

### Constructor {#SpriteConstructor}

#### Syntax {#SpriteConstructorSyntax}

#### Parameters {#SpriteConstructorParameters}

### Properties {#SpriteProperties}

#### `colorLength` {#SpritePropertiesColorLength}

Number of palette colors, calculated at sprite construction

#### `context` {#SpritePropertiesContext}

#### `height` {#SpritePropertiesHeight}
  
Height in pixels of the sprite bitmap

#### `palette` {#SpritePropertiesPalette}

Palette provided stored in sprite instance

#### `tileSet` {#SpritePropertiesTileSet}

Tilset provided stored in sprite instance

#### `tiles` {#SpritePropertiesTiles}

TileGrid provided stored in sprite instance

#### `width` {#SpritePropertiesWidth}

Width in pixels of the sprite bitmap

#### `x` {#SpritePropertiesX}

X position of sprite when drawn in a given bitmap context

#### `y` {#SpritePropertiesY}

Y position of sprite when drawn in a given bitmap context

### Methods {#SpriteMethods}

#### `bg` {#SpriteMethodsBg}

Calulate tile data starting point of given palette background color

#### `collideWith` {#SpriteMethodsCollideWidth}
  
Check if sprite instance is colliding another sprite instance returning
collided tile data position for sprite and collided sprite

#### `drawBitmap` {#SpriteMethodsDraw}

Draw sprite bitmap to given bitmap context at sprite position

#### `fg` {#SpriteMethodsFg}

Calulate tile data starting point of given palette foreground color

#### `getBg` {#SpriteMethodsGetBg}

Get palette foreground color from tile data index

#### `getFg` {#SpriteMethodsGetFg}

Get palette background color from tile data index

#### `getTile` {#SpriteMethodsGetTile}

Get tile set tile index from tile data index
 
#### `processOffset` {#SpriteMethodsProcessOffset}

Proccess each tile X and Y offset draw tile position for making visuals effects

#### `updateBitmap` {#SpriteMethodsUpdateBitmap}

Update sprite bitmap according to tiles data and tiles offset position

---

[↑ Main index](#index)

## TileGrid {#TileGrid}

### Index {#TileGridIndex}

- [Constructor](#TileGridConstructor)
  + [Syntax](#TileGridConstructorSyntax)
  + [Parameters](#TileGridConstructorParameters)
    * [width](#TileGridConstructorParametersWidth)
    * [height](#TileGridConstructorParametersHeight)
    * [animationContext](#TileGridConstructorParametersAnimationContext)
    * [tileSet](#TileGridConstructorParametersTileSet)
- [Properties](#TileGridProperties)
  + [TileGrid.width](#TileGridPropertiesWidth)
  + [TileGrid.height](#TileGridPropertiesHeight)
  + [TileGrid.length](#TileGridPropertiesLength)
  + [TileGrid.data](#TileGridPropertiesData)
- [Methods](#TileGridMethods)
  + [TileGrid.place](#TileGridMethodsPlace)
    * [Syntax](#TileGridMethodsPlaceSyntax)
    * [Parameters](#TileGridMethodsPlaceParameters)
      - [x](#TileGridMethodsPlaceParametersX)
      - [y](#TileGridMethodsPlaceParametersY)
      - [gapX](#TileGridMethodsPlaceParametersGapX)
      - [gapY](#TileGridMethodsPlaceParametersGapY)
  + [TileGrid.printAt](#TileGridMethodsPrintAt)
    * [Syntax](#TileGridMethodsPrintAtSyntax)
    * [Parameters](#TileGridMethodsPrintAtParameters)
      - [destinationDataIndex](#TileGridMethodsPrintAtParametersDestinationIndex)
      - [destinationWidth](#TileGridMethodsPrintAtParametersDestinationWidth)
  + [TileGrid.process](#TileGridMethodsProcess)
    * [Syntax](#TileGridMethodsProcesstSyntax)
    * [Parameters](#TileGridMethodsProcessParameters)
      - [tileProgram](#TileGridMethodsProcessParametersTileProgram)

### Constructor {#TileGridConstructor}
  
#### Syntax {#TileGridConstructorSyntax}

```js
new TileGrid(width,height)
new TileGrid(width,height,animationContext)
new TileGrid(width,height,animationContext,tileSetContext)
```

#### Parameters {#TileGridConstructorParameters}
    
##### `width` {#TileGridConstructorParametersWidth}

##### `height`{#TileGridConstructorParametersHeight}

##### `animationContext` {#TileGridConstructorParametersAnimationContext}

##### `tileSet` {#TileGridConstructorParametersTileSet}

### Properties {#TileGridProperties}

#### `TileGrid.width` {#TileGridPropertiesWidth}

Unsigned integer representing cols number of an TileGrid instance.

#### `TileGrid.height` {#TileGridPropertiesHeight}

Unsigned integer representing rows number of an TileGrid instance.
  
#### `TileGrid.length` {#TileGridPropertiesLength}

Unsigned integer representing total tile data number of an TileGrid instance.

#### `TileGrid.data` {#TileGridPropertiesData}

Array of 16 bit unsigned integer representing tiles data of an TileGrid
instance.

### Methods {#TileGridMethods}

#### `TileGrid.place` {#TileGridMethodsPlace}

As print prerequisite, define TileGrid instance x, y, gapX and gapY values

##### Syntax {#TileGridMethodsPlaceSyntax}

```js
place(x,y,gapX,gapY);
```

##### Parameters {#TileGridMethodsPlaceParameters}

###### `x` {#TileGridMethodsPlaceParametersX}

x-axis coordinate in the destination buffer grid

###### `y` {#TileGridMethodsPlaceParametersY}
  
y-axis coordinate in the destination buffer grid

###### `gapX` {#TileGridMethodsPlaceParametersGapX}
  
x gap between each repeat

###### `gapY` {#TileGridMethodsPlaceParametersGapY}

y gap between each repeat

#### `TileGrid.printAt` {#TileGridMethodsPrintAt}

In printing TileGrid instance data into another TileGrid instance data context;
return TileGrid data at print index 

##### Syntax {#TileGridMethodsPrintAtSyntax}

```js
printAt(destinationDataIndex,destinationWidth)
```

##### Parameters {#TileGridMethodsPrintAtParameters}
  
###### `destinationDataIndex` {#TileGridMethodsPrintAtParametersDestinationIndex}

Destination TileGrid instance data index

###### `destinationWidth` {#TileGridMethodsPrintAtParametersDestinationWidth}

Destination TileGrid width

#### `TileGrid.process` {#TileGridMethodsProcess}

Process TileGrid data with a program following fragment shader principle

##### Syntax {#TileGridMethodsProcesstSyntax}

```js
processAt(tileProgram)
```

##### Parameters {#TileGridMethodsProcessParameters}

###### `tileProgram` {#TileGridMethodsProcessParametersTileProgram}

---

## TileSet {#TileSet}

### Index {#TileSetIndex}

- [Example](#TileSetExample)
- [Constructor](#TileSetConstructor)
  + [Syntax](#TileSetConstructorSyntax)
  + [Parameters](#TileSetConstructorParameters)
- [Properties](#TileSetProperties)
- [Methods](#TileSetMethods)

### Example {#TileSetExample}

### Constructor {#TileSetConstructor}

#### Syntax {#TileSetConstructorSyntax}

#### Parameters {#TileSetConstructorParameters}
 
### Properties {#TileSetProperties}

#### `cols` {#TileSetPropertiesCols}

#### `length` {#TileSetPropertiesLength}

#### `rows` {#TileSetPropertiesRows}

#### `tileHeight` {#TileSetPropertiesTileHeight}

#### `tileWidth` {#TileSetPropertiesTileWidth}

### Methods {#TileSetMethods}

#### `drawTile` {#TileSetMethodsDrawTile}


# Textor

![](./docs/img/textorLogo.gif)

From latin texō (“weave”) + -tor. Could mean something like "The weaver".

It is a text mode render engine experiment. It provides tools for displaying,
animating and interacting text mode graphics from bitmap fonts.

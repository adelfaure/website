/*
  frameProgram(
    f,          //  animation current frame       ->  Animation
    fps,        //  animation current frame rate  ->  Animation
    changeFps,  //  change animation frame rate   ->  Animation
    out         //  bitmap output                 ->  Scene
  )
  tileProgram(
    f,          //  animation current frame ->  Animation
    i,          //  tile index              ->  Sprite   Matrix
    d,          //  tiles data              ->  Sprite   Matrix
    di,         //  tile data at index      ->  Sprite   Matrix
    x,          //  tile x coordinate       ->  Sprite   Matrix
    y,          //  tile y coordinate       ->  Sprite   Matrix
    w,          //  tile width              ->  Sprite  TileSet
    h,          //  tile height             ->  Sprite  TileSet
    l,          //  tile set length         ->  Sprite  TileSet
    c,          //  tile set colors length  ->  Sprite  TileSet
  )
*/

// TODO COMMENT
var endInterfaceContext;

// TODO COMMENT
class FrameContext {

  // PROPERTIES
  
  at;    // animation time
  f;    // animation frame
  fps;  // animation frame rate
  m;    // mouse down
  mx;    // mouse x
  my;    // mouse y
  k;    // keys
  ew;    // end interface width
  eh;    // end interface width
  o;    // end interface bitmapInterface
  cp = (input) => codePoint(input); // codePoint shortcut function
  hex = (input) => hex(input); // hex shortcut function

  // METHODS

  // TODO COMMENT
  setAnimation(animation) {
    const endInterface = endInterfaceContext != undefined ? endInterfaceContext : 0;
    this.m = endInterface  ? endInterfaceContext.mouse : endInterface; 
    this.mx = endInterface ? endInterfaceContext.mouseX : endInterface;
    this.my = endInterface ? endInterfaceContext.mouseY : endInterface;
    this.k = endInterface  ? endInterfaceContext.keys : endInterface;
    this.kd = endInterface  ? endInterfaceContext.keyDown : endInterface;
    this.ew = endInterface  ? endInterfaceContext.bitmapInterface.width : endInterface;
    this.eh = endInterface  ? endInterfaceContext.bitmapInterface.height : endInterface;
    this.o = endInterface  ? endInterfaceContext.bitmapInterface : endInterface;
    this.at = animation.time;
    this.f = animation.frame;
    this.fps = animation.frameRate;
  }
  
  // TODO COMMENT
  setEndInterface(endInterface) {
    endInterfaceContext = endInterface;
  }
}

export const frameContext = new FrameContext();

class FragmentContext {

  // PROPERTIES
  
  i;    // tile data index
  x;    // tile x coordinate
  y;    // tile y coordinate
  w;    // matrix width
  h;    // matrix height
  di;   // tile data at index
  d;    // tiles data
  t;    // tileSet length
  tw;   // tileSet tile width
  th;   // tileSet tile height
  c;    // palette length
  fg = (value) => this.foreground(value);  // fg shortcut function
  bg = (value) => this.background(value);  // bg shortcut function
  gfg = (value) => this.getForeground(value);  // fg shortcut function
  gbg = (value) => this.getBackground(value);  // bg shortcut function
  gt = (value) => this.getTile(value);  // bg shortcut function
  write = (str,x,y) => write(str,x,y); // write shortcut
  cp = (input) => codePoint(input); // codePoint shortcut function
  hex = (input) => hex(input); // hex shortcut function

  // METHODS

  setTileSet(tileSet) {
    this.t = tileSet.length;
    this.tw = tileSet.width;
    this.th = tileSet.height;
  }
  setPalette(palette) {
    this.c = palette.length;
  }
  setMatrix(matrix) {
    this.w = matrix.width;
    this.h = matrix.height;
    this.d = matrix.data; 
  }
  setMatrixAt(matrix,dataIndex) {
    this.i = dataIndex;
    this.x = matrix.dx[dataIndex];
    this.y = matrix.dy[dataIndex];
    this.di = matrix.data[dataIndex];
  }
  getTile(value){
    return (
      value%this.t
    );
  }
  getForeground(value){
    return (
      Math.floor(value/(this.t*this.c))%this.c
    );
  }
  getBackground(value,tileSet,palette){
    return (
      Math.floor(value/(this.t))%this.c
    );
  }
  foreground(value){
    return (
      this.t
      * this.c
      * value
    )
  }
  background(value){
    return (
      this.t
      * value
    )
  }
}
export const fragmentContext = new FragmentContext();

// @ANIMATION
export class Animation {

  // PROPERTIES
 
  // Second duration in milliseconds
  secondDuration = 1000;
 
  // Elapsed frames number
  frame = 0;

  // Frame per second rate that will be seeked during animation running
  targetFrameRate;
  
  // Current animation frame per second rate
  frameRate = 0;
  
  // Timer in milliseconds before updating frame rate
  frameRateTimer = this.secondDuration;

  // Frame counter before updating frame rate
  frameRateCounter = 0;
  
  // TODO COMMENT 
  accumulatedTime = 0;

  // TODO COMMENT
  time = 0;

  // Accumulated time in milliseconds since animation started
  startTime = 0;

  // Time gap since last frame execution
  timeGap = 0;

  // Time gap offset between seeked frame per second rate and actual frame
  // exectution
  timeGapOffset = 0;

  // Animation running status, 1 for running, 0 for not running
  running = 0;

  // Optional output context
  outputContext;

  // CONSTRUCTOR

  // Animation constructor, updated at Animation instance creation
  constructor(
    targetFrameRate,
    frameProgram
  ){
    if (
      targetFrameRate != undefined
      && frameProgram != undefined
    ){
      this.play(
        targetFrameRate,
        frameProgram
      );
    }
  }
  
  // METHODS
  reset(){
    this.timeGapOffset = 0;
    this.timeGap = 0;
    this.time = 0;
    this.frameRateCounter = 0;
    this.frame = 0;
  }
  
  // Play animation
  // TODO COMMENT
  play(
    targetFrameRate,
    // Program that will be updated each frame, determining if animation
    // continue or break, must be asynchronous for eventual await related
    // execution
    frameProgram,
  ){
    if (targetFrameRate == undefined) {
      console.log("Cancel play: no target frame rate provided");
      return;
    }
    if (frameProgram == undefined) {
      console.log("Cancel play: no frame program provided");
      return;
    }
    this.targetFrameRate = targetFrameRate;
    
    // Cannot start if already running
    if (this.running) return;
    
    // Initialize time
    this.startTime = Date.now();
    
    // Initialize time
    this.accumulatedTime = this.startTime;
  
    // Update running status
    this.running = 1;
    
    // Start running frames
    this.run(frameProgram);
  }

  // Pause animation
  pause(){
  
    // Update running status
    this.running = 0;
  }
  
  // Update time and frame related properties, process frame program according
  // to frame rate
  async run(frameProgram){

    const animation = this;
  
    // How many time since last frame
    let timeElapsed = Date.now() - this.accumulatedTime;
  
    // Update animation time with time elapsed
    this.accumulatedTime += timeElapsed;

    // Update time timeGap with time elapsed
    this.timeGap += timeElapsed;
  
    // Update fps print update timer with time elpased
    this.frameRateTimer -= timeElapsed;
  
    // Calculate amount of time separating each frame according to animation
    // target frame rate
    let timeInterval = this.secondDuration/this.targetFrameRate;
  
    // Check if time gap is superior to time interval updated with time gap
    // offset and trigger program function accordingly
    if (this.timeGap >= timeInterval-this.timeGapOffset) {
  
      this.time = this.accumulatedTime-this.startTime;
      // Execute frame function
      frameContext.setAnimation(this);
      this.running = await frameProgram(
        // TODO COMMENT
        frameContext
      );
  
      // Increase frame count
      this.frame++;
  
      // Increase fps print value
      this.frameRateCounter++;
  
      // Calculate time gap offset according to time interval expected and
      // actual time gap
      this.timeGapOffset += this.timeGap - timeInterval;
      this.timeGap = 0;
    }
  
    // Update frame rate
    if (this.frameRateTimer <= 0) {
      this.frameRate = this.frameRateCounter;
      this.frameRateTimer = this.secondDuration;
      this.frameRateCounter = 0;
    }
  
    // Break animation if running status is 0
    if (!this.running) return;
    
    // Request next frame
    requestAnimationFrame(()=>{animation.run(frameProgram)});
  }
}

const specialKeysMap = [
  "Alt",
  "AltGraph",
  "ArrowDown",
  "ArrowLeft",
  "ArrowRight",
  "ArrowUp",
  "Backspace",
  "CapsLock",
  "Control",
  "Dead",
  "Delete",
  "End",
  "Enter",
  "Escape",
  "F1",
  "F10",
  "F11",
  "F12",
  "F2",
  "F3",
  "F4",
  "F5",
  "F6",
  "F7",
  "F8",
  "F9",
  "Home",
  "Insert",
  "Meta",
  "NumLock",
  "PageDown",
  "PageUp",
  "PrintScreen",
  "Shift",
  "Tab",
  "Unidentified",
];

// SPECIFIC TO JAVASCRIPT
function keyToKeyCode(endInterface,key){
  var value = 1;
  const specialKeysIndex = specialKeysMap.indexOf(key);
  if (specialKeysIndex > -1){
    value = (
      endInterface.keys.length
      - specialKeysMap.length
      + specialKeysIndex
    );
  } else if(key.length == 1){
    const keyCharCode = key.charCodeAt(0);
    if (keyCharCode < (endInterface.keys.length-specialKeysMap.length)) {
      value = keyCharCode;
    }
  }
  return value;
}

// @ENDINTERFACE

export class EndInterface{

  // PROPERTIES

  // HTMLElement canvas used to display output bitmap
  //canvas = document.createElement("canvas");

  // HTMLElement canvas 2d context used to manipulate EndInterface bitmap data
  //context = this.canvas.getContext("2d");

  // // EndInterface bitmap width in pixels
  // width;
  // 
  // // EndInterface bitmap height in pixels
  // height;

  // TODO COMMENT
  bitmapInterface;
  mouseX=0;
  mouseY=0;
  mouse=0;
  keys = new Uint8Array(256);
  keyDown = 0;

 
  // CONSTRUCTOR
  constructor(

    // TODO COMMENT
    bitmapInterface,
//    
//    // Bitmap Output instance display pixel width
//    width,
//    
//    // Bitmap Output instance display pixel height
//    height,
//
//    // Pixel size for future bitmap rendering
//    pixelSize,
    
    // HTML element destination for EndInterface display
    destination

  ) {

    this.bitmapInterface = bitmapInterface;

    // Avoid resize pixel interpolation to keep a pixelated display
    this.bitmapInterface.canvas.style.imageRendering = "pixelated";
    destination.appendChild(this.bitmapInterface.canvas);

    // TODO COMMENT
    const endInterface = this;
    this.bitmapInterface.canvas.addEventListener("mousemove",function(e){
      e.preventDefault();
      const rect = endInterface.bitmapInterface.canvas.getBoundingClientRect();
      endInterface.mouseX = (e.clientX-rect.x)/bitmapInterface.pixelSize;
      endInterface.mouseY = (e.clientY-rect.y)/bitmapInterface.pixelSize;
    })
    this.bitmapInterface.canvas.addEventListener("mousedown",function(e){
      e.preventDefault();
      endInterface.mouse = 1;
    })
    this.bitmapInterface.canvas.addEventListener("mouseup",function(e){
      e.preventDefault();
      endInterface.mouse = 0;
    })
    this.bitmapInterface.canvas.addEventListener("keydown",function(e){
      const keyCode = keyToKeyCode(endInterface,e.key);
      endInterface.keys[keyCode] = 1;
      if (!(
        endInterface.keys[228] 
        && endInterface.keys[253] 
        && endInterface.keys[82] 
      )){
        e.preventDefault();
      }
      endInterface.keyDown = keyCode;
    })
    this.bitmapInterface.canvas.addEventListener("keyup",function(e){
      e.preventDefault();
      const keyCode = keyToKeyCode(endInterface,e.key);
      endInterface.keys[keyCode] = 0;
      var down = false;
      if (keyCode != 253 && keyCode != 221) {
        for (var i = 0; i < endInterface.keys.length; i++){
          if (endInterface.keys[i]) {
            down = true;
            break;
          }
        }
      }
      endInterface.keyDown = !down ? 0 : endInterface.keyDown;
    })
    this.bitmapInterface.canvas.setAttribute("tabindex",0);
    this.bitmapInterface.canvas.focus();
    frameContext.setEndInterface(endInterface);
  }

  // METHODS
  
}


// @MATRIX

// Textor Matrix contains data intended to be used or displayed as a grid.
// It can be use to render tiled bitmap graphics from a TileSet instance within
// a Sprite instance as Sprite.tileGrid property.
// It can also be use independently as a positioned tiled texture within
// another Matrix instance.
export class Matrix{
  
  // PROPERTIES
 
  //  Matrix x axis coordinate when printed on another Matrix
  x;
  
  //  Matrix x axis coordinate when printed on another Matrix
  y;
  
  //  Matrix x axis gap between each of its horizontal repeatition when printed on
  // another TilegGrid
  gapX;
  
  //  Matrix y axis gap between each of its vertical repeatition when printed on
  // another Matrix
  gapY;
  
  //  Matrix width
  width;
  
  //  Matrix height
  height;
  
  //  Matrix total tiles number
  length;
  
  //  Matrix tiles data
  data;
  
  //  Matrix precalculated data x axis coordinate store for data iteration
  dx = [];
  
  //  Matrix precalculated data y axis coordinate store for data iteration
  dy = [];

  // CONSTRUCTOR

  //  Matrix constructor, updated at Matrix instance creation
  // TODO COMMENT
  constructor(
    width, 
    height,
  ) {
    this.width = Math.floor(width);
    this.height = Math.floor(height);
    this.length = this.width*this.height;
    this.data = new Uint32Array(this.length);
    
    // Precalculate dx and dy values
    for (var i = 0; i < this.data.length; i++) {
      this.dx[i] = i%this.width;
      this.dy[i] = Math.floor(i/this.width);
    }
    
    // default print placement
    this.place(0,0,0,0);
  }
  
  // METHODS
  
  // As print prerequisite, define Matrix instance x, y, gapX and gapY values
  place(
    x,
    y,
    gapX,
    gapY,
  ){
    // x-axis coordinate in the destination buffer grid
    this.x = Math.floor((x*-1)%(this.width + gapX) + this.width + gapX);
    // y-axis coordinate in the destination buffer grid
    this.y = Math.floor((y*-1)%(this.height + gapY) + this.height + gapY);
    // x gap between each repeat
    this.gapX = Math.floor(gapX);
    // y gap between each repeat
    this.gapY = Math.floor(gapY);
  }
 
  // In printing Matrix instance data into another Matrix instance data
  // context; get Matrix data destination index 
  getPrintIndex(
    // destination Matrix instance data index
    i,
    // destination Matrix width
    w
  ) {

    // origin X
    const ox = this.x;
    
    // origin Y
    const oy = this.y;
    const gapX = this.gapX;
    const gapY = this.gapY;

    // print x
    const px = i%w + ox;

    // print y
    const py = Math.floor(i/w) + oy;

    // print data x
    const pdx = px%(this.width + gapX);

    // if print data x is out of Matrix width; return for avoiding sending
    // data from next data row
    if (pdx >= this.width) return -1;

    // print data y
    const pdy = py%(this.height + gapY)*this.width;

    // return print data index
    return pdx+pdy;
  }

  // In printing Matrix instance data into another Matrix instance data
  // context; return Matrix data at print index 
  printAt(
    destinationIndex,
    destinationWidth
  ) {
    let printIndex = this.getPrintIndex(destinationIndex,destinationWidth);

    // If printIndex is out of Matrix data bounds return 0
    return printIndex > -1 ? this.data[printIndex] : 0;
  }

  // Process Matrix data with a program following fragment shader principle
  processAt(
    tileProgram,
    dataIndex
  ) {
    // TODO COMMNT
    fragmentContext.setMatrixAt(this,dataIndex);
    // Define Matrix instance data at data index with given tileProgram
    this.data[dataIndex] = tileProgram(
      // TODO COMMENT
      fragmentContext,
      frameContext
    );
    
    // Return data at data index value
    return this.data[dataIndex];
  }
  
  // Process all data
  process(
    tileProgram
  ) {
    fragmentContext.setMatrix(this);
    for (var i = 0; i < this.data.length; i++){
      this.processAt(tileProgram,i);
    }
  }
  
  // Convert str to positioned matrix data
  // TODO COMMENT
  write(str,fgValue,bgValue,x,y){
    fgValue = fgValue == undefined ? fragmentContext.c-2 : fgValue;
    bgValue = bgValue == undefined ? 0 : bgValue;
    x = x == undefined ? 0 : x;
    y = y == undefined ? 0 : y;
    let lines = str.split('\n');
    for (var strY = 0; strY < lines.length; strY++){
      for (var strX = 0; strX < lines[strY].length; strX++) {
        let dx = x + strX;
        let dy = y + strY;
        if 
        (
          dx < 0 
          || dx >= this.width
          || dy < 0 
          || dy >= this.height
        ) 
        continue;
        this.data
        [
          dy*this.width + dx
        ] = (
          lines[strY][strX].charCodeAt(0)
          +fragmentContext.fg(fgValue)
          +fragmentContext.bg(bgValue)
        );
      }
    }
  }
}

// Create buffer from string
export function strToMatrix(str){

  // Split string into lines
  let lines = str.split('\n');

  // Use lines number as buffer height and numbers of characters in first line
  // as width
  let matrix = new Matrix(lines[0].length,lines.length);

  // Convert string characters to buffer data
  matrix.process((i)=>lines[i.y][i.x].charCodeAt(0));
  return matrix;
}


// @TILEDBITMAP
export class TiledBitmap{
  
  //PROPERTIES
  bitmapInterface;
  matrix;
  replace;
  updated;
  xOffset;
  updatedXOffset;
  yOffset;
  updatedYOffset;
  tileSet;
  palette;
  collidePointsX = [];
  collidePointsY = [];
  width;
  height;
  cols;
  rows;

  //CONSTRUCTOR
  constructor(
    tileSet,
    palette,
    bitmapInterface_or_Matrix_or_width,
    height_or_pixelSize,
    pixelSize
  ){
    this.tileSet = tileSet;
    this.palette = palette;
    if (arguments[2] instanceof BitmapInterface) {
      this.bitmapInterface = arguments[2];
      this.matrix = new Matrix(
        this.bitmapInterface.width / this.tileSet.width,
        this.bitmapInterface.height / this.tileSet.height
      );
    } else if (arguments[2] instanceof Matrix) {
      this.matrix = arguments[2];
      this.bitmapInterface = new BitmapInterface(
        this.matrix.width * this.tileSet.width,
        this.matrix.height * this.tileSet.height,
        height_or_pixelSize
      );
    } else {
      this.matrix = new Matrix(
        bitmapInterface_or_Matrix_or_width,
        height_or_pixelSize
      );
      this.bitmapInterface = new BitmapInterface(
        this.matrix.width * this.tileSet.width,
        this.matrix.height * this.tileSet.height,
        pixelSize
      );
    }
    this.replace = new Matrix(this.matrix.width,this.matrix.height);
    this.updated = new Matrix(this.matrix.width,this.matrix.height);
    this.xOffset = new Matrix(this.matrix.width,this.matrix.height);
    this.updatedXOffset = new Matrix(this.matrix.width,this.matrix.height);
    this.yOffset = new Matrix(this.matrix.width,this.matrix.height);
    this.updatedYOffset = new Matrix(this.matrix.width,this.matrix.height);
    this.width = this.bitmapInterface.width;
    this.height = this.bitmapInterface.height;
    this.cols = this.matrix.width;
    this.rows = this.matrix.height;
  }

  //METHODS
  update() {
    fragmentContext.setTileSet(this.tileSet);
    fragmentContext.setPalette(this.palette);
    const {matrix} = this;
    const {width,height} = this.tileSet;
    for (var i = 0; i < matrix.length; i++){
      const value = this.replace.data[i] ? this.replace.data[i] : matrix.data[i];
      if (
        this.updated.data[i] != value
        || this.xOffset.data[i] != this.updatedXOffset.data[i]
        || this.yOffset.data[i] != this.updatedYOffset.data[i]
      ) {
        this.updateTile(value,i);
      }
      if (!matrix.data[i]) continue;
      const x1 = (matrix.dx[i]) * width;
      const y1 = (matrix.dy[i]) * height;
      const x2 = (matrix.dx[i]+1) * width;
      const y2 = (matrix.dy[i]+1) * height;
      if (!this.collidePointsX.includes(x1)) this.collidePointsX.push(x1);
      if (!this.collidePointsX.includes(x2)) this.collidePointsX.push(x2);
      if (!this.collidePointsY.includes(y1)) this.collidePointsY.push(y1);
      if (!this.collidePointsY.includes(y2)) this.collidePointsY.push(y2);
    }
  }

  updateTile(
    value,
    index
  ){
    this.tileSet.drawTile(
      this.bitmapInterface.context,

      fragmentContext.gt(value),
      this.palette,
      [
        fragmentContext.gbg(value),
        fragmentContext.gfg(value)
      ],
      (
        this.matrix.dx[index]*this.tileSet.width
        +this.xOffset.data[index]
      ),
      (
        this.matrix.dy[index]*this.tileSet.height
        +this.yOffset.data[index]
      ),
    );
    this.updated.data[index] = value;
    this.updatedXOffset.data[index] = this.xOffset.data[index];
    this.updatedYOffset.data[index] = this.yOffset.data[index];
  }

  process(program){
    fragmentContext.setTileSet(this.tileSet);
    fragmentContext.setPalette(this.palette);
    this.matrix.process(
      program
    );
  }

  write(str,fgValue,bgValue,x,y){
    fragmentContext.setTileSet(this.tileSet);
    fragmentContext.setPalette(this.palette);
    this.matrix.write(str,fgValue,bgValue,x,y);
  }
}

// 

// @BITMAPINTERFACE

// Sprite class is used to link a tileSet, a tileGrid and a palette together
// for rendering a graphical object that can be put freely on a bitmap output
export class BitmapInterface{
  // PROPERTIES
  //width;
  //height;
  //tileSet;
  //palette;
  //matrix;
  //updatedTileMatrix;
  //tileOffsetMatrix;
  //tileReplaceMatrix;
  //xOffsetMatrix;
  //updatedXOffsetMatrix;
  //yOffsetMatrix;
  //updatedYOffsetMatrix;
  canvas;
  context;
  width;
  height;
  pixelSize;

  // CONSTRUCTOR
  constructor(
    width,
    height,
    pixelSize,
    //tileSet,
    //palette,
    //matrix
  ) {
    this.canvas = document.createElement("canvas");
    this.context = this.canvas.getContext("2d");
    pixelSize = pixelSize == undefined ? 1 : pixelSize;
    this.scale(
      width,
      height,
      pixelSize
    ); 
  }
  
  //  // Start by scaling bitmap size and display according to destination width,
  //  // height and pixel size 
  // Scale output bitmap data and display according to destination width,
  // height and pixel size
  scale(
    width,
    height,
    pixelSize
  ) {
    if (arguments.length == 1) {
      pixelSize = width;
      height = this.height*width;
      width = this.width*width;
    }
    height = height%2 ? height - 1 : height;
    width = width%2 ? width - 1 : width;
    // TODO COMMENT
    this.pixelSize = pixelSize;

    // EndInterface width and height represent bitmap width and height based on given
    // destination height and width divided by pixel size
    this.width = Math.floor(width/pixelSize);
    this.height =  Math.floor(height/pixelSize);

    // Update EndInterface canvas with bitmap width and height
    this.canvas.width = this.width;
    this.canvas.height = this.height;

    // Update EndInterface canvas display with display width and height not
    // scaled by pixel size allowing display of bigger pixels for HD screen or
    // responsive adaptation
    this.canvas.style.width = Math.floor(width)+"px";
    this.canvas.style.height = Math.floor(height)+"px";
  }


  draw(bitmapInterface,x,y){
    bitmapInterface.context.drawImage(
      this.canvas,
      Math.floor(x),
      Math.floor(y)
    );
  }
}
// @SPRITE
export class Sprite{
  
  // PROPERTIES
  atlas;
  index = 0;
  length;
  tiledBitmap;
  collision;
  x = 0;
  y = 0;
  width;
  height;
  
  // CONSTRUCTOR
  constructor(
    atlas
  ) {
    atlas = atlas instanceof TiledBitmap ? [atlas] : atlas;
    this.atlas = atlas;
    this.length = atlas.length;
    this.animate(0);
  }
  
  // METHODS
  animate(
    indexShift
  ) {
    indexShift = indexShift == undefined ? 0 : indexShift;
    this.index = this.index + indexShift;
    this.index = (
      this.index < 0 
      ? this.length + this.index
      : this.index > this.length-1
      ? this.index%this.length
      : this.index
    );
    this.tiledBitmap = this.atlas[Math.floor(this.index)];
    this.width = this.tiledBitmap.bitmapInterface.width;
    this.height = this.tiledBitmap.bitmapInterface.height;
    this.collision = new Matrix(
      this.tiledBitmap.matrix.width,
      this.tiledBitmap.matrix.height
    );
  }
  // METHODS
  setFrame(
    index
  ) {
    this.index = index;
    this.tiledBitmap = this.atlas[Math.floor(this.index)];
    this.width = this.tiledBitmap.bitmapInterface.width;
    this.height = this.tiledBitmap.bitmapInterface.height;
    this.collision = new Matrix(
      this.tiledBitmap.matrix.width,
      this.tiledBitmap.matrix.height
    );
  }
  draw(
    bitmapInterface,
    customIndex
  ) {
    if (bitmapInterface instanceof TiledBitmap) {
      bitmapInterface = bitmapInterface.bitmapInterface;
    }
    if (customIndex != undefined) {
      this.atlas[Math.floor(customIndex)].bitmapInterface.draw(bitmapInterface,this.x,this.y);
    } else {
      this.tiledBitmap.bitmapInterface.draw(bitmapInterface,this.x,this.y);
    }
  }
  collide(x,y){
    if (
      x > this.x+this.width-1
      || y > this.y+this.height-1
      || x < this.x
      || y < this.y
    ) return -1
    const collideX = x - this.x;
    const collideY = y - this.y;
    const tileX = Math.floor(collideX / this.tiledBitmap.tileSet.width);
    const tileY = Math.floor(collideY / this.tiledBitmap.tileSet.height);
    const dataIndex = tileY * this.tiledBitmap.matrix.width + tileX;
    return dataIndex;
  }
  collideWith(
    sprite,
    filter,
    xOffset,
    yOffset
  ){
    filter = filter == undefined ? ()=>0 : filter;
    xOffset = xOffset == undefined ? 0 : xOffset;
    yOffset = yOffset == undefined ? 0 : yOffset;
    const collisions = [];
    const {collidePointsX,collidePointsY} = this.tiledBitmap;
    for (var y = 0; y < collidePointsY.length; y++){
      for (var x = 0; x < collidePointsX.length; x++){
        const collision = sprite.collide(Math.floor(collidePointsX[x]+this.x+xOffset),Math.floor(collidePointsY[y]+this.y+yOffset));
        if (collision < 0 || !sprite.tiledBitmap.matrix.data[collision] || filter(sprite.tiledBitmap.matrix.data[collision])) continue;
        collisions.push(collision);
      }
    }
    return collisions;
  }
  save(tileSetsList,palettesList,fileName){
    const {tileSet,palette,matrix} = this.tiledBitmap;
    const data = new Uint32Array((matrix.data.length*this.atlas.length)+4);
    data[0] = tileSetsList.indexOf(tileSet);
    data[1] = palettesList.indexOf(palette);
    data[2] = matrix.width;
    data[3] = matrix.height;
    var dataIndex = 4;
    for (var i = 0; i < this.atlas.length; i++){
      const dataList = this.atlas[i].matrix.data;
      for (var j = 0; j < dataList.length; j++){
        data[dataIndex] = dataList[j];
        dataIndex++;
      }
    }
    console.log(data);
    save([data],fileName,"application/octet-stream");
  }
}

function save(data, filename, type) {
  var file = new Blob(data, {type: type});
  var a = document.createElement("a"), url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);  
  }, 0); 
}

function load(contentProcess) {
  var input = document.createElement('input');
  input.type = 'file';
  
  input.onchange = e => { 
  
    // getting a hold of the file reference
    var file = e.target.files[0];
    // setting up the reader
    var reader = new FileReader();
    reader.readAsText(file,'UTF-8');
  
    // here we tell the reader what to do when it's done reading...
    reader.onload = readerEvent => {
      var content = readerEvent.target.result; 
      contentProcess(content);
    }
  
  }
  
  input.click();
}

function dontCross(
  x1,xw1,y1,yh1,
  x2,xw2,y2,yh2
) {
  return (
        xw1 <= x2
    &&  x1  <= x2
    ||  xw1 >= xw2
    &&  x1  >= xw2
    ||  yh1 <= y2
    &&  y1  <= y2
    ||  yh1 >= yh2
    &&  y1  >= yh2
  );
}


// @PALETTE

export class Palette{
  
  // PROPERTIES
  length;
  data=[];

  // CONSTRUCTOR
  constructor(
    hexFile,
    alpha
  ){
    const colors = hexFile.split('\n');
    for (var i = 0; i < colors.length-1; i++){
      for (var j = 0; j < colors[i].length; j+=2){
        this.data.push(
          Number(
            '0x'
            +colors[i][j]
            +colors[i][j+1]
          )
        );
      }
      this.data.push(255);
    }
    if(alpha)this.data.push(0,0,0,0);
    this.length = this.data.length/4;
  }
}

// @TILESET

export class TileSet{
  
  // PROPERTIES

  // TODO COMMENT

  templateData;

  tileImageData;
  
  // Width of individual tiles
  width;
  
  // Height of individual tiles
  height;
  
  // pixel length of individual tiles
  resolution;

  // Number of tiles per row in template
  cols;
  
  // Number of tiles rows in template
  rows;

  // Total number of tiles in template
  length;

  // CONSTRUCTOR
  constructor(

    // bitmap that will be colorized as color sheets, defining tile set
    // content, must be black and white or 1bit
    template,

    // Width of tiles to process from template sheet
    width,
    
    // Height of tiles to process from template sheet
    height,
    
  ){
    
    // TODO COMMENT
    this.width = width;
    this.height = height;
    this.resolution = width*height;
    this.cols = template.width/width;
    this.rows = template.height/height;
    this.length = this.cols * this.rows;

    this.tileImageData = new ImageData(width,height);
    this.templateData = new Uint8Array(template.width*template.height);
    
    // Create temporary canvas for processing colors into canvas
    const templateCanvas = document.createElement("canvas");
    templateCanvas.width = template.width;
    templateCanvas.height = template.height;
    const templateCanvasContext = templateCanvas.getContext("2d");

    // draw template sheet in canvas for getting its data
    templateCanvasContext.drawImage(template,0,0);

    // Get template sheet image data that will be copied for each new sheet
    // colorization
    const templateImageData = (
      templateCanvasContext.getImageData(0,0,template.width,template.height)
    );

    // TODO comment, and clean ? Make a method ?
    // Will be replaced by already saved as templateData files
    //let fty = 0; 
    for (var i = 0; i < templateImageData.data.length/4; i++) {
      const px = i%template.width;
      const py = Math.floor(i/template.width);
      const tx = Math.floor(px/width);
      const ty = Math.floor(py/height);
      const ti = tx+ty*this.cols;
      const tpx = px%width + py%height*width;
      const td = ti*(width*height)+tpx;
      this.templateData[td] = templateImageData.data[i*4] == 255;
    }
  }

  // METHODS

  // Draw tile at a particular coordinate in given bitmap context
  drawTile(

    // Bitmap context to draw tile in
    context,

    // Tile index within template
    tileIndex,
    
    // TODO COMMENT
    palette,

    // picked colors 
    colors,
    
    // destination context x axis coordinate
    x,

    // destination context y axis coordinate
    y
  ){
    // TODO COMMENT
    const tileStart = (tileIndex)*(this.resolution);
    for (var i = 0; i < this.tileImageData.data.length; i+=4){
      if (tileIndex == 0) {
        this.tileImageData.data[i] = 0;
        this.tileImageData.data[i+1] = 0;
        this.tileImageData.data[i+2] = 0;
        this.tileImageData.data[i+3] = 0;
      } else {
        // template data index
        const tdi = tileStart+i/4;
        // color index 
        const ci = colors[this.templateData[tdi]]*4;
        this.tileImageData.data[i] = palette.data[ci+0];
        this.tileImageData.data[i+1] = palette.data[ci+1];
        this.tileImageData.data[i+2] = palette.data[ci+2];
        this.tileImageData.data[i+3] = palette.data[ci+3];
      }
    }
    context.putImageData(
      this.tileImageData,
      x,
      y
    );
  }
}

// UTILS

export function write(str,x,y){
  str = (typeof str == "string" ? str : String(str));
  x = x == undefined ? 0 : x;
  y = y == undefined ? 0 : y;
  let lines = str.split('\n');
  for (var strY = 0; strY < lines.length; strY++){
    for (var strX = 0; strX < lines[strY].length; strX++) {
      let dx = x + strX;
      let dy = y + strY;
      if (
        fragmentContext.x == x + strX
        && fragmentContext.y == y + strY
      ) {
        return lines[strY][strX].charCodeAt(0);
      }
    }
  }
  return 0;
}

export function codePoint(input){
  if (input.length == 1) {
    return input.charCodeAt(0);
  } else {
    const sequence = [];
    for (var i = 0; i < input.length; i++){
      sequence.push(codePoint(input[i]));
    }
    return sequence;
  }
}

export function hex(...input){
  if (input.length == 1) {
    if (input[0].includes(" ")) {
      const sequence = input[0].split(' ');
      for (var i = 0; i < sequence.length; i++){
        sequence[i]=hex(sequence[i]);
      }
      return sequence;
    } else {
      return parseInt(input[0],16);
    }
  } else {
    const sequence = [];
    for (var i = 0; i < input.length; i++){
      sequence.push(hex(input[i]));
    }
    return sequence;
  }
}

// Import / export

export async function buildPalette(hexFileUrl,alpha){
  const response = await fetch(hexFileUrl);
  const hexFile = await response.text();
  return new Palette(hexFile,alpha);
}

export async function buildTileSet(url,tileWidth,tileHeight,cols,rows){

  let template;

  await new Promise(resolve => {
    template = new Image();
    template.src = url;
    template.onload = resolve;
  });

  return new TileSet(
    template,
    tileWidth,
    tileHeight
  );
}

function readTxtrByte(b){
  const str = b.toString(16);
  return str.length == 1 ? '0'+str:str;
}

export function readTxtrAt(bytes,i){
  return parseInt(
      readTxtrByte(bytes[i*4+3])
     +readTxtrByte(bytes[i*4+2])
     +readTxtrByte(bytes[i*4+1])
     +readTxtrByte(bytes[i*4]),
    16
  );
}

export async function buildAtlas(txtr,tileSetsList,palettesList){
  const response = await fetch(txtr);
  const bytes = await response.bytes();
  const tileSetIndex = readTxtrAt(bytes,0);
  const paletteIndex = readTxtrAt(bytes,1);
  const width        = readTxtrAt(bytes,2);
  const height       = readTxtrAt(bytes,3);
  const length       = width*height;
  var frameIndex = 0;
  const sequence = [];
  for (var i = 4; i < bytes.length/4; i++){
    frameIndex = Math.floor(i/length);
    if (sequence.length < frameIndex){
      if (sequence.length) sequence[sequence.length-1].update();
      sequence.push(
        new TiledBitmap(
          tileSetsList[tileSetIndex],
          palettesList[paletteIndex],
          new Matrix(width,height)
        )
      );
    }
    sequence[frameIndex-1].matrix.data[i-4-length*(frameIndex-1)] = readTxtrAt(bytes,i);
  }
  sequence[sequence.length-1].update();
  return sequence;
}

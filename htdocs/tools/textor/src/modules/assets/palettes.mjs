import {buildPalette} from "textor/textor.mjs";
const palettesPath = import.meta.resolve("textor/")+"../palettes/";
export const base1 = await buildPalette(palettesPath+"base1.hex");
export const base1a = await buildPalette(palettesPath+"base1.hex",true);
export const base2 = await buildPalette(palettesPath+"base2.hex");
export const base2a = await buildPalette(palettesPath+"base2.hex",true);
export const base3 = await buildPalette(palettesPath+"base3.hex");
export const base3a = await buildPalette(palettesPath+"base3.hex",true);
export const base4 = await buildPalette(palettesPath+"base4.hex");
export const base4a = await buildPalette(palettesPath+"base4.hex",true);
export const lisa = await buildPalette(palettesPath+"lisa.hex");
export const ambre = await buildPalette(palettesPath+"ambre.hex");
export const ines = await buildPalette(palettesPath+"ines.hex");
export const unamed = await buildPalette(palettesPath+"unamed.hex");

/* .txtr header index */
/*                    */ export const palettesList = [
/*                  0 */   base1,
/*                  1 */   base1a,
/*                  2 */   base2,
/*                  3 */   base2a,
/*                  4 */   base3,
/*                  5 */   base3a,
/*                  6 */   base4,
/*                  7 */   base4a,
/*                  8 */   lisa,
/*                    */ ];

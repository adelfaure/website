import {buildTileSet} from "textor/textor.mjs";
const tileSetTemplatesPath = import.meta.resolve("textor/")+"../tileSetTemplates/";
export const semigraphRegular8x8  = await buildTileSet(tileSetTemplatesPath+"semigraphRegular8x8.png",8,8);
export const semigraphBold8x8     = await buildTileSet(tileSetTemplatesPath+"semigraphBold8x8.png",8,8);
export const semigraphHeavy8x8    = await buildTileSet(tileSetTemplatesPath+"semigraphHeavy8x8.png",8,8);
export const ascii4x8             = await buildTileSet(tileSetTemplatesPath+"ascii4x8.png",4,8);
export const ascii6x12            = await buildTileSet(tileSetTemplatesPath+"ascii6x12.png",6,12);
export const ascii8x16            = await buildTileSet(tileSetTemplatesPath+"ascii8x16.png",8,16);
export const liblab4x8            = await buildTileSet(tileSetTemplatesPath+"liblab4x8.png",4,8);
export const kirby40x40           = await buildTileSet(tileSetTemplatesPath+"kirby40x40.png",40,40);
export const mario53x53           = await buildTileSet(tileSetTemplatesPath+"mario53x53.png",53,53);
export const compicto100x100      = await buildTileSet(tileSetTemplatesPath+"compictoSeul.png",100,100);
export const compicto2_50x50      = await buildTileSet(tileSetTemplatesPath+"compicto2.png",50,50);
export const mario32x32           = await buildTileSet(tileSetTemplatesPath+"mario32x32.png",32,32);
export const MOUTON1_7x5          = await buildTileSet(tileSetTemplatesPath+"MOUTON1_7x5.png",7,5);
export const compass8x8           = await buildTileSet(tileSetTemplatesPath+"compass8x8.png",8,8);

/* .txtr header index */
/*                    */ export const tileSetsList = [
/*                  0 */   semigraphRegular8x8,
/*                  1 */   semigraphBold8x8,
/*                  2 */   semigraphHeavy8x8,  
/*                  3 */   ascii4x8, 
/*                  4 */   ascii6x12,
/*                  5 */   ascii8x16,
/*                  6 */   liblab4x8,
/*                  7 */   kirby40x40,
/*                  8 */   mario53x53,
/*                  9 */   compicto100x100,
/*                    */ ];

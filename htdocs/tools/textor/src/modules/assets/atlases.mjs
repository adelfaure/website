import {buildAtlas} from "textor/textor.mjs";
import {tileSetsList} from "textor/assets/tileSets.mjs";
import {palettesList} from "textor/assets/palettes.mjs";
const atlasesPath = import.meta.resolve("textor/")+"../atlases/";
export const ball = await buildAtlas(atlasesPath+"ball.txtr",tileSetsList,palettesList);
export const colorBall = await buildAtlas(atlasesPath+"colorBall.txtr",tileSetsList,palettesList);

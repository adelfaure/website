export function write(fragment,str,x,y){
  let lines = str.split('\n');
  for (var strY = 0; strY < lines.length; strY++){
    for (var strX = 0; strX < lines[strY].length; strX++) {
      let dx = x + strX;
      let dy = y + strY;
      if (
        fragment.x == x + strX
        && fragment.y == y + strY
      ) {
        return lines[strY][strX].charCodeAt(0);
      }
    }
  }
  return 0;
}

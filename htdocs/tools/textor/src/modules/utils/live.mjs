import {
  BitmapInterface,
  TiledBitmap,
  EndInterface,
  Sprite,
  Animation
} from "textor/textor.mjs";

import {ascii8x16} from "textor/assets/tileSets.mjs";
import {base1, base3a} from "textor/assets/palettes.mjs";
import {createTileSetMap} from "textor/utils/tileSetMap.mjs";

const encoder = new TextEncoder();
export const log = document.createElement('div');
log.id = "errorLog";
var freeColor = 0;
var fps = 60;
var tileSet = ascii8x16;
var palette = base1;
var scale = 2;
var fragment = ()=>0;
var frame = ()=>1;
var bitmapInterface = 0; 
var endInterface = 0;
var sprite = 0;
var domParent = 0;
var parentRect = 0;
var animation = 0;
var tileSetMap = createTileSetMap(tileSet);
export const showTileSetMap = [0];

export async function updateLive(sourceFunction) {
  const module = await buildModule(sourceFunction);
  if (module) {
    if (animation) { 
      applyModuleChanges(module);
      animation.play(
        fps,
        async function(frameContext){
          return liveFrame(frameContext);
        }
      );
    } else {
      animation = new Animation(fps,
        async function(frameContext){
          return liveFrame(frameContext);
        }
      );
      applyModuleChanges(module);
    }
    return 1;
  } else {
    return 0;
  }
}
async function buildModule(sourceFunction){
  const text = await sourceFunction();
  const blob = new Blob([encoder.encode(text)],{"type":"text/javascript"});
  const module = URL.createObjectURL(blob);
  try {
    return await import(module).then((module) => {
      return module;
    });
  } catch (e){
    const errorStr = e.toString()+' (line '+e.lineNumber+')';
    log.textContent = errorStr;
    log.style.display = "block";
  }
}

function applyModuleChanges(module){
  fps = 60;
  scale = 2;
  tileSet = ascii8x16;
  tileSetMap = createTileSetMap(tileSet);
  palette = base1;
  freeColor = 0;
  fragment = ()=>0;
  frame = ()=>1;
  log.style.display = "none";
  fragment = module.fragment != undefined ? module.fragment : fragment;
  frame = module.frame != undefined ? module.frame : frame;
  const newScale = module.scale != undefined ? module.scale : scale;
  const newPalette = module.palette != undefined ? module.palette : palette;
  const newTileSet = module.tileSet != undefined ? module.tileSet : tileSet;
  const newFps = module.fps != undefined ? module.fps : fps;
  freeColor = module.freeColor != undefined ? module.freeColor : freeColor;
  var updateTiledBitmap = false;
  if (newFps != fps) {
    fps = newFps;
    animation.targetFrameRate = fps;
  }
  if (newScale != scale) {
    parentRect = domParent.getBoundingClientRect();
    scale = newScale;
    bitmapInterface.scale(parentRect.width,parentRect.height,scale);
    updateTiledBitmap = true;
  }
  if (newTileSet != tileSet) {
    tileSet = newTileSet;
    updateTiledBitmap = true;
    tileSetMap = createTileSetMap(tileSet);
  }
  if (newPalette != palette) {
    palette = newPalette;
    updateTiledBitmap = true;
  }
  if (updateTiledBitmap) {
    sprite = new Sprite(
      new TiledBitmap(
        tileSet,
        palette,
        new BitmapInterface(
          parentRect.width,
          parentRect.height,
          scale
        )
      )
    )
  }
  responsive();
}

async function liveFrame(frameContext){
  const {f,k,t} = frameContext;
  var frameValue = 1;
  responsive();
  try {
    frameValue = frame(frameContext);
    if (freeColor) {
      sprite.tiledBitmap.process(fragment);
    } else {
      sprite.tiledBitmap.process((frag,frame)=>{
        const {fg,c,t} = frag;
        const value = fragment(frag,frame)%t+fg(c-1);
        return value;
      });
    }
    sprite.tiledBitmap.update();
    sprite.draw(bitmapInterface);
    if (showTileSetMap[0]) tileSetMap.draw(bitmapInterface);
  } catch (e){
    const errorStr = e.toString()+' (line '+e.lineNumber+')';
    log.textContent = errorStr;
    log.style.display = "block";
  }
  return (frameValue && !k[232]);
}

export async function initLive(initParent,sourceFunction){
  domParent = initParent;
  parentRect = domParent.getBoundingClientRect();
  bitmapInterface = new BitmapInterface(
    parentRect.width,
    parentRect.height,
    scale
  );
  endInterface = new EndInterface(
    bitmapInterface,
    domParent
  );
  sprite = new Sprite(
    new TiledBitmap(
      tileSet,
      palette,
      new BitmapInterface(
        parentRect.width,
        parentRect.height,
        scale
      )
    )
  );
  return await updateLive(sourceFunction);
}

function responsive(){
  const parentRectTest = domParent.getBoundingClientRect();
  if (
    parentRectTest.width != parentRect.width
    || parentRectTest.height != parentRect.height
  ) {
    parentRect = parentRectTest;
    bitmapInterface.scale(parentRect.width,parentRect.height,scale);
    sprite = new Sprite(
      new TiledBitmap(
        tileSet,
        palette,
        new BitmapInterface(
          parentRect.width,
          parentRect.height,
          scale
        )
      )
    )
  }
}


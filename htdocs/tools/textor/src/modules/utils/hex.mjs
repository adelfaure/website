export function hex(...input){
  if (input.length == 1) {
    return parseInt(input[0],16);
  } else {
    const sequence = [];
    for (var i = 0; i < input.length; i++){
      sequence.push(hex(input[i]));
    }
    return sequence;
  }
}

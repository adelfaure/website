import {BitmapInterface,Matrix,TiledBitmap,Sprite,fragmentContext} from "textor/textor.mjs";
import {base2} from "textor/assets/palettes.mjs";

export function createTileSetMap(tileSet) {
  
  const tileSetMapMatrix = new Matrix(tileSet.cols,tileSet.rows);
  tileSetMapMatrix.process((frag)=>frag.i);
  tileSetMapMatrix.place(2,2,2,2);
  
  const mapRulesAssemblageMatrix = new Matrix(tileSet.cols+2,tileSet.rows+2);
  mapRulesAssemblageMatrix.place(1,1,2,2);

  fragmentContext.setTileSet(tileSet);
  fragmentContext.setPalette(base2);

  mapRulesAssemblageMatrix.process((frag)=>{
    const {i,w,fg,bg,x,y} = frag;
    const tileSetMapMatrixPrint = tileSetMapMatrix.printAt(i,w);
    const hexRule = (
      x == 0 && !((y-2)%16) && y || y == 0 && !((x-2)%16) && x
      ? (((x+y-2)/16)%16).toString(16).charCodeAt(0)
      : x == 1 && y > 1 || y == 1 && x > 1
      ? ((x+y-3)%16).toString(16).charCodeAt(0)
      : 0
    );
    return (
      tileSetMapMatrixPrint
      ? tileSetMapMatrixPrint + fg(3) + bg((x+y)%2?1:0)
      : hexRule
      ? hexRule + fg(2)
      : 0
    )
  });

  const tileSetMapBitmap = new BitmapInterface(
    (tileSet.cols+2)*tileSet.width+tileSet.width*2,
    (tileSet.rows+2)*tileSet.height+tileSet.height*2
  );
  
  const tileSetMap = new TiledBitmap(
    tileSet,
    base2,
    tileSetMapBitmap
  );
  
  tileSetMap.process((frag)=>{
    const {i,w,fg,bg,x,y} = frag;
    const mapRulesAssemblageMatrixPrint = mapRulesAssemblageMatrix.printAt(i,w);
    return (
      mapRulesAssemblageMatrixPrint
      ? mapRulesAssemblageMatrixPrint
      : 32
    )
  });
  
  tileSetMap.update();
  
  return new Sprite(tileSetMap);
}

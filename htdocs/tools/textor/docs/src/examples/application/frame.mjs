// frame.mjs
import {BitmapInterface,EndInterface,TiledBitmap} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";
import {base4} from "textor/assets/palettes.mjs";
import {main as fragment} from "./fragment.mjs";

const bitmapInterface = new BitmapInterface(
  window.innerWidth,
  window.innerHeight,
  4
);

const endInterface = new EndInterface(
  bitmapInterface,
  document.body
);

const tiledBitmap = new TiledBitmap(
  semigraphRegular8x8,
  base4,
  bitmapInterface
);

export function main(frame){
  tiledBitmap.process(fragment);
  tiledBitmap.update();
  return !frame.k[13];
}

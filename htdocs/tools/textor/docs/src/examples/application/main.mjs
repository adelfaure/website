// main.mjs
import {Animation} from "textor/textor.mjs";
import {main as frame} from "./frame.mjs";

new Animation(
  60,
  frame
);

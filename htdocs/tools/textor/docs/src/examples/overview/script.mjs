import * as Textor from "textor/textor.mjs";
import * as TileSets from "textor/tileSets.mjs";
import * as Palettes from "textor/palettes.mjs";

var pixelSize = 3;

const endInterface = new Textor.EndInterface(
  new Textor.BitmapInterface(
    window.innerWidth,
    window.innerHeight,
    pixelSize
  ),
  document.body
);

const background = new Textor.Sprite(
  new Textor.TiledBitmap(
    TileSets.ascii8x16,
    Palettes.base4,
    new Textor.BitmapInterface(
      window.innerWidth,
      window.innerHeight,
      pixelSize
    )
  )
);

background.tiledBitmap.process((frag,frame)=>{
  return (
    //Math.random()>0.9 
    //&& !(
    //  frag.x < Math.round(frag.w/2+2)
    //  && frag.x > Math.round(frag.w/2-2)
    //  && frag.y < Math.round(frag.h/2+2)
    //  && frag.y > Math.round(frag.h/2-2)
    //)
    //|| 
    frag.x == 0
    || frag.y == 0
    || frag.x == frag.w-1
    || frag.y == frag.h-1
    ? 35 + frag.fg(8)//Math.floor(frag.x/frag.c+frag.y/frag.c*2)%(frag.c-2)+1)
    : 1
  );
});
background.tiledBitmap.update();

const log = new Textor.Sprite(
  new Textor.TiledBitmap(
    TileSets.ascii8x16,
    Palettes.base1,
    new Textor.BitmapInterface(
      window.innerWidth,
      window.innerHeight,
      pixelSize
    )
  )
);

const balls = [];
const ballsElements = [];
const dirBalls = [];

function addToCollision(collision,from,to,x,y){
  const additionalCollision = from.collideWith(
    to,
    (d)=>d==1,
    x,
    y
  );
  if (additionalCollision) {
    collision.push(...additionalCollision);
  }
}

const ballColorSequence = [];
for (var i = 1; i < Palettes.base4.length-1; i++) {
  let tiledBitmap = new Textor.TiledBitmap(
    TileSets.semigraphBold8x8,
    Palettes.base4,
    2,
    2
  )
  tiledBitmap.process((frag)=>(
    16*12-2+(
      frag.i == 0 ? 0:
      frag.i == 1 ? 1:
      frag.i == 2 ? -1:
      2
    )+frag.fg(i)
  ));
  tiledBitmap.update();
  ballColorSequence.push(tiledBitmap);
}

function ball(frame){
  if (frame.fps && frame.fps < 60) return;
  const ball = new Textor.Sprite(ballColorSequence);
  ball.setFrame(8);
  ball.x = background.width/2 - ball.width/2;
  ball.y = background.height/2 - ball.height/2;
  const ballCollision = [];
  for (var j = 0; j < ballsElements.length; j++){
    addToCollision(ballCollision,ball,ballsElements[j],0,0);
  }
  if (ballCollision.length) return;
  
  ballsElements.push(ball);
  const index = dirBalls.length;
  dirBalls.push(
    Math.random() > 0.5 ? 1 : -1,
    Math.random() > 0.5 ? 1 : -1,
  );
  balls.push(
    function(frame,i){
      const ballXCollision = [];
      addToCollision(ballXCollision,ball,background,dirBalls[index],0);
      for (var j = 0; j < ballsElements.length; j++){
        if (i == j) continue;
        addToCollision(ballXCollision,ball,ballsElements[j],dirBalls[index],0);
      }
      if (!ballXCollision.length) {
        ball.x+=dirBalls[index];
      } else {
        dirBalls[index] *= -1;
        ball.setFrame(Math.floor(Math.random()*ballColorSequence.length));
      }
      const ballYCollision = [];
      addToCollision(ballYCollision,ball,background,0,dirBalls[index+1]);
      for (var j = 0; j < ballsElements.length; j++){
        if (i == j) continue;
        addToCollision(ballYCollision,ball,ballsElements[j],0,dirBalls[index+1]);
      }
      if (!ballYCollision.length) {
        ball.y+=dirBalls[index+1];
      } else {
        dirBalls[index+1] *= -1;
        ball.setFrame(Math.floor(Math.random()*ballColorSequence.length));
      }
      ball.draw(frame.o);
    }
  );
}


const countDown = Infinity;

const animation = new Textor.Animation(
  60,
  function(frame){
    ball(frame);
    background.draw(frame.o);
    for (var i = 0; i < balls.length; i++){
      balls[i](frame,i);
    }
    const th = log.tiledBitmap.tileSet.height;
    const tw = log.tiledBitmap.tileSet.width;
    var str = "FPS: "+frame.fps;
    log.tiledBitmap.write(str,1,0,Math.floor(frame.w/tw)-str.length,Math.floor(frame.h/th)-2);
    var str = "Balls: "+balls.length;
    log.tiledBitmap.write(str,1,0,Math.floor(frame.w/tw)-str.length,Math.floor(frame.h/th)-1);
    log.tiledBitmap.update();
    log.draw(frame.o);
    return frame.t < countDown*1000;
  }
);

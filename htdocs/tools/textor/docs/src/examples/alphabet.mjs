import {BitmapInterface,TiledBitmap,EndInterface} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/tileSets.mjs";
import {base4} from "textor/palettes.mjs";

const pixelSize = 3

const backgroundBitmap = new BitmapInterface(
  window.innerWidth,
  window.innerHeight,
  pixelSize
);

const background = new TiledBitmap(
  semigraphRegular8x8,
  base4,
  backgroundBitmap
);

background.process((frag)=>(65+frag.i%26)+frag.fg(15));
background.update();

const endInterface = new EndInterface(
  backgroundBitmap,
  document.body
);

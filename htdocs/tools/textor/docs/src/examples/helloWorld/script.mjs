import * as Textor from "textor/textor.mjs";
import * as TileSets from "textor/tileSets.mjs";
import * as Palettes from "textor/palettes.mjs";

const width = window.innerWidth;
const height = window.innerHeight;
const pixelSize = 2;

const endInterface = new Textor.EndInterface(
  width,
  height,
  pixelSize,
  document.body
);

const tiledBackground = new Textor.TiledBitmap(
  endInterface.bitmapInterface,
  TileSets.semigraphBold8x8,
  Palettes.base4,
  new Textor.Matrix(
    Math.ceil(width/(TileSets.semigraphBold8x8.width*pixelSize)),
    Math.ceil(height/(TileSets.semigraphBold8x8.height*pixelSize))
  )
);

tiledBackground.write("Hello world");
tiledBackground.write("Hello world",15,0,0,1);
tiledBackground.write("Hello world",0,15,0,2);
tiledBackground.write("Hello world",12,14,5,5);

tiledBackground.update();

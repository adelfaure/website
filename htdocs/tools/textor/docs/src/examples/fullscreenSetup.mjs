import {BitmapInterface,TiledBitmap,EndInterface} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/tileSets.mjs";
import {base4} from "textor/palettes.mjs";

export const screenTileSet = semigraphRegular8x8;
export const screenPalette = base4;
export var pixelSize = 3;

export const screenBitmap = new BitmapInterface(
  window.innerWidth,
  window.innerHeight,
  pixelSize
);

export const screen = new TiledBitmap(
  screenTileSet,
  screenPalette,
  screenBitmap
);

export const endInterface = new EndInterface(
  screenBitmap,
  document.body
);

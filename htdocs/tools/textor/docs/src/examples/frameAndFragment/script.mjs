import * as Textor from "textor/textor.mjs";
import * as TileSets from "textor/tileSets.mjs";
import * as Palettes from "textor/palettes.mjs";
import {main as frame} from "./frame.mjs";

const width = window.innerWidth;
const height = window.innerHeight;
const pixelSize = 2;

const endInterface = new Textor.EndInterface(
  width,
  height,
  pixelSize,
  document.body
);

const tiledBackground = new Textor.TiledBitmap(
  endInterface.bitmapInterface,
  TileSets.semigraphBold8x8,
  Palettes.base4,
  new Textor.Matrix(
    Math.ceil(width/(TileSets.semigraphBold8x8.width*pixelSize)),
    Math.ceil(height/(TileSets.semigraphBold8x8.height*pixelSize))
  )
);

const animation = new Textor.Animation(
  60,
  frame
);


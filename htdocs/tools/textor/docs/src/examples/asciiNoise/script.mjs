import * as Textor from "textor/textor.mjs";
import * as TileSets from "textor/tileSets.mjs";
import * as Palettes from "textor/palettes.mjs";

function fragment(ctx) {

  // Making shortcuts
  const {s,c} = ctx;

  return Math.floor(
    Math.random()

    // TileSet length representing number of its available tile shapes
    * s
    * ( 

      // Palette length representing number of its available colors
      c

      // Mutliplied by itself for taking in range foreground and backgrond
      // combinations
      * c
    )
  ); 
}

const matrixInstance = new Textor.Matrix(
  // Matrix width
  8,
  // Matrix height
  8
);

// Processing matrix data using a fragment function
matrixInstance.process(

  // Fragment function defined before
  fragment,

  // TileSet containing 128 ascii characters in 8 per 16 pixels
  TileSets.ascii8x16,

  // 1 bit black and white palette
  Palettes.base1,
);

console.log(Palettes.base1);


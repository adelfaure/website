const matrixInstance = new Textor.Matrix(
  // Matrix width
  8,
  // Matrix height
  8
);

// Processing matrix data using a fragment function
matrixInstance.process(

  // Fragment function defined before
  fragment,

  // TileSet containing 128 ascii characters in 8 per 16 pixels
  TileSets.ascii8x16,

  // 1 bit black and white palette
  Palettes.base1,
);

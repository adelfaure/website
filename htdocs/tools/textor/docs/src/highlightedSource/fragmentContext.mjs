const framgentContext = {
  i,    // tile index
  x,    // tile x coordinate
  y,    // tile y coordinate
  w,    // tile width
  h,    // tile height
  di,   // tile data at current index
  d,    // tiles data (matrix buffer)
  s,    // tileSet length / number of tiles shapes
  sw,   // tileSet tile width
  sh,   // tileSet tile height
  c,    // palette length / number of colors
  fg,   // Set foreground function
  bg,   // Set background function
  t,    // animation current time in milliseconds
  f,    // animation current frame number
  fps,  // animation frame rate
  m,    // mouse down status
  mx,   // mouse x coordinate
  my,   // mouse y
  k,    // keys inputs
  W,    // EndInterface width
  H,    // EndInterface height
};

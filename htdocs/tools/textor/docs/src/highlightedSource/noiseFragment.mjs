function fragment(ctx) {

  // Making shortcuts
  const {s,c} = ctx;

  return Math.floor(
    Math.random()

    // TileSet length representing number of its available tile shapes
    * s
    * ( 

      // Palette length representing number of its available colors
      c

      // Mutliplied by itself for taking in range foreground and backgrond
      // combinations
      * c
    )
  ); 
}

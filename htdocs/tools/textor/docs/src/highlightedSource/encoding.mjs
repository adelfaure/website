const tileShape = (
  dataValue 
  % tileSet.length
);

const tileBackground = (
  Math.floor(
    dataValue
    / tileSet.length
  )
  % palette.length
);

const tileForeground = (
  Math.floor(
    dataValue
    / (
      tileSet.length
      * palette.length
    )
  )
  % palette.length
);

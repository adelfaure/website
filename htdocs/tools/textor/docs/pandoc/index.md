```html
<!doctype html>
<html>
  <head>
    <style>
      body{
        background-color:#000;
        margin:0;
        overflow:hidden;
      }
    </style>
    <script type="importmap">
      {
        "imports": {
          "textor/": "../../src/modules/"
        }
      }
    </script>
    <script type="module" src="./init.mjs"></script>
  </head>
  <body>
  </body>
</html>
```

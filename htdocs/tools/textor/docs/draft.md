# How it works

Textor is a library designed to create, manage and render 
[text mode](https://polyducks.co.uk/pages/what-is-textmode/)
and 
[tile-based](https://en.wikipedia.org/wiki/Tile-based_video_game)
raster graphics. It is my personal API for making games, tools and artworks. It
is based on the manipulation of grid positionned numbers that are processed as
colored tiles. 

# Overview 

Textor contains ten classes, height of them are intended to setup and update
two context classes properties that are meant to be dispatched between two
kinds of programs: `frame` and `fragment`.

```
.-----------------.  .-----------.  .-----------.  .--------.  .-----------.   
| BitmapInterface |  | TileSet   |  | Palette   |  | Matrix |  | Animation |   
'-----------------'  '-----------'  '-----------'  '--------'  '-----------'   
   |           |        |  |           |     |        |  |        |            
   |  .-----------------'  |           |     |        |  |        |            
   |  |        |           |           |     |        |  |        |            
   |  |  .-----------------------------'     |        |  |        |            
   |  |  |     |           |                 |        |  |        |            
   |  |  |  .-----------------------------------------'  |        |            
   |  |  |  |  |           |                 |           |        |            
   |  |  |  |  '-----.     '--------------.  |           |        |            
   |  |  |  |        |                    |  |           |        |            
   V  V  V  V        V                    |  |           |        |            
.--------------.  .--------------.        |  |           |        |            
| TiledBitmap  |  | EndInterface |        |  |           |        |            
'--------------'  '--------------'        |  |           |        |            
   |                    |                 |  |           |        |            
   V                    |                 |  |           |        |            
.--------.              |                 |  |           |        |            
| Atlas  |              |        .--------------------------------'            
'--------'              |        |        |  |           |                     
   |                    |        |        |  |           |                     
   V                    V        V        V  V           V                     
.--------.        .-----------------.  .--------------------.                  
| Sprite |        | FrameContext    |  | FragmentContext    |                  
'--------'        '-----------------'  '--------------------'                  
                     |  |                    |                                 
                     |  '-----------------.  |                                 
                     |                    |  |                                 
                     V                    V  V                                 
                  Frame program        Fragment program                        
```

## Frame program

Instructions within a `frame program` will be performed at each frame of the
animation process handled by an `Animation` class instance.

A `frame program` used as construction argument of a new `Animation` instance
will be automatically feeded by a package of information through a
`frameContext` argument.

The `Animation` instance will read from `frame program` output, awaiting for a
`true` or `false` value for performing next frame or stopping animation
process.

`EndInterface` instance will feed `frame program` context with user input
values and values useful for painting pixels on screen.

Theses values includes `EndInterface` `BitmapInterface` instance value along
its width and height, user mouse down, mouse x and y coordinates and user
pressed keys.

`Animation` instance will feed `frame program` context with current time, frame
and frame rate values.

```mjs

// frameContext object automatically build and sended as frame program argument
const frameContext = {
  t,    // animation time
  f,    // animation frame
  fps,  // animation frame rate
  m,    // mouse down
  x,    // mouse x
  y,    // mouse y
  k,    // keys
  w,    // end interface width
  h,    // end interface width
  o     // end interface bitmapInterface
}

// Frame program function structure
function frame(frameContext){

  // Must return 1 or 0 value, requesting next frame execution or stopping
  // animation process
  return foo;
}
```

## Fragment program

Inspired how
<a href="https://play.ertdfgcvb.xyz/">Anreas Gysin's Play</a> 
use GLSL programming style for processing text, Textor way of processing tiles
is close to how GLSL fragment shaders process pixels.  Where fragment shaders
calculate color of pixels individually through a program returning four float
values (float4), Textor fragments return single integer value corresponding to
colored tile code.

A `fragment program` used as `process` method argument of `TiledBitmap` or
`Matrix` instance will be automatically feeded by a package of information
through a `fragmentContext` argument.

`Matrix` instance will read from `fragment program` output for defining and/or
updating its stored data.

`Tileset` instance will feed `fragment program` context with availables tiles
number, tile width and tile width.


### Fragment context

```mjs
fragmentContext.i;      // tile data index
fragmentContext.x;      // tile x coordinate
fragmentContext.y;      // tile y coordinate
fragmentContext.w;      // tile width
fragmentContext.h;      // tile height
fragmentContext.di;     // tile data at index
fragmentContext.d;      // tiles data
fragmentContext.t;      // tileSet length
fragmentContext.tw;     // tileSet tile width
fragmentContext.th;     // tileSet tile height
fragmentContext.c;      // palette length
fragmentContext.fg();   // fg
fragmentContext.bg();   // bg
fragmentContext.gfg();  // get fg 
fragmentContext.gbg();  // get bg 
fragmentContext.gt();   // get tile
```


# Encoding

The way Textor handle tiles is similar to 
[character encoding](https://en.wikipedia.org/wiki/Character_encoding), 
assigning numbers to tiles shape, foreground and background color.

Textor encoding system is not static and vary depending on number of available
tile shapes and colors handled by TileSet and Palette classes.

Encoding values are stored and processed throught Matrix class instances
working similary to buffers. Matrix instances will define the X and Y
coordinates of each tile according to their storage index position. 

```
.--------.                                                                   
| Matrix |                                               LEGEND              
'--------'                                                                   
   |  |                                                  i   data index      
   |  '--------------.                                   w   matrix width    
   |                 |                                   d   data value      
   V                 V                                   t   tiles length    
.--------------.  .--------------.                       c   color length    
| Data value   |  | Data index   |                       [ ] floor function  
'--------------'  '--------------'                       %   modulo operation
   |  |  |           |     |                                                 
   |  |  |           |     '--------------.                                  
   |  |  |           |                    |                                  
   |  |  |           V                    V                                  
   |  |  |        .-----.           .-----------.                            
   |  |  |        | i%w |           | [i/w]*w   |                            
   |  |  |        '-----'           '-----------'                            
   |  |  |           |                    |                                  
   |  |  |           V                    V                                  
   |  |  |        .-----------------.  .-----------------.                   
   |  |  |        | Tile X position |  | Tile Y position |                   
   |  |  |        '-----------------'  '-----------------'                   
   |  |  |           |                    |                                  
   |  |  |           |                    '--------------------------------. 
   |  |  |           '--------------------------------------------------.  | 
   |  |  |     .--------------.  .-----------.                          |  | 
   |  |  |     | TileSet      |  | Palette   |                          |  | 
   |  |  |     '--------------'  '-----------'                          |  | 
   |  |  |        |     |  |        |  |                                |  | 
   |  |  |        |     |  |        |  '--------------.                 |  | 
   |  |  |        |     |  '-----------------------.  |                 |  | 
   |  |  |        |     |           |              |  |                 |  | 
   |  |  '--------------------------------------.  |  |                 |  | 
   |  |           |     |           |           |  |  |                 |  | 
   |  '--------------.  |  .--------'           |  |  |                 |  | 
   |              |  |  |  |                    |  |  |                 |  | 
   |  .-----------'  |  |  |                    |  |  |                 |  | 
   |  |              |  |  |                    |  |  |                 |  | 
   V  V              V  V  V                    V  V  V                 |  | 
.--------.        .-----------.              .--------------.           |  | 
| d%t    |        | [d/t]%c   |              | [d/(t*c)]%c  |           |  | 
'--------'        '-----------'              '--------------'           |  | 
   |                 |                          |                       |  | 
   V                 V                          V                       |  | 
.--------------.  .-----------------------.  .-----------------------.  |  | 
| Tile shape   |  | Tile background color |  | Tile foreground color |  |  | 
'--------------'  '-----------------------'  '-----------------------'  |  | 
   |                 |                          |                       |  | 
   |  .--------------'                          |                       |  | 
   |  |  .--------------------------------------'                       |  | 
   |  |  |  .-----------------------------------------------------------'  | 
   |  |  |  |  .-----------------------------------------------------------' 
   |  |  |  |  |                                                             
   V  V  V  V  V                                                             
.-----------------.                                                          
| Raster graphics |                                                          
'-----------------'                                                          
```

## File format: .txtr

```
   32-Bit   --> 0-4294967296    TileSet: semigraphRegular8x8 320 tiles    (t)
 ____|____                      Palette: base4               16+1* colors (c)
|         |                     |___________________________________________|
be 13 01 00 --> 70590 (d)                             |                    
                |__ Tile              --> d%t         --> 190 be
                |__ Background color  --> [d/t]%c     --> 16  10 
                |__ Foreground color  --> [d/(t*c)]%c --> 12  0c
```

```
      Tile set    Palette     Width       Height
0000  00 00 00 00 02 00 00 00 02 00 00 00 02 00 00 00  Header
0010  be 00 00 00 bf 00 00 00 bd 00 00 00 c0 00 00 00  Frames
```

```
      TileSet     Palette     Width       Height
                                                
0000  00 00 00 00 02 00 00 00[02]00 00 00[02]00 00 00  Header
0010 [fe 29 00 00 ff 29 00 00 fd 29 00 00 00 2a 00 00] Frame 0
0020 [3e 3f 00 00 3f 3f 00 00 3d 3f 00 00 40 3f 00 00] Frame 1
0030 [7e 54 00 00 7f 54 00 00 7d 54 00 00 80 54 00 00] Frame 2
0040 [be 69 00 00 bf 69 00 00 bd 69 00 00 c0 69 00 00] Frame 3
0050 [fe 7e 00 00 ff 7e 00 00 fd 7e 00 00 00 7f 00 00] Frame 4
0060 [3e 94 00 00 3f 94 00 00 3d 94 00 00 40 94 00 00] Frame 5
0070 [7e a9 00 00 7f a9 00 00 7d a9 00 00 80 a9 00 00] Frame 6
0080 [be be 00 00 bf be 00 00 bd be 00 00 c0 be 00 00] Frame 7
0090 [fe d3 00 00 ff d3 00 00 fd d3 00 00 00 d4 00 00] Frame 8
00a0 [3e e9 00 00 3f e9 00 00 3d e9 00 00 40 e9 00 00] Frame 9
00b0 [7e fe 00 00 7f fe 00 00 7d fe 00 00 80 fe 00 00] Frame 10
00c0 [be 13 01 00 bf 13 01 00 bd 13 01 00 c0 13 01 00] Frame 11
00d0 [fe 28 01 00 ff 28 01 00 fd 28 01 00 00 29 01 00] Frame 12
00e0 [3e 3e 01 00 3f 3e 01 00 3d 3e 01 00 40 3e 01 00] Frame 13
00f0 [7e 53 01 00 7f 53 01 00 7d 53 01 00 80 53 01 00] Frame 14
```

```
      TileSet     Palette     Width       Height
                                                
0000  00 00 00 00 02 00 00 00[04]00 00 00[02]00 00 00  Header
0010 [fe 29 00 00 ff 29 00 00 fd 29 00 00 00 2a 00 00  Frame 0
0020  3e 3f 00 00 3f 3f 00 00 3d 3f 00 00 40 3f 00 00]        
0030 [7e 54 00 00 7f 54 00 00 7d 54 00 00 80 54 00 00  Frame 1
0040  be 69 00 00 bf 69 00 00 bd 69 00 00 c0 69 00 00]        
```

```
      TileSet     Palette     Width       Height
                                                
0000  00 00 00 00 02 00 00 00[03]00 00 00[03]00 00 00  Header
0010 [fe 29 00 00 ff 29 00 00 fd 29 00 00 00 2a 00 00  Frame 0
0020  3e 3f 00 00 3f 3f 00 00|3d 3f 00 00 40 3f 00 00  Frame 0-1
0030  7e 54 00 00 7f 54 00 00 7d 54 00 00 80 54 00 00] Frame 1
0040 [be 69 00 00 bf 69 00 00 bd 69 00 00 c0 69 00 00  Frame 2   
```

# Assets

## Tile sets

```mjs
buildTileSet(tileSetTemplatePath,tileWidth,tileHeight);
```

## Palettes

```mjs
buildPalette(palettePath);
```

## Atlases

```mjs
buildAtlas(atlasPath,tileSetsList,palettesList);
```

# Basic application

```
                                               .------------------.
                                               | Fragment program |
                                               '------------------'
.--------------------------------------------------------------|-----.
| Frame program                                                |     |
|  .-----------------------------------------.  .-----------.  |     |
|  | BitmapInterface                         |  | Matrix    |  |     |
|  '-----------------------------------------'  '-----------'  |     |
|     |                 ^           |     ^        |     ^     |     |
|     |                 |           |     |        |     |     V     |
|     |              .--------.     |  .--------.  |  .-----------.  |
|     |              | draw   |     |  | update |  |  | process   |  |
|     |              '--------'     |  '--------'  |  '-----------'  |
|     |                 ^           |     ^        |     ^           |
|     |                 |           V     |        V     |           |
|     |                 |        .--------------------------.        |
|     |                 |        | TiledBitmap              |        |
|     |                 |        '--------------------------'        |
|     |                 |           |                                |
|     |                 |           V                                |
|     |                 |        .--------.                          |
|     |                 |        | Atlas  |                          |
|     |                 |        '--------'                          |
|     |                 |           |                                |
|     V                 |           V                                |
|  .--------------.  .-----------------.                             |
|  | EndInterface |  | Sprite          |                             |
|  '--------------'  '-----------------'                             |
'--------------------------------------------------------------------'
     |                                                                          
     V                                                                          
  .-----------.                                                                 
  | Animation |                                                                 
  '-----------'                                                                 
```

## Importmap

```html
<script type="importmap">
  {
    "imports": {
      "textor/": "./absolute/path/to/textor/src/modules/"
    }
  }
</script>
```

## Main module

```html
<script type="module" src="main.mjs"></script>
```

## Basic index.html file for a Textor application

```html
<!-- index.html -->
<!doctype html>
<html>
  <head>
    <style>
      body{
        margin:0;
        background-color:#000;
        overflow:hidden;
      }
      canvas:focus{
        outline:none;
      }
    </style>
    <script type="importmap">
      {
        "imports": {
          "textor/": "./absolute/path/to/textor/src/modules/"
        }
      }
    </script>
    <script type="module" src="./main.mjs"></script>
  </head>
  <body>
  </body>
</html>
```

## Importing Textor values and assets

```mjs
import * as Textor from "textor/textor.mjs";
import * as TileSets from "textor/assets/tileSets.mjs";
import * as Palettes from "textor/assets/palettes.mjs";
import * as Atlases from "textor/assets/atlases.mjs";
```

## Bitmap Interface

```mjs
const bitmapInterface = new Textor.bitmapInterface(
  window.innerWidth,
  window.innerHeight,
  4
);
```

## End interface

```mjs
const endInterface = new Textor.endInterface(
  bitmapInterface,
  document.body
);
```

## Tiled Bitmap

```mjs
const tiledBitmap = new Textor.TiledBitmap(
  TileSets.semigraphRegular8x8,
  TileSets.base4,
  bitmapInterface
);
```

### Tile sets

```mjs
TileSets.semigraphRegular8x8;
TileSets.semigraphBold8x8;
TileSets.semigraphHeavy8x8;
TileSets.ascii4x8;
TileSets.ascii6x12;
TileSets.ascii8x16;
TileSets.liblab4x8;
```

### Palettes

```mjs
Palettes.base1;
Palettes.base2;
Palettes.base4;
```

## Fragment processing

```mjs
tiledBitmap.process(
  function(fragmentContext){
    const {t,c} = fragmentContext;
    return Math.floor(Math.random()*t*c*c)
  }
);
tiledBitmap.update();
```


### Fragment programs

```mjs
// fragment.mjs
export function main(fragmentContext){
  const {t,c} = fragmentContext;
  return Math.floor(Math.random()*t*c*c)
}
```

```mjs
// main.mjs
import {main as fragment} from "./fragment.mjs";

tiledBitmap.process(fragment);
tiledBitmap.update();
```

## Animation

### Frame program

```mjs
// frame.mjs
import {BitmapInterface,EndInterface,TiledBitmap} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";
import {base4} from "textor/assets/palettes.mjs";
import {main as fragment} from "./fragment.mjs";

const bitmapInterface = new BitmapInterface(
  window.innerWidth,
  window.innerHeight,
  4
);

const endInterface = new EndInterface(
  bitmapInterface,
  document.body
);

const tiledBitmap = new TiledBitmap(
  semigraphRegular8x8,
  base4,
  bitmapInterface
);

export function main(frame){
  tiledBitmap.process(fragment);
  tiledBitmap.update();
  return !frame.k[13];
}
```

### Main animation

```mjs
// main.mjs
import {Animation} from "textor/textor.mjs";
import {main as frame} from "./frame.mjs";

new Animation(
  60,
  frame
);
```

### Summary

```html
<!-- index.html -->
<!doctype html>
<html>
  <head>
    <style>
      body{
        margin:0;
        background-color:#000;
        overflow:hidden;
      }
      canvas:focus{
        outline:none;
      }
    </style>
    <script type="importmap">
      {
        "imports": {
          "textor/": "./absolute/path/to/textor/src/modules/"
        }
      }
    </script>
    <script type="module" src="./main.mjs"></script>
  </head>
  <body>
  </body>
</html>
```

```mjs
// fragment.mjs
export function main(fragmentContext){
  const {t,c} = fragmentContext;
  return Math.floor(Math.random()*t*c*c)
}
```

```mjs
// frame.mjs
import {BitmapInterface,EndInterface,TiledBitmap} from "textor/textor.mjs";
import {semigraphRegular8x8} from "textor/assets/tileSets.mjs";
import {base4} from "textor/assets/palettes.mjs";
import {main as fragment} from "./fragment.mjs";

const bitmapInterface = new BitmapInterface(
  window.innerWidth,
  window.innerHeight,
  4
);

const endInterface = new EndInterface(
  bitmapInterface,
  document.body
);

const tiledBitmap = new TiledBitmap(
  semigraphRegular8x8,
  base4,
  bitmapInterface
);

export function main(frame){
  tiledBitmap.process(fragment);
  tiledBitmap.update();
  return !frame.k[13];
}
```

```mjs
// main.mjs
import {Animation} from "textor/textor.mjs";
import {main as frame} from "./frame.mjs";

new Animation(
  60,
  frame
);
```

<!--

## Sprites

### Importing sprite

## Matrix methods

### Write

### Print

# Examples

# API

-->

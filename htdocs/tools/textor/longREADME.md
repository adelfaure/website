# Textor

## Index

- [Introduction](#introduction)
- [Setting up a program using Textor](#setting-up-a-program-using-textor)

## Foreword

Textor is a text mode render engine experiment. It is inspired by numerous
artists pratices of using semigraphics as a medium, creating still, animated
and interactive artworks from legacy fonts (ASCII, PETSCII, Teletext, ANSI
etc.) but also from modified and custom ones (I'll try to share a list of both
textmode artists and tools at some point).

For more about textmode practices, I recommand to read Polyducks [What is
textmode?](https://polyducks.co.uk/pages/what-is-textmode/) or my article
[About ASCII art and Jgs
Font](https://velvetyne.fr/news/about-ascii-art-and-jgs-font/)

On programming side, Textor is mostly inspired by Andreas Gysin
[Play](https://play.ertdfgcvb.xyz/) and Serge Zaitsev
[Fenster](https://github.com/zserge/fenster?tab=readme-ov-file). 

It is my personal API for making games, tools and artworks. While documenting
and commenting isn't my area of comfort, I'm trying to do my best for this
public version hoping it will be useful.

Similar to Gysin's Play, Textor render method is inspired by GLSL programming.
It's API focus on using a `frame` function and a `fragment` function that can
be writtent like separated programs. One being used for performing opertations
at each frame before render and the second for executing code on each render
cell like a fragment shader.

## Textor's main idea

In classic GLSL programming each cell is a pixel waiting for the shader
fragment to output a four float value representing RGBA values of a pixel. In
Gysin's play, each cell are waiting for a string.  In Textor each buffer cell
are waiting for a non signed integral value to be outputed by the fragment that
will be interpreted as a character position in a previously given bitmap font
file. 

For example lets take an example with this given bitmap font :

|
|
|

To resume Textor will interprete given decimal for extracting and displaying
parts of any given picture described as a character sheet.

The API feature a custom `buffer` class designed to manipulate data as a grid.
It is a multi-purpose class that can be used for rendering graphics as well as
serving as sprite, editor brush or window.

This 1.0 version provide basic support for bitmap character set allowing to
manipulate decimals values as images extracted from larger raster files.

I'm working on extending this part of the project with a modular encoding that
will allow the definition and access of custom encoding zones such as colours
or semi-graphic sheets.

Plain text output is also being prepared and should coming soon.

This version is written in javascript following the [javascript modules
syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules).
I'm aiming to release a C version as well. 

## Setting up a program using Textor

NEED APACHE SERVER

First of all you need to setup a file structure for javascript modules and
including the file textor.mjs located in this git repository `/src/core/` folder.

You will run your program by opening the index.html file in a browser.

For example the structure could be something like:

```
myTextorApp/
  index.html
  script.mjs
  modules/
    textor.mjs
```

The index.html must contain a script tag specified as module and linking the
`script` you are working on.

```html
<!docype html>
  <html>
  <head>
    <script type="module" src="script.mjs"></script>
  </head>
  <body>
  </body>
</html>
```

In `script.mjs` you must link `textor.mjs` by adding on top of the file:

```js
import {Textor} from "./modules/textor.mjs";
```

You can now use `Textor` in your program !

### Frame program

Each `Textor` instance run an `Animation` method that rely on
[window.requestAnimationFrame](https://developer.mozilla.org/fr/docs/Web/API/Window/requestAnimationFrame)

To define the code that will be executed at start of each frame you need to
defined a `frame` function with an `input` argument. You also need to specify
an ouput that will tell the animation to keep going or stop, 0 for stop or 1
for keep going. 

For example this will make the animation stop after processing the first frame:

```js
function frame(input){
  return 0;
}
```

The input argument will provide an object with following keys:

```js
{
  // See #animation in #api section
  'animation': {...},

  // See #characterset in #api section
  'characterSet': {...},

  // See keys method of #textor in #api section
  'keys': {...},
  
  // See mouseX method of #textor in #api section
  'mouseX': foo,
  
  // See mouseY method of #textor in #api section
  'mouseY': bar,
  
  // See mouseDown method of #textor in #api section
  'mouseDown': foobar,
  
  // See #buffer in #api section
  'buffer': {...}
}
```

### Fragment program

### Working on frame and fragment as independant programs

## API

### Animation

Textor Animation class instance. Handle frame execution in time.

__Constructor__

```js 
new Animation(program,fps)
```

Asynchrone function `program` will define code executed each frame. It will
automatically receive its contextual `Animation` instance as argument.

Its output will determine if animation continues or stop, 0 for stop, 1 for
feep going. 

```js
import {Animation} from "./modules/textor.mjs"
let animation = new Animation(
  async(animation)=>{
    console.log(animation.frame);
    return 1;
  },
  60
);
animation.play();
// output number of frame elapsed since animation started 60 tims per second
```

Non signed integer `fps` will determine initial animation framerate.

__animation.fps__

Animation editable framerate.

__animation.currentFps__

Animation readable framerate value according to program latency.

__animation.frame__

Animation elapsed frame count.

__animation.play()__

If not running play animation. Textor Animation method will be `play()` by
default after being created, this is not the case for regular instances and it
must be `play()` to run.

```js
import {Animation} from "./modules/textor.mjs"
let animation = new Animation(
  async(animation)=>{
    console.log("Hello and goodbye");
    return animation.frame > 1 ? 0 : 1;
  },
  1
);
animation.play();
// output "Hello and goodbye" three times, once per second, before stopping
```

__animation.pause()__

Stop running animation.


### Buffer

Textor buffer contains data intended to be used or displayed as a grid.  It can
be used for rendering graphics or as a sprite, window or custom tool within the
context of another buffer.

__Constructor__

```js
new Buffer(width,height,textor)
```

Non signed integer `width` will determine buffer width, how many columns of
characters/cells it will store.

Non signed integer `height` will determine buffer height, how many rows of
characters/cells it will store.

Textor instance `textor` is optional. It is used in default Textor.buffer
instance for graphic rendering.

__buffer.width__

Value of buffer width (or cols).

__buffer.height__

Value of buffer height (or rows).

__buffer.length__

Total value of cells.

__buffer.data__

Buffer data arrays, values of each of its cells.

__buffer.place(posX,posY,gapX,gapY)__

Method used for placing buffer print within x and y axis and defining x and y
gap between each of its repeation when printed. By default if not placed a
buffer print will start at x 0 and y 0 and will repeat without any gap.

For example

```js
let buffer = new Buffer(8,8);
buffer.place(4,8,16,24);
```

Will start 8x8 `buffer` from fourth column and eighth row while repeating it
every sixteen column and twenty-four rows when printing.

__buffer.print(index,width)__




### input.characterSet

Textor CharacterSet class instance. Handle render font propreties.

__input.characterSet.length__

Numbers of characters stored in the font.

### Textor

### input.keys

Textor key input handle. Object containing pressed key names associated with a
`1` or `0` value for being currently pressed or not. For example pressing both
`A` and `B` keys after realeasing `C` will look like that.

```json
{
  'A':1,
  'B':1,
  'C':0,
}
```

### input.mouseX

Textor mouse x axis handle. The value correspond to x axis character cell (or
col) under pointer.

### input.mouseY

Textor mouse y axis handle. The value correspond to y axis character cell (or
row) under pointer.

### input.mouseDown

Textor mouse left button handle. `1` or `0` for being currently pressed or not.


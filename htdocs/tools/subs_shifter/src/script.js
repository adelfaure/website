/* Script */ 
let first_shift = -13000;
let last_shift = 0;
let new_subs = "";

fetch('../From.Dusk.Till.Dawn.1996.MULTi.VFF.1080p.BluRay.x265.AC3.srt')
.then(response => {
    if (!response.ok) {
        throw new Error("HTTP error " + response.status);
    }
    return response.arrayBuffer();
})
.then(buffer => {
  const decoder = new TextDecoder('iso-8859-1');
  const text = decoder.decode(buffer);
  return text;
})
.then(text => {
  //console.log(text);
  let sentences = text.split('\r\n\r\n');
  let medium_shift = (last_shift - first_shift) / (sentences.length-1);

  // get first sentence time
  let first_str = sentences[0].match(/.*(?=\s-->\s)/)[0]; 
  let first = first_str.split(/[:,]/);
  first[0] = parseInt(first[0])*60*60*1000;
  first[1] = parseInt(first[1])*60*1000;
  first[2] = parseInt(first[2])*1000;
  first[3] = parseInt(first[3]);
  let time_first = first[0]+first[1]+first[2]+first[3];
 
  // get last sentence time 
  
  let last_str = sentences[sentences.length-2].match(/(?<=\s-->\s).*/)[0];
  let last = last_str.split(/[:,]/);
  last[0] = parseInt(last[0])*60*60*1000;
  last[1] = parseInt(last[1])*60*1000;
  last[2] = parseInt(last[2])*1000;
  last[3] = parseInt(last[3]);
  let time_last = last[0]+last[1]+last[2]+last[3];

  console.log();

  for (var i = 0; i < sentences.length-1; i++) {
    let start_str = sentences[i].match(/.*(?=\s-->\s)/)[0]; 
    let end_str = sentences[i].match(/(?<=\s-->\s).*/)[0];
    
    let time_str = start_str +' --> '+ end_str;
    
    let rest = sentences[i].split(time_str);
    rest[0] = rest[0].replace('\r','');
    rest[1] = rest[1].replace('\r','');
    //console.log(rest);

    // Start

    let start = start_str.split(/[:,]/);
    start[0] = parseInt(start[0])*60*60*1000;
    start[1] = parseInt(start[1])*60*1000;
    start[2] = parseInt(start[2])*1000;
    start[3] = parseInt(start[3]);
    let time_start = start[0]+start[1]+start[2]+start[3];

//  console.log((time_last - time_first), (time_start - time_first), ((time_start - time_first) / (time_last - time_first) * 100) );

    time_start += first_shift + last_shift * (time_start - time_first) / (time_last - time_first);
    let start_new_hour = Math.floor(time_start/60/60/1000);
    let start_new_min = Math.floor(time_start/60/1000)-start_new_hour*60;
    let start_new_sec = Math.floor(time_start/1000)-start_new_min*60-start_new_hour*60*60;
    let start_new_mil = Math.floor(time_start)-start_new_sec*1000-start_new_min*1000*60;
    let start_new_hour_str = 
      String(start_new_hour).length == 2 ? 
      String(start_new_hour) : 
      ('0' + String(start_new_hour));
    let start_new_min_str = 
      String(start_new_min).length == 2 ? 
      String(start_new_min) : 
      ('0' + String(start_new_min));
    let start_new_sec_str = 
      String(start_new_sec).length == 2 ? 
      String(start_new_sec) : 
      ('0' + String(start_new_sec));
    let start_new_mil_str = 
      String(start_new_mil).length == 3 ? 
        String(start_new_mil).length == 2 ? 
          ('00' + String(start_new_mil)):
            ('' + String(start_new_mil)):
        String('0'+start_new_mil)
    start_str = start_new_hour_str +':'+ start_new_min_str +':'+ start_new_sec_str +','+ start_new_mil_str;

    // End

    let end = end_str.split(/[:,]/);
    end[0] = parseInt(end[0])*60*60*1000;
    end[1] = parseInt(end[1])*60*1000;
    end[2] = parseInt(end[2])*1000;
    end[3] = parseInt(end[3]);
    let time_end = end[0]+end[1]+end[2]+end[3];
    time_end += first_shift + last_shift * (time_end - time_first) / (time_last - time_first);
    let end_new_hour = Math.floor(time_end/60/60/1000);
    let end_new_min = Math.floor(time_end/60/1000)-end_new_hour*60;
    let end_new_sec = Math.floor(time_end/1000)-end_new_min*60-end_new_hour*60*60;
    let end_new_mil = Math.floor(time_end)-end_new_sec*1000-end_new_min*1000*60;
    let end_new_hour_str = 
      String(end_new_hour).length == 2 ? 
      String(end_new_hour) : 
      ('0' + String(end_new_hour));
    let end_new_min_str = 
      String(end_new_min).length == 2 ? 
      String(end_new_min) : 
      ('0' + String(end_new_min));
    let end_new_sec_str = 
      String(end_new_sec).length == 2 ? 
      String(end_new_sec) : 
      ('0' + String(end_new_sec));
    let end_new_mil_str = 
      String(end_new_mil).length == 3 ? 
        String(end_new_mil).length == 2 ? 
          ('00' + String(end_new_mil)):
            ('' + String(end_new_mil)):
        String('0'+end_new_mil)
    end_str = end_new_hour_str +':'+ end_new_min_str +':'+ end_new_sec_str +','+ end_new_mil_str;
    new_subs += '\n\n'+rest[0]+start_str + ' --> ' + end_str+rest[1];
    new_subs = new_subs.replace('\r','');
  }
  log.textContent = new_subs;
})
.catch(error => {
    // Handle/report error
});

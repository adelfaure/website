// Step, value by which things are moving, including cursor position, camera, size changes for picture and cursor
let step = false;

// Writing text instead of semi-graphics
let mode_write = false;

// Cursor size
let cursor;
let cursor_w = 1;
let cursor_h = 1;

// Picture size
let picture;
let picture_w = 32;
let picture_h = 32;

// Update cursor set list
function update_cursor_set_list(element){
  element.cursor_w = cursor_w;
  element.cursor_h = cursor_h;
  textor_sprite["cursor"] = [
    set_list_area(
      textor_encoding.quick_key({"character":' ',"color":"dark","background":"bright"}),
      cursor_w,cursor_h
    ),
  "none"];
}

// Cursor transparency, display inverted colors of a sprite to cursor sprite depending on which sprite key is hidden by cursor
function cursor_transparency(send,get,frame,set_list_data) {
  if (!get || get.name != "cursor") return;
  let cursor_set_list_x = (
    send.x - Math.floor(get.x) + set_list_data[1]
  )
  let cursor_set_list_y = (
    send.y - Math.floor(get.y) + set_list_data[2]
  )
  let set_list_data_descr = textor_encoding.compiled_descr[set_list_data[0]];
  let transparency_descr = {
    "character":set_list_data_descr.character,
    "color": (
      set_list_data_descr.color == "bright" ? "dark" :
      set_list_data_descr.color == "dark" ? "bright" :
      set_list_data_descr.color == "sweet" ? "bitter" :
      set_list_data_descr.color == "bitter" ? "sweet" :
      set_list_data_descr.color == "soft" ? "special" :
      "soft"
    ),
    "background": (
      set_list_data_descr.background == "bright" ? "dark" :
      set_list_data_descr.background == "dark" ? "bright" :
      set_list_data_descr.background == "sweet" ? "bitter" :
      set_list_data_descr.background == "bitter" ? "sweet" :
      set_list_data_descr.background == "soft" ? "special" :
      "soft"
    ),
    "rotate":set_list_data_descr.rotate,
    "horizontal_mirror":set_list_data_descr.horizontal_mirror,
    "vertical_mirror":set_list_data_descr.vertical_mirror,
  };
  let index = cursor_set_list_x%cursor_w+cursor_set_list_y*cursor_w;
  textor_sprite["cursor"][0][index][0] = textor_encoding.descr_to_true_key(transparency_descr);
}

let layouts = {
  "azerty":[
    "&é\"'(-è_çàazertyuiopqsdfghjklmwxcvbn,;:! ",
 // "1234567890AZERTYUIOPQSDFGHJKLMWXCVBN?./§ "
  ],
  "qwerty":[
    "1234567890qwertyuiopasdfghjkl;zxcvbnm,./ ",
 // "!@#$%^&*()QWERTYUIOPASDFGHJKL:ZXCVBNM<>? "
  ],
};
let special_key = false;
let layout = "qwerty";
let alt_semi = false;
let semigraphic_layout = [
  /* & */ "edge",
  /* é */ "edge_corner",
  /* " */ "edge_path",
  /* ' */ "edge_end",
  /* ( */ "edge_square",
  /* - */ "big_round_turning",
  /* è */ "diagonal",
  /* _ */ "diagonal_half_crossing",
  /* ç */ "diagonal_cross",
  /* à */ "diagonal_turning",
  /* a */ "saw",
  /* z */ "torve_turning",
  /* e */ "big_torve_turning",
  /* r */ "small_connexion",
  /* t */ "big_connexion",
  /* y */ "big_round_connexion",
  /* u */ "line",
  /* i */ "turning",
  /* o */ "round_turning",
  /* p */ "near_cross",
  /* q */ "cross",
  /* s */ "circle",
  /* d */ "half",
  /* f */ "corner",
  /* g */ "double_corner",
  /* h */ "round_corner",
  /* j */ "bump",
  /* k */ "big_bump",
  /* l */ "big_round_corner",
  /* m */ "full_circle",
  /* w */ "medium_circle",
  /* x */ "small_circle",
  /* c */ "dot",
  /* v */ "big_slope",
  /* b */ "small_pike",
  /* n */ "big_pike",
  /* , */ "losange",
  /* ; */ "soft_slope",
  /* : */ "half_checker",
  /* ! */ "checker",
  /*   */ " "
];

let semigraphic_layout_alt = [
  /* & */ "mirror",
  /* é */ "rotate",
  /* " */ "load",
  /* ' */ "save",
  /* ( */ "arrow",
  /* - */ "pen",
  /* è */ "pick_color",
  /* _ */ "pick_background",
  /* ç */ "edge_square",
  /* à */ "copy",
  /* a */ "reverse",
  /* z */ "step",
  /* e */ " ",
  /* r */ " ",
  /* t */ " ",
  /* y */ " ",
  /* u */ " ",
  /* i */ " ",
  /* o */ " ",
  /* p */ " ",
  /* q */ " ",
  /* s */ " ",
  /* d */ " ",
  /* f */ " ",
  /* g */ " ",
  /* h */ " ",
  /* j */ " ",
  /* k */ " ",
  /* l */ " ",
  /* m */ " ",
  /* w */ " ",
  /* x */ " ",
  /* c */ " ",
  /* v */ " ",
  /* b */ " ",
  /* n */ " ",
  /* , */ " ",
  /* ; */ " ",
  /* : */ " ",
  /* ! */ " ",
  /*   */ " "
];
let control_layout = [
  /* 1& */ ["medium_circle","dark","bright","0","false","false"],
  /* 2é */ ["medium_circle","soft","dark","0","false","false"],
  /* 3" */ ["medium_circle","sweet","dark","0","false","false"],
  /* 4' */ ["medium_circle","bitter","dark","0","false","false"],
  /* 5( */ ["medium_circle","special","dark","0","false","false"],
  /* 6- */ ["medium_circle","bright","dark","0","false","false"],
  /* 7è */ ["mirror","dark","special","90","false","false"],
  /* 8_ */ ["mirror","dark","special","0","false","false"],
  /* 9ç */ ["rotate","dark","special","0","true","false"],
  /* 0à */ ["rotate","dark","special","0","false","false"],
  /* Aa */ ["load","dark","special","0","false","false"],
  /* Zz */ ["save","dark","special","0","false","false"],
  /* Ee */ ["arrow","special","dark","0","false","false"],
  /* Rr */ ["pen","dark","special","0","false","false"],
  /* Tt */ ["diagonal_cross","bitter","dark","0","false","false"],
  /* Yu */ ["pen","special","dark","0","false","false"],
  /* Uu */ ["mirror", "special","dark","90","false","false"],
  /* Ii */ ["mirror", "special","dark","0","false","false"],
  /* Oo */ ["rotate","special","dark","0","true","false"],
  /* Pp */ ["rotate", "special","dark","0","false","false"],
  /* Qq */ ["diagonal_cross","bitter","dark","0","false","false"],
  /* Ss */ ["arrow","special","dark","90","true","false"],
  /* Dd */ ["arrow","special","dark","0","false","true"],
  /* Ff */ ["arrow","special","dark","90","false","false"],
  /* Gg */ ["save","dark","special","90","false","false"],
  /* Hh */ ["load","dark","special","90","false","false"],
  /* Jj */ ["pick_color","dark","special","0","false","false"],
  /* Kk */ ["pick_background","dark","special","0","false","false"],
  /* Ll */ ["edge_square","special","dark","0","false","false"],
  /* Mm */ ["edge_square","dark","special","0","false","false"],
  /* Ww */ ["diagonal_cross","bitter","dark","0","false","false"],
  /* Xx */ ["copy","dark","special","0","false","false"],
  /* Cc */ ["copy","special","dark","0","false","false"],
  /* Vv */ ["reverse","dark","special","0","false","false"],
  /* Bb */ ["reverse","special","dark","0","false","false"],
  /* Nn */ ["diagonal_cross","bitter","dark","0","false","false"],
  /* ?, */ ["diagonal_cross","bitter","dark","0","false","false"],
  /* .; */ ["step","dark","special","0","false","false"],
  /* /: */ ["+","dark","special","0","false","false"],
  /* §! */ ["-","dark","special","0","false","false"],
];

let picture_before = [];
let picture_after = [];

function memorize(after){
  let memory_screen = textor_sprite_to_screen(textor_sprite["picture"]);
  if (after) {
    picture_after.push(memory_screen);
  } else {
    picture_before.push(memory_screen);
  }
}

function remember(after){
  if (after) {
    if (!picture_after.length) return;
    let memory_screen = picture_after.pop();
    memorize();
    textor_sprite["picture"] = textor_screen_to_sprite(memory_screen);
    picture_w = memory_screen.width;
    picture_h = memory_screen.height;
  } else {
    if (!picture_before.length) return;
    let memory_screen = picture_before.pop();
    memorize(true);
    textor_sprite["picture"] = textor_screen_to_sprite(memory_screen);
    picture_w = memory_screen.width;
    picture_h = memory_screen.height;
  }
}

function update_picture_size(){
  let previous_picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
  let new_picture_screen = new Textor_screen(
    textor_encoding.quick_key(
      {"character":' ',"background":"bright"}
    ),picture_w,picture_h
  );
  new_picture_screen.add(previous_picture_screen,0,0);
  textor_sprite["picture"] = textor_screen_to_sprite(new_picture_screen);
}

function apply(descr,fun){
  if (descr.color == descr.background) descr.character = ' ';
  return {
    "character": fun["character"] ? fun["character"](descr) : descr.character,
    "color":fun["color"] ? fun["color"](descr) : descr.color,
    "background":fun["background"] ? fun["background"](descr) : descr.background,
    "rotate":fun["rotate"] ? fun["rotate"](descr) : descr.rotate,
    "horizontal_mirror":fun["horizontal_mirror"] ? fun["horizontal_mirror"](descr) : descr.horizontal_mirror,
    "vertical_mirror":fun["vertical_mirror"] ? fun["vertical_mirror"](descr) : descr.vertical_mirror
  }
}

function apply_set_list(set_list,fun){
  for (var i = 0; i < set_list.length; i++) {
    let descr = textor_encoding.compiled_descr[set_list[i][0]];
    let new_descr = apply(descr,fun);
    set_list[i][0] = textor_encoding.descr_to_true_key(new_descr);
  }
}

function apply_transparency(set_list,textor_encoding){
  apply_set_list(set_list,{
    "color": descr => (
      descr.color == "bright" ? "dark" :
      descr.color == "dark" ? "bright" :
      descr.color == "sweet" ? "bitter" :
      descr.color == "bitter" ? "sweet" :
      descr.color == "soft" ? "special" :
      "soft"
    ),
    "background": descr => (
      descr.background == "bright" ? "dark" :
      descr.background == "dark" ? "bright" :
      descr.background == "sweet" ? "bitter" :
      descr.background == "bitter" ? "sweet" :
      descr.background == "soft" ? "special" :
      "soft"
    )
  });
}

function apply_horizontal_mirror(set_list,textor_encoding){
  apply_set_list(set_list,{
    "horizontal_mirror":descr => (
      descr.horizontal_mirror == "true" ? "false" :"true"
    )
  });
}
function apply_vertical_mirror(set_list,textor_encoding){
  apply_set_list(set_list,{
    "vertical_mirror":descr=>(
      descr.vertical_mirror == "true" ? "false" :"true"
    )
  });
}
function apply_rotation(set_list,textor_encoding){
  apply_set_list(set_list,{
    "rotate":descr=>(
      descr.rotate == '0' ? '90' :
      descr.rotate == '90' ? '180' :
      descr.rotate == '180' ? '270' :
      '0'
    )
  });
}
function apply_anti_rotation(set_list,textor_encoding){
  apply_set_list(set_list,{
    "rotate":descr=>(
      descr.rotate == '0' ? '270' :
      descr.rotate == '270' ? '180' :
      descr.rotate == '180' ? '90' :
      '0'
    ),
  });
}

function apply_color(set_list,textor_encoding){
  apply_set_list(set_list,{
    "color": ()=>color
  });
}

function apply_background(set_list,textor_encoding){
  apply_set_list(set_list,{
    "background":()=>background,
  });
}

function apply_reverse(set_list,textor_encoding){
  apply_set_list(set_list,{
    "color":descr=>(
      descr.color == descr.background ? (
        descr.color == "bright" ? "dark" :
        descr.color == "dark" ? "bright" :
        descr.color == "sweet" ? "bitter" :
        descr.color == "bitter" ? "sweet" :
        descr.color == "soft" ? "special" :
        "soft"
      ):descr.background
    ),
    "background":descr=>(
      descr.color == descr.background ? (
        descr.color == "bright" ? "dark" :
        descr.color == "dark" ? "bright" :
        descr.color == "sweet" ? "bitter" :
        descr.color == "bitter" ? "sweet" :
        descr.color == "soft" ? "special" :
        "soft"
      ) : descr.color
    ),
  });
}

let control_functions = [
  /* 1& */ function(){color = "dark"},
  /* 2é */ function(){color = "soft"},
  /* 3" */ function(){color = "sweet"},
  /* 4' */ function(){color = "bitter"},
  /* 5( */ function(){color = "special"},
  /* 6- */ function(){color = "bright"},
  /* 7è */ function(){
    horizontal_mirror = horizontal_mirror == "false" ? "true" : "false";
  },
  /* 8_ */ function() {
    vertical_mirror = vertical_mirror == "false" ? "true" : "false";
  },
  /* 9ç */ function() {
    rotate = (
      rotate == '0' ? '270' :
      rotate == '270' ? '180' :
      rotate == '180' ? '90' :
      '0'
    );
  },
  /* 0à */ function() {
    rotate = (
      rotate == '0' ? '90' :
      rotate == '90' ? '180' :
      rotate == '180' ? '270' :
      '0'
    );
  },
  /* Aa */ function() {
    memorize();
    picture_after = [];
    var input = document.createElement('input');
    input.type = 'file';
    input.onchange = e => { 
       // getting a hold of the file reference
       var file = e.target.files[0];
       // setting up the reader
       var reader = new FileReader();
       reader.readAsText(file,'UTF-8');
       // here we tell the reader what to do when it's done reading...
       reader.onload = readerEvent => {
          var content = readerEvent.target.result; // this is the content!
          textor_sprite["picture"] = textor_matrix_to_set_list(
            textor_image_to_matrix(content)
          );
       }
    }
    input.click();
  },
  /* Zz */ function(){
    let date = new Date();
    download([
      textor_matrix_to_image(
        textor_set_list_to_matrix(textor_sprite["picture"][0])
      )],
      "textor_image_"+date.toISOString().slice(0,10)+".txt","text/plain;charset=utf-8"
    );
  },
  /* Ee */ function(){
    if (display_help) return;
    memorize();
    picture_after = [];
    picture_h = picture_h > step_y() ? picture_h - step_y() : 1;
    update_picture_size();
  },
  /* Rr */ function(){
    mode_write = !mode_write;
  },
  /* Tt */ function(){
  },
  /* Yy */ function(){
    alt_semi = !alt_semi;
  },
  /* Uu */ function(){
    if (display_help) return;
    memorize();
    picture_after = [];
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_horizontal_mirror(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    cursor_screen.content = matrix_mirror_horizontal_new(cursor_screen.width,cursor_screen.content);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Ii */ function(){
    if (display_help) return;
    memorize();
    picture_after = [];
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_vertical_mirror(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    cursor_screen.content = matrix_mirror_vertical_new(cursor_screen.width,cursor_screen.height,cursor_screen.content);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Oo */ function(){
    if (display_help) return;
    if (cursor_w!=cursor_h) return;
    memorize();
    picture_after = [];
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_anti_rotation(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    cursor_screen.content = matrix_antirotate_90(cursor_screen.width,cursor_screen.content);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Pp */ function(){
    if (display_help) return;
    if (cursor_w!=cursor_h) return;
    memorize();
    picture_after = [];
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_rotation(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    cursor_screen.content = matrix_rotate_90_new(cursor_screen.width,cursor_screen.content);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Qq */ function(){},
  /* Ss */ function(){
    if (display_help) return;
    memorize();
    picture_after = [];
    picture_w = picture_w > step_x() ? picture_w - step_x() : 1;
    update_picture_size();
  },
  /* Dd */ function(){
    if (display_help) return;
    memorize();
    picture_after = [];
    picture_h = picture_h + step_y();
    update_picture_size();
  },
  /* Ff */ function(){
    if (display_help) return;
    picture_w = picture_w + step_x();
    update_picture_size();
  },
  /* Gg */ function(){
    if (display_help) return;
    remember();
  },
  /* Hh */ function() {
    if (display_help) return;
    remember(true);
  },
  /* Jj */ function() {
    if (display_help) return;
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    color = textor_encoding.compiled_descr[textor_sprite["cursor"][0][0][0]].color;
  },
  /* Kk */ function(){
    if (display_help) return;
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    background = textor_encoding.compiled_descr[textor_sprite["cursor"][0][0][0]].background;
  },
  /* Ll */ function(){
    if (display_help) return;
    memorize();
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_color(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Mm */ function(){
    if (display_help) return;
    memorize();
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_background(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Ww */ function(){}, 
  /* Xx */ function(){
    if (display_help) return;
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    textor_sprite["copy"] = [textor_copy_set_list(textor_sprite["cursor"][0]),"none"];
  },
  /* Cc */ function(){
    if (display_help) return;
    memorize();
    let copy_screen = textor_sprite_to_screen(textor_sprite["copy"]);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(copy_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Vv */ function(){
    let temp = color;
    color = background;
    background = temp;
  },
  /* Bb */ function(){
    if (display_help) return;
    memorize();
    apply_transparency(textor_sprite["cursor"][0],textor_encoding);
    apply_reverse(textor_sprite["cursor"][0],textor_encoding);
    let cursor_screen = textor_sprite_to_screen(textor_sprite["cursor"]);
    let picture_screen = textor_sprite_to_screen(textor_sprite["picture"]);
    picture_screen.add(cursor_screen,cursor.x-picture.x,cursor.y-picture.y);
    textor_sprite["picture"] = textor_screen_to_sprite(picture_screen);
  },
  /* Nn */ function(){},
  /* ?, */ function(){},//["random","dark","bright","0","false","false"],
  /* .; */ function(){
    step = !step;
  },
  /* /: */ function(){
    display_scale++;
  },
  /* §! */ function(){
    display_scale = display_scale > 1 ? display_scale-1 : display_scale;
  }
];
let color = "dark";//"bright";
let background = "bright";//"dark";
let rotate = "0";
let horizontal_mirror = "false";
let vertical_mirror = "false";

function update_layout_view(){
  textor_sprite["layout_view"] = [[],"none"];
  for (var i = 0; i < layouts[layout][0].length-1; i++){
    let key = layouts[layout][0][i];
    textor_sprite["layout_view"][0].push([
      textor_encoding.descr_to_true_key({
        "character":key,
        "color":key == last_typed ? "bright" : "sweet",
        "background":"dark",
        "rotate":'0',//rotate,
        "horizontal_mirror":"false",//horizontal_mirror,
        "vertical_mirror":"false",//vertical_mirror
      }),i%10,Math.floor(i/10)*(keys[special_key]?3:2)]
    );
    textor_sprite["layout_view"][0].push([
      textor_encoding.descr_to_true_key({
        "character":(
          //keys[special_key] ? control_layout[i][0] :
          mode_write ? key : (
            alt_semi ? 
            semigraphic_layout_alt[i] :
            semigraphic_layout[i]
          )
        ),
        "color":(
          //keys[special_key] ? control_layout[i][1] :
          color
        ),
        "background":(
          //keys[special_key] ? control_layout[i][2] :
          background
        ),
        "rotate":(
          //keys[special_key] ? control_layout[i][3] :
          rotate
        ),
        "horizontal_mirror":(
          //keys[special_key] ? control_layout[i][4] :
          horizontal_mirror
        ),
        "vertical_mirror":(
          //keys[special_key] ? control_layout[i][5] :
          vertical_mirror
        )
      }),i%10,Math.floor(i/10)*(keys[special_key]?3:2)+(keys[special_key]?2:1)]
    );
    if (keys[special_key]) {
      textor_sprite["layout_view"][0].push([
        textor_encoding.descr_to_true_key({
          "character":(
            control_layout[i][0]
          ),
          "color":(
            control_layout[i][1]
          ),
          "background":(
            control_layout[i][2]
          ),
          "rotate":(
            control_layout[i][3]
          ),
          "horizontal_mirror":(
            control_layout[i][4]
          ),
          "vertical_mirror":(
            control_layout[i][5]
          )
        }),i%10,Math.floor(i/10)*3+1]
      );
    }
  }
}

function type(send,get,frame,set_list_data) {
  if (keys[special_key] || !typing || !get || get.name != "cursor" || !last_typed || (last_typed != "Backspace" && last_typed.length > 1)) return;
  let character = (
    last_typed == "Backspace" ? ' ' :
    mode_write ? last_typed :
    ( alt_semi ?
      semigraphic_layout_alt[layouts[layout][keys["Shift"] ? 1 : 0].indexOf(last_typed)] :
      semigraphic_layout[layouts[layout][keys["Shift"] ? 1 : 0].indexOf(last_typed)]
    )
  );
  if (!character) return;
  if (!get.typed) {
    memorize();
    picture_after = [];
  }
  set_list_data[0] = textor_encoding.descr_to_true_key({
    "character":character,
    "color":color,
    "background":background,
    "rotate":rotate,
    "horizontal_mirror":horizontal_mirror,
    "vertical_mirror":vertical_mirror
  });
  get.typed = true;
}

function step_x(){
  return step ? cursor_w : 1;
}
function step_y(){
  return step ? cursor_h : 1;
}

// Init copy sprite
textor_sprite["copy"] = [[],"none"];

// Init picture sprite
textor_sprite["picture"] = [
  set_list_area(
    textor_encoding.quick_key({"character":' ',"background":background,"color":color}),
    picture_w,picture_h
  ),
"none"];

let display_help = true;

function textor_editor_main(){
  for (var i = 0; i < textor_element_list.length; i++) {
    textor_element_to_remove_list.push(textor_element_list[i]);
  }
  let layout_view = new Textor_element("layout_view","layout_view",0,1,3,()=>false,
    function(element,frame){
      update_layout_view();
    }
  );
  let help = new Textor_element("help","help",11,1,2,()=>false,
    function(element,frame){
      element.sprite_name = display_help ? "help" : "";
      if (keys["F1"] && typing) {
        display_help = !display_help;
      }
      if (!display_help) return;
      if (keys["ArrowUp"] && typing) {
        element.y--;
      }
      if (keys["ArrowDown"] && typing) {
        element.y++;
      }
      if (keys["ArrowLeft"] && typing) {
        element.x--;
      }
      if (keys["ArrowRight"] && typing) {
        element.x++;
      }
    }
  );
  let fps_element = new Textor_element("fps","fps",0,0,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      textor_sprite["fps"] = [str_to_set_list("FPS "+animation_fps,textor_encoding,{
        "color":"bright"
      }),"none"];
    }
  )
  // Picture element
  picture = new Textor_element("picture","picture",Math.floor(scene.width/2)-picture_w/2,Math.floor(scene.height/2)-picture_h/2,1,
    function(send,get,frame,set_list_data) {
      // Apply picture to cursor transparency
      cursor_transparency(send,get,frame,set_list_data);
      type(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      element.sprite_name = display_help ? "" : "picture";
      if (!typing) return;
      
      // Changing picture position using [control + arrow keys]
      if (keys[special_key] && key_down) {
        if (!display_help) {
          element.x = (
            keys["ArrowRight"] ? element.x + step_x() : 
            keys["ArrowLeft"] ? element.x - step_x() : 
            element.x
          );
          element.y = (
            keys["ArrowDown"] ? element.y + step_y() : 
            keys["ArrowUp"] ? element.y - step_y() : 
            element.y
          );
        }
        let fun = control_functions[layouts[layout][keys["Shift"] ? 1 : 0].indexOf(last_typed)];
        if (fun) fun();
      }
    }
  );
  
  // Cursor element
  cursor = new Textor_element("cursor","cursor",Math.floor(scene.width/2),Math.floor(scene.height/2),2,send => send,
    function(element,frame) {
      element.sprite_name = display_help ? "" : "cursor";
      if (display_help) return;
      if (element.typed) {
        element.typed = false;
        element.x = (
          last_typed != "Backspace" && element.x < scene.width-step_x() ? element.x + step_x() : 
          element.x
        );
      }
      if (!typing) return;
      if (!keys[special_key]) {
  
        if (keys["Backspace"]) {
          element.x = (
            element.x - step_x() > 0 ? element.x - step_x() : 
            element.x
          );
        // Changing cursor size using [shift + arrow keys]
        } else if (keys["Shift"]) {
          cursor_w = (
            keys["ArrowRight"] ? cursor_w + step_x() : 
            keys["ArrowLeft"] && cursor_w > step_x() ? cursor_w - step_x() : 
            keys["ArrowLeft"] && cursor_w <= step_x() ? 1 : 
            cursor_w
          );
          cursor_h = (
            keys["ArrowDown"] ? cursor_h + step_y() : 
            keys["ArrowUp"] && cursor_h > step_y() ? cursor_h - step_y() : 
            keys["ArrowUp"] && cursor_h <= step_y() ? 1 : 
            cursor_h
          );
        
        // Changing cursor position using [arrow keys]
        } else {
          element.x = (
            last_typed == "ArrowRight" && element.x < scene.width-1 ? element.x + step_x() : 
            last_typed == "ArrowLeft" && element.x > 0 ? element.x - step_x() : 
            element.x
          );
          element.y = (
            last_typed == "ArrowDown" && element.y < scene.height-1 ? element.y + step_y() : 
            last_typed == "ArrowUp" && element.y > 0 ? element.y - step_y() : 
            element.y
          );
        }
      }
  
      // Update cursor set list
      update_cursor_set_list(element);
    }
  );
}

function textor_editor_intro(){
  // Fps counter
  let fps_element = new Textor_element("fps","fps",0,0,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      element.y = scene.height-1;
      textor_sprite["fps"] = [str_to_set_list("FPS "+animation_fps,textor_encoding,{
        "color":"bright"
      }),"none"];
    }
  )
  let x = 1;
  let y = 1;
  let intro_text = new Textor_element("intro_text","intro_text",x,y,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      textor_sprite["intro_text"] = [str_to_set_list("Select your keyboard layout :",textor_encoding,{
        "color":"special"
      }),"none"];
    }
  )
  y++;
  for (var i in layouts) {
    let layout_name = i;
    let layout_list_element = new Textor_element(layout_name,layout_name,x,y,3,
      function(send,get,frame,set_list_data) {
        cursor_transparency(send,get,frame,set_list_data);
        if (get && get.name == "cursor") {
          send.color = "sweet";
          if (mouse_down) {
            layout = layout_name;
            textor_special_key();
          }
        } else {
          send.color = "soft";
        }
        return send;
      },
      function(element,frame) {
        textor_sprite[layout_name] = [str_to_set_list('- '+layout_name,textor_encoding,{
          "color":this.color,
        }),"none"];
      }
    );
    layout_list_element.color = "soft";
    y++;
  }
  let layout_name = "custom";
  let layout_list_element = new Textor_element(layout_name,layout_name,x,y,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      if (get && get.name == "cursor") {
        send.color = "sweet";
        if (mouse_down) {
          //layout = layout_name;
          //textor_editor_main();
          textor_custom_layout();
        }
      } else {
        send.color = "soft";
      }
      return send;
    },
    function(element,frame) {
      textor_sprite[layout_name] = [str_to_set_list('- '+layout_name,textor_encoding,{
        "color":this.color,
      }),"none"];
    }
  );
  layout_list_element.color = "soft";
  y++;
  // Cursor element
  cursor = new Textor_element("cursor","cursor",Math.floor(scene.width/2),Math.floor(scene.height/2),Infinity,send => send,
    function(element,frame) {
      textor_sprite["cursor"] = [
        set_list_area(
          textor_encoding.quick_key({"character":' ',"color":"dark","background":"bright"}),
          cursor_w,cursor_h
        ),
      "none"];
      element.x = mouse_x;
      element.y = mouse_y;
    }
  );
}

let custom_layout = [];

function textor_custom_layout(){
  for (var i = 0; i < textor_element_list.length; i++) {
    textor_element_to_remove_list.push(textor_element_list[i]);
  }
  let fps_element = new Textor_element("fps","fps",0,0,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      element.y = scene.height-1;
      textor_sprite["fps"] = [str_to_set_list("FPS "+animation_fps,textor_encoding,{
        "color":"bright"
      }),"none"];
    }
  )
  let custom_layout_illu = new Textor_element("custom_layout_illu","custom_keys_pic",1,1,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      if (typing && last_typed.length == 1) {
        if (custom_layout.indexOf(last_typed) == -1) {
          custom_layout.push(last_typed);
        }
        if (custom_layout.length == 40) {
          layouts["custom"] = [custom_layout.concat([' '])];
          layout = "custom"; 
          textor_special_key();
        }
      }
    }
  )
  let custom_layout_counter = new Textor_element("custom_layout_counter","custom_layout_counter",2,2,4,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      textor_sprite["custom_layout_counter"] = [str_to_set_list(custom_layout.length+'/40',textor_encoding,{
        "color":"bright"
      }),"none"];
    }
  )
}

function textor_special_key(){
  console.log("COUCOU");
  for (var i = 0; i < textor_element_list.length; i++) {
    textor_element_to_remove_list.push(textor_element_list[i]);
  }
  let fps_element = new Textor_element("fps","fps",0,0,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      element.y = scene.height-1;
      textor_sprite["fps"] = [str_to_set_list("FPS "+animation_fps,textor_encoding,{
        "color":"bright"
      }),"none"];
    }
  )
  let text_special_key = new Textor_element("text_special_key","text_special_key",1,1,3,
    function(send,get,frame,set_list_data) {
      cursor_transparency(send,get,frame,set_list_data);
      return send;
    },
    function(element,frame) {
      textor_sprite["text_special_key"] = [str_to_set_list("Choose a special key. Exemple : Control",textor_encoding,{
        "color":"special"
      }),"none"];
      if (typing) {
        special_key = last_typed;
        textor_editor_main();
      }
    }
  )
}

textor_editor_intro();

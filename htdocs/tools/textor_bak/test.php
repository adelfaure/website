<!doctype html>
<html>
  <head>
    <meta charset="utf8"/>
    <link rel="stylesheet" href="textor_style/textor_style.css">
  </head>
  <body>
    <img class="asset" id="textor_charset" defer src="textor_asset/textor_charset.png">
    <pre class="asset" id="textor_charset_key_redirection"><?php include "textor_asset/textor_charset_key_redirection.txt" ?></pre>
    <pre class="asset" id="textor_charset_redirection_origin"><?php include "textor_asset/textor_charset_redirection_origin.txt" ?></pre>
  </body>
  <script defer src="textor_lib/textor_core/textor_core_screen.js"></script>
  <script defer src="test.js"></script>
</html>

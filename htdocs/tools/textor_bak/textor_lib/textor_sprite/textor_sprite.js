console.log("textor_sprite.js");
// TODO SET_LIST_OBJECT

function set_list_to_descr_list(set_list,encoding) {
  this.descr_list = [];
  for (var i = 0; i < set_list.length; i++) {
    this.descr_list.push(encoding.true_key_to_descr(set_list[i][0]));
  }
  return descr_list;
}

function set_list_area(fill,w,h) {
  let set_list = [];
  let i = 0;
  while (set_list.length < w*h){
    set_list.push([fill,i%w,Math.floor(i/w)])
    i++;
  }
  return set_list;
}

function str_to_set_list(str,encoding,quick_descr){
  let set_list = [];
  let x = 0;
  let y = 0;
  for (var i = 0; i < str.length; i++) {
    if (str[i] == '\n') {
      x = 0;
      y++;
      continue;
    }
    quick_descr["character"] = str[i];
    set_list.push([
      encoding.quick_key(quick_descr),
      x,
      y
    ]);
    x++;
  }
  return set_list;
}

function textor_image_file_to_set_list(encoding,textor_image_file,transparency){
  let width = textor_image_file.charCodeAt(0);
  let height = textor_image_file.charCodeAt(1);
  let set_list = [];
  let x = 0;
  let y = 0;
  for (var i = 2; i < textor_image_file.length; i++) {
    let key = textor_image_file.charCodeAt(i);
    let descr = encoding.true_key_to_descr(key);
    if (descr.color == transparency && descr.background == transparency) continue;
    set_list.push([
      key,
      (i-2)%width,
      Math.floor((i-2)/width)
    ]);
  }
  return set_list;
}


function textor_set_list_to_matrix(set_list){
  let matrix = [];
  let width = 0;
  let height = 0;
  for (var i = 0; i < set_list.length; i++){
    width = set_list[i][1] > width ? set_list[i][1] : width;
    height = set_list[i][2] > height ? set_list[i][2] : height;
  }
  while (matrix.length < (width+1)*(height+1)) matrix.push(0);
  for (var i = 0; i < set_list.length; i++){
    matrix[set_list[i][1]+set_list[i][2]*(width+1)] = set_list[i][0];
  }
  return [matrix,width,height];
}

function replace_in_set_list(set_list,encoding,value_a,value_b){
  for (var i = 0; i < set_list.length; i++){
    let descr = encoding.true_key_to_descr(set_list[i][0]);
    for (var j in descr) {
      descr[j] = descr[j] == value_a ? value_b : descr[j];
    }
    set_list[i][0] = encoding.descr_to_true_key(descr);
  }
  return set_list;
}

function textor_copy_set_list(set_list){
  let copy = [];
  while (copy.length < set_list.length){
    let set = [
      set_list[copy.length][0],
      set_list[copy.length][1],
      set_list[copy.length][2]
    ]
    copy.push(set);
  }
  return copy;
}

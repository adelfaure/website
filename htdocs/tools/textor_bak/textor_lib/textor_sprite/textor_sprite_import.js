let textor_sprite = {};

function import_sprite(encoding,name,transparency){
  return new Promise(
    resolve => { 
      read_textor_image_file("./textor_asset/"+name+".txt").then(
        textor_image_file => {
          //textor_sprite[name] = [textor_image_file_to_set_list(encoding,textor_image_file,transparency),transparency];
          textor_sprite[name] = textor_matrix_to_set_list(
            textor_image_to_matrix(textor_image_file),transparency
          );
          console.log("Textor sprite "+name+" imported");
          resolve();
        }
      );
    }
  );
}
// var content = readerEvent.target.result; // this is the content!
// textor_sprite["picture"] = textor_matrix_to_set_list(
//   textor_image_to_matrix(content)
// );

function import_sprites(encoding) {
  let import_options = sprite_import_list.pop();
  return import_sprite(encoding,import_options[0],import_options[1]).then(()=>{
    if (sprite_import_list.length) {
      return import_sprites(encoding);
    } else {
      return;
    }
  });
}

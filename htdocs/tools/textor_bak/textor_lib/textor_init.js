console.log("textor_init.js");
let textor_encoding = new Textor_encoding(textor_encoding_setup);
textor_encoding.import(
  JSON.parse('['+document.getElementById("textor_charset_key_redirection").innerText+']'),
  JSON.parse('['+document.getElementById("textor_charset_redirection_origin").innerText+']'),
  document.getElementById("textor_charset")
);


// TODO NOT GENERIC, SHOULD BE SCENE PROTOTYPE SPECIFIC TO CANVAS CONTEXT
let canvas = document.createElement("canvas");
document.body.appendChild(canvas);

let canvas_context = canvas.getContext("2d");
let scene = new Textor_scene(
  textor_encoding,
  canvas_context,
  Math.floor(window.innerWidth/display_scale/textor_encoding.matrix_width),
  Math.floor(window.innerHeight/display_scale/textor_encoding.matrix_height)
);

import_sprites(textor_encoding).then(()=>{
  console.log("All Textor sprites imported");
  // TODO NOT GENERIC, SHOULD BE SCENE PROTOTYPE SPECIFIC TO CANVAS CONTEXT
  
  start_animation(fps);
  start_animation_event(1,textor_process);
});


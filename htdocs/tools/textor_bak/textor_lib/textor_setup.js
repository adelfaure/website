console.log("textor_setup.js");

let textor_encoding_setup = textor_encoding_default_setup;
textor_encoding_setup.quick_key_preset = {
  "character" : ' ',
  "rotate" : '0',
  "horizontal_mirror" : "false",
  "vertical_mirror" : "false",
  "color" : "soft",
  "background" : "dark"
};
let display_scale = 3;
let fps = 60;

let sprite_import_list = [
  ["cat","soft"],
  ["cat_play","soft"],
  ["cat_walk","soft"],
  ["custom_keys_pic","none"],
  ["help","none"],
];

console.log("textor_encoding_export.js");
console.log("compiling...");
encoding.compile_all();

// CANVAS
let bitmap = document.createElement("canvas");
bitmap.style.backgroundColor = "rgba(0,0,0,0)";
bitmap.width = 2048;
bitmap.height = 2048;
document.body.appendChild(bitmap);
// CONTEXT
let ctx = bitmap.getContext("2d");

for (var i = 0; i < encoding.compiled.length; i++){
  let imageData = ctx.createImageData(encoding.matrix_width,encoding.matrix_height);
  imageData.data.set(encoding.compiled[i].split(','));
  ctx.putImageData(imageData,(i%256)*8,Math.floor(i/256)*8);
}

let key_redirection_str = "";
for (var i = 0; i < encoding.key_redirection.length; i++){
  key_redirection_str += encoding.key_redirection[i]+(i < encoding.key_redirection.length-1 ? ',' : '');
}
let redirection_origin_str = "";
for (var i = 0; i < encoding.redirection_origin.length; i++){
  redirection_origin_str += encoding.redirection_origin[i]+(i < encoding.redirection_origin.length-1 ? ',' : '');
}


// EXPORT
function download(data, filename, type) {
    var file = new Blob(data, {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}


function base64ToByteArrays(base64){
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return byteArrays;
}

let exported = false;
document.addEventListener("keydown",function(){
  if (exported) return;
  exported = true;
  var bitmapFile = bitmap.toDataURL("image/png");
  var bitmapFile64 = bitmapFile.replace(/^data:image\/(png|jpeg);base64,/, "");
  let byteArrays = base64ToByteArrays(bitmapFile64);
  download(byteArrays,"textor_charset","image/png");
  download([key_redirection_str],"textor_charset_key_redirection.txt","text/plain;charset=utf-8");
  download([redirection_origin_str],"textor_charset_redirection_origin.txt","text/plain;charset=utf-8");
});

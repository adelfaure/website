console.log("textor_encoding_matrix.js");
// MATRIX

function matrix(encoding,matrix_content) {
  return matrix_content;
}

function matrix_rotate_90(encoding,matrix_content) {
  let matrix_width = encoding.matrix_width;
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = x*matrix_width+(matrix_width-1)-y;
    new_matrix.push(matrix_content[index]);
  }
  return new_matrix.reverse();
}

function matrix_rotate_180(encoding,matrix_content) {
  return matrix_content.reverse();
}

function matrix_rotate_270(encoding,matrix_content) {
  return matrix_rotate_90(encoding,matrix_rotate_180(encoding,matrix_content));
}


function matrix_mirror_horizontal(encoding,matrix_content) {
  let matrix_width = encoding.matrix_width;
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = matrix_width-1-x+y*matrix_width;
    new_matrix.push(matrix_content[index]);
  }
  return new_matrix;
}

function matrix_mirror_vertical(encoding,matrix_content) {
  let matrix_width = encoding.matrix_width;
  let matrix_height = encoding.matrix_height;
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = (matrix_height-1-y)*matrix_width+x;
    new_matrix.push(matrix_content[index]);
  } 
  return new_matrix;
}

function matrix_fill(encoding,matrix_content,value,fill) {
  for (var i = 0; i < matrix_content.length; i++){
    if (matrix_content[i] === value) matrix_content[i] = fill;
  }
  return matrix_content;
}

// NEED TO REPLACE OLD WITH NEW

function matrix_mirror_horizontal_new(matrix_width,matrix_content) {
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = matrix_width-1-x+y*matrix_width;
    new_matrix.push(matrix_content[index]);
  }
  return new_matrix;
}

function matrix_mirror_vertical_new(matrix_width,matrix_height,matrix_content) {
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = (matrix_height-1-y)*matrix_width+x;
    new_matrix.push(matrix_content[index]);
  } 
  return new_matrix;
}

function matrix_rotate_90_new(matrix_width,matrix_content) {
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = x*matrix_width+(matrix_width-1)-y;
    new_matrix.push(matrix_content[index]);
  }
  return new_matrix.reverse();
}

function matrix_antirotate_90(matrix_width,matrix_content) { // (NEW)
  let new_matrix = [];
  for (var i = 0; i < matrix_content.length; i++) {
    let y = Math.floor(i/matrix_width);
    let x = i%matrix_width;
    let index = x*matrix_width+(matrix_width-1)-y;
    new_matrix.push(matrix_content[index]);
  }
  return new_matrix;
}


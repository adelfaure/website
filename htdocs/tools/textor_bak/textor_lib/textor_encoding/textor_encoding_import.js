console.log("textor_encoding_import.js");

// import charset key redirection
encoding.key_redirection = JSON.parse('['+document.getElementById("textor_charset_key_redirection").innerText+']');

// import charset bitmap
encoding_bitmap_ctx = encoding.bitmap.getContext("2d");
encoding_bitmap_ctx.drawImage(document.getElementById("textor_charset"),0,0);

console.log("textor_encoding.js");

// TEXTOR ENCODING

function pass_steps() {
  let steps = [];
  for (var i = 1; i < arguments.length; i++){
    let val = arguments[i];
    for (var j = i+1; j < arguments.length; j++){
      val *= arguments[j];
    }
    steps.push(val);
  }
  steps.push(1);
  return steps;
}

function pass_to_key(pass_indexes,pass_steps) {
  let key = 0;
  for (var i = 0; i < pass_indexes.length; i++) {
    key += pass_indexes[i] * pass_steps[i];
  }
  return key;
}

function key_to_pass(key,pass_steps) {
  let pass = [];
  for (var i = 0; i < pass_steps.length; i++) {
    let pass_index = Math.floor(key/pass_steps[i]);
    pass.push(pass_index);
    key -= pass_index * pass_steps[i]
  }
  return pass;
}

function steps_from_setup(setup) {
  let steps = [];
  for (var i in setup.body) {
    steps.push(Object.keys(setup.body[i]).length);
  }
  return pass_steps(...steps);
}

Textor_encoding = function(setup) {
  this.setup = setup;
  this.setup_name = setup.name;
  this.setup_keys = Object.keys(setup.body);
  this.setup_sub_keys = [];
  for (var i = 0; i < this.setup_keys.length; i++) {
    this.setup_sub_keys.push(Object.keys(setup.body[this.setup_keys[i]]));
  }
  this.steps = steps_from_setup(setup);
  this.range = this.steps[0] * Object.keys(this.setup.body[this.setup_keys[0]]).length;
  this.compiled = [];
  this.key_redirection = [];
  this.redirection_origin = [];
  this.bitmap = document.createElement("canvas");
  this.bitmap.style.backgroundColor = "rgba(0,0,0,0)";
  this.bitmap.width = 2048;
  this.bitmap.height = 2048;
  this.matrix_width = this.setup.matrix_width;
  this.matrix_height = this.setup.matrix_height;
  this.quick_key_preset = this.setup.quick_key_preset;
  this.compiled_descr = [];
  this.found_descr = [];
  console.log("Textor encoding initialized with "+this.setup_name+" setup");
}

Textor_encoding.prototype.key_to_pass = function(key){
  return key_to_pass(key,this.steps);
}

Textor_encoding.prototype.pass_to_key = function(pass){
  return pass_to_key(pass,this.steps);
}

Textor_encoding.prototype.descr_to_pass = function(descr){
  let pass = [];
  for (var i = 0; i < this.setup_keys.length; i++) {
    for (var j in descr) {
      if (j == this.setup_keys[i]) {
        pass.push(this.setup_sub_keys[i].indexOf(descr[j]));
      }
    }
  }
  return pass;
}

Textor_encoding.prototype.descr_to_key = function(descr){
  return this.pass_to_key(this.descr_to_pass(descr));
}

Textor_encoding.prototype.pass_to_descr = function(pass){
  let descr = {};
  for (var i = 0; i < this.setup_keys.length; i++){
    descr[this.setup_keys[i]] = this.setup_sub_keys[i][pass[i]];
  }
  return descr;
}


Textor_encoding.prototype.key_to_descr = function(key){
  return this.pass_to_descr(this.key_to_pass(key));
}

/* * * * * * * * * * * * * * * * * * * *
 * NOT GENERIC ? SPECIFIC TO CANVAS ?  *
 * * * * * * * * * * * * * * * * * * * */

Textor_encoding.prototype.compile_key = function(key){
  let descr = this.key_to_descr(key);
  let compilation = undefined;
  for (var i in descr) {
    compilation = this.setup[i][descr[i]](this,compilation);
  }

  // NOT GENERIC, USED FOR ENCODING COMPRESSION
  return compilation.join(',');
}

Textor_encoding.prototype.compile_all = function(){
  for (var i = 0; i < this.range; i++) {
    let content = this.compile_key(i);

    // ENCODING COMPRESSION
    let index = this.compiled.indexOf(content);
    if (index < 0){
      this.key_redirection.push(this.compiled.length);
      if (this.redirection_origin[this.compiled.length] == undefined) this.redirection_origin[this.compiled.length] = i;
      this.compiled.push(content);
    } else {
      //if (this.redirection_origin[index] == undefined) this.redirection_origin[index] = i;
      this.key_redirection.push(index);
    }
  }
  console.log(this.range-this.compiled.length,"entries have been compressed out of",this.range,((this.range-this.compiled.length)/this.range*100).toFixed(2)+'%');
  if (this.compiled.length > 65536) {
    console.log("Number of compiled entries",this.compiled.length,"is incompatible with 16-bit",65536);
  } else {
    console.log("Number of compiled entries",this.compiled.length,"is compatible with 16-bit",65536);
  }
}

Textor_encoding.prototype.true_key = function(key){
  return this.key_redirection[key];
}

Textor_encoding.prototype.true_key_to_descr = function(true_key){
  return this.key_to_descr(this.key_redirection.indexOf(true_key));
}

Textor_encoding.prototype.descr_to_true_key = function(descr){
  let code = Object.values(descr).join('');
  if (this.found_descr[code]) return this.found_descr[code];
  let key = this.true_key(this.pass_to_key(this.descr_to_pass(descr)));
  this.found_descr[code] = key;
  return key;
}

Textor_encoding.prototype.quick_key = function(quick_descr){
  let descr = {};
  for (var i in this.quick_key_preset) {
    descr[i] = quick_descr[i] ? quick_descr[i] : this.quick_key_preset[i];
  }
  let key = this.pass_to_key(this.descr_to_pass(descr));
  return this.true_key(key);
}

Textor_encoding.prototype.print_compilation = function(compilation){
  let str = '';
  let matrix_width = this.matrix_width;
  for (var i = 0; i < compilation.length; i++) {
    str += compilation[i] + (!((i+1)%matrix_width) ? '\n' : '');
  }
  return str;
}

Textor_encoding.prototype.draw_key = function(ctx,key,x,y){
  let map_width = this.bitmap.width/this.matrix_width;
  let map_x = key%map_width*this.matrix_width;
  let map_y = Math.floor(key/map_width)*this.matrix_height;
  ctx.drawImage(this.bitmap,map_x,map_y,this.matrix_width,this.matrix_height,x,y,this.matrix_width,this.matrix_height);
}

Textor_encoding.prototype.compile_descr = function(){
  for (var i = 0; i < this.redirection_origin.length; i++) {
    this.compiled_descr[i] = this.key_to_descr(this.redirection_origin[i]);
  }
};

Textor_encoding.prototype.import = function(key_redirection,redirection_origin,charset_image){
  // import charset key redirection
  this.key_redirection = key_redirection; 
  this.redirection_origin = redirection_origin; 
  this.compile_descr();
  // import charset bitmap
  encoding_bitmap_ctx = this.bitmap.getContext("2d");
  encoding_bitmap_ctx.drawImage(charset_image,0,0);
}

console.log("textor_element.js");

let textor_element_list = [];
let textor_element_to_remove_list = [];

Textor_element = function(name,sprite_name,x,y,priority,event,process_event){
  this.name = name;
  this.sprite_name = sprite_name;
  this.x = x;
  this.y = y;
  this.priority = priority;
  this.event = event ? event : () => false;
  this.process_event = process_event ? process_event : () => false;
  textor_element_list.push(this);
}

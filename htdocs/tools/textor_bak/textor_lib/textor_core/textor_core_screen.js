console.log("textor_core_screen.js");

Textor_screen = function(filler,width,height){
  this.width = Math.floor(width);
  this.height = Math.floor(height);
  this.content = [];
  while(this.content.length != width*height) this.content.push(filler);
}

Textor_screen.prototype.fill = function(filler) {
  this.content.fill(filler);
}

// TODO in utils ?
function pos_to_index(x,y,width){
  return Math.floor(y) * width + Math.floor(x);
}
// TODO in utils ?
function index_to_pos(index,width){
  return [index%width,Math.floor(index/width)];
}

Textor_screen.prototype.valid = function(x,y) {
  return (
    x >= this.width
    || x < 0
    || y >= this.height
    || y < 0
  ) ? false : true;
}

Textor_screen.prototype.set = function(val,x,y) {
  if (!this.valid(x,y)) return null;
  return this.content[pos_to_index(x,y,this.width)] = val;
}

Textor_screen.prototype.get = function(x,y) {
  if (!this.valid(x,y)) return null;
  return this.content[pos_to_index(x,y,this.width)];
}

Textor_screen.prototype.print = function(){
  let str = '';
  for (var i = 0; i < this.content.length; i++) {
    str += (i && Number.isInteger(i/this.width) ? '\n' : '') + this.content[i]
  }
  return str;
}

Textor_screen.prototype.add = function(screen,start_x,start_y){
  for (var i = 0; i < screen.content.length; i++) {
    let pos = index_to_pos(i,screen.width);
    pos[0] += start_x;
    pos[1] += start_y;
    this.set(...[screen.content[i],pos[0],pos[1]]);
  }
}

function textor_sprite_to_screen(sprite){
  let width = 0;
  let height = 0;
  let set_list = sprite[0];
  for (var i = 0; i < set_list.length; i++){
    width = set_list[i][1] > width ? set_list[i][1] : width;
    height = set_list[i][2] > height ? set_list[i][2] : height;
  }
  let screen = new Textor_screen(0,width+1,height+1);
  for (var i = 0; i < set_list.length; i++){
    screen.set(...set_list[i]);
  }
  return screen;
}

function textor_screen_to_sprite(screen){
  let set_list = [];
  for (var i = 0; i < screen.content.length; i++) {
    set_list.push([screen.content[i]].concat(index_to_pos(i,screen.width)));
  }
  return [set_list,"none"];
}



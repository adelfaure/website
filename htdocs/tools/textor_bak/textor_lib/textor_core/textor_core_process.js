console.log("textor_process.js");


// TODO NOT GENERIC, SPECIFIC TO BITMAP AND SHOULD BE SCENE PROTOTYPE
// CANVAS CONTEXT ARG IS NOT GENERIC
function update_canvas_size(textor_encoding,canvas_context){
  let size_w = Math.floor(window.innerWidth/display_scale);
  let size_h = Math.floor(window.innerHeight/display_scale);
  if (size_w != canvas.width || size_h != canvas.height) {
    canvas.width = size_w;
    canvas.height = size_h;
    canvas.style.transform = "scale("+display_scale+")";
    // TODO SHOULD NOT RECREATE ALL SCENE
    scene = new Textor_scene(
      textor_encoding,
      canvas_context,
      Math.floor(size_w/textor_encoding.matrix_width),
      Math.floor(size_h/textor_encoding.matrix_height)
    );
    console.log("Canvas size updated");
  }
}

// TODO ENCODING SHOULD BE AN ARGUMENT OF TEXTOR PROCESS, MAYBE ANIMATION SHOULD BE A TEXTOR GLOBAL OBJECT PROTOTYPE
// SAME GOES FOR CANVAS_CONTEXT ARG
function textor_process(frame){
  if (!frame) console.log("Textor process launched");
  update_canvas_size(textor_encoding,canvas_context);

  scene.clear();
  
  while (textor_element_to_remove_list.length) {
    textor_element_list.splice(textor_element_list.indexOf(textor_element_to_remove_list.pop()),1);
  }
  // TODO NOT GENERIC, SPECIFIC TO 2D
  textor_element_list.sort(function(a,b){
    return a.priority - b.priority || a.y - b.y;
  });
  for (var i = 0; i < textor_element_list.length; i++) {
    textor_element_list[i].process_event(textor_element_list[i],frame);
    scene.add(textor_element_list[i],frame);
  }
  for (var i = 0; i < textor_element_list.length; i++) {
    scene.add_to_print(textor_element_list[i],frame);
  }

  scene.print(canvas_context);
};

console.log("textor_core_scene.js");

Textor_scene = function(encoding,context,width,height){
  this.encoding = encoding;
  this.context = context;
  this.width = width;
  this.height = height;
  this.display_filler = this.encoding.quick_key({});
  this.display = new Textor_screen(
    this.display_filler,this.width,this.height
  );
  console.log("Textor scene display screen initialized");
  this.printed_display = new Textor_screen(
    null,this.width,this.height
  );
  console.log("Textor scene printed display screen initialized");
  this.event = new Textor_screen(
    () => false,this.width,this.height
  );
  console.log("Textor scene event screen initialized");
  console.log("Textor scene initialized with "+encoding.setup_name+" encoding setup");
}

// TODO NOT GENERIC, ONLY WORKS WITH BITMAP KEYS
Textor_scene.prototype.print = function(context){
  for (var i = 0; i < this.display.content.length; i++) {
    if (this.display.content[i] == this.printed_display.content[i]) continue;
    this.encoding.draw_key(
      context,
      this.display.content[i],
      i%this.width * this.encoding.matrix_width,
      Math.floor(i/this.width) * this.encoding.matrix_height
    );
    this.printed_display.content[i] = this.display.content[i];
  }
}

Textor_scene.prototype.clear = function(){
  this.display.fill(this.display_filler);
  this.event.fill(() => false);
}

Textor_scene.prototype.add = function(element,frame) {
  if (!textor_sprite[element.sprite_name]) return;
  let set_list = textor_sprite[element.sprite_name][0];

  for (var i = 0; i < set_list.length; i++){
    let _x = set_list[i][1]+element.x;
    let _y = set_list[i][2]+element.y;
    let set_list_data = set_list[i];
    let element_event = get => element.event(
      element,get,frame,set_list_data
    );
    if (this.event.get(_x,_y)) {
      let send = element_event(this.event.get(_x,_y)());
      let get = this.event.get(_x,_y)(element_event());
      this.event.set(element_event,_x,_y);
    }
  }
}

Textor_scene.prototype.add_to_print = function(element,frame) {
  if (!textor_sprite[element.sprite_name]) return;
  
  let set_list = textor_sprite[element.sprite_name][0];
  let transparency = textor_sprite[element.sprite_name][1];
  for (var i = 0; i < set_list.length; i++){
    let _x = set_list[i][1]+element.x;
    let _y = set_list[i][2]+element.y;
    
    let key = set_list[i][0];
    
    let descr = this.encoding.compiled_descr[key];
 
    if (descr && (descr.background == transparency || descr.color == transparency)) {
      if (descr.background == descr.color) continue;
      let target_descr = this.encoding.compiled_descr[this.display.get(_x,_y)];
      if (target_descr == null) continue;
      let set_descr = {
        "character" : descr.character,
        "color" : descr.color == transparency ? target_descr.color : descr.color,
        "background" : descr.background == transparency ? target_descr.background : descr.background,
        "rotate": descr.rotate,
        "horizontal_mirror" : descr.horizontal_mirror,
        "vertical_mirror" : descr.vertical_mirror
      };
      
      this.display.set(this.encoding.descr_to_true_key(set_descr,
        set_descr.character+set_descr.color+set_descr.background+set_descr.rotate+set_descr.horizontal_mirror+set_descr.vertical_mirror
      ),_x,_y);

    } else {
      this.display.set(key,_x,_y);
    }
  }
}

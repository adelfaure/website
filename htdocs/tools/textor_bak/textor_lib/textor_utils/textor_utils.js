Array.prototype.random = function(){
  return this[Math.floor(Math.random()*this.length)];
}
Array.prototype.random_index = function(){
  return Math.floor(Math.random()*this.length);
}
Array.prototype.random_extract = function(){
  return this.splice(this.random_index(),1)[0];
}
Array.prototype.shuffle = function(){
  let new_content = [];
  while (this.length) new_content.push(this.random_extract());
  while (new_content.length) this.push(new_content.pop());
}

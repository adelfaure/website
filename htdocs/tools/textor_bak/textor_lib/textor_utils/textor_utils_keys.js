// TODO NOT GENERIC, SPECIFIC TO DOM, SHOULD BE INTO A TEXTOR GLOBAL OBJECT WITH DOM CONTEXT OPTION
// TODO RENAME TO USER INPUT
console.log("textor_keys.js");

let keys = {};
let last_typed = undefined;
let key_down = false;
let typing = false;
let circumflex_mode = false;
let trema_mode = false;

document.addEventListener("keydown",function(e){
  e.preventDefault();
  typing = true;
  key_down = true;
  keys[e.key] = true;
  circumflex_mode = e.key == 'Dead' && !keys["Shift"] ? true : circumflex_mode;
  trema_mode = e.key == 'Dead' && keys["Shift"] ? true : trema_mode;
  last_typed = e.key;
});

document.addEventListener("keyup",function(e){
  //e.preventDefault();
  typing = false;
  keys[e.key] = false;
  let is_key_down = true;
  for (var i in keys) {
    if (keys[i]) {
      is_key_down = false;
      break;
    }
  }
  key_down = is_key_down ? false : key_down;
  last_typed = is_key_down ? undefined : last_typed;
});

// MOUSE

let mouse_x = 0;
let mouse_y = 0;
let mouse_down = false;

// TODO SHOULD BE INITIALIZED BY A GLOBAL TEXTOR OBJECT
document.addEventListener("mousemove",function(e){
  mouse_x = e.clientX/textor_encoding.matrix_width/display_scale;
  mouse_y = e.clientY/textor_encoding.matrix_height/display_scale;
});
document.addEventListener("mousedown",function(e){
  mouse_down = true;
});
document.addEventListener("mouseup",function(e){
  mouse_down = false;
});
start_animation_event(1,function(frame){
  typing = false;
});

console.log("textor_utils_image.js");
// TODO WEIRD SCRIPT NAME, RELATIVE TO BOTH TEXTOR FILES AND IMAGE

function read_textor_image_file(url){
  return fetch(url, {
    "method": "GET"
  }).then( response => {
    return response.blob().then( blob => {
      var reader = new FileReader();
      reader.readAsText(blob,'UTF-8');
      return new Promise( resolve => {
        reader.onload = readerEvent => {
          resolve(readerEvent.target.result);
        }
      });
    });
  });
}

function compress_image_char_count(char,char_count){
  let compress_code = String.fromCharCode(65535)+String(char_count)+String.fromCharCode(65535)+char+String.fromCharCode(65535);
  if (compress_code.length < char_count) {
    return compress_code;
  } else {
    let str = '';
    while(str.length < char_count) str += char;
    return str;
  }
}

// TODO COMPRESS REPEATED DATA
function textor_matrix_to_image(matrix){
  let str = String.fromCharCode(matrix[1])+String.fromCharCode(matrix[2]);
  let char = null;
  let char_count = 0;
  for (var i = 0; i < matrix[0].length; i++){
    let new_char = String.fromCharCode(matrix[0][i]);
    if (new_char != char) {
      if (char_count) str += compress_image_char_count(char,char_count);
      char_count = 0;
      char = new_char;
    }
    char_count++;
  }
  str += compress_image_char_count(char,char_count);
  return str;
}

function textor_matrix_to_set_list(matrix,transparency) {
  let set_list = [[],transparency ? transparency : 'none'];
  let width = matrix[1];
  let height = matrix[2];
  for (var i = 0; i < matrix[0].length; i++){
    set_list[0].push([matrix[0][i],i%width,Math.floor(i/width)]);
  }
  return set_list;
}

function textor_image_to_matrix(image){
  let matrix = [];
  let width = image.charCodeAt(0)+1;
  let height = image.charCodeAt(1)+1;
  let read_code = false;
  let read_code_char = false;
  let code_char = null;
  let code_count = '';
  for (var i = 2; i < image.length; i++){
    let code = image.charCodeAt(i);
    if (code == 65535 && !read_code) {
      read_code = true;
      continue;
    } else if (code == 65535 && read_code && !read_code_char) {
      read_code_char = true;
      continue;
    } else if (code == 65535 && read_code && read_code_char) {
      code_count = Number(code_count);
      code = code_char.charCodeAt(0);
      for (var j = 0; j < code_count; j++){
        matrix.push(code);
      }
      read_code = false;
      read_code_char = false;
      code_char = null;
      code_count = '';
      continue;
    }
    if (read_code && !read_code_char) {
      code_count += image[i];
    } else if (read_code && read_code_char){
      code_char = image[i];
    } else {
      matrix.push(code);
    }
  }
  return [matrix,width,height];
}

// Function to download data to a file
function download(data, filename, type) {
  var file = new Blob(data, {type: type});
  if (window.navigator.msSaveOrOpenBlob) // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename);
  else { // Others
    var a = document.createElement("a"),
    url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);  
    }, 0); 
  }
}

<!doctype html>
<html>
  <head>
    <meta charset="utf8"/>
    <link rel="stylesheet" href="textor_style/textor_style.css">
  </head>
  <body>
    <img class="asset" id="textor_charset" defer src="textor_asset/textor_charset.png">
    <pre class="asset" id="textor_charset_key_redirection"><?php include "textor_asset/textor_charset_key_redirection.txt" ?></pre>
    <pre class="asset" id="textor_charset_redirection_origin"><?php include "textor_asset/textor_charset_redirection_origin.txt" ?></pre>
  </body>
  
  
  <!-- textor utils -->
  <script defer src="textor_lib/textor_utils/textor_utils.js"></script>
  <script defer src="textor_lib/textor_utils/textor_utils_time.js"></script>
  <script defer src="textor_lib/textor_utils/textor_utils_animation.js"></script>
  <script defer src="textor_lib/textor_utils/textor_utils_keys.js"></script>
  <script defer src="textor_lib/textor_utils/textor_utils_image.js"></script>

  <!-- textor encoding -->
  <script defer src="textor_lib/textor_encoding/textor_encoding_matrix.js"></script>
  <script defer src="textor_lib/textor_encoding/textor_encoding_setup.js"></script>
  <script defer src="textor_lib/textor_encoding/textor_encoding.js"></script>
  
  <!-- textor setup -->
  <script defer src="textor_lib/textor_setup.js"></script>
  
  <!-- textor core -->
  <script defer src="textor_lib/textor_core/textor_core_screen.js"></script>
  <script defer src="textor_lib/textor_core/textor_core_scene.js"></script>
  <script defer src="textor_lib/textor_core/textor_core_element.js"></script>
  <script defer src="textor_lib/textor_core/textor_core_process.js"></script>
  
  <!-- textor sprite -->
  <script defer src="textor_lib/textor_sprite/textor_sprite.js"></script>
  <script defer src="textor_lib/textor_sprite/textor_sprite_import.js"></script>
  
  <!-- bonus lib -->
  <script defer src="textor_lib/textor_live_coding.js"></script>

  <!-- textor init -->
  <script defer src="textor_lib/textor_init.js"></script>
  
  <!-- textor script -->
  <script defer src="textor_script.js"></script>
  <script defer src="script.js"></script>
  
  <!-- textor encoding 
  <script defer src="textor_lib/textor_encoding/textor_encoding_matrix.js"></script>
  <script defer src="textor_lib/textor_encoding/textor_encoding.js"></script>
  <script defer src="textor_lib/textor_encoding/textor_encoding_setup.js"></script>
    <script defer src="textor_lib/textor_encoding/textor_encoding_import.js"></script>-->
  
  <!-- Setup
  <script defer src="setup.js"></script>
  
    textor image
  <script defer src="textor_lib/textor_image.js"></script>
  
    textor common
  <script defer src="textor_lib/textor_time.js"></script>
  <script defer src="textor_lib/textor_animation.js"></script>
  <script defer src="textor_lib/textor_keys.js"></script>
  <script defer src="textor_lib/textor_screen.js"></script>
  <script defer src="textor_lib/textor_set_list.js"></script>
  <script defer src="textor_lib/textor_element.js"></script>
  <script defer src="textor_lib/textor_scene.js"></script>
  <script defer src="textor_lib/textor_process.js"></script>
  <script defer src="textor_lib/textor_sprite.js"></script>
  <script defer src="textor_lib/textor_start.js"></script>
  

   -->
</html>

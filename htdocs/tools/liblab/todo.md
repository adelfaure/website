# TODO

## Ocode (character encoding)

- semi graphic + Latin (basic and extended)
- color
- background color

## Parser ? Functions ?

- Ocode parser \0 || Ocode functions O(code|sheet,sub_code)



## Before 2023-04

```
v0.1

Switch to Draw mode                             Control + 1             X
Switch to Write mode                            Control + 2             X
Switch to Move camera mode                      Control + 3             X
Switch to Change color mode                     Control + 4             X
Switch to Change sheet mode                     Control + 5             X
Switch to Change step mode                      Control + 6             X

Grow width selection                            Control + ArrowRight    X
Grow height selection                           Control + ArrowDown     X
Shrink width selection                          Control + ArrowLeft     X
Shrink height selection                         Control + ArrowUp       X
Copy                                            Control + c             X
Paste                                           Control + v             X
Invert colors                                   Control + i             X
Pick color                                      Control + p             X
Fill                                            Control + f             X
Undo                                            Control + z             X
Redo                                            Control + y             X
Load file                                       Control + l             X
Save file                                       Control + s             X

v0.2

Zoom in                                         Mode 3  + i             X
Zoom out                                        Mode 3  + o             X
parametrable buffer export                      Control + e             X
parametrable buffer import                      Control + m             X
fix full white refresh bug                                              .
clear document                                  Control + Alt + a       /
add loading to undoable memory                                          .
fix color bug -> Check hex colors for bg !!!                            .
fix undo/redo bug (camera ?)                                            X
fix unknown undo/redo bug                                               .
Fix pick text color bug                                                 .
Cursor position                                                         .
X/Y steps                                                               .
fix change cursor size limit bug                                        X
Try PixiJs                                                              .
Help mode (7 mode ?)                                                    .
fg,bg,glyph index display                                               .

v0.4 (music)

live code mode (by fg,bg,glyph)
music mode with play zones (by color ?)
animations ?

v0.5

Better load / save function
Key info for sheet mode and step mode
Better resize function
menus (intro etc.)
  Display keys
limit cursor to screen
parametrable png export
auto color declinaisonfrom one imported png
```

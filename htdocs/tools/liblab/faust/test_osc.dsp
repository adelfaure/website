import("stdfaust.lib");
freq = hslider("h:[0]frequences/freq",400,200,800,800/32);
freq2 = hslider("h:[0]frequences/freq2",400,200,800,800/32);
freq3 = hslider("h:[0]frequences/freq3",400,200,800,800/32);
gain = hslider("gain",0.5,0,1,0.1);

dur= hslider("duration",0.5,0.01,10,0.01);

att = hslider("attack",0.1,0.001,0.5,0.001);
rel = hslider("release",0.5,0.01,2,0.01);

gate = checkbox("checkbox");

process = os.osc(freq)*en.ar(att,rel,gate) * gain ,os.osc(freq2)*en.ar(att,rel,gate) * gain,os.osc(freq3)*en.ar(att,rel,gate) * gain :> _ :ef.echo(1,dur,0.5) <: _ , _;

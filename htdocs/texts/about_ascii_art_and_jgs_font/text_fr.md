% À propos de l'ASCII Art et de Jgs Font
% Adel Faure
% 2022

L'ASCII Art c'est quoi ?
------------------------

![Starry Night, Veni, Vidi, ASCII, 2020](img/starry_night_by_venividiascii.png)

Expliquer ce que signifie ASCII Art n'est pas simple. Plutôt que de délimiter
une pratique bien définie, l'ASCII Art vient brouiller une distinction commune
entre image et texte, pour le monde de l'art, et entre "interface graphique" et
"mode texte", pour l'informatique.

Au sens strict, l'expression designe les images composée a l'aide des 128
caractères présents dans l'_American Standard Code for Information Interchange_
(abbrévié ASCII). Bien que "Text Art" ou "Textmode Art" soit aussi employé,
"ASCII Art" ou plus simplement "ASCII" est devenu une manière de décrire toute
image produite à l'aide d'éléments typographiques. En 1999 dans _The History of
ASCII (text) Art_, Joan G. Stark décrit l'ASCII de la manière suivante :

> They are "non-graphical graphics". Its palette is limited to the symbols
> and characters that you have available to you on your computer keyboard.[^1]

> Ce sont des "élements graphiques non-graphiques". Leur palette se limite aux
> symboles et caractères que ton clavier d'ordinateur te permet d'utiliser.

Cette définition joue sur l'ambiguëté de sens, entre le terme anglais
"graphic", comme objet figuratif et "graphic", comme élément d'une interface
graphique. À l'époque où Stark écrit, les premiers réseaux sociaux (Usenet,
BBS, Minitel, Ceefax, etc.), encore très populaires, fonctionnent "en mode
texte". Des interfaces dans lesquelles l'écran est un comme un quadrillage où
chaque case permet d'afficher un glyphe. Tandis que ces dernières disparaissent
progressivement au profit des interfaces graphiques, Stark souligne l'ambiguëté
du statut de l'ASCII Art dans ce processus. La présence d'éléments graphiques
dans des environnements textuels devenant cette bizarerie de l'ASCII. 

Comme le décrit Stark, il s'agit de faire avec ce que nous propose un clavier
d'ordinateur. Partant de ce principe il faut imaginer que chaque système
associé à un clavier mène à un ASCII différent. C'est ainsi que nous trouvons
le PETSCII associé au Commodore PET/CBM, l'ANSI avec les BBS (Bulletin Board
Systems), l'ATASCII avec Atari, le Shift-JIS avec le mode Katakana des claviers
japonais, le Teletext pour le Videotext (Prestel, Minitel). Dans cet
écosystème, l'expression ASCII désignant plus spécifiquement le style Amiga
(oldschool et newschool) ou le style Usenet (line-style et solid-style)[^2].
Chacun de ces ASCII ayant sa propre scène artitique, avec ses groupes, ses
artistes et parfois même sa propre plateforme de publication.

- [www.asciiarena.se](https://www.asciiarena.se) - Amiga ASCII
- [www.16colo.rs](https://www.16colo.rs) - ANSI
- [www.csdb.dk](https://www.csdb.dk) - PETSCII
- [www.teletextart.co.uk](https://www.teletextart.co.uk) - Teletext

On trouve dans cette diversité de styles autant de raisons d'être et
d'usages. Chuck Peddle, lorsqu'il designe le PETSCII, inclu des trames et des
formes géométriques, ce qui facilita la création de jeux sur un système
strictement limité à un affiche en mode texte.

![Joust, The Code Works, 1980, Commodore PET/CBM](img/joust-commodore-pet-cbm-screenshot-unhorsed.png)

Dans _ASCII art : From a Commodity Into an Obscurity_, Heikki Lotvonen, rapelle
le rôle social de l'ASCII art dans la scène ANSI émergeante. Les utilisateurs
non-hackers mais doués en ASCII art pouvaient gagner accès aux contenus des BBS
pirates en échange d'illustrations.

![BBS stats menu, Sole Assassin, 1994](img/SA-STAT.CIA.png)

Du fait qu'il très simple de le reproduire et le modifier (copié-collé),
L'ASCII fut le moyen de prédilection des memes sur les premiers réseaux de
forums. Voir l'immense archive de
[www.asciiartfarts.com](http://www.asciiartfarts.com/) (qui contient
malheuresement de très nombreux exemples homophobes, misogynes et/ou racistes…)

![MEMENTS, 2006](img/mements.png)

Certains des personnages populaires d'internet viennent spécifiquement de
l'ASCII art. C'est le cas par exemple de "Kuma" (plus tard Pedobear) ou de
"Domo" dont la forme et les postures particulières tirent leur origine de
SHIFT-JIS partagées sur 2chan.

![The original "Kuma" of 2chan (trouvé sur [www.knowyourmeme.com](https://knowyourmeme.com/memes/pedobear)) et son équivalent contemporain](img/kuma.gif)

![Compilation of 2chan characters (trouvé sur [www.outsiderjapan.pbworks.com)](http://outsiderjapan.pbworks.com/w/page/9758331/2channel?mode=print) et une peluche du personnage "Domo"](img/2ch_AA_Characters.gif)

Plus généralement, partout et de tout temps où le texte mécanisé propose des
contraintes, on risque de découvrir une forme d'ASCII spécifique.

Dans _Neither Good, Fast, Nor Cheap: Challenges of Early Arabic Letterpress
Printing_, Hala Auji décrit comment les imprimeurs des premières presses du
moyen-orient contournent les limites de la composition au plomb pour produire
des ornements.

> Manuscripts, for example, used illumination devices, akin to frontispieces
> and headpieces, called a sarlawh or ‘unwan. These were often elaborately
> hand-colored and gilded, to indicate the start of each book and its
> subsequent chapters […]. To recall these elaborate designs in their printed
> books, employees at this press creatively employed varied ornamental sorts,
> as well as punctuation marks, to create similar compositions.

> Les manuscrits, par exemple, comportaient des enluminures, à la manière de
> frontispices, appelés sarlawh ou 'unwan'. Ces derniers, souvent très
> élaborés, était colorés et dorés à la main, afin d'indiquer le début de
> chaque livre et des chapitres suivants […]. Pour rappeler ces motifs dans
> leurs livres imprimés, les employés de cette presse ont utiliser divers types
> d'ornements, ainsi que des signes de ponctuation, reproduisant de manière
> créative des compositions similaires.

![Page de Nasif al-Yaziji, Kitab Fasl al-Khitab fi Usul Lughat al-I‘rab, Beirut: American Mission Press, 1836](img/FIG3_NasifYaziji_Fasl_al_khitab_Widener.jpg)

Cette pratique de l'improvisation n'était pas rare dans le domaine de
l'impression au plomb. Une nécéssité quand les pièces viennent à manquer ou un
loisir pour les employés les plus passionés.

![printersgrammarw0000smit_0153.jpg](img/printersgrammarw0000smit_0153.jpg)

> Such are the shifts which sometimes are made, where neither Cuts nor Flowers
> are provided, to dress the first page of a Work : and therefore a double rule
> is often used ; the rather, because it takes off the trouble of making up
> Head-pieces without proper Sorts.

> Ainsi sont les adaptations qui sont parfois faites, quand ni séparateur ni
> fleurs ne sont fournies, pour habiller la première page d'un ouvrage. Dans ce"
> cas une règle double est souvent utilisée ; d'autant plus qu'elle nous évite
> d'improviser des frontispices sans les pièces adéquates

![Improvisation, Alfred P. Fluhr, fin XVIII](img/alfred_p_fluhr_improvisation.png)

> An improvised illustration created by Alfred P. Fluhr, an apprentice with the Martin B. Brown Compagny, New York city, is reproduced. The design was constructed with parenthese and rules in a playful mood during spare moments. A little experimenting of this kind during odd moments may help constructive ability, but the fad should not be permitted to develop into a habit. Practical composition will be of more benefit to a boy who aims to attain distinction as a job-printer.

> Une illustration improvisée crée par Alfred P. Fluhr, un apprenti de la Martin B. Brown Compagny, New York city, est reproduite. Le dessin fut élaboré à l'aide de parenthèes et de règles dans une humeur espiègle durant les moments de pauses. Un peu d'expérimentation de ce genre de temps à autre peux aider à développer l'habilité de composition, mais cette tendance ne doit pas devenir une habitude. La composition concrète sera plus bénéfique à un garçon qui souhaite obtenir la qualification d'imprimeur.

Certains imprimeurs, choissant spécifiquement d'exploiter ce type de méthodes
pour produire des oeuvres furent en quelque sorte les ASCII artistes de leur
temps.

![The Antique Shop, Albert Schiller, 1938](img/TheAntiqueshop_AlbertSchiller.jpg)


![Kojak, Bob Neill, 1982](img/kojak_bob_neil.jpg)




[^3]: The history of the changing status of ASCII art [ASCII art From a Commodity Into an Obscurity by Heikki Lotvonen](./docs/ASCII_art_From_a_Commodity_Into_an_Obscurity.pdf)


Au delà de l'historique, cette manière de jouer avec ou de ruser le texte, de
trouver la liberté au travers de la grille (de la cage ?), est peut-être ce que
qui caractrise le mieux les "arts ASCII".

> The lure of ASCII art might not be in the nostalgia of how it looks, but what
> it represents: the ideals of "cyberspace". It stands for a wistful longing
> for those pre-internet days when corporations hadn't yet taken control of our
> digital day-to-day and the community was still in control of organising
> itself.

> L'attrait de l'ASCII art ne réside peut-être pas dans son apparente
> nostalgie, mais plutôt dans ce qu'il représente : les idéaux du
> "cyber-espace". Il transmet la mémoire de nos réseaux avant l'avènement
> d'internet, lorsque que les industriels n'avaient pas encore pris le contrôle
> de notre vie quotidienne en ligne, et que les communautées avaient encore le
> pouvoir de s'auto-organiser.

[To the gallery](#gallery)

<!-- 
Qu'est ce que l'ASCII art est une question laquelle chaque artiste ASCII répond

ASCII art tutorials :

-> Daniel C. Au (dcau)
-> Jonathon R. Oglesbee (JRO)
-> Susie Oviatt
-> Normand Veilleux
-> Rowan Crawford (Row)
-> Maija Haavisto (DiamonDie, mh)
-> Targon
-> Joan G Stark

ASCII art Faqs

-> Bob Allison (Scarecrow)
-> Jorn Barger
-->

### BOB NEIL BOOK OF TYPEWRITER ART

typewriter art

Flora Stacey butterfly 1898

http://www.roysac.com/learn/images/jgs_typebutterfly.jpg


### ALBERT SCHILLER

art of printing ornament


Joan G. Stark
-------------

![Joan G. Stark autoportrait and standard signature](img/jgs.png)


When I started to take interest in ASCII art I quickly came across Joan G Stark
work, feeling that I was looking at something I already knew. Whithout being
sure, it felt like looking again at drawings I saw during my childhood,
recalling the first times I experienced the internet. What is certain is that
Stark was an impressively prolific artist during the 90s and 2000s, leaving
behind a strong legacy on internet aesthetics and vernacular practices. Her
style is straight-forward, like a text mode _clear line_. She most likely drew
every possible animal, plant and monter. For each gesture and scene of everyday
life you can find one of her drawings. The same thing can be said for each
element that constitutes a landscape. 

Malheuresement Jgs à arrêter de publier depuis le début des années 2000. Son
succès et cette diversité dans son travail l'ayant exposé à de très nombreux
détournement de ses oeuvres sans sa permission ni attribution ou compensation,
ce que Joan à finit par ne plus supporter. As ldb sums it in " I Like Making
ASCII Art " :

> Joan was the most prolific and later, the most broken hearted as more and
> more of her ASCII art was stolen – credit for the work ripped off or claimed
> by someone else. [^4]

While today I didn't know personnaly Joan G. Stark but this name is still
resonnating in ascii art "themed" channels on discord, reddit, social medias
RESPECT ASCII ARTIST CAMPAIGN
https://asciiartist.com/from-the-original-respect-ascii-artists-campaign-page/

Jgs Font design
---------------

pour encore plus brouiller cette distinction texte-image.

supprimer, effacer la discontinuité entre les charactères, faire oublier que c'est des caractères

En faisant cela je propose une sorte de style d'ascii particulier.

Concerning specifically Jgs Font approach to ASCII art. Though it includes
CP437 characters set (ANSI art) it was designed while studying Usenet ASCII art
line-style (or oldschool), a style specific to the Usenet ASCII artists at work
between the 80s and 2000s among which Joan G Stark figures as emblematic.

While being a very strong scene at the time, leaving an immense impact on
internet aesthetic, there are few Usenet artists still active today and many
sites and links relative to the scene are now dead.

Trying to give you an idea of what this scene production look like, I've
gathered here the links and text files that I use as references to work on Jgs
Font :

Gallery
-------

A totally arbitrary and non-exhaustive gallery showing different styles/techniques of ASCII-art.

![Cage, dwimmer, 2021 - PETSCII](img/DWIMMER-CAGE.PNG)

![Snow, LDA, 2021 - ANSI](img/LDA-SNOW.ANS.png)

![Croweye, Specter, 2021 - ATASCII](img/croweye_by_specter.png)

![妖怪がしゃどくろ, 機動戦艦艦長, 2022 - SHIFT-JIS](img/rich_beatle.jpg)

![The Giant's Causeway, SimplySarah, 2021 - TELETEXT](img/simply_sarah.png)

![From "Alice in Wonderland" collection, Allen Mullen, 1999? (exact date unknown) - Newschool ASCII](img/allen_mullen.png)

![Dragon, Joan G Stark, 1996? (exact date unknown) - Oldschool ASCII](img/jgs_dragon.png)


[^1]: [The History of ASCII (Text) Art
by Joan G. Stark](https://www.roysac.com/asciiarthistory.html)
[^2]: Aesthetic features of PETSCII, ANSI and ASCII [What is Textmode by Polyducks](http://polyducks.co.uk/what-is-textmode/)
[^3]: The history of the changing status of ASCII art [ASCII art From a Commodity Into an Obscurity by Heikki Lotvonen](./docs/ASCII_art_From_a_Commodity_Into_an_Obscurity.pdf)
[^4]: Being an ascii artist since 1996 [I Like Making ASCII Art](https://asciiartist.com/i-like-making-ascii-art/)

https://www.printmag.com/daily-heller/albert-schiller-rediscovered/

https://archive.org/details/bob-neills-book-of-typewriter-art/

ASCII art tutorials :

-> Daniel C. Au (dcau)
-> Jonathon R. Oglesbee (JRO)
-> Susie Oviatt
-> Normand Veilleux
-> Rowan Crawford (Row)
-> Maija Haavisto (DiamonDie, mh)
-> Targon
-> Joan G Stark

ASCII art Faqs

-> Bob Allison (Scarecrow)
-> Jorn Barger

https://printinghistory.org/challenges-of-early-arabic-printing/
